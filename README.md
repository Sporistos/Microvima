#To compile, please go to hyphasma/ folder and run:

qmake src/hyphasma.pro

make

#You will need to install the following packages beforehand

sudo apt-get install freeglut3-dev

sudo apt-get install qt5-qmake

sudo apt-get install qtbase5-dev

sudo apt-get install qtcreator

sudo apt-get install libqt5svg5

sudo apt-get install libqt5printsupport5


#If you don't find the available qt packages in your distribution

apt-cache search qtbase

apt-cache search libqt5

#It is possible to run hyphasma without openGL nor using Qt, for this you need to change the generated makefile and remove -IblablaQt

#The genreated executable is Zapotec.

#use: ./Zapotec parameter_files/parameter_file

#do not use the .par extension

#do not use old Makefile in other folders like in hyphasma/src