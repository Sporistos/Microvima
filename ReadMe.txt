
1- How to prepare the computer for Hyphasma:

to start a command line in a folder, right click -> open terminal here

Have latex installed (MiKtex for instance)
	=> to test, try commands: latex, pdflatex, dvips, ps2pdf, they should exist in command line.
	
Have R installed 
	=> try R or Rscripts in command line

Have gle installed 
	http://glx.sourceforge.net/downloads/downloads.html

Have QTcreator, Qt and Mingw32 installed (all in one)
	https://www.qt.io/download-open-source/#section-2
	Qt 5.6.2 for Windows 32-bit (MinGW 4.9.2, 1.0 GB) 
	http://download.qt.io/official_releases/qt/5.6/5.6.2/qt-opensource-windows-x86-mingw492-5.6.2.exe
	
Add Mingw into the windows path :
	Inside folder browser, right click on 'this computer' => advanced settings => Change environment variables
		=> inside the 'Path Variable', add the Mingw folder.
		For instance, in my case, I added ";C:\Qt\Qt5.7.0\Tools\mingw530_32\bin"
		(all folders are separated by ;)
	Check if it works ? Open a new terminal wherever, type g++ or type mingw32-make

In a terminal, do (to make sure qmake makes Makefiles for windows/mingw32)
	set QMAKESPEC=win32-g++

Copy the .sig files into the generated folder by Qt (build ...)


2 - List of modifications I did in hyphasma to make it work on windows / mingw32-make

in hyphasma.pro : QMAKE_CXXFLAGS += -static-libgcc -static-libstdc++ -s
	
in kinetics.h
		#define n_vol_max 20
		static const double use_sigma_n_1;// = 0.;
		
in kinetics.cpp
		const double GCkinetics::use_sigma_n_1 = 0.;
		
in setparam.h
		#define namelength 500
		static const double N_A;// = 6.02205e+23; // mol^-1

in setparam.cpp
		const short Parameter::namelength = 500;
		const double Parameter::N_A = 6.02205e+23; // mol^-1

in brainbow.h
		static const double infinitesimal_time; // = 1.e-08;

in brainbow.cpp
		const double brainbow::infinitesimal_time = 1.e-08;

in random.h
		static const double Ny;// = 10;

in random.cpp
		const double gauss_randomize::Ny = 10;

in grid.h
		static const double pi;// = 3.141592654;

in grid.cpp
		const double grid::pi = 3.141592654;

in signals.h
		static const double SIGNAL_MAX;// = 1.0e+25;

in signals.cpp
		const double sigs::SIGNAL_MAX = 1.0e+25;

in cellthis.h
		static const double Avogadro;// = 6.02205e+23; // mol^-1
		static const double MFaraday;// = 9.6485309e-02; // Faraday constant in C/(micromol)
		// 9.6e+04 C/mol = 9.6e+04*1e-06 C / 1e-06mol = 9.6e-02 C/micromol
		static const double Faraday;// = 9.6485309e+04; // Faraday constant in C/(mol)
		static const double Rydberg;// = 8.315; // in J/(K*mol)
		static const double pi;// = 3.141592654;
		static const double z_K = 1.0; // valence of potassium ions
		static const double z_Na = 1.0; // valence of sodium ions
		static const double z_Ca = 2.0; // valence of calcium ions

in cellthis.cpp
		const double cellbeta::Avogadro = 6.02205e+23; // mol^-1
		const double cellbeta::MFaraday = 9.6485309e-02; // Faraday constant in C/(micromol)
		// 9.6e+04 C/mol = 9.6e+04*1e-06 C / 1e-06mol = 9.6e-02 C/micromol
		const double cellbeta::Faraday = 9.6485309e+04; // Faraday constant in C/(mol)
		const double cellbeta::Rydberg = 8.315; // in J/(K*mol)
		const double cellbeta::pi = 3.141592654;
		const double cellbeta::z_K = 1.0; // valence of potassium ions
		const double cellbeta::z_Na = 1.0; // valence of sodium ions
		const double cellbeta::z_Ca = 2.0; // valence of calcium ions


in cellthis.h
		
		
		
		
		
		
	listfiles="bcinflow09 bcinflow09"
for %f in (%listFiles%) do (
	


)
