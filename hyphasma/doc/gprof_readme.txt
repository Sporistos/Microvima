Manual for usage of gprof for the localisation of slow routines:

Compile the code with the option -pg

run the code (say hyphasma) --> this generates gmon.out

convert gmon.out to a readable output by evoking gprof hyphasma > gprof.out

read gprof.out with less
