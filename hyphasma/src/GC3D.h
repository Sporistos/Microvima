#ifndef GC3D_H
#define GC3D_H

#ifdef ALLOW_GC3D
// For the protein tests.
#ifdef _win32
#include "freeglut/include/GL/glut.h"
#else
#include <GL/glut.h>
#endif


#include "cellman.h"
#include "space.h"
#include <vector>
using namespace std;

//void init(void);
namespace GC3D {
void initGC3D(int argc, char** argv);
void display();
void glDisplay(int argc, char** argv);
void drawBitmapText(string s,float x,float y,float z);
void nextToDisplay( dynarray<cellCC>* CC_list,
                    dynarray<cellCB>* CB_list,
                    dynarray<cellTC>* TC_list,
                    dynarray<cellFDC>* FDC_list,
                    dynarray<cellOUT>* OUT_list,
                    dynarray<long>* STROMA_list, space *s, double t);
void waitForRightClick();
void clearDisplay();
}

#endif
#endif // PLOT3D_H
