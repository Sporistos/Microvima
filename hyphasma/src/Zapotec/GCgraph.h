#ifndef GRAPH_
#define GRAPH_

using namespace std;
#include <iostream>			// cin cout
#include <iomanip>			// << setprecision() <<
#include <cmath>			// min(x,y)
#include <vector>			// tout est en vecteur car tailles grandes -> alloc dynamiques
#include <queue>			// pour dijkstra
#include <string>			// lecture des arguments du main
#include <fstream>			// i/o fichiers par << et >>
#include <cstdlib>			// for linux, atoi ...
#include <ctime>			// for srand(time NULL)
#include <map>
#include <sstream>
#include <set>

#define DBGG  false
enum {IGM, IGG};

// type of value associated with each edge.
typedef long double valeur;

/// edge : from a node to another one, with additional information (weight)
struct edge {
    int dest;
    int destSeqId;
    valeur weight;
   // edge(int d, valeur w): dest(d), weight(w) {}
    edge(int d, long id,  valeur w): dest(d), destSeqId(id), weight(w){}
    string print(){
        stringstream s;
        s << "->" << dest << "/ID=" << destSeqId << "(" << weight << ") ";
        return s.str();
    }
    string txt(){
        stringstream s;
        s << destSeqId << "\t" << weight << "\t";
        return s.str();
    }
};

/// In each node, will be a list of graphcells.
struct graphcells {
    graphcells(double _timeCreation, string _seq = string(""), double _bestAffinity = NAN, int _typeIG = IGM) : timeCreation(_timeCreation), seq(_seq), bestAffinity(_bestAffinity), typeIG(_typeIG) {}
    double timeCreation;
    double bestAffinity;
    string seq;
    int typeIG;
    string print(){
        stringstream s;
        s << "Cell(t= " << timeCreation << " ,Aff= " << bestAffinity << " ,Ig= " << typeIG << " ) seq= " << seq;
        return s.str();
    }
    string headers(){return string("time_gen\tbestAff\ttype_Ig\tseq");}
    string txt(){
        stringstream s;
        s << timeCreation << "\t" << bestAffinity << "\t" << typeIG << "\t" << seq;
        return s.str();
    }
};

/// Node : a sequence (ID), and a list of graphcells
struct node {
    vector<edge> sons;
    vector<edge> reverse;
    int founder;



    bool alreadyPointsTo(int index){
        int S = sons.size();
        for(int i = 0; i < S; ++i){
            if(sons[i].dest == index) return true;
        } 
        return false;
    }

    vector<graphcells> atThisNode;
    int nbCells(){return atThisNode.size();}
    node(int _indexInGraph = -1, long _seqID = -1) :
        founder(-1),
        seqID(_seqID),
        indexInGraph(_indexInGraph) {}

    // redundant information
    long seqID;
    int indexInGraph;

    void clear(){
        sons.clear();
        reverse.clear();
        atThisNode.clear();
        seqID = -1;
        indexInGraph = -1;
    }

    string print(){
        stringstream s;
        s << "\nNode:index= " << indexInGraph << " ,seqID= " << seqID << " ,Founder: " << founder;
        s << " At_this_Node: ";
        for(int i = 0; i < (int) atThisNode.size(); ++i){
            s << atThisNode[i].print();
        }
        s << " ...parents: ";
        for(int i = 0; i < (int) reverse.size();++i){
            s << reverse[i].print();
        }
        s << " ,sons: ";
        for(int i = 0; i < (int) sons.size();++i){
            s  << sons[i].print();
        }
        return s.str();
    }
    string header(){return string("seqID\tfounderID\tNbCells\tCells...\tNbParents\tParents...\tNbSons\tSons...");}
    string txt(){
        stringstream s;
        s << seqID << "\t" << founder << "\t" << atThisNode.size() << "\t";
        for(int i = 0; i < (int) atThisNode.size(); ++i){
            s << atThisNode[i].txt();
        }
        s << "\t" << reverse.size() << "\t";
        for(int i = 0; i < (int) reverse.size();++i){
            s << reverse[i].txt();
        }
        s << "\t" << sons.size() << "\t";
        for(int i = 0; i < (int) sons.size();++i){
            s  << sons[i].txt();
        }
        return s.str();
    }
};



struct liste_adjacence {
    string print(){
        stringstream s;
        s << "Graph, number of nodes : " << taille << ", list of founders :";
        set<int>::iterator it;
        for (it=listFounders.begin(); it!=listFounders.end(); ++it){
           s << ' ' << *it;
        }
        s << "\n";
        for(int i = 0; i < taille;++i){
            s << table[i]->print();
        }
        return s.str();
    }

    /// adjacency list (vector of list of edges)
    vector<node *> table;
    /// current number of nodes in the graph
    int taille;

    /// rebuild the graph with an other number of nodes
    void resize(int _taille){
        table.resize(_taille);
        for(int i = taille; i < _taille; ++i){
            table[i] = new node(i);
        }
        taille = _taille;
    }
    /// get the number of nodes
    int size(){return taille;}
    /// void constructor
    liste_adjacence(){taille = 0; table.clear();}
    /// constructor of a new graph with _taille nodes
    liste_adjacence(int _taille){
        resize(_taille);
    }


    /// works with IDs : converts ID to node index / requests the creation of a new node if the IDs doesn't exist
    std::map<long,int> mymap;
    int getNodeIndexOrCreate(long _seqID){
        if(DBGG) cerr << "Look for seqID=" << _seqID;
        if(_seqID < 0) return -1;
        std::map<long,int>::iterator it;
        it = mymap.find(_seqID);
        if (it != mymap.end()){ // if already exists
            if(DBGG) cerr << " found: " << it->second << endl;
            return it->second;
        }
        int index = taille;
        resize(taille+1);
        table[index]->seqID = _seqID;
        table[index]->indexInGraph = index;
        if(DBGG) cerr << " created: " << index << endl;
        mymap.insert ( std::pair<long,int>(_seqID,index) ); // new taille
        return index;
    }

    /// to access the list of edges starting from node i
    node & operator [](int i){		//passe la référence
        if((i < 0) || (i >= taille)) cerr << "Unauthorized call to adjacency table (index " << i << ")\n";
        return *(table[i]);
    }


    /// to add an edge, from node i to node j with weight w
    /*void addEdge(int i, int j, valeur w){ // from an index
        if(DBGG) cerr << "Add edge[" <<i << "," << j << "]" << endl;
        table[i]->sons.push_back(edge(j, w));
        table[j]->reverse.push_back(edge(i, w));
    }*/
    void addEdgeWithIDs(long ID1origin, long ID2dest, valeur w){ // from an index
        if(DBGG) cerr << "Want to add edge [seqID=" <<ID1origin << "," << ID2dest << "]" << endl;
        if(ID1origin < 0) return;
        int index1 = getNodeIndexOrCreate(ID1origin);
        int index2 = getNodeIndexOrCreate(ID2dest);
        if(DBGG) cerr << "Found indexes " << index1 << "," << index2 << endl;
        if(! table[index1]->alreadyPointsTo(index2)){
            table[index1]->sons.push_back(edge(index2, ID2dest, w));
            if(index2 >= 0 ){
                table[index2]->reverse.push_back(edge(index1, ID1origin, w));
            }
        } else if(DBGG) cerr << "Interaction already existed "<< endl;
    }

    void addCell(long IDseq, long IDmother, graphcells cellToAdd, valeur w ){
        addEdgeWithIDs(IDmother, IDseq, w);
        int index1 = getNodeIndexOrCreate(IDseq);
        table[index1]->atThisNode.push_back(cellToAdd);     // makes an implicit copy
    }

    /// to destruct
    ~liste_adjacence(){
        for(int i = 0; i < taille; ++i){
            table[i]->clear();
            delete table[i];
        }
    }

    vector<int> tempLabels;
    set<int> listFounders;
    void FindFounders(){
        listFounders.clear();
        tempLabels.clear();
        tempLabels.resize(taille, 0);
        for(int i = 0; i < taille; ++i){
            listFounders.insert(recursiveFounder(i));
        }
    }

    int recursiveFounder(int start){
        if((start < 0) || (start >= taille)) {cerr << "ERR: recursiveFounders(" << start << "), out of bounds index (taille = " << taille << endl; return 0;}
        if(table[start]->founder > 0) return table[start]->founder;
        if(tempLabels[start] > 0) cerr << "ERR : recursiveFounder(" << start << "), it looks like this node has been seen twice. Circular tree ?" << endl;
        tempLabels[start] = 1;
        if(table[start]->reverse.size() == 0) {table[start]->founder = start; return start;} // no parent : I am a founder   // note: founder cell should always be inthe tree ??
        if(table[start]->reverse.size() > 1) cerr << "ERR: recursiveFounders(" << start << "), this function only works when there is only one parent per node !! here are " << table[start]->reverse.size() << "parents" << endl;
        int motherNode = table[start]->reverse[0].dest;
        table[start]->founder = recursiveFounder(motherNode);
        return table[start]->founder;
    }

};



void TestGraph();

#endif // GRAPH

