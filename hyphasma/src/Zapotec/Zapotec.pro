include("../Ymir/Ymir.pri")
include("../latFit/latFit.pri")

HEADERS += \
    affinityspace.h \
    binaryLineages.h \
    binarySequences.h \
    cubes.h \
    figures.h \
    foldedCubic.h \	
    foldedFree.h \
    perelsonSequence.h \
    perelsonGeneralized.h \
    probabilisticArup.h \
    sequencespaces.h \
    ../Tests/common.h

SOURCES += \
    binaryLineages.cpp \
    binarySequences.cpp \
    cubes.cpp \
    figures.cpp \
    foldedCubic.cpp \	
    foldedFree.cpp \
    perelsonSequence.cpp \
    perelsonGeneralized.cpp \
    probabilisticArup.cpp \
    sequencespaces.cpp \
    ../Tests/common.cpp \
    zapmain.cpp

win32: TARGET = Zapotec.exe
unix: TARGET = Zapotec

QMAKE_CXXFLAGS += "-Wno-old-style-cast" "-Wno-shorten-64-to-32" "-Wno-sign-conversion" "-Wno-old-style-cast" "-Wno-implicit-int-float-conversion"

QT += core gui widgets

#https://gcc.gnu.org/onlinedocs/gcc-4.9.2/gcc/Warning-Options.html
CONFIG += warn_off
#https://gcc.gnu.org/onlinedocs/gcc-4.9.2/gcc/Warning-Options.html these are different
QMAKE_CXXFLAGS += -std=c++11 -Wno-extra
# -Wno-oldstyle-case -Wno-unused-parameter -Wno-shorten-64-to-32 -Wno-sign-conversion -Wno-ignored-qualifiers -Wno-type-limits -Wno-misleading-indentation -Wno-sign-compare -Wno-implicit-float-conversion
# QMAKE_CXXFLAGS += -Wall -Wextra -Wunsafe-loop-optimizations -pedantic -Wfloat-equal -Wundef -Wpointer-arith -Wcast-align -Wunreachable-code

