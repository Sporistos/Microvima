#include <sstream>
#include <cmath> //pow
#include <iostream>
#include "../Tools/zaprandom.h"
#include "binaryLineages.h"
// for tests
#include <fstream>
#include <algorithm>
#include "../Tests/common.h"


string printVec2(vector<double> v){
    stringstream res;
    for(unsigned int i = 0; i < v.size(); ++i){
        res << "\t" << v[i];
    }
    return res.str();
}


// Operators / functions to set up a new binaryLineage
binaryLineage::binaryLineage(int _size, int _lcon){
    if (_size < 0) {_size = 0;}
    size = _size;
    if((_lcon < 0) || (_lcon >= size)) cerr << "ERR: binaryLineage, lcon " << _lcon << " incompatible with size " << size << endl;
    lcon = _lcon;
    content.resize(_size, false);
    accessibility.resize(_size, 1.);
}

binaryLineage::binaryLineage(string boolText, int _lcon, vector<double> _accessibility){
    size = boolText.size();
    if((_lcon < 0) || (_lcon >= size)) cerr << "ERR: binaryLineage, lcon " << _lcon << " incompatible with size " << size << endl;
    lcon = _lcon;
    content.resize(size, false);
    for (int i = 0; i < size; ++i){
        if((boolText[i] != '1') && (boolText[i] != '0')) {
            cerr << "ERR:sequence(" << boolText << "), un-allowed character : " << boolText[i] << endl;
        }
        content[i] = (boolText[i] == '1');
    }
    setAccessibility(_accessibility);
}

binaryLineage::binaryLineage(int _size, long long ID, int _lcon, vector<double> _accessibility) {
    // ': sequence(_size)' leads to a warning because only works in C++11, so copied constructor
    if (_size < 0) {_size = 0;}
    size = _size;
    if((_lcon < 0) || (_lcon >= size)) cerr << "ERR: binaryLineage, lcon " << _lcon << " incompatible with size " << size << endl;
    lcon = _lcon;
    content.resize(_size, false);
    recursive_binary(ID, this->content);
    setAccessibility(_accessibility);
}

binaryLineage::binaryLineage(binaryLineage *toCopy){
    size = toCopy->size;
    lcon = toCopy->lcon;
    if((int) toCopy->content.size() != size) cerr << "ERR: binaryLineage::binaryLineage(toCopy), toCopy->content has wrong size " << endl;
    if((int) toCopy->accessibility.size() != size) cerr << "ERR: binaryLineage::binaryLineage(toCopy), toCopy->accessibility has wrong size " << endl;
    content = toCopy->content;
    accessibility = toCopy->accessibility;
}

void binaryLineage::setAccessibility(vector<double> _accessibility){
    if(_accessibility.size() == 0){
        accessibility.clear();
        accessibility.resize(size, 1.0);
    } else {
        if((int) _accessibility.size() != size){
            cerr << "ERR: binaryLineage -> setAccessibility, inconsistent size between them (" << size << " vs " << _accessibility.size() << ")" << endl;
            _accessibility.resize(size, 0); // upgrades/downgrades to the good size
        }
        accessibility = _accessibility;
    }
}

void binaryLineage::setAccessibilityUniform(){
    accessibility.resize(size, 0);
    for(int i = 0; i < size; ++i){
        accessibility[i] = random::uniformDouble(0,1);
    }
}

#ifdef USE_DISTRIBUTIONS
void binaryLineage::setAccessibilityFromDistribution(distribution* d){
    if(!d) {cerr << "ERR: binaryLineage::setAccessibilityFromDistribution, empty distribution pointer" << endl; return;}
    accessibility.resize(size, 0);
    for(int i = 0; i < size; ++i){
        accessibility[i] = d->getRandValue();
    }
}
#endif


// Accessing data
int binaryLineage::operator [](int i){
    if ((i < 0) || (i >= size)){
        cerr << "sequence[" << i << "] out of bounds (size=" << size << ")\n";
        return 0;
    }
    return (content[i]) ? 1 : 0;
}

int binaryLineage::getAccess(int i){
    if ((i < 0) || (i >= size)){
        cerr << "sequence[" << i << "] out of bounds (size=" << size << ")\n";
        return 0;
    }
    return accessibility[i];
}

bool binaryLineage::operator==(binaryLineage &b) {
    if(size != b.size) return false;
    if(lcon != b.lcon) return false;
    for (int i = 0; i < size; ++i) {
        if(content[i] != b[i]) return false;
    }
    return true;
}


// Meaningful functions. returns 1: success, 0: silent, -1: fail
int binaryLineage::mutateOnePosition() {
    if(lcon == size) {cerr << "ERR: binaryLineage, sequence with only conserved residues can not be mutated" << endl; return -1;}
    int position = random::uniformInteger(0, size-1-lcon);
    if ((position >= size) || (position < 0)) {
        cerr << "Random does shit, binaryLineage.cpp::mutateOnePosition";
        return -1; // fail
    }
    content[position] = !content[position];
    return +1; // success
}
void binaryLineage::randomize() {
    for (int i = 0; i < size - lcon; ++i) {
        if (random::uniformDouble(0,1) < 0.5) {
            content[i] = !content[i];
        }
    }
}
double binaryLineage::affinity(binaryLineage *x, binaryLineage *y)
{
    // we suppose that the sequences were made correctly (i.e. accessibility and content have size size). This is ok because all constructors make sure this problem should not happen.
    if (x->size != y->size) {
        std::cout << "Err affinity: length of antigen and BCR are not identical!";
        return -1;
    }
    double sum = 0;
    for (int i = 0; i < x->size; ++i){
        sum += x->accessibility[i] * y->accessibility[i] * ((x->content[i] == y->content[i]) ? 1 : -1);
    }
    return sum / x->size;
}

double binaryLineage::hamming(binaryLineage *x, binaryLineage *y) {
    if (x->size != y->size) {
        std::cout << "Err affinity: length of antigen and BCR are not identical!";
        return -1;
    }
    int length = x->size;
    int sum = 0;
    for (int i = 0; i < length; ++i) {
        sum += ((*x)[i] != (*y)[i]) ? 1 : 0;
    }
    return sum;
}


// tests / tool functions
string binaryLineage::print() {
    stringstream out;
    if(((int) content.size() != size) || ((int) accessibility.size() != size)) cerr << "ERR: Allocating problem, binaryLineage::print()" << endl;
    for (int i = 0; i < size; ++i) {
        out << content[i] << "[" << accessibility[i] << "]  ";
    }
    return out.str();
}

string binaryLineage::printAcc() {
    stringstream out;
    if(((int) content.size() != size) || ((int) accessibility.size() != size)) cerr << "ERR: Allocating problem, binaryLineage::print()" << endl;
    for (int i = 0; i < size; ++i) {
        out << accessibility[i] << "\t";
    }
    return out.str();
}

string binaryLineage::printSeq() {
    stringstream out;
    if(((int) content.size() != size) || ((int) accessibility.size() != size)) cerr << "ERR: Allocating problem, binaryLineage::print()" << endl;
    for (int i = 0; i < size; ++i) {
        out << content[i] ;
    }
    return out.str();
}


/// @brief Transform a long number into binary sequence, inside a given vector (already sized).
/// example: recursive_binary(2354, a_bool_vector_to_fill);
// Alternative is to use bitset, but size has to be known at compilation -> not convenient
void binaryLineage::recursive_binary(long ID, vector<bool> &towrite, int currentposition){
    if (ID < 0) {cerr << "ERR::sequence::binary, negative number " << ID << endl; return;}
    if (currentposition < 0){    // default option -1 means need to start from the first position
        currentposition = towrite.size() - 1;
    }
    if (currentposition >= (int) towrite.size()){
        cerr << "Err: sequence::binary(), currentposition is out of bounds ("
             << currentposition << "), while vector size is " << towrite.size() << endl;
        return;
    }
    if (ID <= 1) {towrite[currentposition] = (ID != 0); return;}  // to stop recursion
    else if(currentposition == 0) { // this means that ID > 1, and no space for it.
        cerr << "ERR: recursive_binary, the given number was too big and could not fit into a "
             << " vector of size " << towrite.size() << endl;
        return;
    }
    long remainder = ID % 2;
    towrite[currentposition] = (remainder != 0);
    recursive_binary(ID >> 1, towrite, currentposition - 1);
}

string binaryLineage::test(){
#define MODE_OUT cerr

    MODE_OUT << "================ Test binaryLineage basic functions =================" << endl;
    binaryLineage S1(5);
    MODE_OUT << "S1: Empty sequence, size 5 " << S1.print() << "\n\n";
    binaryLineage S2(string("010111101011111"));
    MODE_OUT << "S2: Sequence from binary text 010111101011111, no accessibility defined -> " << S2.print() << "\n\n";
    binaryLineage S3(string("01oAX5!="));
    MODE_OUT << "S3: Sequence from text with bad characters '01oAX5!=' ->" << S3.print() << "\n\n";
    binaryLineage S4(&S2);
    MODE_OUT << "S4: Copy from previous sequence S2 " << S4.print() << "\n\n";
    binaryLineage S4b(15);
    S4b = S2;
    MODE_OUT << "S4: = from previous sequence S2 " << S4b.print() << "\n\n";
    long long int x5 = 158230;
    int size5 = 10;
    binaryLineage S5a(2*size5, x5); // size 10
    MODE_OUT << "S5a: Sequence from binary : " << S5a.print() << " from " << x5 << " with size " << 2*size5 << "\n\n";
    binaryLineage S5b(size5, x5); // size 10
    MODE_OUT << "S5b: Sequence from binary (too big number -> error): " << S5b.print() << " from " << x5 << " with size " << size5 << "\n\n";
    MODE_OUT << "Accessing positions of S5b one by one [] ";
    MODE_OUT << S5a[0] << S5a[1] << S5a[2] << "..." << S5a[S5a.size-1] << "\n\n";
    MODE_OUT << "Now testing bad positions : ";
    MODE_OUT << S5a[-1] << S5a[S5a.size] << "\n\n";
    MODE_OUT << "S6: Randomized sequence ";
    binaryLineage S6(10);
    S6.randomize();
    MODE_OUT << S6.print() << "\n\n";
    MODE_OUT << "S7: Randomized sequence, from an existing one ";
    binaryLineage S7(15,257);
    S7.randomize();
    MODE_OUT << S7.print() << "\n\n";
    MODE_OUT << "Consecutive mutations" << endl;
    binaryLineage S8("0000000000");
    for(int i = 0; i < 10; ++i){
        S8.mutateOnePosition();
        MODE_OUT << S8.print() << endl;
    }
    binaryLineage S9(0);
    S9 = S8;
    MODE_OUT << "Compare equal sequences : " << S8.print() << ((S8  == S9) ? "=" : "!=")  << S9.print() << "\n\n";
    S9.mutateOnePosition();
    MODE_OUT << "Compare different sequences : " << S8.print() << ((S8 == S9) ? "=" : "!=")  << S9.print() << "\n\n";

    binaryLineage S9a(10);
    S9a.setAccessibilityUniform();
    MODE_OUT << "Uniform random accessibility " << S9a.print() << "\n\n";


    MODE_OUT << "Test affinity functions on single examples" << "\n";
    binaryLineage S10("0000011111");   // reference sequence AG1
    binaryLineage S11("1100011111");   // reference sequence AG2
    binaryLineage S21("0000001111");   // match 4+5 to AG1, 4+3 to AG2
    binaryLineage S22("1000000111");   // match 1+3+3 to AG1, 1+3+3 to AG2





    /*
    MODE_OUT << "1 - basic affinity " << "\n\n";

    MODE_OUT << "Set up : two antigens : \n";
    MODE_OUT << "\t" << S10.print() << "\n";
    MODE_OUT << "\t" << S11.print() << "\n";
    MODE_OUT << "And two antibodies : \n";
    MODE_OUT << "\t" << S21.print() << "\n";
    MODE_OUT << "\t" << S22.print() << "\n\n";

    MODE_OUT << "seqAff of " << S10.print() << " - \n"
             << "          " << S21.print() << " = "
             << " (r=1)" << binarySequence::seq_affinity(&S10, &S21,1,-1,seqAff)
             << " (r=2)" << binarySequence::seq_affinity(&S10, &S21,2,-1,seqAff)
             << " (r=7)" << binarySequence::seq_affinity(&S10, &S21,7,-1,seqAff) << "\n\n";
    MODE_OUT << "seqAff of " << S10.print() << " - \n"
             << "          " << S22.print() << " = "
             << " (r=1)" << binarySequence::seq_affinity(&S10, &S22,1,-1,seqAff)
             << " (r=2)" << binarySequence::seq_affinity(&S10, &S22,2,-1,seqAff)
             << " (r=7)" << binarySequence::seq_affinity(&S10, &S22,7,-1,seqAff) << "\n\n";
    MODE_OUT << "seqAff of " << S11.print() << " - \n"
             << "          " << S21.print() << " = "
             << " (r=1)" << binarySequence::seq_affinity(&S11, &S21,1,-1,seqAff)
             << " (r=2)" << binarySequence::seq_affinity(&S11, &S21,2,-1,seqAff)
             << " (r=7)" << binarySequence::seq_affinity(&S11, &S21,7,-1,seqAff) << "\n\n";
    MODE_OUT << "seqAff of " << S11.print() << " - \n"
             << "          " << S22.print() << " = "
             << " (r=1)" << binarySequence::seq_affinity(&S11, &S22,1,-1,seqAff)
             << " (r=2)" << binarySequence::seq_affinity(&S11, &S22,2,-1,seqAff)
             << " (r=7)" << binarySequence::seq_affinity(&S11, &S22,7,-1,seqAff) << "\n\n";

//    seqAff, seqAffNorm, seqAffMaxClusterSize, seqAffWindow, seqAffSlidingWindow, seqBiggestSubsequence, seqMultiEpitopeAffinity,
    MODE_OUT << "2 - Normalize, affinity more than maxclusters^r goes to 1" << endl;
    binarySequence S00("0000000000");   // reference sequence AG1
    binarySequence S31("0011111111");   // match 2
    binarySequence S32("0000111111");   // match 4
    binarySequence S33("0000001111");   // match 6
    binarySequence S34("0000000011");   // match 8
    binarySequence S35("1000010100");   // match 4+1+2
    binarySequence S36("1000000100");   // match 6+2

    MODE_OUT << "Set up : one antigens : \n";
    MODE_OUT << "\t" << S00.print() << "\n";
    MODE_OUT << printBinatySeqAff(&S00, &S31, seqAffNorm, 4);
    MODE_OUT << printBinatySeqAff(&S00, &S32, seqAffNorm, 4);
    MODE_OUT << printBinatySeqAff(&S00, &S33, seqAffNorm, 4);
    MODE_OUT << printBinatySeqAff(&S00, &S34, seqAffNorm, 4);
    MODE_OUT << printBinatySeqAff(&S00, &S35, seqAffNorm, 4);
    MODE_OUT << printBinatySeqAff(&S00, &S36, seqAffNorm, 4);

    MODE_OUT << "3 - Normalize, clusters of more than a certain size do not increase affinity" << endl;

    MODE_OUT << "Set up : one antigens : \n";
    MODE_OUT << "\t" << S00.print() << "\n";
    MODE_OUT << printBinatySeqAff(&S00, &S31, seqAffMaxClusterSize, 5);
    MODE_OUT << printBinatySeqAff(&S00, &S32, seqAffMaxClusterSize, 5);
    MODE_OUT << printBinatySeqAff(&S00, &S33, seqAffMaxClusterSize, 5);
    MODE_OUT << printBinatySeqAff(&S00, &S34, seqAffMaxClusterSize, 5);
    MODE_OUT << printBinatySeqAff(&S00, &S35, seqAffMaxClusterSize, 5);
    MODE_OUT << printBinatySeqAff(&S00, &S36, seqAffMaxClusterSize, 5);


    MODE_OUT << "\n4 - Sliding windows,facing sequences" << endl;

    MODE_OUT << "Set up : one antigens : \n";
    binarySequence S40("1111111111");   // match 5
    MODE_OUT << "\t" << S40.print() << "\n";
    binarySequence S41("0011111000");   // match 5
    binarySequence S42("1110000111");   // match 3+3, far
    binarySequence S43("1101111000");   // match 4+2, close
    MODE_OUT << printBinatySeqAff(&S40, &S41, seqAffWindow, 4);
    MODE_OUT << printBinatySeqAff(&S40, &S42, seqAffWindow, 4);
    MODE_OUT << printBinatySeqAff(&S40, &S43, seqAffWindow, 4);
    MODE_OUT << printBinatySeqAff(&S40, &S41, seqAffWindow, 6);
    MODE_OUT << printBinatySeqAff(&S40, &S42, seqAffWindow, 6);
    MODE_OUT << printBinatySeqAff(&S40, &S43, seqAffWindow, 6);

    MODE_OUT << "\n5 - All possible sliding windows" << endl;

    MODE_OUT << "Set up : two antigens : \n";
    binarySequence S50("0011111000");
    binarySequence S54("1010101011111111111");
    MODE_OUT << "\t" << S50.print() << "\n";
    MODE_OUT << "\t" << S54.print() << "\n";
    MODE_OUT << printBinatySeqAff(&S54, &S50, seqAffSlidingWindow, 4);

    binarySequence S51("1111111111");   // match 5
    binarySequence S52("1010110111");   // match 6
    binarySequence S53("1111101011");   // match 5 or 6
    MODE_OUT << printBinatySeqAff(&S54, &S51, seqAffSlidingWindow, 4);
    MODE_OUT << printBinatySeqAff(&S54, &S52, seqAffSlidingWindow, 4);
    MODE_OUT << printBinatySeqAff(&S54, &S53, seqAffSlidingWindow, 4);
    MODE_OUT << printBinatySeqAff(&S54, &S51, seqAffSlidingWindow, 8);
    MODE_OUT << printBinatySeqAff(&S54, &S52, seqAffSlidingWindow, 8);
    MODE_OUT << printBinatySeqAff(&S54, &S53, seqAffSlidingWindow, 8);

    MODE_OUT << "\n6 All possible sliding windows, beyond borders" << endl;

    MODE_OUT << "Set up : two antigens (Yesss!) : \n";
    binarySequence S55("0011111000");
    binarySequence S56("0111110001111111111"); // with shifting, should be 9
    MODE_OUT << "\t" << S55.print() << "\n";
    MODE_OUT << "\t" << S56.print() << "\n";
    MODE_OUT << printBinatySeqAff(&S56, &S55, seqAffSlidingWindow, 10);
    MODE_OUT << printBinatySeqAff(&S56, &S55, seqAffSlidingWindowOverBorders, 10);
*/
    return string("");
}



bool compBinaryLineages(pair<double, binaryLineage*> a, pair<double, binaryLineage*> b) {
   return a.first > b.first;
}


string testBinaryLineage(int L) {

    stringstream subFolder;
    subFolder << "Lineage-L" << L;
    string folder = string("C:/Users/Philippe/Desktop/Sequences/") + subFolder.str() + string("/");
    createFolder(folder);

    #define myOut cout
    //stringstream out;
    myOut << "Testing the properties of binary lineages : \n";
    myOut << "   ->     L= " << L << "\t(Size of sequences)" << endl;
    myOut << "==== Part 1 : enumerates all (if possible), or a lot of sequences and sort them by affinity to get the best ones : ====" << endl;

#define resolutiondistrib 100
#define maxSequencesToEnumeate 500000
#define nClones 50

    // The reference antigen is 00000...
    binaryLineage * ref = new binaryLineage(L);
    myOut << "Reference sequence : " << ref->print() << endl;

    // To generate sequences with discrete accessibility values:
    vector<double> acc_values = {0, 0.1, 0.4, 1.0};
    vector<double> acc_densities = {0.25, 0.25, 0.25, 0.25};
    distribution* distrib = new probaLawFromTable(acc_values, acc_densities, false);
    myOut << ((probaLawFromTable*) distrib)->print() << endl;


    vector<vector<double>* > allHistos;
    vector<vector<double>* > allLogDistribs;

    // in case L is big, will only sample maxSequencesToEnumeate sequences.
    int total = 0;
    int maxim = pow(2, L);
    if (L > 26) {
        maxim = maxSequencesToEnumeate + 1;           // to avoid it to become negative ...
    }

    myOut << "1 ----------- Distribution of affinities ----------------------" << endl;
    // now generates clones with different accessibilities, and study each clone separately for possible affinities.
    for(int nc = 0; nc < nClones; ++nc)
    {
        // try two hypotheses for the accessibility: 1/ uniform (first half) and 2/ discrete, high 1.0, medium0.4, low 0.1, 0
        binaryLineage * refAccess = new binaryLineage(L);

        // now decides the accessibility to be taken for all this clone: either
        if(nc < nClones / 2){
            refAccess->setAccessibilityFromDistribution(distrib);
        } else {
            static bool printed = false; // prints only the first time
            if(!printed){myOut << " ---- Now, with uniform distribution of accessibilities ---" << "\n\n"; printed = true;}
            refAccess->setAccessibilityUniform();
        }
        myOut << " ************ Now using the clone with the following accessibilities *********** " << endl;
        myOut << refAccess->printAcc() << endl;

        // will store a large list of random sequences, with their affinity to ref
        vector<pair<double, binaryLineage*> > store;


        vector<double> histo;
        vector<double> logDistrib;
        histo.resize(resolutiondistrib + 1);


        // if possible to enumerate all, do it one by one.
        bool enumerateAll = (maxim < maxSequencesToEnumeate);
        if (enumerateAll) {
            for (int i = 0; i < maxim; ++i) {
                binaryLineage * a = new binaryLineage(L, (long) i, 0, refAccess->accessibility);
                double affi = binaryLineage::affinity(a, ref);
                histo[(int) (((double) resolutiondistrib) * (0.5 + affi/2.))] += 1.0; // put into the histogram
                store.push_back(pair<double, binaryLineage*> (affi,a));
                logDistrib.push_back(log10(affi+1e-6));
                total++;
                //cout << affi << endl;
            }
        } else {
            // if not, than sample randomly
            for (int i = 0; i < maxSequencesToEnumeate; ++i) {
                if(((i%100000) == 0) || (i == maxSequencesToEnumeate-1)) cout << i << "/" << maxSequencesToEnumeate << endl;
                binaryLineage * a = new binaryLineage(L);
                a->setAccessibility(refAccess->accessibility);
                a->randomize();
                double affi = binaryLineage::affinity(a, ref);
                histo[(int) (((double) resolutiondistrib) * (0.5 + affi/2.))] += 1.0;
                store.push_back(pair<double, binaryLineage*> (affi,a));
                logDistrib.push_back(log10((0.5 + affi/2.)+1e-6));
                total++;
                //cout << affi << endl;
            }
        }

        myOut << "Distribution of affinities\n";
        for (int i = 0; i < (int) histo.size(); ++i) {
            histo.at(i) /= (double) total;
            myOut << i << "\t"  << -1. + 2. * (double (i) * (1.0 / (double) resolutiondistrib ))
                << "\t" << -1. + 2. * (double (i + 1) * (1.0 / (double) resolutiondistrib ))
                << "\t" << histo.at(i) << endl;
        }
        myOut << "Distribution of affinities in LOG scale\n";
        histogramFromDistrib v(logDistrib, 100);
        myOut << v.print() << endl;

        myOut << "\nSequences and affinity, "
            << ((enumerateAll) ? " in the order of ID\n" : " randomly generated\n");
        for (int i = 0; i < min(200, (int) store.size()); ++i) {
            myOut << i << "\t" << store[i].second->printSeq() << "\t" << store[i].first << "\n";
        }

        myOut << "\nSequences, sorted from the best, out of the "
            << min(maxim,(int) maxSequencesToEnumeate) << " evaluated sequences\n";
        std::sort(store.begin(), store.end(), compBinaryLineages);
        for (int i = 0; i < 200; ++i) {
            myOut << i << "\t" << store[i].second->printSeq() << "\t" << store[i].first << "\n";
        }

        if (enumerateAll) {
            myOut << "\nAffinity of sequences taken randomly\n";
            for (int i = 0; i < 100; ++i) {
                binaryLineage * seqtmp = new binaryLineage(L);
                seqtmp->setAccessibility(refAccess->accessibility);
                seqtmp->randomize();
                myOut << i << "\t" << binaryLineage::affinity(seqtmp,ref) << "\t" << seqtmp->printSeq() << "\n";
            }
        }

        allHistos.push_back(new vector<double>(histo));
        allLogDistribs.push_back(new vector<double>(logDistrib));













        myOut << "2 ------------------------- Mutation histograms (for each clone) -----------------------" << endl;

        // Cuts the distribution of affinities in blocs of 5% of the sequences
 #define sizeClasses 0.05
        myOut << "\nEqual classes of affinities (best percentiles)" << endl;
        vector<double> classes;
        vector<double> valuesClasses;

        std::reverse(store.begin(), store.end()); // now to put it increasing

        // every 5% of sequences browsed (fraction), store the value of affinity
        double fraction = 0.00;
        for(unsigned int i = 0; i < store.size(); ++i){
            //out << store[i].second->print() << " Aff " << store[i].first << endl;
            if((((double) i / (double) (store.size())) >= fraction - 1e-6) || (i == store.size() - 1)){
                //out << "]" << fraction - sizeClasses << "," << fraction << "]\t" << store[i].first << endl;
                valuesClasses.push_back(store[i].first);
                classes.push_back(fraction);
                fraction += sizeClasses;
            }
        }
        myOut << "Affinity classes: cell at percent XX has the affinity value of: (last line=log, 0->-10)" << endl;
        myOut << printVec2(classes) << endl;
        myOut << printVec2(valuesClasses) << endl;
        for(unsigned int i = 0; i < valuesClasses.size(); ++i){
            myOut << "\t" << log10((0.5 + valuesClasses[i] / 2.) + 1e-10);
        } myOut << endl;

        myOut << " Now, for each class of sequences, makes the distribution of affinities." << endl;

        vector<double> newAffinitiesByMutation;      // collecting new affinities only inside one class
        vector<double> foldAffinitiesByMutation;
        vector< vector<double>> tableAbsMutations;   // to store the histograms inside each class.
        vector< vector<double>> tableFoldMutations;
        vector<double> TOTALnewAffinitiesByMutation; // collecting new affinities for all sequences.
        vector<double> TOTALfoldAffinitiesByMutation;
        // For making histograms in term of fold induction, the following groups/classes will be used to make histograms
        vector<double> classesFoldInd = {-10, -7.5, -5, -3.25, -2.5, -2, -1.4, -1.2, -1.1, -1.01, -0.99, -0.9, -0.8, -0.6, -0.4, -0.2, -0.1, 0,0.1,0.2,0.4,0.6,0.8,0.9,0.99,1.01,1.1,1.2,1.4,2,2.5,3.25,5,7.5,10};
        // For making histograms in term of affinity, the classes inside valueclasses will be used.

        classes.push_back(1.0); // to avoid seg fault

        // Now will browse again, class by class. Values of sequences inside valuesClasses[currentClass-1] and valuesClasses[currentClass]
        fraction = sizeClasses;
        int currentClass = 1;

        for(unsigned int i = 0; i < store.size(); ++i){
            binaryLineage* thisSeq = store[i].second;
            double oldAff = binaryLineage::affinity(thisSeq, ref);
            if((oldAff > valuesClasses[currentClass] + 1e-6) || (oldAff < valuesClasses[currentClass-1] - 1e-6) ) cerr << "Class problems, sequence outside its class" << endl;

            // within one class, add the new possible affinities
            for(int j = 0; j < thisSeq->size * 2; ++j){ // might get sequence of different lengths !
                binaryLineage seq(thisSeq);
                seq.mutateOnePosition();
                double newAff = binaryLineage::affinity(&seq, ref);
                newAffinitiesByMutation.push_back(newAff);
                if(fabs(oldAff) > 1e-8) foldAffinitiesByMutation.push_back(newAff / oldAff);
                TOTALnewAffinitiesByMutation.push_back(newAff);
                if(fabs(oldAff) > 1e-8) TOTALfoldAffinitiesByMutation.push_back(newAff / oldAff);
                //out << thisSeq->print() << " Aff " << oldAff << "->" << seq.print() << " Newaff " << newAff << " Fold " << newAff / oldAff << endl;
            }
            if((((double) i / (double) (store.size())) >= fraction - 1e-6) || (i == store.size() - 1)){
                if(fabs(oldAff) > 1e-8) {//out << "Histograms for mutation for the class [" << valuesClasses[currentClass-1] << "," << valuesClasses[currentClass] << "]" << endl;
                    histogramFromDistrib resAbsForThisClass(newAffinitiesByMutation,valuesClasses);
                    tableAbsMutations.push_back(resAbsForThisClass.densities);
                    //out << "New affinities" << endl;
                    //out << resAbsForThisClass.print(true);
                    histogramFromDistrib resFoldForThisClass(foldAffinitiesByMutation,classesFoldInd);
                    tableFoldMutations.push_back(resFoldForThisClass.densities);
                    myOut << "Fold increase in the affinity" << endl;
                    myOut << resFoldForThisClass.print(true);
                    //out << endl << endl << endl;
                    newAffinitiesByMutation.clear();
                    foldAffinitiesByMutation.clear();
                    currentClass++;
                    fraction += sizeClasses;
                }
            }

         }
        myOut << "Outputing the affinity changes from all sequences" << endl;
        histogramFromDistrib TOTALresAbsForThisClass(TOTALnewAffinitiesByMutation,valuesClasses);
        myOut << "New affinities" << endl;
        myOut << TOTALresAbsForThisClass.print(true);
        histogramFromDistrib TOTALresFoldForThisClass(TOTALfoldAffinitiesByMutation,classesFoldInd);
        myOut << "Fold increase in the affinity" << endl;
        myOut << TOTALresFoldForThisClass.print(true);

        myOut << "Outputing the affinity changes (new affinity value), from each class of affinity" << endl;
        myOut << "\t";
        for(unsigned int j = 0; j < tableAbsMutations.size(); ++j){
            myOut << "[" << valuesClasses[j] << "," << valuesClasses[j+1] << "]\t";
        }
        myOut << "\n";
        for(unsigned int i = 0; i < TOTALresAbsForThisClass.densities.size(); ++i){
            myOut << "[" << TOTALresAbsForThisClass.lowBoundsXs[i] << "," << TOTALresAbsForThisClass.highBoundsXs[i] << "]";
            for(unsigned int j = 0; j < tableAbsMutations.size(); ++j){
                myOut << "\t" << tableAbsMutations[j][i];
            }
            myOut << endl;
        }

        myOut << "Outputing the affinity changes (new affinity value), from each class of affinity" << endl;
        myOut << "\t";
        for(unsigned int j = 0; j < tableFoldMutations.size(); ++j){
            myOut << "[" << valuesClasses[j] << "," << valuesClasses[j+1] << "]\t";
        }
        myOut << "\n";
        for(unsigned int i = 0; i < TOTALresFoldForThisClass.densities.size(); ++i){
            myOut << "[" << TOTALresFoldForThisClass.lowBoundsXs[i] << "," << TOTALresFoldForThisClass.highBoundsXs[i] << "]";
            for(unsigned int j = 0; j < tableFoldMutations.size(); ++j){
                myOut << "\t" << tableFoldMutations[j][i];
            }
            myOut << endl;
        }


        myOut << "==== Part 2 : Evaluating cross-reactivity for this clone : ====" << endl;

        for(int k = 0; k < 2; ++k){

            binaryLineage ref2(ref);
            ref2.mutateOnePosition();
            ref2.mutateOnePosition();

            stringstream fname;
            fname << folder << "DotPlot2CloseAG-Cl" << nc << "-L" << L << " rep" << k << "_" << ref->printSeq() << "to" << ref2.printSeq() << ".txt";
            fstream of(fname.str().c_str(), ios::out);

            vector<double> affRef;
            vector<double> affRef2;

            for(int i = 0; i < min(maxim,20000); ++i){
                binaryLineage test(ref2);
                test.setAccessibility(refAccess->accessibility); // this particular clone
                test.randomize();
                double aff1 = binaryLineage::affinity(&test, ref);
                double aff2 = binaryLineage::affinity(&test, &ref2);
                affRef.push_back(aff1);
                affRef2.push_back(aff2);
                of << aff1 << "\t" << aff2 << endl;
            }
            of.close();
        }

        for(int k = 0; k < 2; ++k){

            binaryLineage ref2(ref);
            ref2.mutateOnePosition();
            ref2.mutateOnePosition();
            ref2.mutateOnePosition();
            ref2.mutateOnePosition();
            ref2.mutateOnePosition();
            ref2.mutateOnePosition();
            ref2.mutateOnePosition();

            stringstream fname;
            fname << folder << "DotPlot6CloseAG-Cl" << nc << "-L" << L << "rep" << k << "_" << ref->printSeq() << "to" << ref2.printSeq() << ".txt";
            fstream of(fname.str().c_str(), ios::out);

            vector<double> affRef;
            vector<double> affRef2;

            for(int i = 0; i < min(maxim,20000); ++i){
                binaryLineage test(ref2);
                test.setAccessibility(refAccess->accessibility); // this particular clone
                test.randomize();
                double aff1 = binaryLineage::affinity(&test, ref);
                double aff2 = binaryLineage::affinity(&test, &ref2);
                affRef.push_back(aff1);
                affRef2.push_back(aff2);
                of << aff1 << "\t" << aff2 << endl;
            }
            of.close();
        }

        for(int k = 0; k < 2; ++k){
            binaryLineage ref2(ref);
            ref2.randomize();

            stringstream fname;
            fname << folder << "DotPlotRandCloseAG-Cl" << nc << "-L" << L << "rep" << k << "_" << ref->printSeq() << "to" << ref2.printSeq() << ".txt";
            fstream of(fname.str().c_str(), ios::out);


            vector<double> affRef;
            vector<double> affRef2;

            for(int i = 0; i < min(maxim,20000); ++i){
                binaryLineage test(ref2);
                test.setAccessibility(refAccess->accessibility); // this particular clone
                test.randomize();
                double aff1 = binaryLineage::affinity(&test, ref);
                double aff2 = binaryLineage::affinity(&test, &ref2);
                affRef.push_back(aff1);
                affRef2.push_back(aff2);
                of << aff1 << "\t" << aff2 << endl;
            }
            of.close();
        }
    }

    myOut << " =============== Distribution of affinities, one column one clone =============== \n";
    for (int i = 0; i < resolutiondistrib; ++i) {
        myOut << i << "\t"  << -1. + 2. * (double (i) * (1.0 / (double) resolutiondistrib ))
            << "\t" << -1. + 2. * (double (i + 1) * (1.0 / (double) resolutiondistrib ));
        for(int j = 0; j < nClones; ++j){
            /*double total = 0; // this is already done inside the first print loop (/= total)
            for(int k = 0; k < allHistos[j]->size(); ++k){
                total += (*allHistos[j])[k];
            }*/
            myOut << "\t" << (*allHistos[j])[i];
        }
        myOut << "\n";
    }







/*ofstream ffin(folder + string("Output.txt"));
ffin << out.str();
ffin.close();
return out.str();*/


       int nbAntigens = 10;
       myOut << "Generating randomly " << nbAntigens << " antigens " << endl;
       vector<binaryLineage*> ags;
       for (int i = 0; i < nbAntigens; ++i) {
          binaryLineage * seq = new binaryLineage(L);
          seq->randomize();
          ags.push_back(seq);
          myOut << "\tAg nr " << i << "\t" << seq->print() << endl;
       }
       myOut << "\nNumber of antigens recognized by randomly generated sequences, based on threshold\n";

       myOut << "  -> (for the first 100 sequences : ) In the case of random sequences" << endl;
       total = 0;
        #define thresholdRecoAg 0.1
       int nbDiscardedSeq = 0;  // sequences that don't recognize anything
       int countprint = 0;
       for (int k = 0; k < min(maxim, (int) maxSequencesToEnumeate); ++k) {
          if (k == 100) {
             myOut << "  -> (for the remaining sequences) for sequences recognizing at least an antigen with affinity 0.1" << endl;
          }
          total++;

          // for each sequence,
          bool recoAtLeastOne = false;
          vector<double> nbRecoDepThresh(10, 0.0);
          vector<double> affinityEach(nbAntigens, 0.0);
          binaryLineage * seqtmp = new binaryLineage(L);
          seqtmp->randomize();
          for (int j = 0; j < nbAntigens; ++j) {
             double thisAff = binaryLineage::affinity(seqtmp,
                                                     ags[j]);
             if ((thisAff > thresholdRecoAg) || (k < 100)) {
                recoAtLeastOne = true;
             } else { nbDiscardedSeq++; }
             affinityEach[j] = thisAff;
             for (int i = 0; i <= (int) (9.99 * thisAff); ++i) {
                if (i < 10) { nbRecoDepThresh[i]++; }
             }
          }
          if (recoAtLeastOne && (countprint < 5000)) {
             countprint++;
             myOut << "RandSeq " << k << ", " << seqtmp->print() << " ";
             myOut << "nbAgPerThreshold:";
             for (int i = 0; i < 10; ++i) {
                myOut << "\t" << nbRecoDepThresh[i];
             }
             myOut << "\taffPerAg:";
             for (int i = 0; i < nbAntigens; ++i) {
                myOut << "\t" << affinityEach[i];
             }
             myOut << endl;
          }
          delete seqtmp;
       }
       myOut << "   ... Nb of sequences analyzed: " << total << endl;
       myOut << "   ... Nb of sequences discarded: " << nbDiscardedSeq
           << "(except the 100 first ones, i.e. among the :" << total - 100 << " remaining)" << endl;

       myOut << "==== Part 3 : Evaluating the effect of mutations : ====" << endl;

       binaryLineage * start = new binaryLineage(L);    // starting by '0000' : the best sequence
       myOut << "NbMut\tsequence\taffinity\n";
       for (int i = 0; i < 2 * L; ++i) {
          myOut << i << "\t" << start->print() << "\t" << binaryLineage::affinity(start,ref) << endl;
          start->mutateOnePosition();
       }

       myOut << "\tReaching a good affinity\t" << endl;
       start->randomize();
       double prevaff = binaryLineage::affinity(start, ref);

       bool stop = false;
       for (int i = 0; (i < L) && (!stop); ++i) {
          myOut << "sequence : " << start->print() << "\tAff:\t" << prevaff << "\t";
          myOut << "PossibleMut:";
          vector<int> posGoodMutations;
          for (int i = 0; i < L; ++i) {
             binaryLineage stmp = binaryLineage(start);
             stmp.content[i] = !stmp.content[i];
             double newaff = binaryLineage::affinity(&stmp, ref);
             myOut << "\t" << newaff;
             if (newaff > prevaff) { posGoodMutations.push_back(i); }
          }
          myOut << endl;
          if (posGoodMutations.size() > 0) {
             int nextmut = random::uniformInteger(0,posGoodMutations.size() - 1);
             start->content[posGoodMutations[nextmut]] = !start->content[posGoodMutations[nextmut]];
             prevaff = binaryLineage::affinity(start, ref);
          } else {
             stop = true;
          }
       }


       /*for (int i = 0; i < (int) store.size(); ++i) {
          delete store[i].second;
       }


       myOut << "\tReaching a good affinity\t" << endl;
       start->randomize();*/
       /*double prevaff = binarySequence::seq_affinity(start, ref, R, maxClusters, typeAffinityFunction);

       bool stop = false;
       for (int i = 0; (i < L) && (!stop); ++i) {
          out << "sequence : " << start->print() << "\tAff:\t" << prevaff << "\t";
          out << "PossibleMut:";
          vector<int> posGoodMutations;
          for (int i = 0; i < L; ++i) {
             binarySequence stmp = binarySequence(start);
             stmp.content[i] = !stmp.content[i];
             double newaff
                = binarySequence::seq_affinity(&stmp, ref, R, maxClusters, typeAffinityFunction);
             out << "\t" << newaff;
             if (newaff > prevaff) { posGoodMutations.push_back(i); }
          }
          out << endl;
          if (posGoodMutations.size() > 0) {
             int nextmut = irandom(posGoodMutations.size() - 1);
             start->content[posGoodMutations[nextmut]] = !start->content[posGoodMutations[nextmut]];
             prevaff = binarySequence::seq_affinity(start, ref, R, maxClusters, typeAffinityFunction);
          } else {
             stop = true;
          }
       }*/



    myOut << "2D plot of affinities to 2 antigens" << endl;
    myOut << "Case 1 : two close antigens, one mutation away" << endl;

    //return out.str();
    return string(""); // IF NOT, SEGFAULT !!!!!
}
