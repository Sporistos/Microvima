#ifndef BooleanLineages_h
#define BooleanLineages_h
#include <vector>
#include <string>
using namespace std;

// comment the define to use this file independently
#define USE_DISTRIBUTIONS
#ifdef USE_DISTRIBUTIONS
#include "../Tools/distribution.h"
#endif

/** This sequence representation is derived from the model of Nourmohammad, Plos Genetics 2016
* Antibody = binary sequence with accessibilities at each position (= binary lineage)
* epitope =  only binary sequence, accessibilities are 0 everywhere
*            the epitope can be filled with a series of '1's at its end to represent conserved residues.
*            therefore, the randomize function can be done by avoiding the last 'conserved residues' */

struct binaryLineage {

    // Operators / functions to set up a new binaryLineage
    binaryLineage(int _size, int _lcon = 0);                                                                   /// sequence of '0's of this size, accessibilities are all 1.
    binaryLineage(string boolText, int _lcon = 0, vector<double> _accessibility = vector<double>());           /// string of '0' and '1'. Other characters become '0'. Accessibility not given -> 1 for each position
    binaryLineage(int _size, long long ID, int _lcon = 0, vector<double> _accessibility = vector<double>());   /// long long to binary. Max 2^63 No accessibility given -> 1 everywhere
    binaryLineage(binaryLineage * toCopy);

    void setAccessibility(vector<double> _accessibility);
    void setAccessibilityUniform();
    #ifdef USE_DISTRIBUTIONS
    void setAccessibilityFromDistribution(distribution *d);
    #endif

    // Data storage. Note: no additional/global parameters for this type of sequences
    vector<bool> content;
    vector<double> accessibility;
    int size;                                  /// could be static - here, allow sequences of different sizes
    int lcon;                                  /// positions that will not be randomized nor mutated (conserved) - from the antigen side
                                               /// note: only antigens will have such constraint.

    // Accessing data
    int operator [](int i);                    /// Note : can not be used for assigning values
    int getAccess(int i);                      /// get the accessibility at that position
    void operator =(binaryLineage& B) { content = B.content; size = B.size; lcon = B.lcon; accessibility = B.accessibility;}
    bool operator ==(binaryLineage& b);

    // Meaningful functions
    void randomize();                                              /// flips every position with probability 0.5. Last lcon sites not impacted. Accessibility kept
    int mutateOnePosition();                                       /// pick random position and flips it. Last lcon sites not impacted. Doesn't change accessibility. returns success 1, silent 0, or fail -1 (stop)
    static double affinity(binaryLineage *x, binaryLineage *y);    /// not supposed to be used but could be useful.
    static double hamming(binaryLineage  *x, binaryLineage *y);    /// number of mutations between two sequences to be equal.

    // Documentation / Test functions
    string print();
    string printSeq();
    string printAcc();
    static string test();

    // tool function. Same as in binary sequences, but repeated here so this file can be used independently
    static void recursive_binary(long ID, vector<bool> &towrite, int currentposition = -1);

    ~binaryLineage() { }
};

string testBinaryLineage(int L);

#endif
