#include <sstream>
#include <cmath> //for pow
#include "../Tools/zaprandom.h"
#include "binarySequences.h"
#include <algorithm> // shuffle


// Operators / functions to set up a new binaryLineage
binarySequence::binarySequence(int _size){
    if (_size < 0) {_size = 0;}
    size = _size;
    content.resize(_size, false);
    conservedPositions.resize(_size, false);
}

binarySequence::binarySequence(string boolText){
    size = boolText.size();
    content.resize(size, false);
    for (int i = 0; i < size; ++i){
        if((boolText[i] != '1') && (boolText[i] != '0')) {
            cerr << "ERR:sequence(" << boolText << "), un-allowed character : " << boolText[i] << endl;
        }
        content[i] = (boolText[i] == '1');
    }
    conservedPositions.resize(size, false);
}

binarySequence::binarySequence(binarySequence *toCopy){
    size = toCopy->size;
    content.resize(size);
    for (int i = 0; i < size; ++i){
        content[i] = toCopy->content[i];
    }
    conservedPositions.resize(size, false);
    for (int i = 0; i < size; ++i){
        conservedPositions[i] = toCopy->conservedPositions[i];
    }
}

binarySequence::binarySequence(int _size, long long ID) {
    // ': sequence(_size)' leads to a warning because only works in C++11, so copied constructor
    if (_size < 0) {_size = 0;}
    size = _size;
    content.resize(_size, false);
    conservedPositions.resize(_size, false);
    recursive_binary(ID, this->content);
}

void binarySequence::setConservedPositions(std::vector<bool> _conservedPositions){
    if((int) _conservedPositions.size() != size) cerr << "ERR: binarySequence::setConservedPositions, non-matching size " << endl;
    conservedPositions = _conservedPositions;
}

void binarySequence::setConservedPositions(std::string _conservedPositions){
    if((int) _conservedPositions.size() != size) cerr << "ERR: binarySequence::setConservedPositions, non-matching size " << endl;
    conservedPositions.resize(size);
    for(int i = 0; i < size; ++i){
        conservedPositions[i] = _conservedPositions[i];
    }
}

void binarySequence::setConservedPositions(int lcon){
    conservedPositions.clear();
    conservedPositions.resize(lcon, true);
    conservedPositions.resize(size, false);
    random::shuffle(conservedPositions);
}

// Accessing data
int binarySequence::operator [](int i){
    if ((i < 0) || (i >= size)){
        cerr << "sequence[" << i << "] out of bounds (size=" << size << ")\n";
        return 0;
    }
    return (content[i]) ? 1 : 0;
}

bool binarySequence::operator ==(binarySequence &b) {
    if(size != b.size) return false;
    for (int i = 0; i < size; ++i) {
        if(conservedPositions[i] != b.conservedPositions[i]) return false;
    }
    for (int i = 0; i < size; ++i) {
        if(content[i] != b[i]) return false;
    }
    return true;
}


// Meaningful functions. returns 1: success, 0: silent, -1: fail
int binarySequence::mutateOnePosition() {
    int cpt = 0;
    while(cpt < 1e6){
        int position = random::uniformInteger(0, size-1);
        if ((position >= size) || (position < 0)) {
            cerr << "Random does shit, sequencespace.cpp::mutateOnePosition";
            return -1; // fail
        }
        if(conservedPositions[position] == false){
            content[position] = !content[position];
            return +1; // success
        }
        cpt++;
    }
    cerr << "ERR: binarySequence::mutateOnePosition could not find a non-conserved position" << endl;
    return 0; // silent
}

void binarySequence::randomize() {
    for (int i = 0; i < size; ++i) {
        if(!conservedPositions[i]){
            if (random::uniformDouble(0,1) < 0.5) {
                content[i] = !content[i];
            }
        }
    }
}

// for debugging, this define can generate a lot of text
#define SEQAFF_VERBOSE false

double binarySequence::r = NAN;
int binarySequence::maxSizeClusters = 0;
int binarySequence::type_affinity_function = seqAff;
bool binarySequence::initialized = false;
bool binarySequence::use_logarithm_affinity = false;
bool binarySequence::rescale_exponent = 1.0;


void binarySequence::initializeDefaultParameters(int _type_affinity_function, double _r, double _maxSizeClusters, bool _use_logarithm_affinity, double _rescale_exponent){
    r = _r;
    maxSizeClusters = _maxSizeClusters;
    if((type_affinity_function != seqAff) && (_maxSizeClusters == NAN)) {
        cerr << "ERR: binarySequence::initializeDefaultParameters, you should give a "
                "valid maxSizeClusters in order to use complex types of affinity (different than seqAff)" << endl;
    }
    type_affinity_function = _type_affinity_function;
    use_logarithm_affinity = _use_logarithm_affinity;
    rescale_exponent = _rescale_exponent;
    initialized = true;    
}

// Big affinity function that encompasses all the types of binary affinities.
double binarySequence::affinity(binarySequence *x, binarySequence *y, int _type_affinity_function, double _r, int _maxSizeClusters)
{
    if((!x) || (!y)){
        cerr << "ERR: binarySequence::affinity received a NULL sequence" << endl;
    }

    // taking default predefined values if not given here.
    if((_maxSizeClusters == -1) && (_type_affinity_function != seqAff) && (_type_affinity_function != seqAffNorm)){
        if(!initialized) cerr << "ERR: default parameters for binarySequence have not been given !!!" << endl;
        else {_maxSizeClusters = maxSizeClusters;}
    }
    if(_r == -1) {
        if(!initialized) cerr << "ERR: default parameters for binarySequence have not been given !!!" << endl;
        else{_r = r;}
    }
    if(_type_affinity_function == -1){_type_affinity_function = type_affinity_function;}  // note: putting this line up won't change anything

    double res = NAN;   // result
    switch (_type_affinity_function){
    // The original seq_affinity function
    // double sequence::seq_affinity(sequence* x, sequence* y, double r){
    // This is slightly different than sahams model here by saying there
    // is affinity if sequence are EQUAL instead of COMPLEMENTARY, in order to have BCR-AG-TCRs
    // are close if they have the same sequence (easier to interprete)
    case seqAff:{
        if(SEQAFF_VERBOSE) cerr << "Sr=" << _r;
        if (x->size != y->size) {
            std::cout << "Err affinity: length of antigen and BCR are not identical!";
            return -1;
        }
        int l = 0;
        double sum = 0;
        for (int i = 0; i < x->size; ++i){
            if ((*x)[i] == (*y)[i]){               // as long as equal, increases matching size l
                ++l;
            } else {
                if(SEQAFF_VERBOSE && (l>0)) cerr << "C" << l;
                sum += std::pow((double) l,_r);      // when not equal anymore, close the match
                l = 0;
            }
        }
        if (l > 1e-6) {
            sum += std::pow(l,_r);                  // if the match not closed, close it ; l = 0 if already closed
            if(SEQAFF_VERBOSE) cerr << "C" << l;
        }
        res = sum / std::pow((double) x->size,_r);
        // Other Idea : implement our own my_pow that precomputes all the values of pow(int i[0..length], r) to save time
        break;
    }

    // Same but instead of normalizing by L^r, normalizes to maxSizeClusters^r, and every exceeding values are brought to 1.0
    case seqAffNorm:{
        if (x->size != y->size){
            std::cout << "Err affinity: length of antigen and BCR are not identical!";
            return -1;
        }
        int l = 0;
        double sum = 0;
        for (int i = 0; i < x->size; ++i){
            if ((*x)[i] == (*y)[i]){               // as long as equal, increases matching size
                ++l;
            } else {
                sum += std::pow((double) l,_r);      // when not equal anymore, close the match
                if(SEQAFF_VERBOSE && (l>0)) cerr << "C" << l;
                l = 0;
            }
        }
        if (l > 1e-6) {
            sum += std::pow(l,_r);                  // if the match not closed, close it ; l = 0 if already closed
            if(SEQAFF_VERBOSE) cerr << "C" << l;
        }
        res = min(1.0, (sum / std::pow((double) _maxSizeClusters,_r)));
        break;
    }


    // The cluster size is limited, with no further increase in affinity.
    // to 1.0 if exceeds
    case seqAffMaxClusterSize:{
        if (x->size != y->size){
            std::cout << "Err affinity: length of antigen and BCR are not identical!";
            return -1;
        }
        int l = 0;
        double sum = 0;
        for (int i = 0; i < x->size; ++i){
            if ((*x)[i] == (*y)[i]){               // as long as equal, increases matching size
                ++l;
            } else {
                sum += std::pow((double) min(l, _maxSizeClusters) ,_r);      // when not equal anymore, close the match
                if(SEQAFF_VERBOSE && (l>0)) cerr << "C" << l << "->" << min(l, _maxSizeClusters);
                l = 0;
            }
        }
        if (l > 1e-6) {
            sum += std::pow(min(l, _maxSizeClusters),_r);                  // if the match not closed, close it ; l = 0 if already closed
            if(SEQAFF_VERBOSE) cerr << "C" << l << "->" << min(l, _maxSizeClusters);
        }
        int maxCover = x->size / _maxSizeClusters;
        res = sum / ((double) maxCover * std::pow((double) _maxSizeClusters,_r));
        break;
    }

    // the new affinity function with sizewindow. Call with sizewindow = -1 to get the original
    // affinity function. to keep the algorithm linear and not compute an affinity for each
    // window separately, computes the affinity of the sliding window in real time by adding
    // the contribution of the right new position taken and removing the contribution of the
    // left position which is left by the sliding windows.
    // double sequence::seq_affinity(sequence* x, sequence* y, double r, int sizewindow){
    case seqAffWindow:{
        int sizewindow = _maxSizeClusters;
        if (x->size != y->size){
            std::cout << "Err affinity: length of antigen and BCR are not identical!";
            return -1;
        }
        if (sizewindow < 1) {sizewindow = x->size;}

        // Step 1 : Counting inside the matches, from the left and from the right. Example, if the
        // example if match is 011111000111,
        vector<int> left(x->size, -1);     // -1 1 2 3 4 5 6 -1 -1 -1 1 2 3
        // -> remaining of the cluster at this position on the right
        vector<int> right(x->size, -1);    // -1 6 5 4 3 2 1 -1 -1 -1 3 2 1
        // -> size of the cluster if taking one more position on the right
        int l = 0;
        for (int i = 0; i < x->size + 1; ++i) {
            // the +1 allows to close the last match if it touches the end of the sequence
            if ((i < x->size) && ((*x)[i] == (*y)[i])) {
                // as long as the sequences are equal, increases the matching size l. For the last
                // check (i = x->size, directly go to the 'else' to close the last match)
                ++l;
                left[i] = l;                         // 1 2 3 4 5 ... inside the match/cluster
            } else {
                for (int j = 1; j <= l; ++j) {
                    // goes backwards from the end of the cluster (i-1) to its beginning (i-l)
                    right[i - j] = j;                 // ... 5 4 3 2 1  inside the match/cluster
                }
                l = 0;
            }
        }
        if(SEQAFF_VERBOSE) {cerr<<"L"; for(int i = 0; i < x->size; ++i) {cerr << left[i];} cerr<<"R"; for(int i = 0; i < x->size; ++i) {cerr << right[i];} }
        // Step2 : moving a sliding window and getting its affinity (windowsum) each time.
        double windowsum = 0;          // affinity of the sliding window
        double maxWindowSum = 0;       // maximum encountered
        for (int i = 0; i < x->size; ++i) {            // note: need to start at i=0 to eat the first matches
            // the sliding window now 'eat' the position i and 'leaves' the position i - sizewindow
            int posToRemove = i - sizewindow;

            // Eating : when the sliding windows take a new position of a match/cluster
            if (left[i] > 0) {
                // if new position taken by the window is in a match
                windowsum += std::pow(left[i], _r);               // then add the match
                if ((i > 0) && (left[i - 1] > 0)) {
                    windowsum -= std::pow(left[i - 1], _r);        // but if the last position was already
                    // in the macth, then remove (match-1)^r
                }
            }

            // Pooing : when the sliding windows leaves a position that was in a match
            if ((posToRemove >= 0)          // if the sliding window started leaving positions
                    && (right[posToRemove] > 0)) {
                // if the position that the window is leaving was in a match,
                windowsum -= std::pow(right[posToRemove], _r);    // then remove that match^r
                if (right[posToRemove + 1] > 0) {
                    windowsum += std::pow(right[posToRemove + 1], _r);
                    // but in case it stays in the match, restore the remaining match-1
                }
            }
            if(SEQAFF_VERBOSE) cerr << "W:" << windowsum << " ";
            maxWindowSum = max(maxWindowSum, windowsum);        // Note : do not put the condition
            // if(i >= sizewindow-1)  in case the sequence is smaller than the
            // window. Anyway, as long as i < sizewindow, the sum only
            // increases so the maximum will always increase and it doesn't
            // make a difference to put this condition or not.

            // cout << ((i < sizewindow-1)? 0 : windowsum) << endl;
            // cout << "left [" << i << "] = " << left[i] << "\t";
            // cout << "right[" << i << "] = " << right[i] << "\t";
        }
        res = maxWindowSum / std::pow((double) sizewindow, _r);
        break;
    }
    case seqAffSlidingWindow: { // This version only allows full sliding windows. In the borders, no sliding
        int L1 = x->size;
        int L2 = y->size;
        double res = 0;
        for(int i = 0; i < L1 - _maxSizeClusters+1; ++i){
            for(int j = 0; j < L2 - _maxSizeClusters+1; ++j){
                double affThisWindow = partialAffinity(x, i, _maxSizeClusters+i-1, y, j, _maxSizeClusters+j-1, _r); // the first and last positions are included
                if(SEQAFF_VERBOSE) cerr << "W[" << i << "," << j << "]=" << affThisWindow << " ";
                res = max(res, affThisWindow);
            }
            if(SEQAFF_VERBOSE) cerr << "\n";
        }
        break;
    }
    // this version tests all possible windows
    case seqAffSlidingWindowOverBorders: {
        int L1 = x->size;
        int L2 = y->size;
        double res = 0;
        for(int i = -_maxSizeClusters+1; i < L1; ++i){
            for(int j = -_maxSizeClusters+1; j < L2; ++j){
                int newI1 = max(0,i);
                int newI2 = min(L1-1,i+_maxSizeClusters-1); // last position, included
                int newJ1 = max(0,j);
                int newJ2 = min(L2-1,j+_maxSizeClusters-1);
                int sizeMaxWindow = min(newI2-newI1+1, newJ2-newJ1+1);
                double affThisWindow = partialAffinity(x, newI1, newI1 + sizeMaxWindow-1, y, newJ1, newJ1 + sizeMaxWindow-1, _r); // the first and last positions are included
                affThisWindow = affThisWindow * std::pow((double) sizeMaxWindow,_r) / std::pow((double) _maxSizeClusters,_r); // very important ...
                if(SEQAFF_VERBOSE) cerr << "W[" << i << "|" << newI1 << "-" << newI2 << "," << j << "|" << newJ1 << "-" << newJ2 << "," << sizeMaxWindow << "]=" << affThisWindow << " ";
                res = max(res, affThisWindow);
            }
            if(SEQAFF_VERBOSE) cerr << "\n";
        }
        break;
    }
    }  // end switch
    if(res == NAN) cerr << "sequence::seq_affinity(), unknown type of affinity function "
         << _type_affinity_function << endl;
    // not sure this option makes sense at all ...
    if((use_logarithm_affinity == true) && (!std::isnan(res)) && (!std::isinf(res))) return log(max(1e-15, res));
    return std::pow(res, rescale_exponent);
}

// note : pEND included.
double binarySequence::partialAffinity(binarySequence *s1, int pStart1, int pEnd1, binarySequence* s2, int pStart2, int pEnd2, double r){
    if (!s1 || !s2) return NAN;
    int L1 = s1->size;
    int L2 = s2->size;
    if((pStart1 < 0) || (pStart1 > pEnd1) || (pEnd1 >= L1)){
        cerr << "Err: partialAffinity : pStart1(" << pStart1 << ") or pEnd1(" << pEnd1 << ") incorrect. size of s1(" << s1->size << ")" << endl;
        return NAN;
    }
    if((pStart2 < 0) || (pStart2 > pEnd2) || (pEnd2 >= L2)){
        cerr << "Err: partialAffinity : pStart2(" << pStart2 << ") or pEnd2(" << pEnd2 << ") incorrect. size of s2(" << s2->size << ")" << endl;
        return NAN;
    }
    if((pEnd2 - pStart2) != (pEnd1 - pStart1)){
        cerr << "Err: partialAffinity, the two desired windows should have sime size !" << endl;
        return NAN;
    }
    int windowSize = pEnd1 - pStart1+1; // pStart and pEnd are INCLUDED
    if((windowSize > L1) || (windowSize > L2)){ // Should not happen because pEnd < size
        cerr << "Err : partialAffinity, the size window is bigger than (at least) one of the sequences" << endl;
        return NAN;
    }
    int l = 0;
    double sum = 0;
    for (int i = 0; i < windowSize; ++i) {
        if ((*s1)[i+pStart1] == (*s2)[i+pStart2]) {
            ++l;
        } else {
            sum += std::pow((double) l,r);
            l = 0;
        }
    }
    if(SEQAFF_VERBOSE) {cerr<<"("; for(int i = 0; i < windowSize; ++i) {cerr << (*s1)[i+pStart1];} cerr << "-"; for(int i = 0; i < windowSize; ++i) cerr << (*s2)[i+pStart2]; cerr<<")";}
    if (l > 1e-6) {
        sum += std::pow(l,r);          // if match not closed, close it ; l = 0 if already closed
    }
    return sum / std::pow((double) windowSize,r);
}

double binarySequence::hamming(binarySequence *x, binarySequence *y) {
    if (x->size != y->size) {
        std::cout << "Err binarySequence::hamming: length of antigen and BCR are not identical!";
        return -1;
    }
    int length = x->size;
    int sum = 0;
    for (int i = 0; i < length; ++i) {
        sum += ((*x)[i] != (*y)[i]) ? 1 : 0;
    }
    return sum;
}

string binarySequence::print() {
    stringstream out;
    for (int i = 0; i < size; ++i) {
        out << content[i];
    }
    return out.str();
}



// tool / test functions
/// /// @brief Transform a long number into binary sequence, inside a given vector (already sized).
/// example: recursive_binary(2354, a_bool_vector_to_fill);
// Alternative is to use bitset, but size has to be known at compilation -> not convenient
void binarySequence::recursive_binary(long ID, vector<bool> &towrite, int currentposition){
    if (ID < 0) {cerr << "ERR::sequence::binary, negative number " << ID << endl; return;}
    if (currentposition < 0){    // default option -1 means need to start from the first position
        currentposition = towrite.size() - 1;
    }
    if (currentposition >= (int) towrite.size()){
        cerr << "Err: sequence::binary(), currentposition is out of bounds ("
             << currentposition << "), while vector size is " << towrite.size() << endl;
        return;
    }
    if (ID <= 1) {towrite[currentposition] = (ID != 0); return;}  // to stop recursion
    else if(currentposition == 0) { // this means that ID > 1, and no space for it.
        cerr << "ERR: recursive_binary, the given number was too big and could not fit into a "
             << " vector of size " << towrite.size() << endl;
        return;
    }
    long remainder = ID % 2;
    towrite[currentposition] = (remainder != 0);
    recursive_binary(ID >> 1, towrite, currentposition - 1);
}



// subfunctions for the test function
string printBinarySeqAff(binarySequence * S1, binarySequence *S2, int typeAffFUnct, int maxSizeClusters){
    static int last_typeAffFUnct = -1;
    static int last_Cluster = -1;
    stringstream res;
    if((typeAffFUnct != last_typeAffFUnct) || (maxSizeClusters != last_Cluster)){
        last_typeAffFUnct = typeAffFUnct;
        last_Cluster = maxSizeClusters;
        res << "\nFrom now, affinitype is : ";
        switch(typeAffFUnct){
        case seqAff: {res << "seqAff: Basic affinity\n"; break;}
        case seqAffNorm: {res << "seqAffNorm: Maximum affinity when cluster^r reached (cluster=" << maxSizeClusters << "\n"; break;}
        case seqAffMaxClusterSize: {res << "seqAffMaxClusterSize: Clusters of more than size " << maxSizeClusters << " do not contribute\n"; break;}
        case seqAffWindow: {res << "seqAffWindow Facing sequences sliding window (" << maxSizeClusters << ")\n"; break;}
        case seqAffSlidingWindow: {res << "All possible facing windows of size (" << maxSizeClusters << ")\n"; break;}
        case seqAffSlidingWindowOverBorders: {res << "All possible facing windows of size, Exceeding borders (" << maxSizeClusters << ")\n"; break;}
        default: res << "??\n";
        }
    }
    res << "seqAff of " << S1->print() << " - \n"
        << "          " << S2->print() << " = ";
    for(int i = 1; i < 8; ++i){
        res << " (r=" << i << ")" << binarySequence::affinity(S1, S2,i,maxSizeClusters,typeAffFUnct);
    }
    res << "\n";
    return res.str();
}

string binarySequence::nameTypeAff(int typeAffFUnct){
    switch(typeAffFUnct){
    case seqAff: {return string("seqAff");}
    case seqAffNorm: {return string("seqAffNorm");}
    case seqAffMaxClusterSize: {return string("seqAffMaxClusterSize");}
    case seqAffWindow: {return string("seqAffWindow");}
    case seqAffSlidingWindow: {return string("seqAffSlidingWindow");}
    case seqAffSlidingWindowOverBorders: {return string("seqAffSlidingWindowOverBorders");}
    default: return string("");
    }
}

string binarySequence::test(){
#define MODE_OUT cerr

    MODE_OUT << "================ Test constructor / basic functions =================" << endl;
    binarySequence S1(5);
    MODE_OUT << "S1: Empty sequence, size 5 " << S1.print() << "\n\n";
    binarySequence S2(string("010111101011111"));
    MODE_OUT << "S2: Sequence from binary text 010111101011111 -> " << S2.print() << "\n\n";
    binarySequence S3(string("01oAX5!="));
    MODE_OUT << "S3: Sequence from text with bad characters '01oAX5!=' ->" << S3.print() << "\n\n";
    binarySequence S4(&S2);
    MODE_OUT << "S4: Copy from previous sequence S2 " << S4.print() << "\n\n";
    binarySequence S4b(15);
    S4b = S2;
    MODE_OUT << "S4: = from previous sequence S2 " << S4b.print() << "\n\n";
    long long int x5 = 158230;
    int size5 = 10;
    binarySequence S5a(2*size5, x5); // size 10
    MODE_OUT << "S5a: Sequence from binary : " << S5a.print() << " from " << x5 << " with size " << 2*size5 << "\n\n";
    binarySequence S5b(size5, x5); // size 10
    MODE_OUT << "S5b: Sequence from binary (too big number -> error): " << S5b.print() << " from " << x5 << " with size " << size5 << "\n\n";
    MODE_OUT << "Accessing positions of S5b one by one [] ";
    MODE_OUT << S5a[0] << S5a[1] << S5a[2] << "..." << S5a[S5a.size-1] << "\n\n";
    MODE_OUT << "Now testing bad positions : ";
    MODE_OUT << S5a[-1] << S5a[S5a.size] << "\n\n";
    MODE_OUT << "S6: Randomized sequence ";
    binarySequence S6(0);
    S6.randomize();
    MODE_OUT << S6.print() << "\n\n";
    MODE_OUT << "S7: Randomized sequence, from an existing one ";
    binarySequence S7(15,257);
    S7.randomize();
    MODE_OUT << S7.print() << "\n\n";
    MODE_OUT << "Consecutive mutations" << endl;
    binarySequence S8("0000000000");
    for(int i = 0; i < 10; ++i){
        S8.mutateOnePosition();
        MODE_OUT << S8.print() << endl;
    }
    binarySequence S9(0);
    S9 = S8;
    MODE_OUT << "Compare equal sequences : " << S8.print() << ((S8 == S9) ? "=" : "!=")  << S9.print() << "\n\n";
    S9.mutateOnePosition();
    MODE_OUT << "Compare different sequences : " << S8.print() << ((S8 == S9) ? "=" : "!=")  << S9.print() << "\n\n";

    MODE_OUT << "Test affinity functions on single examples" << "\n";
    binarySequence S10("0000011111");   // reference sequence AG1
    binarySequence S11("1100011111");   // reference sequence AG2
    binarySequence S21("0000001111");   // match 4+5 to AG1, 4+3 to AG2
    binarySequence S22("1000000111");   // match 1+3+3 to AG1, 1+3+3 to AG2


    MODE_OUT << "1 - basic affinity " << "\n\n";

    MODE_OUT << "Set up : two antigens : \n";
    MODE_OUT << "\t" << S10.print() << "\n";
    MODE_OUT << "\t" << S11.print() << "\n";
    MODE_OUT << "And two antibodies : \n";
    MODE_OUT << "\t" << S21.print() << "\n";
    MODE_OUT << "\t" << S22.print() << "\n\n";

    MODE_OUT << "seqAff of " << S10.print() << " - \n"
             << "          " << S21.print() << " = "
             << " (r=1)" << binarySequence::affinity(&S10, &S21,1,-1,seqAff)
             << " (r=2)" << binarySequence::affinity(&S10, &S21,2,-1,seqAff)
             << " (r=7)" << binarySequence::affinity(&S10, &S21,7,-1,seqAff) << "\n\n";
    MODE_OUT << "seqAff of " << S10.print() << " - \n"
             << "          " << S22.print() << " = "
             << " (r=1)" << binarySequence::affinity(&S10, &S22,1,-1,seqAff)
             << " (r=2)" << binarySequence::affinity(&S10, &S22,2,-1,seqAff)
             << " (r=7)" << binarySequence::affinity(&S10, &S22,7,-1,seqAff) << "\n\n";
    MODE_OUT << "seqAff of " << S11.print() << " - \n"
             << "          " << S21.print() << " = "
             << " (r=1)" << binarySequence::affinity(&S11, &S21,1,-1,seqAff)
             << " (r=2)" << binarySequence::affinity(&S11, &S21,2,-1,seqAff)
             << " (r=7)" << binarySequence::affinity(&S11, &S21,7,-1,seqAff) << "\n\n";
    MODE_OUT << "seqAff of " << S11.print() << " - \n"
             << "          " << S22.print() << " = "
             << " (r=1)" << binarySequence::affinity(&S11, &S22,1,-1,seqAff)
             << " (r=2)" << binarySequence::affinity(&S11, &S22,2,-1,seqAff)
             << " (r=7)" << binarySequence::affinity(&S11, &S22,7,-1,seqAff) << "\n\n";

    //    seqAff, seqAffNorm, seqAffMaxClusterSize, seqAffWindow, seqAffSlidingWindow, seqBiggestSubsequence, seqMultiEpitopeAffinity,
    MODE_OUT << "2 - Normalize, affinity more than maxclusters^r goes to 1" << endl;
    binarySequence S00("0000000000");   // reference sequence AG1
    binarySequence S31("0011111111");   // match 2
    binarySequence S32("0000111111");   // match 4
    binarySequence S33("0000001111");   // match 6
    binarySequence S34("0000000011");   // match 8
    binarySequence S35("1000010100");   // match 4+1+2
    binarySequence S36("1000000100");   // match 6+2

    MODE_OUT << "Set up : one antigens : \n";
    MODE_OUT << "\t" << S00.print() << "\n";
    MODE_OUT << printBinarySeqAff(&S00, &S31, seqAffNorm, 4);
    MODE_OUT << printBinarySeqAff(&S00, &S32, seqAffNorm, 4);
    MODE_OUT << printBinarySeqAff(&S00, &S33, seqAffNorm, 4);
    MODE_OUT << printBinarySeqAff(&S00, &S34, seqAffNorm, 4);
    MODE_OUT << printBinarySeqAff(&S00, &S35, seqAffNorm, 4);
    MODE_OUT << printBinarySeqAff(&S00, &S36, seqAffNorm, 4);

    MODE_OUT << "3 - Normalize, clusters of more than a certain size do not increase affinity" << endl;

    MODE_OUT << "Set up : one antigens : \n";
    MODE_OUT << "\t" << S00.print() << "\n";
    MODE_OUT << printBinarySeqAff(&S00, &S31, seqAffMaxClusterSize, 5);
    MODE_OUT << printBinarySeqAff(&S00, &S32, seqAffMaxClusterSize, 5);
    MODE_OUT << printBinarySeqAff(&S00, &S33, seqAffMaxClusterSize, 5);
    MODE_OUT << printBinarySeqAff(&S00, &S34, seqAffMaxClusterSize, 5);
    MODE_OUT << printBinarySeqAff(&S00, &S35, seqAffMaxClusterSize, 5);
    MODE_OUT << printBinarySeqAff(&S00, &S36, seqAffMaxClusterSize, 5);


    MODE_OUT << "\n4 - Sliding windows,facing sequences" << endl;

    MODE_OUT << "Set up : one antigens : \n";
    binarySequence S40("1111111111");   // match 5
    MODE_OUT << "\t" << S40.print() << "\n";
    binarySequence S41("0011111000");   // match 5
    binarySequence S42("1110000111");   // match 3+3, far
    binarySequence S43("1101111000");   // match 4+2, close
    MODE_OUT << printBinarySeqAff(&S40, &S41, seqAffWindow, 4);
    MODE_OUT << printBinarySeqAff(&S40, &S42, seqAffWindow, 4);
    MODE_OUT << printBinarySeqAff(&S40, &S43, seqAffWindow, 4);
    MODE_OUT << printBinarySeqAff(&S40, &S41, seqAffWindow, 6);
    MODE_OUT << printBinarySeqAff(&S40, &S42, seqAffWindow, 6);
    MODE_OUT << printBinarySeqAff(&S40, &S43, seqAffWindow, 6);

    MODE_OUT << "\n5 - All possible sliding windows" << endl;

    MODE_OUT << "Set up : two antigens : \n";
    binarySequence S50("0011111000");
    binarySequence S54("1010101011111111111");
    MODE_OUT << "\t" << S50.print() << "\n";
    MODE_OUT << "\t" << S54.print() << "\n";
    MODE_OUT << printBinarySeqAff(&S54, &S50, seqAffSlidingWindow, 4);

    binarySequence S51("1111111111");   // match 5
    binarySequence S52("1010110111");   // match 6
    binarySequence S53("1111101011");   // match 5 or 6
    MODE_OUT << printBinarySeqAff(&S54, &S51, seqAffSlidingWindow, 4);
    MODE_OUT << printBinarySeqAff(&S54, &S52, seqAffSlidingWindow, 4);
    MODE_OUT << printBinarySeqAff(&S54, &S53, seqAffSlidingWindow, 4);
    MODE_OUT << printBinarySeqAff(&S54, &S51, seqAffSlidingWindow, 8);
    MODE_OUT << printBinarySeqAff(&S54, &S52, seqAffSlidingWindow, 8);
    MODE_OUT << printBinarySeqAff(&S54, &S53, seqAffSlidingWindow, 8);

    MODE_OUT << "\n6 All possible sliding windows, beyond borders" << endl;

    MODE_OUT << "Set up : two antigens (Yesss!) : \n";
    binarySequence S55("0011111000");
    binarySequence S56("0111110001111111111"); // with shifting, should be 9
    MODE_OUT << "\t" << S55.print() << "\n";
    MODE_OUT << "\t" << S56.print() << "\n";
    MODE_OUT << printBinarySeqAff(&S56, &S55, seqAffSlidingWindow, 10);
    MODE_OUT << printBinarySeqAff(&S56, &S55, seqAffSlidingWindowOverBorders, 10);

    return string("");
}
