#ifndef BinarySequences_h
#define BinarySequences_h

#include <iostream>
#include <vector>
#include <string>

enum typeAffFunction {
    seqAff, seqAffNorm, seqAffMaxClusterSize, seqAffWindow, seqAffSlidingWindow, seqAffSlidingWindowOverBorders, seqBiggestSubsequence, seqMultiEpitopeAffinity, seqNumberTypesAffFunctions
};

struct binarySequence {

    // Operators / functions to set up a new binarySequence
    binarySequence(int _size);                 /// sequence of '0's of this size
    binarySequence(std::string boolText);      /// string of '0' and '1'. Other characters become '0'
    binarySequence(binarySequence * toCopy);
    binarySequence(int _size, long long ID);   /// long long to binary. Max 2^63
    void setConservedPositions(std::vector<bool> _conservedPositions);
    void setConservedPositions(std::string _conservedPositions);
    void setConservedPositions(int lcon);      /// will be put at random positions

    // Data storage
    std::vector<bool> content;
    std::vector<bool> conservedPositions;      /// option: positions with a 1 will not mutate / randomize
                                               /// Note: only antigens have conserved positions !!!
    size_t size;                                  /// could be static if all sequences have the same size

    // Accessing data
    int operator [](int i);                    /// Note : can not be used for assigning values
    void operator =(binarySequence& B) {content = B.content; size = B.size; conservedPositions=B.conservedPositions;}
    bool operator ==(binarySequence& b);

    // default parameters: if not specified inside affinity, these default values will be used
    static void initializeDefaultParameters(int _type_affinity_function, double _r, double _maxSizeClusters = NAN, bool _use_logarithm_affinity = false, double _rescale_exponent = 1.0);
    static double r;
    static int maxSizeClusters;
    static int type_affinity_function;
    static bool initialized;
    static bool use_logarithm_affinity;
    static bool rescale_exponent;

    // Meaningful functions
    void randomize(); /// flips every position with probability 0.5. Keeps conserved last elements
    int mutateOnePosition();                  /// pick random position and flips it
    static double affinity(binarySequence * x, binarySequence * y, int _type_affinity_function = -1, double _r = -1, int _maxSizeClusters = -1);
    static double hamming(binarySequence * x, binarySequence * y);

    // Documentation / Test functions
    virtual std::string print();
    static std::string test();
    static std::string testAffinityFunctions(double L, double R, int maxClusters, int typeAffinityFunction);

    // Tool functions
    static std::string nameTypeAff(int typeAffFUnct);
protected:
    static double partialAffinity(binarySequence* s1, int pStart1, int pEnd1, binarySequence* s2, int pStart2, int pEnd2, double r);
    void recursive_binary(long ID, vector<bool> &towrite, int currentposition = -1);
public:
    virtual ~binarySequence() { }
};


#endif
