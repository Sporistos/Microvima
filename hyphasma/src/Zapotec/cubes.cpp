
#include <fstream>
#include <map>
#include <set>
#include <cmath>
#include "cubes.h"
#include "../Ymir/plot3d.h"

bool excludeCube(int IDcoord){
    vector<int> v1 = lattice::positionFromID(IDcoord);
    return ((abs((XWidth / 2) - v1[0]) > 1) || (abs((YWidth / 2)-v1[1]) > 1) || (abs((ZWidth / 2)-v1[2]) > 1));
}

bool excludeCube(struct3D* toCheck){
    if(!toCheck) return false;
    for( set<int>::iterator sit = toCheck->occupiedPositions.begin(); sit != toCheck->occupiedPositions.end(); ++sit){
        if(excludeCube(*sit)) return true;
    }
    return false;
}

vector<struct3D*> generateCubes(int length, int IDposStart, moveDirection absStartDir, struct3D *alreadyFoldedTail);

/*
 * Cubic foldings of size 2	2
Cubic foldings of size 3	8
Cubic foldings of size 4	23
Cubic foldings of size 5	68
Cubic foldings of size 6	183
Cubic foldings of size 7	482
Cubic foldings of size 8	1207
Cubic foldings of size 9	2962
Cubic foldings of size 10	6933
Cubic foldings of size 11	15641
Cubic foldings of size 12	33544
Cubic foldings of size 13	68400
Cubic foldings of size 14	131739
Cubic foldings of size 15	236464
then memory crash
*/
vector<struct3D*> generateCubes(int length){
    vector<struct3D*> res;
    {
        //// Philippe TO SOLVE !!! : there is a case missing: start from the center of the face and goes to the center of the cube.
        /// this structure is anyways enumerated from the other side so it's fine but in the reasoning, it should be generated !!

        string seq = string("S");
        int cubeCenter = lattice::idFromPosisition(XWidth/2, YWidth/2, -1+ ZWidth/2);
        struct3D* startingTail1 = new struct3D(seq, UnDefined, cubeCenter );
        if(!excludeCube(startingTail1)){
            vector<struct3D*> resSSS = generateCubes(length - 1, startingTail1->endingPosition, Up, startingTail1);
            for(unsigned int j = 0; j < resSSS.size(); ++j){
                struct3D* fused = new struct3D(fuse(seq,resSSS[j]->sequence),UnDefined,cubeCenter);
                res.push_back(fused);
                delete resSSS[j];
            }

            vector<struct3D*> resSSSL = generateCubes(length - 1, startingTail1->endingPosition, Left, startingTail1);
            for(size_t j = 0; j < resSSSL.size(); ++j){
                struct3D* fused = new struct3D(fuse(seq,resSSSL[j]->sequence),UnDefined,cubeCenter);
                res.push_back(fused);
                delete resSSSL[j];
            }

            vector<struct3D*> resSSSR = generateCubes(length - 1, startingTail1->endingPosition, Right, startingTail1);
            for(size_t j = 0; j < resSSSR.size(); ++j){
                struct3D* fused = new struct3D(fuse(seq,resSSSR[j]->sequence),UnDefined,cubeCenter);
                res.push_back(fused);
                delete resSSSR[j];
            }
        }
    }
    {
        string seq = string("S");
        int cubeBorder = lattice::idFromPosisition(-1+ XWidth/2, -1+ YWidth/2, -1+ ZWidth/2);
        struct3D* startingTail2 = new struct3D(seq, UnDefined, cubeBorder);
        if(!excludeCube(startingTail2)){
            vector<struct3D*> resSSS = generateCubes(length - 1, startingTail2->endingPosition, Up, startingTail2);
            for(size_t j = 0; j < resSSS.size(); ++j){
                struct3D* fused = new struct3D(fuse(seq,resSSS[j]->sequence),UnDefined,cubeBorder);
                res.push_back(fused);
                delete resSSS[j];
            }

            vector<struct3D*> resSSSL = generateCubes(length - 1, startingTail2->endingPosition, Left, startingTail2);
            for(size_t j = 0; j < resSSSL.size(); ++j){
                struct3D* fused = new struct3D(fuse(seq,resSSSL[j]->sequence),UnDefined,cubeBorder);
                res.push_back(fused);
                delete resSSSL[j];
            }
        }
    }

    {
        string seq = string("SS");
        int cubeBorder = lattice::idFromPosisition(-1+ XWidth/2, -1+ YWidth/2, -1+ ZWidth/2);
        struct3D* startingTail2 = new struct3D(seq, UnDefined, cubeBorder);
        if(!excludeCube(startingTail2)){
            vector<struct3D*> resSSS = generateCubes(length - 2, startingTail2->endingPosition, Up, startingTail2);
            for(size_t j = 0; j < resSSS.size(); ++j){
                struct3D* fused = new struct3D(fuse(seq,resSSS[j]->sequence),UnDefined,cubeBorder);
                res.push_back(fused);
                delete resSSS[j];
            }

            vector<struct3D*> resSSSL = generateCubes(length - 2, startingTail2->endingPosition, Left, startingTail2);
            for(size_t j = 0; j < resSSSL.size(); ++j){
                struct3D* fused = new struct3D(fuse(seq,resSSSL[j]->sequence),UnDefined,cubeBorder);
                res.push_back(fused);
                delete resSSSL[j];
            }
        }
    }
    return res;
}

vector<struct3D*> generateCubesOneMore(int length){
    if(length < 4) return generateCubes(length);
    vector<struct3D*> smallerSize = generateCubesOneMore(length - 1);

    vector<struct3D*> res;

    size_t NbSmallerProts = smallerSize.size();
    for(size_t i = 0; i < NbSmallerProts; ++i){
        struct3D* alreadyFoldedTail = smallerSize[i];
        int newPos = alreadyFoldedTail->endingPosition;
        moveDirection absStartDir = lastAbsDirection(*alreadyFoldedTail);
        for(int d1 = 0; d1 < Nb_Moves_Relative; ++d1){ // It is relative because it starts from a Ox and Oy observer coordinates, so only enumerates relative moves (not colliding) and transforms into absolute.

            moveDirection nextAbsDir = nextAbsoluteMove(absStartDir, initialYaxis(absStartDir), (moveDirection) d1).first;
            if(nextAbsDir == UnDefined) { cerr << "ERR: inside generateCubes, couldn't find the nextAbsoluteMove(Ox=" << intToMoveChar(absStartDir) << ", Oy=" << intToMoveChar(initialYaxis(absStartDir)) << ", nextRel=" << intToMoveChar(d1) << ")" << endl; exit(-1); }
            int neighbor1 = lattice::getIdFromAbsoluteMove(newPos, nextAbsDir);

            if((!contains(alreadyFoldedTail->occupiedPositions, neighbor1)) /*&& (!contains(forbiddenVolume, neighbor1))*/){
                if(!excludeCube(neighbor1)){
                    struct3D* expandedCurrentFolding = new struct3D(*alreadyFoldedTail);
                    bool succeded = expandedCurrentFolding->pushBackAbsoluteMove(absStartDir); // fuse could also have worked
                    if(succeded && expandedCurrentFolding->properlyFolded){
                        res.push_back(expandedCurrentFolding);
                    }
                }
            }
            delete alreadyFoldedTail;
        }
    }
    //cout << "Recursive cubic foldings, size " << length << ", got " << res.size() << " sequences " << endl;
    return res;
}

vector<struct3D*> generateCubes(int length, int IDposStart, moveDirection absStartDir, struct3D* alreadyFoldedTail){
    vector<struct3D*> res;
    int newPos = lattice::getIdFromAbsoluteMove(IDposStart, absStartDir);
    if(contains(alreadyFoldedTail->occupiedPositions, newPos)) return res;
    if(excludeCube(newPos)) return res;

    if(length == 0) return res;
    if(length == 1) {
        struct3D* single = new struct3D(string(1,intToMoveChar(absStartDir)), UnDefined, IDposStart); // to make string from char, string(1,c)
        res.push_back(single);
        return res;
    }

    for(int d1 = 0; d1 < Nb_Moves_Relative; ++d1){ // It is relative because it starts from a Ox and Oy observer coordinates, so only enumerates relative moves (not colliding) and transforms into absolute.

        // there is a simpler function for that, no ?
        moveDirection nextAbsDir = nextAbsoluteMove(absStartDir, initialYaxis(absStartDir), (moveDirection) d1).first;
        if(nextAbsDir == UnDefined) { cerr << "ERR: inside generateCubes, couldn't find the nextAbsoluteMove(Ox=" << intToMoveChar(absStartDir) << ", Oy=" << intToMoveChar(initialYaxis(absStartDir)) << ", nextRel=" << intToMoveChar(d1) << ")" << endl; exit(-1); }
        int neighbor1 = lattice::getIdFromAbsoluteMove(newPos, nextAbsDir);

        if((!contains(alreadyFoldedTail->occupiedPositions, neighbor1)) /*&& (!contains(forbiddenVolume, neighbor1))*/){
            if(!excludeCube(neighbor1)){
                struct3D* expandedCurrentFolding = new struct3D(*alreadyFoldedTail);
                bool succeded = expandedCurrentFolding->pushBackAbsoluteMove(absStartDir); // fuse could also have worked
                if(succeded && expandedCurrentFolding->properlyFolded){
                    vector<struct3D*> recursive = generateCubes(length - 1, newPos, nextAbsDir, expandedCurrentFolding);
                    size_t nbS = recursive.size();
                    for(size_t i = 0; i < nbS; ++i){
                        struct3D* currentS = recursive[i];
                        if(static_cast<int>(currentS->sequence.size()) != length - 1){
                            cerr << "ERR: generateSelfFoldings(), recursive call gave prots of size " << currentS->sequence.size() << " instead of length - 1 = " << length - 1 << endl;
                        }
                        // All the proteins are supposed to be 1/ not colliding with the pre-folded structure; 2/ of good length and 3/ with proper min nr of interactions
                        // needs to be checked : - that they don't auto-fold, i.e. they don't touch the current point. Upstream resursive calls will solve previous positions
                        if((!contains(currentS->occupiedPositions, IDposStart))){
                            string newS = fuse(string(1,intToMoveChar(absStartDir)) /*expandedCurrentFolding.sequence*/, currentS->sequence);
                            res.push_back(new struct3D(newS, UnDefined, IDposStart));
                        }
                        delete currentS;
                    }
                }
            }
        }
    }
    //cout << "Recursive cubic foldings, size " << length << ", got " << res.size() << " sequences " << endl;
    return res;
}

void testCubicFoldings(){
   /* vector<struct3D*> v1 = generateCubesOneMore(10);
    cerr << "Found " << v1.size() <<  " structures" << endl;
    if(true){
       char *c[] = {(char*)"Hello",NULL};
        glDisplay(0,c);
        for(int i = 0; i < min(10000, (int) v1.size()); ++i){
            addToDisplay(v1[i], false); // need to be a new XXX to be still existing after the function ends.
        }
        glutMainLoop();
    }*/

    for(int i = 26; i < 27; ++i){
        testCubicFoldings(i,false);
    }
        //stestCubicFoldings(26,false);
    //}
}

void  testCubicFoldings(int size, bool show, string folder){

    /*Cubic foldings of size 2	2
    Cubic foldings of size 3	8
    Cubic foldings of size 4	23
    Cubic foldings of size 5	68
    Cubic foldings of size 6	183
    Cubic foldings of size 7	482
    Cubic foldings of size 8	1207
    Cubic foldings of size 9	2962
    Cubic foldings of size 10	6933
    Cubic foldings of size 11	15641
    Cubic foldings of size 12	33544
    Cubic foldings of size 13	68400
    Cubic foldings of size 14	131739
    Cubic foldings of size 15	236464*/

    //vector<struct3D*> v1 = generateCubes(int length, int IDposStart, moveDirection absStartDir, struct3D & alreadyFoldedTail);
    vector<struct3D*> v1 = generateCubes(size);
    cerr << "Cubic foldings of size " << size << "\t" << v1.size() << endl;
    if(show){
        #ifdef ALLOW_GRAPHICS
        //char *c[] = {(char*)"Hello",NULL};
        glDisplay(); //(0,c);
        for(int i = 0; i < min(10000, (int) v1.size()); ++i){
            addToDisplay(v1[i], false); // need to be a new XXX to be still existing after the function ends.
        }
        glutMainLoop();
        #endif
    } else {
        stringstream res;
        for(size_t i = 0; i < v1.size(); ++i){
            res << v1[i]->startingPosition << "\t" << v1[i]->sequence << endl;
            delete v1[i];
        }
        stringstream fname;
        if(folder.size() > 0){
            char lastFolderChar = folder[folder.size() - 1];
            if(lastFolderChar != '/') folder.append(string("/"));
        }
        fname << folder << "cubicStructuresL" << size << ".txt";
        ofstream f(fname.str());
        f << res.str();
        f.close();
    }
}





void readCubes(string fileCompact){
    cout << "Opening " << fileCompact << endl;
    cout << "Writing the non-redundant structures inside file nonRed" << fileCompact << endl;
    ifstream f(fileCompact);
    if(!f) {cerr << "File not found " << fileCompact << endl; return;}
    string newFile = addBeforeFileType(fileCompact, string("NonRedundant"));
    ofstream f2(newFile);
    if(!f2){cerr << "Could not write into " << newFile << endl; return;}

    string structure;
    set<string> nonredundant;
    int startPos;
    while((f >> startPos)){
        f >> structure;
        //cout << structure  << endl;
        //struct3D s(structure, UnDefined, lattice::centralPosition());
        string normed = normalizeAbsolute(structure);
        // 1/ the same structure should not happen twice.
        if(nonredundant.find(normed) != nonredundant.end()){
            cout << "ERR: the file has multiple times the same structure upon rotation" << endl;
        }
        string revNormed = normalizeAbsolute(revert(structure));
        if(nonredundant.find(revNormed) != nonredundant.end()){
            //cout << "Structure " << structure << "(norm" << normed << ") has already reciprocal struct " << revNormed << endl;
        }
        else {
            nonredundant.insert(normed);
            struct3D s = struct3D(structure);
            string codeInteractions = codeSelfInteractions(s);
            f2 << startPos << "\t" << structure << "\t" << codeInteractions << endl;
        }
    }
    f.close();
    f2.close();
    cout << "Non Redundant size:" << nonredundant.size() << endl;
}

// Be careful that a face is viewed only from one side, and rotation keeps this side.
// the same face seen from the other side is a symmetry and is completely different.
vector<string> rotateFace(string f){
    if(f.size() != 9) cerr << "ERR: rotateFace, incorrect size for face " << f << ", should be 9" << endl;
    // 0  1  2      6   3   0       2   5   8       8   7   6
    // 3  4  5      7   4   1       1   4   7       5   4   3
    // 6  7  8      8   5   2       0   3   6       2   1   0
    vector<string> all;
    all.push_back(f);
    {
    string res = string(9, '0');
    res[0] = f[6];     res[1] = f[3];       res[2] = f[0];
    res[3] = f[7];     res[4] = f[4];       res[5] = f[1];
    res[6] = f[8];     res[7] = f[5];       res[8] = f[2];
    all.push_back(res);
    }
    {
    string res = string(9, '0');
    res[0] = f[2];     res[1] = f[5];       res[2] = f[8];
    res[3] = f[1];     res[4] = f[4];       res[5] = f[7];
    res[6] = f[0];     res[7] = f[3];       res[8] = f[6];
    all.push_back(res);
    }
    {
    string res = string(9, '0');
    res[0] = f[8];     res[1] = f[7];       res[2] = f[6];
    res[3] = f[5];     res[4] = f[4];       res[5] = f[3];
    res[6] = f[2];     res[7] = f[1];       res[8] = f[0];
    all.push_back(res);
    }
    return all;
}

int p(int x,int y,int z){
    return lattice::idFromPosisition(XWidth/2 + x, YWidth/2 + y, ZWidth/2 + z);
}

char AAatPos(int posInLattice, map<int,char>& storage){
    if(storage.find(posInLattice) == storage.end()){
        cerr << "ERR: no AA could be found at position " << posInLattice << " = " << printVector(lattice::positionFromID(posInLattice)) << endl;
        return '?';
    }
    return storage[posInLattice];
}

vector<string> get6Faces(int pos, string absSeqCube, string AAseq){
    superProtein* t = new superProtein(absSeqCube, pos);
    if(excludeCube(t->structure)) {cerr << "ERR: getFaces(pos=" << pos << ", absSeq=" << absSeqCube << "), this doesn't fit in a 3x3x3 cube" << endl;}
    vector<string> res = get6Faces(t, AAseq);
    delete t;
    return res;
}

vector<string> get6Faces(superProtein* t, string AAseq){
    // note: doesn't check if inside exclude cube... be careful!

    // if a proper sequence is not given, then writes is at a 'position' inside the protein, starting from a,b,c...
    if(AAseq.size() != 27) AAseq = string("abcdefghijklmnopqrstuvwxyz?");
    AAseq[26] = 'z'+1; // which stupid char comes after z? here it will be

    // now, wants to access the AA at position X, so makes a map.
    map<int, char> posToAA = map<int, char>();
    size_t L = t->points.size();
    if(L != 27) cerr << "ERR: get6Faces, the given structures doesn't have 27 residues" << endl;
    for(size_t i = 0; i < L; ++i){
        posToAA[t->points[i].IDposition] = AAseq[i];
    }

    char p1 = AAatPos(p(-1, -1, 1), posToAA);
    char p2 = AAatPos(p(0, -1, 1), posToAA);
    char p3 = AAatPos(p(1, -1, 1), posToAA);
    char p4 = AAatPos(p(-1, -1, 0), posToAA);
    char p5 = AAatPos(p(0, -1, 0), posToAA);
    char p6 = AAatPos(p(1, -1, 0), posToAA);
    char p7 = AAatPos(p(-1, -1, -1), posToAA);
    char p8 = AAatPos(p(0, -1, -1), posToAA);
    char p9 = AAatPos(p(1, -1, -1), posToAA);

    char p11 = AAatPos(p(-1, 0, 1), posToAA);
    char p12 = AAatPos(p(0, 0, 1), posToAA);
    char p13 = AAatPos(p(1, 0, 1), posToAA);
    char p14 = AAatPos(p(-1, 0, 0), posToAA);
    //char p15 = AAatPos(p(0, 0, 0), posToAA); //not used ...
    char p16 = AAatPos(p(1, 0, 0), posToAA);
    char p17 = AAatPos(p(-1, 0, -1), posToAA);
    char p18 = AAatPos(p(0, 0, -1), posToAA);
    char p19 = AAatPos(p(1, 0, -1), posToAA);

    char p21 = AAatPos(p(-1, 1, 1), posToAA);
    char p22 = AAatPos(p(0, 1, 1), posToAA);
    char p23 = AAatPos(p(1, 1, 1), posToAA);
    char p24 = AAatPos(p(-1, 1, 0), posToAA);
    char p25 = AAatPos(p(0, 1, 0), posToAA);
    char p26 = AAatPos(p(1, 1, 0), posToAA);
    char p27 = AAatPos(p(-1, 1, -1), posToAA);
    char p28 = AAatPos(p(0, 1, -1), posToAA);
    char p29 = AAatPos(p(1, 1, -1), posToAA);

    // The faces have to be seen from the outside, very important !!
    vector<string> all;
    { // from face
        string res = string(9, '0');
        res[0] = p1;     res[1] = p2;       res[2] = p3;
        res[3] = p4;     res[4] = p5;       res[5] = p6;
        res[6] = p7;     res[7] = p8;       res[8] = p9;
        all.push_back(res);
    }
    { // from right side
        string res = string(9, '0');
        res[0] = p3;     res[1] = p13;       res[2] = p23;
        res[3] = p6;     res[4] = p16;       res[5] = p26;
        res[6] = p9;     res[7] = p19;       res[8] = p29;
        all.push_back(res);
    }
    { // from top
        string res = string(9, '0');
        res[0] = p1;     res[1] = p11;       res[2] = p21;
        res[3] = p2;     res[4] = p12;       res[5] = p22;
        res[6] = p3;     res[7] = p13;       res[8] = p23;
        all.push_back(res);
    }
    { // from back (reversed)
        string res = string(9, '0');
        res[0] = p23;     res[1] = p22;       res[2] = p21;
        res[3] = p26;     res[4] = p25;       res[5] = p24;
        res[6] = p29;     res[7] = p28;       res[8] = p27;
        all.push_back(res);
    }
    { // from left (reversed)
        string res = string(9, '0');
        res[0] = p21;     res[1] = p11;       res[2] = p1;
        res[3] = p24;     res[4] = p14;       res[5] = p4;
        res[6] = p27;     res[7] = p17;       res[8] = p7;
        all.push_back(res);
    }
    { // from under (reversed)
        string res = string(9, '0');
        res[0] = p7;     res[1] = p8;       res[2] = p9;
        res[3] = p17;     res[4] = p18;       res[5] = p19;
        res[6] = p27;     res[7] = p28;       res[8] = p29;
        all.push_back(res);
    }

    return all;
}

string mirrorFace(string f){
    string res = string(9, '0');
    res[0] = f[2];     res[1] = f[1];       res[2] = f[0];
    res[3] = f[5];     res[4] = f[4];       res[5] = f[3];
    res[6] = f[8];     res[7] = f[7];       res[8] = f[6];
    return res;
}

void testFaces(){
    // function tested:
    //vector<string> get6Faces(int pos, string absSeqCube, string AAseq);
    //vector<string> rotateFace(string f);

    #ifdef ALLOW_GRAPHICS
    superProtein* s1 = new superProtein(string("SUUSULLSULLRUSRSRRLLUSLUDD"), 129056);
    superProtein* s2 = new superProtein(string("SLLRRSUURSRDDRLLDDSDLSDLSR"), 128991);
    glDisplay();
    addToDisplay(s1, false);
    addToDisplay(s2, false);
    #endif

    cout << "6 main faces for 129056, SUUSULLSULLRUSRSRRLLUSLUDD" << endl;
    vector<string> face6a = get6Faces(129056, string("SUUSULLSULLRUSRSRRLLUSLUDD"));
    cout << printVector(face6a) << endl;

    cout << "6 main faces for 128991, SLLRRSUURSRDDRLLDDSDLSDLSR" << endl;
    vector<string> face6b = get6Faces(128991, string("SLLRRSUURSRDDRLLDDSDLSDLSR"));
    cout << printVector(face6b) << endl;

    /* For this structure, the 3D aas are at the following positions:
     *      w  x  m
     *    v  o  n
     *  u  p  q
     *
     *      h  y  l
     *    i  j  k
     *  t  s  r
     *
     *      g  z  {
     *    f  c  b
     *  e  d  a
     * */

    cout << "Now rotating the first of them" << endl;
    if(face6b.size() > 0){
        cout << printVector(rotateFace(face6b[0])) << endl;
    }

    cout << "Mirror of the first face" << endl;
    cout << mirrorFace(face6b[0]) << endl;


    #ifdef ALLOW_GRAPHICS
    glutMainLoop();
    #endif
}


///// ------------------- Functions to get the best structure and the affinities -----------------------
///// these functions will have the same structure as for the 'free lattice ligands'

// do the list of inside affinities thingy and store it.
vector<string> listCubicInteractions;
vector<string> listCubicStructures;
vector<int> listCubicStartPositions;
bool cubicLoaded = false;
void loadCubicInteractions(string fileCompact){
    cout << "Opening cubic structures and interactions from file " << fileCompact << endl;
    string foundFile = "";
    ifstream f1(fileCompact);
    if(!f1) {
        if(fileCompact.size() > 0){
            cerr << "ERR: File not found " << fileCompact << endl;
            cerr << "try to load from: cubicStructuresL26.txt directly in the application folder" << endl;
        }
        ifstream f2("cubicStructuresL26NonRedundant.txt");
        if(!f2){
            cerr << "Hey ! - Looks like a first run ! -" << endl;
            cerr << "Couldn't find the list of 3x3x3 cubic foldings. That's fine, will generate them !" << endl;
            cerr << "Be prepared, it takes quite some time and a few gigabytes of memory ..." << endl;
            cerr << "If you want to avoid it, try to find the file cubicStructuresL26NonRedundant.txt" << endl;
            cerr << "it should be inside the xiv folder. Alternately, you can download it from the Gitlab." << endl;
            cerr << "you can put this file into the folder where the executable is generated," << endl;
            cerr << "or specifiy inside foldedCubic.cpp where to load the file (loadCubicInteractions(file))" << endl;
            testCubicFoldings(26, false); // will create cubicStructuresL26.txt
            readCubes(string("cubicStructuresL26.txt")); // will create the nonredundantfile
        } else {
            f2.close();
        }
        foundFile = string("cubicStructuresL26NonRedundant.txt");
    } else {
        foundFile = fileCompact;
        f1.close();
    }

    ifstream f(foundFile);
    if(!f){
        cerr << "ERR: loadCubicInteractions, giving up, could not open cubic folding structures file, sorry !" << endl;
        return;
    }

    int cpt = 0;
    int startPos;
    string structure;
    string interactions;
    while((f >> startPos)){
        f >> structure;
        f >> interactions;
        listCubicStartPositions.push_back(startPos);
        listCubicStructures.push_back(structure); // I don't do .reserve() because anyway this function is called only once. no need for optimizations here
        listCubicInteractions.push_back(interactions);
        cpt++;
    }
    cubicLoaded = true;
    cout << "   ... " << cpt << " structures read." << endl;
}

int codeStartPos(char c){
    if(c == '1') return 129056;
    else return 128991;
}
char codeStartPosC(int pos){
    if(pos == 129056) return '1';
    if(pos == 128991) return '2';
    else cerr << "Unknown start pos " << pos << endl;
    return '?';
}

vector<std::pair<int, string>> unCompact(string listStr){
    vector<std::pair<int, string> > res;
    int NStr = listStr.size() / 27;
    if((NStr * 27) != (int) listStr.size()) cerr << "ERR: getBestCubicStructures, the memorized list of structures should be a multiple of 27 in size (" << listStr << ")" << endl;
    for(int i = 0; i < NStr; ++i){
        res.push_back(std::pair<int, string>(codeStartPos(listStr[i*27]), listStr.substr(i*27+1, 26)));
        //cerr << "Test stupid start " << codeStartPos(listStr[i*27]) << endl;
    }
    return res;
}

//int code (char c1, char c2){
//    return 256*(i) + j;
//}


// this is the time-consuming function here !!
vector<std::pair<int, string>> getBestCubicStructures(string AAsequence){
    if(!cubicLoaded) cerr << "ERR: getBestCubicStructures, the list of cubic structures or interactions has not been loaded." << endl;
    // put the option that if you give empty AAseq, then you print the map... and possibility to load from a file as well.

    // here, memory of previously called structures !!
    static map<string, string> bestKnownStructures; // will add the structures on top of eachother in a common string.
    if(bestKnownStructures.find(AAsequence) != bestKnownStructures.end()){
        return unCompact(bestKnownStructures[AAsequence]);
    }

    int NS = listCubicInteractions.size();
    if((int) listCubicStructures.size() != NS) cerr << "ERR: non-matching size between listCubicStructures and interactions" << endl;

    // preconputes the affinity of each possible interaction (couple of pos1-pos2) according to the given AA sequence.
    map<int, double> affSingleInteractions; // to learn once per sequence, and will be applied to all interaction codes
    affSingleInteractions.clear();
    for(int i = 0; i < 27; ++i){
        // cases of interaction between positions inside the receptor (easier)
        for(int j = 0; j < 27; ++j){
            int singleCode =  256*i + j;
            double val = AAaffinity(AA_ID(AAsequence[i]), AA_ID(AAsequence[j]));
            affSingleInteractions.insert(pair<int, double>(singleCode, val)); // now, only interaction affinities.
            //cout << (char) ('a' + i) << (char) ('a' + j) << "->" << receptorAASeq[i] << receptorAASeq[j] << " aff=" << val << endl;
        }
    }

    // Paralellize here ! and make a merge function! or make a common file to store structures accessible during sim by other simulations
    // put a timer here !
    double bestEnergy = 1e9; // or whatever ...
    string bestStructures = string();
    for(int i = 0; i < NS; ++i){

        string interactions = listCubicInteractions[i];
        double energy = 0;       // get affinity from this list of (inside) interactions !

        int ICS = interactions.size();
        if(((ICS / 2) * 2) != ICS) cerr << "ERR: cubicAffinities, non-even interaction code " << interactions << endl;
        for(int j = 0; j < ICS; j = j + 2){
            int singleCode = 256*(interactions[j] - 'a') + (interactions[j+1] - 'a');
            std::map<int, double>::iterator it = (affSingleInteractions.find(singleCode));
            if (it != affSingleInteractions.end()){ // found
                energy += it->second; // a..z
            } else {
                cerr << interactions[j] << interactions[j+1] << "(code " << singleCode << "), single interaction code not found [3]" << endl;
            }
        }

        if(energy < bestEnergy){
            bestStructures.clear();
            bestStructures.append(string(1,codeStartPosC(listCubicStartPositions[i])));
            bestStructures.append(listCubicStructures[i]);
            bestEnergy = energy;
        }
        if(fabs(energy - bestEnergy) < 1e-10){ // this might not work due to double error calculations.
            bestStructures.append(string(1,codeStartPosC(listCubicStartPositions[i])));
            bestStructures.append(listCubicStructures[i]); // note: we might need to remember the starting position in space as well, shit!!!
        }
    }
    bestKnownStructures[AAsequence] = bestStructures;
    return unCompact(bestStructures);
}

double cubicEnergy(superProtein *s1, string AAseq1, superProtein *s2, string AAseq2);

double energyToAffinity(double e){
    return std::exp((e + 30));
}
double affinityToEnergy(double a){
    return - 30 + std::log(a);
}

// memorizes the ligand !!
double cubicAffinity(string AAseq1, string AAseq2){
    vector<std::pair<int, string> > bestStructs1 = getBestCubicStructures(AAseq1);
    vector<std::pair<int, string> > bestStructs2 = getBestCubicStructures(AAseq2);
    cout << "cubicAffinity(" << AAseq1 << " , " << AAseq2 << "), got " << bestStructs1.size() << " x " << bestStructs2.size() << " folded structures" << endl;
    double energy = 1e9;
    for(unsigned int i = 0; i < bestStructs1.size(); ++i){
        for(unsigned int j = 0; j < bestStructs2.size() ; ++j){
            superProtein* s1 = new superProtein(bestStructs1[i].second, bestStructs1[i].first);
            superProtein* s2 = new superProtein(bestStructs2[j].second, bestStructs2[j].first);
            if(excludeCube(s1->structure)) cerr << "S1 Not a cube !!" << endl;
            if(excludeCube(s2->structure)) cerr << "S2 Not a cube !!" << endl;
            double bindEnergy = cubicEnergy(s1, AAseq1, s2, AAseq2);
            energy = min(energy, bindEnergy);
            //cout << "   ... for opt1=" << bestStructs1[i].second << " and opt2=" << bestStructs2[j].second << ", got energy " << energy << endl;
            delete s1;
            delete s2;
        }
    }
    return energyToAffinity(energy);
}



// can be optimized by manually putting all sizes to 27 etc
double cubicEnergy(superProtein* s1, string AAseq1, superProtein* s2, string AAseq2){
    int cpt = 0;
    double bestEnergy = 1e9;
    // for this one, do not rotate,
    vector<string> facesS1 = get6Faces(s1, AAseq1); // the faces will contain the good AAs directly.
    for(unsigned int i0 = 0; i0 < facesS1.size(); ++i0){
        string face1 = mirrorFace(facesS1[i0]);

        // for the second one, will try all rotations.
        vector<string> facesS2 = get6Faces(s2, AAseq2);
        int L = facesS2.size(); // should be 6
        for(int i = 0; i < L; ++i){
            vector<string> rots = rotateFace(facesS2[i]);
            for(unsigned int j = 0; j < rots.size(); ++j){
                string face2 = rots[j];
                if(face1.size() != face2.size()) cerr << "ERR: non matching size of two faces" << endl;
                double energy = 0;
                for(unsigned int k = 0; k < face1.size(); ++k){
                    energy += AAaffinity(AA_ID(face1[k]), AA_ID(face2[k]));
                }
                bestEnergy = min(bestEnergy, energy);
                cpt++;
            }
        }
    }
    //cout << "tried " << cpt << " rotations" << endl;
    return bestEnergy;
}

void testCubicAffinities(){
    // todo: regenerate the files if they don't exist
    loadCubicInteractions("../xiv/cubicStructuresL26NonRedundant.txt");
    string AAsequence = "ALAGNRWXTYZKDUAAWXYUIAMXYWU";
    checkAAseq(AAsequence);
    AAsequence = string("ALAGNRWGTYWKDNAAWNYTIAMLYWL");
    checkAAseq(AAsequence);
    vector<std::pair<int, string>> res1 = getBestCubicStructures(AAsequence);
    cout << "Finding the best cubic sequences for AAseq=" << AAsequence << endl;
    for(unsigned int i = 0; i < res1.size(); ++i){
        cout << res1[i].first << "," << res1[i].second << endl;
    }
    {
        double aff = cubicAffinity(AAsequence, AAsequence);
        cout << "Affinity between " << AAsequence << " to itself: " << aff << " (i.e. energy " << affinityToEnergy(aff) << ")" << endl;
    }

    // this takes 20 seconds on my computer(Core i7, use one core only) => 5 affinity computations per second. Yeaah !!
    for(int i = 0; i < 100; ++i){
        string seq1 = randomProt(27);
        string seq2 = randomProt(27);
        double aff = cubicAffinity(seq1, seq2);
        cout << "Affinity between " << seq1 << " to " << seq2 << " = " << aff << " (i.e. energy " << affinityToEnergy(aff) << ")" << endl;
    }
}





