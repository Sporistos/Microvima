#ifndef CUBES_H
#define CUBES_H

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <set>
#include <iterator>

#include "../Ymir/proteins.h"
#include "../Ymir/receptorligand.h"
using namespace std;

vector<struct3D*> generateCubes(int length, int IDposStart, moveDirection absStartDir, struct3D* alreadyFoldedTail);
vector<struct3D*> generateCubes(int length);
void readCubes(string fileCompact);
void testCubicFoldings(int size, bool show, string folder = string(""));
void testCubicFoldings();

vector<string> get6Faces(int pos, string absSeqCube, string AAseq = string(""));
vector<string> get6Faces(superProtein *t, string AAseq = string(""));
vector<string> rotateFace(string f);
string mirrorFace(string f);
void testFaces();

void loadCubicInteractions(string fname = string("")); // without fname, will look directly in application folder or generate the file
vector<std::pair<int, string> > getBestCubicStructures(string AAsequence); // position, structure.
double cubicAffinity(string AAseq1, string AAseq2);
double cubicEnergy(struct3D* s1, string AAseq1, struct3D* s2, string AAseq2);
void testCubicAffinities();
#endif
