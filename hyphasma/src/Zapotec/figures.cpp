#include "../Ymir/fastaffinity.h"
#include "figures.h"
#include "../Tools/stopwatch.h"
#include "../Tools/zaprandom.h"
#include "../Tools/distribution.h"

using namespace std;

void Figure1(int ligand, int receptorSize){

    precise_stopwatch stopwatch; // careful, doesnt work for more than 2 hours
    int minInteract = 4;
    double KT = 1;
    affinityOneLigand* T1 = NULL;
    switch(ligand){
        case 1: {
            string simpleAccessible = string("UDRLLRLLRLLR");
            string AAsimple = string("AAAAAAAAAAAAA");
            T1 = new affinityOneLigand(simpleAccessible, AAsimple, -1, receptorSize, minInteract, -1, KT);
            //displayLigand("UDRLLRLLRLLR", -1);
            break;
        }
        case 2: {
            set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(30,33,33)).occupiedPositions;
            vector<int> blockV = vector<int>(1, -2); // -2 inserts the flat square
            std::copy(block.begin(), block.end(), std::back_inserter(blockV));
            T1 = new affinityOneLigand("RRUUSURRDSSRSRSSDDSSRS", "GAKLPLYLIVGAKLPCYLIVLYD", lattice::idFromPosisition(30,32,33), receptorSize, minInteract, -1, KT, blockV);
            //displayLigand("RRUUSURRDSSRSRSSDDSSRS", lattice::idFromPosisition(30,32,33), blockV);
            break;
        }
        case 3: {
            vector<int> blockV = vector<int>(1, -2); // -2 inserts the flat square
            set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(31,34,33)).occupiedPositions;
            std::copy(block.begin(), block.end(), std::back_inserter(blockV));
            set<int> block2 = struct3D("RSSSUUSSS", UnDefined, lattice::idFromPosisition(36,33,33)).occupiedPositions;
            std::copy(block2.begin(), block2.end(), std::back_inserter(blockV));
            T1 = new affinityOneLigand("DRRDSRSRSDSDSRSRRLLRSSRSSRSSDDSSRSSRS", "HILKGALYVPPVYHILVKSPLIVIAAKLGRALYPNVGR", lattice::idFromPosisition(32,31,35), receptorSize, minInteract, -1, KT, blockV);
            //displayLigand("DRRDSRSRSDSDSRSRRLLRSSRSSRSSDDSSRSSRS", lattice::idFromPosisition(32,31,35), blockV);
            break;
        }
        default: {
            cerr << "Figure1(), please chose ligand 1, 2 or 3" << endl;
            return;
        }
    }

    unsigned int actual_wait_time = stopwatch.elapsed_time<unsigned int, std::chrono::microseconds>();
    cerr << actual_wait_time / 1000. << "ms Elapsed to compute structures" << endl;

    //cout << "Computing 1000 affinities for ligand " << simpleAccessible << " (" << AAsimple << "), receptors " << receptorSize << " minI=4" << endl;

    double vmin = 0;
    for(int i = 0; i < 1000; ++i){
        string Px = randomProt(receptorSize+1);
        std::pair<double, double> res = T1->affinity(Px);
        vmin = min(vmin, res.first);
        //cout << "affinity(" << Px << ") = \t" << res.first << "\t" << res.second << endl;
    }
    unsigned int actual_wait_time2 = stopwatch.elapsed_time<unsigned int, std::chrono::microseconds>();
    cerr << actual_wait_time2 / 1000.<< " ms Elapsed to compute 1000 affinities" << endl;

    cout << "Best Aff(type Best) so far:" << vmin << endl;

    for(int i = 0; i < 1; ++i){
        string Px = randomProt(receptorSize+1);
        cout << "Details of the structures and affinities" << endl; // for " << simpleAccessible << " (Ag=" << AAsimple << ", BCR=" << Px << "), receptors " << receptorSize << " minI=4" << endl;
        std::pair<double, double> res = T1->affinity(Px, true);
        cout << "affinity(" << Px << ") = \t" << res.first << "\t" << res.second << endl;
    }
}

// mask = 0 or 1s, 1 means conserved: do not mutate
string mutate(string s, string mask = string()){
    if(mask.size() == 0) mask = string(s.size(), '0');
    int cpt = 0;
    while(cpt < 100){
        char test = randomAA();
        int position = random::uniformInteger(0,s.size()-1);
        if ((position >= (int) s.size()) || (position < 0)) {
           cerr << "Random does shit, sequencespace.cpp::mutateOnePosition";
           return string(); // fail
        }
        if((mask[position] == '0') && (test != s[position])){
             s[position] = test;
             return s; // success
        }
        cpt++;
    }
    cerr << "ERR: foldedFree::mutateOnePosition, got trouble to mutate an AA" << endl;
    return s; // silent
}

void Figure2(int ligand, int receptorSize){
    //ligand = 2;

    precise_stopwatch stopwatch; // careful, doesnt work for more than 2 hours
    int minInteract = 4;
    string agSeq;
    string agStruct;
    double KT = 1.0;
    vector<int> blockV;

    affinityOneLigand* T1 = NULL;
    switch(ligand){
        case 1: {
            agStruct = string("UDRLLRLLRLLR");
            agSeq = string("AAAAAAAAAAAAA");
            T1 = new affinityOneLigand(agStruct, agSeq, -1, receptorSize, minInteract, -1, KT);
            //displayLigand(agStruct, -1);
            break;
        }
        case 2: {
            set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(30,33,33)).occupiedPositions;
            blockV = vector<int>(1, -2); // -2 inserts the flat square
            std::copy(block.begin(), block.end(), std::back_inserter(blockV));
            agStruct = string("RRUUSURRDSSRSRSSDDSSRS");
            agSeq = string("GAKLPLYLIVGAKLPCYLIVLYD");
            T1 = new affinityOneLigand(agStruct, agSeq, lattice::idFromPosisition(30,32,33), receptorSize, minInteract, -1, KT, blockV);
            //displayLigand(agStruct, lattice::idFromPosisition(30,32,33), blockV);
            break;
        }
        case 3: {
            blockV = vector<int>(1, -2); // -2 inserts the flat square
            set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(31,34,33)).occupiedPositions;
            std::copy(block.begin(), block.end(), std::back_inserter(blockV));
            set<int> block2 = struct3D("RSSSUUSSS", UnDefined, lattice::idFromPosisition(36,33,33)).occupiedPositions;
            std::copy(block2.begin(), block2.end(), std::back_inserter(blockV));
            agStruct = string("DRRDSRSRSDSDSRSRRLLRSSRSSRSSDDSSRSSRS");
            agSeq = string("HILKGALYVPPVYHILVKSPLIVIAAKLGRALYPNVGR");
            T1 = new affinityOneLigand(agStruct, agSeq, lattice::idFromPosisition(32,31,35), receptorSize, minInteract, -1, KT, blockV);
            //displayLigand(agStruct, lattice::idFromPosisition(32,31,35), blockV);
            break;
        }
        case 4: {
            agStruct = string("SRLLRLLRLLRDURRLRRSLLSRRLRRLLRRLRRSLLSRRLRR");
            agSeq = string(   "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            //T1 = new affinityOneLigand(agStruct, agSeq, lattice::idFromPosisition(32,32,34), receptorSize, minInteract, -1, KT);
            displayLigand("SRLLRLLRLLRDURRLRRSLLSRRLRRLLRRLRRSLLSRRLRR", lattice::idFromPosisition(32,32,34));
            break;
        }
        case 5: {
            agStruct = string("SRLLRLLRLLRDURRLRRSLLSRRLRRLLRRLRRSLLSRRLRR");
            agSeq = string(   "HILKGALYVPPVYHILVKSPLIVIAAKLGRALYPNVGRRVAPHG");
            T1 = new affinityOneLigand(agStruct, agSeq, lattice::idFromPosisition(32,32,34), receptorSize, minInteract, -1, KT);
            //displayLigand("SRLLRLLRLLRDURRLRRSLLSRRLRRLLRRLRRSLLSRRLRR", lattice::idFromPosisition(32,32,34));
            break;
        }
            // ligand 3 but without open loop
        case 6: {
            blockV = vector<int>(1, -2); // -2 inserts the flat square
            set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(30,34,33)).occupiedPositions;
            std::copy(block.begin(), block.end(), std::back_inserter(blockV));
            set<int> block2 = struct3D("RSSSUUSSS", UnDefined, lattice::idFromPosisition(34,33,33)).occupiedPositions;
            std::copy(block2.begin(), block2.end(), std::back_inserter(blockV));
            agStruct = string("SDRRDSRSRSDSDSRSRRLLRSSRSSRSSDDSSRSSRS");
            agSeq = string("PHILKGALYVPPVYHILVKSPLIVIAAKLGRALYPNVGR");
            T1 = new affinityOneLigand(agStruct, agSeq, lattice::idFromPosisition(31,31,35), receptorSize, minInteract, -1, KT, blockV);
            //displayLigand(agStruct, lattice::idFromPosisition(32,31,35), blockV);
            break;
        }
            // ligand 3but only the pocket side, same AAs. the pocket can be touched opposite way
        case 7: {
            blockV = vector<int>(1, -2); // -2 inserts the flat square
            set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(30,34,33)).occupiedPositions;
            std::copy(block.begin(), block.end(), std::back_inserter(blockV));
            set<int> block2 = struct3D("RSSSUUSSS", UnDefined, lattice::idFromPosisition(34,33,33)).occupiedPositions;
            std::copy(block2.begin(), block2.end(), std::back_inserter(blockV));
            agStruct = string("SDRRDSRSRSDSDSRSRRLL");
            agSeq = string(   "PHILKGALYVPPVYHILVKSP");
            T1 = new affinityOneLigand(agStruct, agSeq, lattice::idFromPosisition(31,31,35), receptorSize, minInteract, -1, KT, blockV);
            //displayLigand(agStruct, lattice::idFromPosisition(32,31,35), blockV);
            break;
        }
        default: {
            cerr << "Figure1(), please chose ligand 1, 2 or 3" << endl;
            return;
        }
    }
    unsigned int actual_wait_time = stopwatch.elapsed_time<unsigned int, std::chrono::microseconds>();
    cerr << actual_wait_time / 1000. << "ms Elapsed to prepare structures" << endl;

    // 1 - Distribution of ENERGIES (best & stats) for those ligands

    if(0){
        int nbAffinities = 10000;
        ministat a = ministat();
        vector<double> valuesBest;
        vector<double> valuesStat;
        double vmin = 0;
        for(int i = 0; i < nbAffinities; ++i){
            string Px = randomProt(receptorSize+1);
            std::pair<double, double> res = T1->affinity(Px);
            vmin = min(vmin, res.first);
            a.add(res.first);
            valuesBest.push_back(res.first);
            valuesStat.push_back(res.second);
            if((i % 100) == 0) cout << "   ... " << i/(nbAffinities/100) << " % done" << endl;
            //cout << "affinity(" << Px << ") = \t" << res.first << "\t" << res.second << endl;
        }
        vector<double> intBoundaries;
        for(double d = -100; d < -9; ++d){
            intBoundaries.push_back(d-0.5);
        }
        histogramFromDistrib h1(valuesBest, intBoundaries);
        cout << h1.print(true) << endl;
        histogramFromDistrib h2(valuesStat, intBoundaries);
        cout << h2.print(true) << endl;

        unsigned int actual_wait_time2 = stopwatch.elapsed_time<unsigned int, std::chrono::microseconds>();
        cerr << actual_wait_time2 / 1000.<< " ms Elapsed to compute " << nbAffinities << " affinities" << endl;
    }

    // 1b - Distribution of energy of structures for 10 BCRs


    if(0){

        vector<string> specialBCRs;
        if(ligand == 1){
            specialBCRs = {"IPLCLVAEV", "WFIGTKAEI", "SKARFVVQW", "SFCFEKMHR", "IAMYEEHQA", "PYMDDMKHQ", "GSNKDKTSK", "WTKKKRSKQ"};
        }

        if(ligand == 2){
            specialBCRs = {"TLFLFWPLW", "WFVYCIIQV", "WHVMQWVGV", "FFHSTGGLA", "YDDGKHDES", "SRSANDKDK"};
        }
        for(int i = 0; i < 6; ++i){
            string Px = randomProt(receptorSize+1);
            if(i < (int) specialBCRs.size()) Px = specialBCRs[i];
            //cout << "Details of the structures and affinities" << endl; // for " << simpleAccessible << " (Ag=" << AAsimple << ", BCR=" << Px << "), receptors " << receptorSize << " minI=4" << endl;
            std::pair<double, double> res = T1->affinity(Px, true);
            cout << "..." << Px << ", best= " << res.first << ", stat= " << res.second << endl;
            //if(res.first < -35){
            //    std::pair<double, double> res2 = T1->affinity(Px, true);
            //}
        }
    }


    // 2 - Impact of KT on statistical energy between BCRs

    if(0){
        vector<double> KTs_test = {0.001, 0.01, 0.05, 0.1, 0.2, 0.5, 0.8, 1.0, 2.0, 5.0, 10.0, 50, 1000};
        int nbAffinities = 1000;
        for(unsigned k = 0; k < KTs_test.size(); ++k){
            cout << " ========= Test affinities with KT = " << KTs_test[k] << " ===========" << endl;
            T1->KT = KTs_test[k];
            ministat a = ministat();
            vector<double> valuesBest;
            vector<double> valuesStat;
            double vmin = 0;
            for(int i = 0; i < nbAffinities; ++i){
                string Px = randomProt(receptorSize+1);
                std::pair<double, double> res = T1->affinity(Px);
                vmin = min(vmin, res.first);
                a.add(res.first);
                cout << res.second << "/t";
                valuesBest.push_back(res.first);
                valuesStat.push_back(res.second);
                if((i % 100) == 0) cout << "   ... " << i/(nbAffinities/100) << " % done" << endl;
                //cout << "affinity(" << Px << ") = \t" << res.first << "\t" << res.second << endl;
            }
            vector<double> intBoundaries;
            for(double d = -100; d < -9; ++d){
                intBoundaries.push_back(d-0.5);
            }
            histogramFromDistrib h1(valuesBest, intBoundaries);
            cout << h1.print(true) << endl;
            histogramFromDistrib h2(valuesStat, intBoundaries);
            cout << h2.print(true) << endl;

            unsigned int actual_wait_time2 = stopwatch.elapsed_time<unsigned int, std::chrono::microseconds>();
            cerr << actual_wait_time2 / 1000.<< " ms Elapsed to compute " << nbAffinities << " affinities" << endl;
        }
    }

    //3 - Hist of mutations from receptors of different affinities


    if(0){

        vector<string> specialBCRs;
        if(ligand == 1){
            specialBCRs = {"IPLCLVAEV", "WFIGTKAEI", "SKARFVVQW", "SFCFEKMHR", "IAMYEEHQA", "PYMDDMKHQ", "GSNKDKTSK", "WTKKKRSKQ"};
        }

        if(ligand == 2){
            specialBCRs = {"TLFLFWPLW", "WFVYCIIQV", "WHVMQWVGV", "FFHSTGGLA", "YDDGKHDES", "SRSANDKDK"};
        }

        if(ligand == 6){
            specialBCRs = {"VFCLFLIFL", "FLLFMFLWL", "WLCLLFVIL", "ALCLLFIIV", "VFHLFIIFL", "RVSLQILFL",  "LWTIMFTVF",  "MFNVPFVPF",  "RYLRLDQPE",  "RVASHLGNN",  "QGGYNHDST", "QSRKNAQVK"};
        }
        for(unsigned int i = 0; i < specialBCRs.size(); ++i){
            string Px = randomProt(receptorSize+1);
            if(i < specialBCRs.size()) Px = specialBCRs[i];
            std::pair<double, double> res = T1->affinity(Px, false);
            cout << "..." << Px << ", best= " << res.first << ", stat= " << res.second << endl;

            int nbAffinities = 1000;
            ministat a = ministat();
            vector<double> valuesBest;
            vector<double> valuesStat;
            double vmin = 0;
            for(int j = 0; j < nbAffinities; ++j){
                string Py = mutate(Px);
                std::pair<double, double> res = T1->affinity(Py);
                vmin = min(vmin, res.first);
                a.add(res.first);
                cout << Py << "=" << res.first << "\t";
                valuesBest.push_back(res.first);
                valuesStat.push_back(res.second);
                //if((i % 100) == 0) cerr << "   ... " << i/(nbAffinities/100) << " % done" << endl;
                //cout << "affinity(" << Px << ") = \t" << res.first << "\t" << res.second << endl;
            }
            vector<double> intBoundaries;
            for(double d = -110; d < -29; ++d){
                intBoundaries.push_back(d-0.5);
            }
            histogramFromDistrib h1(valuesBest, intBoundaries);
            cout << h1.print(true) << endl;
            //histogramFromDistrib h2(valuesStat, intBoundaries);
            //cout << h2.print(true) << endl;
        }
    }

    if(1){

        int nMutations = 8;
        string agSeqMut = "PHILKGALYVPPVYHILVKSPLIVIAAKLGRALYPNVGR"; //agSeq;
                         //123456789012345678901234567890123456789
        string mask     = "010010000010100000011111111111011111111";
        //agSeqMut[38] = 'Q';

        for(int i = 0; i < nMutations; ++i){
            agSeqMut = mutate(agSeqMut, mask);
        }

        agSeqMut = randomProt(agSeq.size());
        affinityOneLigand* T2 = new affinityOneLigand(agStruct, agSeqMut, lattice::idFromPosisition(31,31,35), receptorSize, minInteract, -1, KT, blockV);

        int nbEgal = 0;
        int nbAffinities = 1000;
        vector<double> valuesBest1;
        vector<double> valuesBest2;
        for(int j = 0; j < nbAffinities; ++j){
            string Px = randomProt(receptorSize+1);
            std::pair<double, double> res = T1->affinity(Px);
            std::pair<double, double> res2 = T2->affinity(Px);
            cout << Px << "=" << res.first << " " << res2.first << "\n";
            valuesBest1.push_back(res.first);
            valuesBest2.push_back(res2.first);
            if(res.first == res2.first) nbEgal++;
            if(nbEgal > 10) j = nbAffinities;
            //if((i % 100) == 0) cerr << "   ... " << i/(nbAffinities/100) << " % done" << endl;
            //cout << "affinity(" << Px << ") = \t" << res.first << "\t" << res.second << endl;
        }
    }

        //        int nbAffinities = 1000;
        //        T1->KT = 1;
        //        ministat a = ministat();
//        vector<double> valuesBest;
//        vector<double> valuesStat;
//        double vmin = 0;
//        double threshold = -80;

//        for(int i = 0; i < nbAffinities; ++i){
//            string Px = randomProt(receptorSize+1);
//            std::pair<double, double> res = T1->affinity(Px);
//            vmin = min(vmin, res.first);
//            a.add(res.first);
//            //cout << res.second << "/t";
//            //valuesBest.push_back(res.first);
//            valuesStat.push_back(res.second);
//            if((i % 100) == 0) cout << "   ... " << i/(nbAffinities/100) << " % done" << endl;
//            //cout << "affinity(" << Px << ") = \t" << res.first << "\t" << res.second << endl;
//        }
//        vector<double> intBoundaries;
//        for(double d = -100; d < -9; ++d){
//            intBoundaries.push_back(d-0.5);
//        }
//        histogramFromDistrib h1(valuesBest, intBoundaries);
//        cout << h1.print(true) << endl;
//        histogramFromDistrib h2(valuesStat, intBoundaries);
//        cout << h2.print(true) << endl;

//        unsigned int actual_wait_time2 = stopwatch.elapsed_time<unsigned int, std::chrono::microseconds>();
//        cerr << actual_wait_time2 / 1000.<< " ms Elapsed to compute " << nbAffinities << " affinities" << endl;







//    agStruct = string("SDRRDSRSRSDSDSRSRRLL");
//    agSeq = string(   "PHILKGALYVPPVYHILVKSP");

//    //double KT = 0.5;
//    //double coeffDiv = 7;
//    int LseqBCRs = receptorSize+1;       // in AAs
//    int LseqAGs = agSeq.size();
//    // foldedFree::initializeDefaultParameters(int foldedFree_typeAffinity, par.Value.foldedFree_rescale_log_affinitymax);
//    //foldedFree::initializeDefaultParameters(0, coeffDiv);
//    // ligandDatabase::initialize(LseqsBCR-1, par.Value.foldedFree_min_contacts, -1 , par.Value.foldedFree_temperature);
//    //ligandDatabase::initialize(LseqBCRs-1, 4, -1 , KT);

//    vector<bool> mask = {0,0,1,1,0,0,0,0,1,1,0,0,0,0};
//    // 137248  SSRLLRLLRLLRS	0
//    foldedFree* Antigen = new foldedFree(LseqAGs);
//    Antigen->setConservedPositions(mask);
//    Antigen->randomize();
//    Antigen->setStructure("SSRLLRLLRLLRS",137248);





//    double threshold = 0.01;
//    vector<double> KTs = {1.0}; //0.001, 0.01, 0.05, 0.1, 0.2, 0.5, 0.8, 1.0, 2.0, 3.0};
//    vector<double> Coeff = {1, 2, 4, 6, 7, 8, 9, 10};
//    for(unsigned int k = 0; k < KTs.size(); ++k){
//        for(unsigned int c = 0; c < Coeff.size(); ++c){
//            //cerr << "KT=" << KTs[k] << ", Coeff/=" << Coeff[c] << endl;
//            foldedFree::initializeDefaultParameters(0, Coeff[c]);
//            ligandDatabase::initialize(LseqBCRs-1, 4, -1, KTs[k]); // -1 is default

//            double percentSuccess = 0;
//            int cpt=0;
//            ministat a = ministat();
//            ministat mut = ministat();
//            for(int i = 0; i < 20; ++i){
////                string Px = randomProt(LseqBCRs);
//                foldedFree* newBCR = new foldedFree(LseqBCRs);
//                double res = 0;
//                while((cpt < 10000) && (res < threshold)){
//                    if((cpt % 100) == 0) cerr << "." << percentSuccess << "/20" << endl;
//                    newBCR->randomize();
//                    res = foldedFree::affinity(newBCR, Antigen);
//                    cpt++;
//                }
//                if(res > threshold) {
//                    percentSuccess++;
//                    a.add(res);
//                    double best = res;
//                    for(int j = 0; j < 10; ++j){
//                        foldedFree* newBCR2 = new foldedFree(newBCR);
//                        newBCR2->mutateOnePosition();
//                        double res2 = foldedFree::affinity(newBCR2, Antigen);
//                        best = max(res2, best);
//                        delete newBCR2;
//                    }
//                    cout << "affinity(" << newBCR->content <<  ") = \t" << res <<  " or " << - (log(res) * Coeff[c] + tempBigCoeff) << " -> " << best << " or " << - (log(best) * Coeff[c] + tempBigCoeff)  << " so *= " << best / (max(1e-10, res)) << endl;
//                    mut.add(best);
//                }
//                delete newBCR;
//            }
//            cout << "KT= " << KTs[k] << "\tCoeff= " << Coeff[c] << "%succ" << percentSuccess / (double) cpt << "\tRand " << a.sumUp() << " Improv by 5 mut:" << mut.sumUp() << endl;
//        }
//    }
//}


    //

}
