#include "foldedCubic.h"
#include <set>
#include <sstream>
#include "../Ymir/proteins.h"
#include "cubes.h"
#include "../Tools/zaprandom.h"


/// ============================ Part I: sequences and LCS-based affinity exactly according to Perelson papers =================================

bool foldedCubic::foldedCubicLoaded = false;

foldedCubic::foldedCubic(string text){
   size = text.size();
   content.resize(size, false);
   for (int i = 0; i < size; ++i){
      if(!correctAA(text[i])){
          cerr << "ERR:foldedCubic(" << text << "), un-allowed amino-acid : " << text[i] << endl;
      }
      content[i] = text[i];
   }
   conservedPositions.resize(size, false);
}

foldedCubic::foldedCubic(foldedCubic *toCopy){
   size = toCopy->size;
   content.resize(size);
   for (int i = 0; i < size; ++i){
      content[i] = toCopy->content[i];
   }
   conservedPositions = toCopy->conservedPositions;
}

foldedCubic::foldedCubic(int _size){
   if (_size < 0) {_size = 0;}
   size = _size;
   content.resize(_size, 'A');
   conservedPositions.resize(_size, false);
}

char foldedCubic::operator [](int i){
   if ((i < 0) || (i > size)){
      cerr << "foldedCubic[" << i << "] out of bounds (size=" << size << ")\n";
      return '?';
   }
   return content[i];
}

double foldedCubic::affinity(foldedCubic * receptor1, foldedCubic * receptor2)
{
    if(!foldedCubicLoaded) loadCubicInteractions("../xiv/cubicStructuresL26NonRedundant.txt");
    double aff = cubicAffinity(receptor1->content, receptor2->content);
    return aff;
    // or can also return affinityToEnergy(aff)
}

double foldedCubic::hamming(foldedCubic * x, foldedCubic * y) {
    if (x->size != y->size) {
        std::cout << "Err foldedCubic::hamming between two receptors, the length of the two receptors are not identical!";
        return -1;
    }
    int length = x->size;
    int sum = 0;
    for (int i = 0; i < length; ++i) {
        sum += ((*x)[i] != (*y)[i]) ? 1 : 0;
    }
    return sum;
}

// Meaningful functions. returns 1: success, 0: silent, -1: fail
int foldedCubic::mutateOnePosition() {
   int position = random::uniformInteger(0,size-1);
   if ((position >= size) || (position < 0)) {
      cerr << "Random does shit, foldedCubic.cpp::mutateOnePosition";
      return -1; // fail
   }
   int cpt = 0;
   while(cpt < 100){
       char test = randomAA();
       if(test != content[position]){
            content[position] = test;
            return +1; // success
       }
   }
   cerr << "ERR: foldedCubic::mutateOnePosition, got trouble to mutate an AA" << endl;
   return 0; // silent
}

void foldedCubic::setConservedPositions(std::vector<bool> _conservedPositions){
    if((int) _conservedPositions.size() != size) cerr << "ERR: foldedCubic::setConservedPositions, non-matching size " << endl;
    conservedPositions = _conservedPositions;
}

void foldedCubic::setConservedPositions(std::string _conservedPositions){
    if((int) _conservedPositions.size() != size) cerr << "ERR: foldedCubic::setConservedPositions, non-matching size " << endl;
    conservedPositions.resize(size);
    for(int i = 0; i < size; ++i){
        conservedPositions[i] = _conservedPositions[i];
    }
}

void foldedCubic::setConservedPositions(int lcon){
    conservedPositions.clear();
    conservedPositions.resize(lcon, true);
    conservedPositions.resize(size, false);
    random::shuffle(conservedPositions);
}

/*void foldedCubic::randomInsertion(int _size) {
   //int positionStart = irandom(size);
}
void foldedCubic::randomDeletion(int _size) {

}*/

void foldedCubic::randomize() {
    // No, should use the local random, not the one from Ymir !! content = randomProt(size);
}

bool foldedCubic::operator ==(foldedCubic &b) {
    if(size != b.size) return false;
    for (int i = 0; i < size; ++i) {
       if((*this)[i] != b[i]) return false;
    }
    for (int i = 0; i < size; ++i) {
       if(conservedPositions[i] != b.conservedPositions[i]) return false;
    }
    return true;
}

string foldedCubic::print() {
   stringstream out;
   for (int i = 0; i < size; ++i) {
      out << content[i];
   }
   return out.str();
}









string foldedCubic::test(){
#define MODE_OUT cerr
    MODE_OUT << "================ Test constructor / basic functions =================" << endl;
    foldedCubic S1(5);
    MODE_OUT << "S1: Empty sequence, size 5 " << S1.print() << "\n\n";
    foldedCubic S2(string("ABDBCBDABDCCADB"));
    MODE_OUT << "S2: Sequence from binary text ABDBCBDABDCCADB -> " << S2.print() << "\n\n";
    foldedCubic S3(string("AABCGSTHDY01oAX5!="));
    MODE_OUT << "S3: Sequence from text with bad characters 'AABCGSTHDY01oAX5!=' ->" << S3.print() << "\n\n";
    foldedCubic S4(&S2);
    MODE_OUT << "S4: Copy from previous sequence S2 " << S4.print() << "\n\n";
    foldedCubic S4b(15);
    S4b = S2;
    MODE_OUT << "S4: = from previous sequence S2 " << S4b.print() << "\n\n";




    MODE_OUT << "Accessing positions of S5b one by one [] ";
    MODE_OUT << S2[0] << S2[1] << S2[2] << "..." << S2[S2.size-1] << "\n\n";
    MODE_OUT << "Now testing bad positions : ";
    MODE_OUT << S2[-1] << S2[S2.size] << "\n\n";
    MODE_OUT << "S6: Randomized sequence ";
    foldedCubic S6(10);
    S6.randomize();
    MODE_OUT << S6.print() << "\n\n";
    //MODE_OUT << "S7: Randomized sequence, from an existing one ";
    //foldedCubic S7(15,257);
    //S7.randomize();
    //MODE_OUT << S7.print() << "\n\n";
    MODE_OUT << "Consecutive mutations" << endl;
    foldedCubic S8("BBBBBBBBBB");
    for(int i = 0; i < 20; ++i){
        S8.mutateOnePosition();
        MODE_OUT << S8.print() << endl;
    }
    foldedCubic S9(0);
    S9 = S8;
    MODE_OUT << "Compare equal sequences : " << S8.print() << ((S8==S9) ? "=" : "!=")  << S9.print() << "\n\n";
    S9.mutateOnePosition();
    MODE_OUT << "Compare different sequences : " << S8.print() << ((S8==S9) ? "=" : "!=")  << S9.print() << "\n\n";
    S9.randomize();
    MODE_OUT << "Random sequence: " << S9.print() << endl;

    MODE_OUT << "Test affinity functions on single examples" << "\n";
    //foldedCubic::alphabetsize = 5;
    foldedCubic S10("AAAAAAAAAA");   // reference sequence AG1
    foldedCubic S11("AABCDDCBAA");   // reference sequence AG2
    foldedCubic S21("AABBCCDDEE");   // match 4+5 to AG1, 4+3 to AG2
    foldedCubic S22("EEDDCCBBAA");   // match 1+3+3 to AG1, 1+3+3 to AG2

    /*
    MODE_OUT << affinity(&S21, &S10, 0.5, 1.0) << endl;
    MODE_OUT << printPerelsonSeqAff(&S21, &S11, 0.5, 1.0) << endl;
    MODE_OUT << printPerelsonSeqAff(&S22, &S10, 0.5, 1.0) << endl;
    MODE_OUT << printPerelsonSeqAff(&S22, &S11, 0.5, 1.0) << endl;

    MODE_OUT << "Test LCS subfunctions" << "\n";
    perelsonSequence::alphabetsize = 4;

    MODE_OUT << "Show longuest substring after consecutive mutations" << endl;
    perelsonSequence S32("BBBBBBBBBB");
    perelsonSequence S32b = S32;
    for(int i = 0; i < 20; ++i){
        S32.mutateOnePosition();
        MODE_OUT << S32.print() << " with LCS to original " << LCSsize(S32, S32b) << "-" << LCSsize(S32b, S32)  << " and hamming (mut nr) " << perelsonSequence::hamming(&S32, &S32b) << endl;
    }
    MODE_OUT << "Now with a 2-char sequence " << endl;
    perelsonSequence S33("AABBAABBAABB");
    perelsonSequence S33b = S32;
    for(int i = 0; i < 20; ++i){
        S33.mutateOnePosition();
        MODE_OUT << S33.print() << " with LCS to original " << LCSsize(S33, S33b) << "-" << LCSsize(S33b, S33)  << " and hamming (mut nr) " << perelsonSequence::hamming(&S33, &S33b) << endl;
    }
    MODE_OUT << "And with a complex one " << endl;
    perelsonSequence S34("CADBCBADCCAB");
    perelsonSequence S34b = S34;
    for(int i = 0; i < 20; ++i){
        S34.mutateOnePosition();
        MODE_OUT << S34.print() << " with LCS to original " << LCSsize(S34, S34b) << "-" << LCSsize(S34b, S34)  << " and hamming (mut nr) " << perelsonSequence::hamming(&S34, &S34b) << endl;
    }

    perelsonSequence::alphabetsize = 5;
    MODE_OUT << "\nShowing the table of all possible LCS" << endl;
    perelsonSequence S35("ABCD");
    perelsonSequence S36("CBCAB");
    MODE_OUT << "All possible LCS for " << S35.print() << endl;
    MODE_OUT << "                 and " << S36.print() << " are " << printVector(AllLCS(S35.content, S36.content)) << endl;

    MODE_OUT << "\nShowing the table of all possible LCS" << endl;
    perelsonSequence S37("ABAEDBCBAC");
    perelsonSequence S38("BDCDBCCEA");
    MODE_OUT << "All possible LCS for " << S37.print() << endl;
    MODE_OUT << "                 and " << S38.print() << " are " << printVector(AllLCS(S37.content, S38.content)) << endl;

    MODE_OUT << "\nShowing the table of all possible LCS" << endl;
    perelsonSequence S39(25);
    S39.randomize();
    perelsonSequence S40(25);
    S40.randomize();
    MODE_OUT << "All possible LCS for " << S39.print() << endl;
    MODE_OUT << "                 and " << S40.print() << " are " << printVector(AllLCS(S39.content, S40.content)) << endl;

    MODE_OUT << "\nNow Showing the table of all possible CS (not longuest)" << endl;
    perelsonSequence S41(25);
    S41.randomize();
    perelsonSequence S42(25);
    S42.randomize();
    MODE_OUT << "All possible LCS for " << S41.print() << endl;
    MODE_OUT << "                 and " << S42.print() << " of size more than 9 are " << printVector(filterSizeElements(AllLCS(S41.content, S42.content, true), 9)) << endl;


    //vector<string> toPrint = AllLCS(S30.content, S31.content);
    //printVector(toPrint);
    */
    return string(""); // if MODE_OUT is stringstream, return .str().
}
