#ifndef FoldedShakh_h
#define FoldedShakh_h

#include <iostream>
#include <vector>
#include <string>
using namespace std;


struct foldedCubic {

   static bool foldedCubicLoaded;              /// automatically done when affinity is called first time.

   // Operators / functions to set up a new foldedCubic
   foldedCubic(int _size);                     /// sequence of 'A's of this size
   foldedCubic(string text);                   /// string of characters (A-Z). Other characters become 'A'
   foldedCubic(foldedCubic * toCopy);
   void setConservedPositions(std::vector<bool> _conservedPositions);
   void setConservedPositions(std::string _conservedPositions);
   void setConservedPositions(int lcon);      /// will be put at random positions

   // Data storage. Note: no additional/global parameters for this type of sequences
   string content;
   int size;
   vector<bool> conservedPositions;

   // Accessing data
   char operator [](int i);                     /// Note : can not be used for assigning values
   void operator = (foldedCubic& B) {content = B.content; size = B.size; conservedPositions=B.conservedPositions;}
   bool operator == (foldedCubic &b);

   // Smarter functions:
   void randomize();                            /// Randomize every position (uniform chance for each character)
   int mutateOnePosition();                    /// pick random position and changes character with uniform proba for other characters
   static double affinity(foldedCubic * receptor1, foldedCubic * receptor2); // not virtual
   static double hamming(foldedCubic *x, foldedCubic *y);         /// min number of mutations to reach target. Symmetrical

   // Documentation / Test functions
   std::string print();
   static string test();

   ~foldedCubic(){}
};



#endif
