#include "foldedFree.h"
#include "../Tools/zaprandom.h"
#include <set>
#include <sstream>
#include "../Ymir/fastaffinity.h"
#include "../Tools/nucleotides.h"
#include "../Tools/distribution.h"

/// ============================ Part I: sequences and LCS-based affinity exactly according to Perelson papers =================================

bool ligandDatabase::initialized = false;
int ligandDatabase::sizeReceptors = 10;
int ligandDatabase::minInteract = 6;
int ligandDatabase::minSelfFoldings = -1;
double ligandDatabase::KT = 1.0;
vector<affinityOneLigand*> ligandDatabase::content = vector<affinityOneLigand*>();
std::map<string,int> ligandDatabase::ligandToId = std::map<string,int>();



affinityOneLigand* ligandDatabase::getPreCalculations(string ligandAAseq, string ligandStructSeq, int startPosition, vector<int> & forbiddenPositions){
    if(!initialized) cerr << "ERR: to use the foldedFree sequence representation, you have to first call the function initializeFoldedFree(int _sizeReceptors, int _minInteract, int _minSelfFoldings, double _KT)" << endl;
    if(ligandAAseq.size() != (ligandStructSeq.size() + 1)){
        cerr << "ERR: getPreCalculations (fastaffinity.cpp), incompatible ligand AA seq " << ligandAAseq << " and structure " << ligandStructSeq << ". Should be one more AA than moves in the structure " << endl;
        return NULL;
    }

    // 1 - test if this ligand has already been defined (i.e. possible recceptor structures computed). If yes, just return, if not needs to create it
    stringstream compacted; compacted << ligandAAseq << ligandStructSeq << startPosition << codePos(forbiddenPositions);
    std::map<string,int>::iterator it = ligandToId.find(compacted.str());
    if(it != ligandToId.end()){
        int IDligand = it->second;
        if(IDligand >= (int) content.size()) cerr << "ERR: getPreCalculations, some bullshit is clogging the pipes" << endl;
        return content[IDligand];
    }

    // 2 - if not found, need to create the affinityOneLigand class for it
    affinityOneLigand* newone = new affinityOneLigand(ligandStructSeq, ligandAAseq, startPosition, sizeReceptors, minInteract, minSelfFoldings, KT, forbiddenPositions);
    content.push_back(newone);
    ligandToId[compacted.str()] = content.size() - 1;
    return newone;
}

void ligandDatabase::initialize(int _sizeReceptors, int _minInteract, int _minSelfFoldings, double _KT){
    if(initialized) {
        if((sizeReceptors != _sizeReceptors) || (minInteract != _minInteract) || (minSelfFoldings != _minSelfFoldings)){
            cerr << "ERR: you are initializing multiple times initializeFoldedFree with different structural parameters. Only allowed to change KT !!" << endl;
            exit(-1);
        }
    }
    sizeReceptors = _sizeReceptors;
    minInteract = _minInteract;
    minSelfFoldings = _minSelfFoldings;
    KT = _KT;
    initialized = true;
    //cout << "Initialize ligandDatabase" << KT << endl;
}

foldedFree::foldedFree(string text){
   size = text.size();
   content.resize(size, false);
   for (int i = 0; i < size; ++i){
      if(!correctAA(text[i])){
          cerr << "ERR:foldedFree(" << text << "), un-allowed amino-acid : " << text[i] << endl;
      }
      content[i] = text[i];
   }
   conservedPositions.resize(size, false);
   folding = NULL;
   shieldedPositions = NULL;
}

foldedFree::foldedFree(foldedFree *toCopy){
   size = toCopy->size;
   content.resize(size);
   if(toCopy->folding){
       folding = new struct3D(*(toCopy->folding));
   } else folding = NULL;
   for (int i = 0; i < size; ++i){
      content[i] = toCopy->content[i];
   }
   conservedPositions = toCopy->conservedPositions;
   if(toCopy->shieldedPositions){
      shieldedPositions = new vector<int>(*(toCopy->shieldedPositions));
   } else {
       shieldedPositions = NULL;
   }
}

foldedFree::foldedFree(int _size){
   if (_size < 0) {_size = 0;}
   size = _size;
   content.resize(_size, 'A');
   conservedPositions.resize(_size, false);
   folding = NULL;
   shieldedPositions = NULL;
}

char foldedFree::operator [](int i){
   if ((i < 0) || (i > size)){
      cerr << "foldedFree[" << i << "] out of bounds (size=" << size << ")\n";
      return '?';
   }
   return content[i];
}

void foldedFree::setConservedPositions(std::vector<bool> _conservedPositions){
    if((int) _conservedPositions.size() != size) cerr << "ERR: foldedFree::setConservedPositions, non-matching size " << endl;
    conservedPositions = _conservedPositions;
}

void foldedFree::setConservedPositions(std::string _conservedPositions){
    if((int) _conservedPositions.size() != size) cerr << "ERR: foldedFree::setConservedPositions, non-matching size " << endl;
    conservedPositions.resize(size);
    for(int i = 0; i < size; ++i){
        conservedPositions[i] = _conservedPositions[i];
    }
}

void foldedFree::setConservedPositions(int lcon){
    conservedPositions.clear();
    conservedPositions.resize(lcon, true);
    conservedPositions.resize(size, false);
    random::shuffle(conservedPositions);
}

void foldedFree::setStructure(struct3D* _folding, vector<int> _shieldedPositions){
    folding = new struct3D(*_folding);
    shieldedPositions = new vector<int>(_shieldedPositions);
}

void foldedFree::setStructure(string structSeq, int startPosition, vector<int> _shieldedPositions){
    folding = new struct3D(structSeq, UnDefined, startPosition);
    shieldedPositions = new vector<int>(_shieldedPositions);
}

void foldedFree::setStructure(string structSeq, int startPosition, set<int> _shieldedPositions, int shieldingOptions){
    folding = new struct3D(structSeq, UnDefined, startPosition);
    shieldedPositions = new vector<int>();
    if(shieldingOptions != 0) shieldedPositions->push_back(shieldingOptions);
    for(set<int>::iterator it = _shieldedPositions.begin(); it != _shieldedPositions.end(); ++it){
        shieldedPositions->push_back(*it);
    }
}


int foldedFree::typeAffinity = freeFoldBest;
double foldedFree::rescalingFactor = 1.0;

void foldedFree::initializeDefaultParameters(int _typeAffinity, double _rescalingFactor){
    typeAffinity = _typeAffinity;
    rescalingFactor = _rescalingFactor;
}

/*double foldedFree::affinity(foldedFree * receptor, string ligandAASeq, string ligandStructureSeq, int startingPosition){
    if(ligandStructureSeq.size()== 0) return NAN;
    affinityOneLigand* pre = ligandDatabase::getPreCalculations(ligandAASeq, ligandStructureSeq, startingPosition);
    std::pair<double, double> aff = pre->affinity(receptor->content);
    if(typeAffinity == freeFoldBest) return aff.first;
    else return aff.second;
}

double foldedFree::affinity(foldedFree * receptor, struct3D* ligand){
    if(ligand->sequence.size()== 0) return NAN;
    ////affinityOneLigand* pre = ligandDatabase::getPreCalculations(getAAs(ligand), ligand->sequence, ligand->points[0].IDposition);
    std::pair<double, double> aff = pre->affinity(receptor->content);
    if(typeAffinity == freeFoldBest) return aff.first;
    else return aff.second;
}*/

// previous was 43 foor antigen L1
#define tempBigCoeff 100.
double foldedFree::affinity(foldedFree * receptor, foldedFree * ligand){
    if((ligand->folding == NULL) || (ligand->folding->sequence.size() == 0)){
        cerr << "ERR: foldedFree::affinity(foldedFree* a, b), the ligand has no structure !!" << endl;
        return NAN;
    }
    if(!ligand->shieldedPositions) ligand->shieldedPositions = new vector<int>();
    affinityOneLigand* pre = ligandDatabase::getPreCalculations(ligand->content, ligand->folding->sequence, ligand->folding->startingPosition, *(ligand->shieldedPositions));
    std::pair<double, double> aff = pre->affinity(receptor->content);
    //cout << "Aff " << aff.first << "," << aff.second << " - " << max(0.001, rescalingFactor) << ","  << exp(aff.second / max(0.001, rescalingFactor))  << endl;
    double res;
    if(typeAffinity == freeFoldBest) {
        res = exp((-tempBigCoeff - aff.first) / max(0.001, rescalingFactor));
        //cout << "  Best:" << aff.first << " -> " << res << endl;
    }
    else {
        res=exp((-tempBigCoeff - aff.second) / max(0.001, rescalingFactor));
        //cout << "  Stat:" << aff.second << " -> " << res << endl;
    }
    if(std::isinf(res) || std::isnan(res)) return 0;
    return res;
}

double foldedFree::retrieveEnergy(double aff){
    if(aff < 0) {
        cerr << "ERR: foldedFree::retrieveEnergy, negative aff" << endl;
        return NAN;
    }
    return - (log(aff) * rescalingFactor + tempBigCoeff);
}
double foldedFree::affinityFromEnergy(double E){
    return exp((-tempBigCoeff - E) / max(0.001, rescalingFactor));
}


double foldedFree::hamming(foldedFree * x, foldedFree * y) {
    if (x->size != y->size) {
        std::cout << "Err foldedFree:hamming between two receptors, the length of the two receptors are not identical!";
        return -1;
    }
    int length = x->size;
    int sum = 0;
    for (int i = 0; i < length; ++i) {
        sum += ((*x)[i] != (*y)[i]) ? 1 : 0;
    }
    return sum;
}

// Meaningful functions. returns 1: success, 0: silent, -1: fail
int foldedFree::mutateOnePosition() {
   int cpt = 0;
   while(cpt < 100){
       char test = randomAA();
       int position = random::uniformInteger(0,size-1);
       if ((position >= size) || (position < 0)) {
          cerr << "Random does shit, sequencespace.cpp::mutateOnePosition";
          return -1; // fail
       }
       if((test != content[position]) && (!conservedPositions[position])){
            content[position] = test;
            return +1; // success
       }
       cpt++;
   }
   cerr << "ERR: foldedFree::mutateOnePosition, got trouble to mutate an AA" << endl;
   return 0; // silent
}

/*void perelsonSequence::randomInsertion(int _size) {
   //int positionStart = irandom(size);
}
void perelsonSequence::randomDeletion(int _size) {

}*/

void foldedFree::randomize() {
    string randomContent = randomProt(size);
    if(conservedPositions.size() != randomContent.size()) cerr << "ERR: FoldedFree, inconsistent size of content and conserved positions" << endl;
    for(unsigned int i = 0; i < conservedPositions.size(); ++i){
        if(!conservedPositions[i]) content[i] = randomContent.at(i);
    }
}

bool foldedFree::operator ==(foldedFree &b) {
    if(size != b.size) return false;
    for (int i = 0; i < size; ++i) {
       if((*this)[i] != b[i]) return false;
    }
    for (int i = 0; i < size; ++i) {
       if(conservedPositions[i] != b.conservedPositions[i]) return false;
    }
    return true;
}

string foldedFree::print() {
   stringstream out;
   for (int i = 0; i < size; ++i) {
      out << content[i];
   }
   /*
   out << "\tConserved:";
   for (int i = 0; i < size; ++i) {
      out << conservedPositions.at(i);
   }
   if(folding){
       out << "\tStructure:";
       out << folding->sequence;
   } else {
       out << "\tNoStructure";
   }
   if(shieldedPositions){
       out << "\tShielded:";
       for (int i = 0; i < (int) shieldedPositions->size(); ++i) {
          out << shieldedPositions->at(i) << " ";
       }
   } else {
       out << "\tNoShieldedPositions";
   }*/
   return out.str();
}




// should not contain a stop codon (see randomDNA)
void nucleotideFree::randomize(){
    string randomContent = randomDNA(3*size);
    clearStops(randomContent); // replaces a stop by a random codon (non stop) - do not use for mutation!
    // Note: conserved positions are defined at the protein level, not DNA
    if(conservedPositions.size() != randomContent.size()/3) cerr << "ERR: nucleotideFree, inconsistent size of content and conserved positions" << endl;
    if((int) nucleotideContent.size() != 3* size) cerr << "ERR: nucleotideFree, nucleotideContent is not 3x bigger than AA content" << endl;
    for(int i = 0; i < size; ++i){
        if(!conservedPositions[i]) {
            for(int j = 0; j < 3; ++j){
                nucleotideContent.at(3*i+j) = randomContent.at(3*i+j);
            }
        }
    }
    content = convertToProtein(nucleotideContent);
    containsStop = false;
}

// return -1 if stop codon or fail, 0 if silent, and +1 if successful
int nucleotideFree::mutateOnePosition() {
   int cpt = 0;
   while(cpt < 100){
       int position = random::uniformInteger(0,3*size-1);
       if ((position >= 3*size) || (position < 0)) {
          cerr << "Random does shit, nucleotideFree::mutateOnePosition";
          return -1;
       }
       char test = randomNucleotide();
       if((test != nucleotideContent.at(position)) && (!conservedPositions[position/3])){
            nucleotideContent.at(position) = test;
            string newContent = convertToProtein(nucleotideContent, containsStop); // updates whether there is a stop
            // now test whether silent or stop codon
            if(!content.compare(newContent)) return 0; // silent mutation, even if already contains a stop
            content = newContent;
            if(containsStop) return -1; // stop codon
            return +1; // success
       }
       cpt++;
   }

   cerr << "ERR: nucleotideFree::mutateOnePosition, got trouble to mutate an AA" << endl;
   return 0; // the sequence was not changed (for instance all conserved)
}



// script to manually test affinities between antigens (on that structure) and antibodies
void generateLibrary(string agStruct, string agAAseq, int startPos, int LseqBCRs, int minInteract, double thresholdEnergy, vector<int> shieldedPositions, int nToParse){




    int LseqsAGs  = agStruct.size() + 1;
    foldedFree* Antigen = new foldedFree(LseqsAGs);
    Antigen->setStructure(agStruct, startPos, shieldedPositions);




    //vector<string> agSequences = {"IPMCDPSMYRHPERMCLETVYHLQTGDNKMCWWFDPFQT", "WQMTQPNEYFHPDNMQVEQRYHLCTGDEKMCRWFDEFQT", "TSMRSPCHYLPPYRMIEELVYHLRTGDVKMCSWFDQFQT"};
    double Coeff = 3;
    double KT = 1;
    foldedFree::initializeDefaultParameters(0, Coeff);
    ligandDatabase::initialize(LseqBCRs-1, minInteract, -1 , KT);

    cout << "Ag, BCR, Aff " << endl;

    //void generateLibrary(){
    double threshold = foldedFree::affinityFromEnergy(thresholdEnergy); // 0.001;
    //double thresholdEnergy = - (log(threshold) * Coeff + tempBigCoeff);
    //}

    string Ag = agAAseq;
    Antigen->content = Ag;

    // generate library
    string file = string("../") + fnameLibrary(agStruct, Ag, LseqBCRs-1, thresholdEnergy, minInteract, shieldedPositions);
    int cptfound = 0;
    cerr << "will add up best sequences into file " << file << endl;
    for(int k = 0; k < nToParse; ++k){
        if((k % 100) == 0) cerr << "." << cptfound << "/" << k << "\n";
        foldedFree* newBCR2 = new foldedFree(LseqBCRs);
        newBCR2->randomize();
        double res = foldedFree::affinity(newBCR2, Antigen);
        if(res > threshold){
            double Energy = - (log(res) * Coeff + tempBigCoeff); // sorry to re-convert, need a better function
            ofstream f2(file, ios::app);
            if(!f2) {
                cerr << "File not opened" << endl;
                return;
            }
            f2 << Energy << "\t" << newBCR2->content << "\n";
            f2.close();
            cout << res << "\t" << Energy << "\t" << newBCR2->content << "\n";
            cptfound++;
        }
    }
    return;
}

void FigureUpDown(){
    // Aim of this figure: what is the increase or decrease by mutations.

    // 1 - starts by a super high affinity BCR and disrupts by mutations, randomly


    // 2 - starts by a BCR with a predefined affinity and tries to increase it. Question: how many mutations ?


    // 3 - show history in a GC of the best and how it reached there
    // => need more outputs in hyphasma.



}


void testDistribsFoldedFree(){
    //double KT = 0.5;
    //double coeffDiv = 7;

    random::initialize();

    string agStruct;
    vector<int> blockV;
    string agSeq;
    int startPos = -1;

    int ligand = 6; // 2 or 5 or 6
    int LseqBCRs = 9;       // in AAs


    switch(ligand){
            case 1: {
                agStruct = string("UDRLLRLLRLLR");
                agSeq = string("AAAAAAAAAAAAA");
                startPos = -1;
                break;
            }
            case 2: {
                set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(30,33,33)).occupiedPositions;
                blockV = vector<int>(1, -2); // -2 inserts the flat square
                std::copy(block.begin(), block.end(), std::back_inserter(blockV));
                agStruct = string("RRUUSURRDSSRSRSSDDSSRS");
                agSeq = string("GAKLPLYLIVGAKLPCYLIVLYD");
                startPos = lattice::idFromPosisition(30,32,33);
                break;
            }
            case 3: {
                blockV = vector<int>(1, -2); // -2 inserts the flat square
                set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(31,34,33)).occupiedPositions;
                std::copy(block.begin(), block.end(), std::back_inserter(blockV));
                set<int> block2 = struct3D("RSSSUUSSS", UnDefined, lattice::idFromPosisition(36,33,33)).occupiedPositions;
                std::copy(block2.begin(), block2.end(), std::back_inserter(blockV));
                agStruct = string("DRRDSRSRSDSDSRSRRLLRSSRSSRSSDDSSRSSRS");
                agSeq = string("HILKGALYVPPVYHILVKSPLIVIAAKLGRALYPNVGR");
                startPos = lattice::idFromPosisition(32,31,35);
                break;
            }
            case 4: {
                agStruct = string("SRLLRLLRLLRDURRLRRSLLSRRLRRLLRRLRRSLLSRRLRR");
                agSeq = string(   "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                startPos = lattice::idFromPosisition(32,32,34);
                break;
            }
            case 5: {
                agStruct = string("SRLLRLLRLLRDURRLRRSLLSRRLRRLLRRLRRSLLSRRLRR");
                agSeq = string(   "HILKGALYVPPVYHILVKSPLIVIAAKLGRALYPNVGRRVAPHG");
                startPos = lattice::idFromPosisition(32,32,34);
                break;
            }
            // ligand 3 but without open loop
            case 6: {
                blockV = vector<int>(1, -2); // -2 inserts the flat square
                set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(30,34,33)).occupiedPositions;
                std::copy(block.begin(), block.end(), std::back_inserter(blockV));
                set<int> block2 = struct3D("RSSSUUSSS", UnDefined, lattice::idFromPosisition(34,33,33)).occupiedPositions;
                std::copy(block2.begin(), block2.end(), std::back_inserter(blockV));
                agStruct = string("SDRRDSRSRSDSDSRSRRLLRSSRSSRSSDDSSRSSRS");
                agSeq = string("PHILKGALYVPPVYHILVKSPLIVIAAKLGRALYPNVGR");
                startPos = lattice::idFromPosisition(31,31,35);
                break;
            }
                // ligand 3but only the pocket side, same AAs. the pocket can be touched opposite way
            case 7: {
                blockV = vector<int>(1, -2); // -2 inserts the flat square
                set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(30,34,33)).occupiedPositions;
                std::copy(block.begin(), block.end(), std::back_inserter(blockV));
                set<int> block2 = struct3D("RSSSUUSSS", UnDefined, lattice::idFromPosisition(34,33,33)).occupiedPositions;
                std::copy(block2.begin(), block2.end(), std::back_inserter(blockV));
                agStruct = string("SDRRDSRSRSDSDSRSRRLL");
                agSeq = string(   "PHILKGALYVPPVYHILVKSP");
                startPos = lattice::idFromPosisition(31,31,35);
                break;
            }

            default: {
                cerr << "Figure1(), please chose ligand 1, 2 or 3" << endl;
                return;
            }
        }
///
//MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!
    //MAYDAY, there is no blockV !!!!!

    int LseqsAGs  = agStruct.size() + 1;
    // foldedFree::initializeDefaultParameters(int foldedFree_typeAffinity, par.Value.foldedFree_rescale_log_affinitymax);
    //foldedFree::initializeDefaultParameters(0, coeffDiv);
    // ligandDatabase::initialize(LseqsBCR-1, par.Value.foldedFree_min_contacts, -1 /* default */, par.Value.foldedFree_temperature);
    //ligandDatabase::initialize(LseqBCRs-1, 4, -1 /* default */, KT);
    foldedFree* Antigen = new foldedFree(LseqsAGs);

    double probaMask = 0.2;
    vector<bool> mask = vector<bool>(LseqsAGs, false);
    cout << "Mask is:";
    for(int i = 0; i < LseqsAGs; ++i){
        if(random::uniformDouble(0,1) < probaMask) mask[i] = true;
        if(mask[i]) cout << "1"; else cout << "0";
    }
    cout << endl;
    Antigen->setConservedPositions(mask);
    Antigen->randomize();
    Antigen->setStructure(agStruct, startPos, blockV);

    // script to manually test affinities between antigens (on that structure) and antibodies
    if(0){
        vector<string> agSequences = {"IPMCDPSMYRHPERMCLETVYHLQTGDNKMCWWFDPFQT", "WQMTQPNEYFHPDNMQVEQRYHLCTGDEKMCRWFDEFQT", "TSMRSPCHYLPPYRMIEELVYHLRTGDVKMCSWFDQFQT"};
        vector<string> BCRSequences = {"MFVLMIILV", "MFCLLLILL", "MFCLLLILI", "MFCLLFILI", "MFMLLFILI", "MFVLLFILI"};
        double Coeff = 3;
        foldedFree::initializeDefaultParameters(0, Coeff);
        ligandDatabase::initialize(LseqBCRs-1, 4, -1 , 1.);

        cout << "Ag, BCR, Aff " << endl;

        //void generateLibrary(){
        double threshold = 0.001;
        double thresholdEnergy = - (log(threshold) * Coeff + tempBigCoeff);
        //}

        //for(int i = 0; i < agSequences.size(); ++i){
        int i = 2;
            string Ag = agSequences[i];
            Antigen->content = Ag;

            // generate library
            string file = fnameLibrary(agStruct, Ag, LseqBCRs-1, thresholdEnergy, 4, blockV);
            int cptfound = 0;
            cerr << "will write best sequences into " << file << endl;
            for(int k = 0; k < 1000000; ++k){
                if((k % 100) == 0) cerr << "." << cptfound << "/" << k << "\n";
                foldedFree* newBCR2 = new foldedFree(LseqBCRs);
                newBCR2->randomize();
                double res = foldedFree::affinity(newBCR2, Antigen);
                if(res > threshold){
                    double Energy = - (log(res) * Coeff + tempBigCoeff); // sorry to re-convert, need a better function
                    ofstream f2(file, ios::app);
                    if(!f2) cerr << "File not opened" << endl;
                    f2 << Energy << "\t" << newBCR2->content << "\n";
                    f2.close();
                    cout << res << "\t" << Energy << "\t" << newBCR2->content << "\n";
                    cptfound++;
                }
            }

            /*for(int j = 0; j < BCRSequences.size(); ++j){
                string BCRseq = BCRSequences[j];

                foldedFree* newBCR2 = new foldedFree(BCRseq);
                double res = foldedFree::affinity(newBCR2, Antigen);
                cout << Ag << "\t" << BCRseq << "\t" <<  - (log(res) * Coeff + tempBigCoeff) << "\t" << res << "\n";
                delete newBCR2;
            }*/
        //}
        return;
    }

    // script to manually test affinities between antigens (on that structure) and antibodies
    if(0){
        vector<string> agSequences = {"IPMCDPSMYRHPERMCLETVYHLQTGDNKMCWWFDPFQT", "WQMTQPNEYFHPDNMQVEQRYHLCTGDEKMCRWFDEFQT", "TSMRSPCHYLPPYRMIEELVYHLRTGDVKMCSWFDQFQT"};
        vector<string> BCRSequences = {"MFVLMIILV", "MFCLLLILL", "MFCLLLILI", "MFCLLFILI", "MFMLLFILI", "MFVLLFILI"};
        double Coeff = 3;
        foldedFree::initializeDefaultParameters(0, Coeff);
        ligandDatabase::initialize(LseqBCRs-1, 4, -1 , 1.);

        cout << "Ag, BCR, Aff " << endl;

        for(size_t i = 0; i < agSequences.size(); ++i){
            string Ag = agSequences[i];
            Antigen->content = Ag;


            for(size_t j = 0; j < BCRSequences.size(); ++j){
                string BCRseq = BCRSequences[j];

                foldedFree* newBCR2 = new foldedFree(BCRseq);
                double res = foldedFree::affinity(newBCR2, Antigen);
                cout << Ag << "\t" << BCRseq << "\t" <<  - (log(res) * Coeff + tempBigCoeff) << "\t" << res << "\n";
                delete newBCR2;
            }
        }
    }



    double threshold = 0.002;
    vector<double> KTs = {1.0}; //0.001, 0.01, 0.05, 0.1, 0.2, 0.5, 0.8, 1.0, 2.0, 3.0};
    vector<double> Coeff = {6, 7, 8, 9, 10};
    for(unsigned int k = 0; k < KTs.size(); ++k){
        for(unsigned int c = 0; c < Coeff.size(); ++c){
            //cerr << "KT=" << KTs[k] << ", Coeff/=" << Coeff[c] << endl;
            foldedFree::initializeDefaultParameters(0, Coeff[c]); // best affinity
            ligandDatabase::initialize(LseqBCRs-1, 4, -1 /* default */, KTs[k]);

            double percentSuccess = 0;
            int cpt=0;
            ministat a = ministat();
            ministat mut = ministat();
            for(int i = 0; i < 20; ++i){
//                string Px = randomProt(LseqBCRs);
                foldedFree* newBCR = new foldedFree(LseqBCRs);
                double res = 0;
                while((cpt < 10000) && (res < threshold)){
                    if((cpt % 100) == 0) {
                        cerr << "." << percentSuccess << "/20" << endl;
                        cout << endl;
                    }
                    newBCR->randomize();
                    res = foldedFree::affinity(newBCR, Antigen);
                    cout << newBCR->content << "\t" << res << "\n";
                    cpt++;
                }
                if(res > threshold) {
                    percentSuccess++;
                    a.add(res);
                    double best = res;
                    for(int j = 0; j < 10; ++j){
                        foldedFree* newBCR2 = new foldedFree(newBCR);
                        newBCR2->mutateOnePosition();
                        double res2 = foldedFree::affinity(newBCR2, Antigen);
                        best = max(res2, best);
                        delete newBCR2;
                    }
                    cout << "affinity(" << newBCR->content <<  ") = \t" << res <<  " or " << - (log(res) * Coeff[c] + tempBigCoeff) << " -> " << best << " or " << - (log(best) * Coeff[c] + tempBigCoeff)  << " so *= " << best / (max(1e-10, res)) << endl;
                    mut.add(best);
                }
                delete newBCR;
            }
            cout << "KT= " << KTs[k] << "\tCoeff= " << Coeff[c] << "%succ" << percentSuccess / (double) cpt << "\tRand " << a.sumUp() << " Improv by 5 mut:" << mut.sumUp() << endl;
        }
    }
}



string foldedFree::test(){
#define MODE_OUT cerr
    MODE_OUT << "================ Test constructor / basic functions =================" << endl;
    foldedFree S1(5);
    MODE_OUT << "S1: Empty sequence, size 5 " << S1.print() << "\n\n";
    foldedFree S2(string("ABDBCBDABDCCADB"));
    MODE_OUT << "S2: Sequence from binary text ABDBCBDABDCCADB -> " << S2.print() << "\n\n";
    foldedFree S3(string("AABCGSTHDY01oAX5!="));
    MODE_OUT << "S3: Sequence from text with bad characters 'AABCGSTHDY01oAX5!=' ->" << S3.print() << "\n\n";
    foldedFree S4(&S2);
    MODE_OUT << "S4: Copy from previous sequence S2 " << S4.print() << "\n\n";
    foldedFree S4b(15);
    S4b = S2;
    MODE_OUT << "S4: = from previous sequence S2 " << S4b.print() << "\n\n";




    MODE_OUT << "Accessing positions of S5b one by one [] ";
    MODE_OUT << S2[0] << S2[1] << S2[2] << "..." << S2[S2.size-1] << "\n\n";
    MODE_OUT << "Now testing bad positions : ";
    MODE_OUT << S2[-1] << S2[S2.size] << "\n\n";
    MODE_OUT << "S6: Randomized sequence ";
    foldedFree S6(10);
    S6.randomize();
    MODE_OUT << S6.print() << "\n\n";
    MODE_OUT << "Consecutive mutations" << endl;
    foldedFree S8("BBBBBBBBBB");
    for(int i = 0; i < 20; ++i){
        S8.mutateOnePosition();
        MODE_OUT << S8.print() << endl;
    }
    foldedFree S9(0);
    S9 = S8;
    MODE_OUT << "Compare equal sequences : " << S8.print() << ((S8==S9) ? "=" : "!=")  << S9.print() << "\n\n";
    S9.mutateOnePosition();
    MODE_OUT << "Compare different sequences : " << S8.print() << ((S8==S9) ? "=" : "!=")  << S9.print() << "\n\n";
    S9.randomize();
    MODE_OUT << "Random sequence: " << S9.print() << endl;

    MODE_OUT << "Test affinity functions on single examples" << "\n";
    foldedFree S10("AAAAAAAAAA");   // reference sequence AG1
    foldedFree S11("AABCDDCBAA");   // reference sequence AG2
    foldedFree S21("AABBCCDDEE");   // match 4+5 to AG1, 4+3 to AG2
    foldedFree S22("EEDDCCBBAA");   // match 1+3+3 to AG1, 1+3+3 to AG2

    /*
    MODE_OUT << affinity(&S21, &S10, 0.5, 1.0) << endl;
    MODE_OUT << printPerelsonSeqAff(&S21, &S11, 0.5, 1.0) << endl;
    MODE_OUT << printPerelsonSeqAff(&S22, &S10, 0.5, 1.0) << endl;
    MODE_OUT << printPerelsonSeqAff(&S22, &S11, 0.5, 1.0) << endl;

    MODE_OUT << "Test LCS subfunctions" << "\n";
    perelsonSequence::alphabetsize = 4;

    MODE_OUT << "Show longuest substring after consecutive mutations" << endl;
    perelsonSequence S32("BBBBBBBBBB");
    perelsonSequence S32b = S32;
    for(int i = 0; i < 20; ++i){
        S32.mutateOnePosition();
        MODE_OUT << S32.print() << " with LCS to original " << LCSsize(S32, S32b) << "-" << LCSsize(S32b, S32)  << " and hamming (mut nr) " << perelsonSequence::hamming(&S32, &S32b) << endl;
    }
    MODE_OUT << "Now with a 2-char sequence " << endl;
    perelsonSequence S33("AABBAABBAABB");
    perelsonSequence S33b = S32;
    for(int i = 0; i < 20; ++i){
        S33.mutateOnePosition();
        MODE_OUT << S33.print() << " with LCS to original " << LCSsize(S33, S33b) << "-" << LCSsize(S33b, S33)  << " and hamming (mut nr) " << perelsonSequence::hamming(&S33, &S33b) << endl;
    }
    MODE_OUT << "And with a complex one " << endl;
    perelsonSequence S34("CADBCBADCCAB");
    perelsonSequence S34b = S34;
    for(int i = 0; i < 20; ++i){
        S34.mutateOnePosition();
        MODE_OUT << S34.print() << " with LCS to original " << LCSsize(S34, S34b) << "-" << LCSsize(S34b, S34)  << " and hamming (mut nr) " << perelsonSequence::hamming(&S34, &S34b) << endl;
    }

    perelsonSequence::alphabetsize = 5;
    MODE_OUT << "\nShowing the table of all possible LCS" << endl;
    perelsonSequence S35("ABCD");
    perelsonSequence S36("CBCAB");
    MODE_OUT << "All possible LCS for " << S35.print() << endl;
    MODE_OUT << "                 and " << S36.print() << " are " << printVector(AllLCS(S35.content, S36.content)) << endl;

    MODE_OUT << "\nShowing the table of all possible LCS" << endl;
    perelsonSequence S37("ABAEDBCBAC");
    perelsonSequence S38("BDCDBCCEA");
    MODE_OUT << "All possible LCS for " << S37.print() << endl;
    MODE_OUT << "                 and " << S38.print() << " are " << printVector(AllLCS(S37.content, S38.content)) << endl;

    MODE_OUT << "\nShowing the table of all possible LCS" << endl;
    perelsonSequence S39(25);
    S39.randomize();
    perelsonSequence S40(25);
    S40.randomize();
    MODE_OUT << "All possible LCS for " << S39.print() << endl;
    MODE_OUT << "                 and " << S40.print() << " are " << printVector(AllLCS(S39.content, S40.content)) << endl;

    MODE_OUT << "\nNow Showing the table of all possible CS (not longuest)" << endl;
    perelsonSequence S41(25);
    S41.randomize();
    perelsonSequence S42(25);
    S42.randomize();
    MODE_OUT << "All possible LCS for " << S41.print() << endl;
    MODE_OUT << "                 and " << S42.print() << " of size more than 9 are " << printVector(filterSizeElements(AllLCS(S41.content, S42.content, true), 9)) << endl;


    //vector<string> toPrint = AllLCS(S30.content, S31.content);
    //printVector(toPrint);
    */
    return string(""); // if MODE_OUT is stringstream, return .str().
}
