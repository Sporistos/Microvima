#ifndef FoldFree_h
#define FoldFree_h

#include <iostream>
#include <vector>
#include <string>
#include "../Ymir/fastaffinity.h"
#include "../Ymir/proteins.h"
using namespace std;

struct ligandDatabase {
    static bool initialized;
    static int sizeReceptors, minInteract, minSelfFoldings;
    static double KT;
    static vector<affinityOneLigand*> content;
    static std::map<string,int> ligandToId;
    static affinityOneLigand* getPreCalculations(string ligandAAseq, string ligandStructSeq, int startPosition, vector<int> &forbiddenPositions);
    static void initialize(int _sizeReceptors, int _minInteract, int _minSelfFoldings, double _KT);
};



struct foldedFree {

   // Operators / functions to set up a new foldedFree
   foldedFree(int _size);                 /// sequence of 'A's of this size
   foldedFree(string text);               /// string of characters (A-Z). Other characters become 'A'
   foldedFree(foldedFree * toCopy);
   void setConservedPositions(std::vector<bool> _conservedPositions);
   void setConservedPositions(std::string _conservedPositions);
   void setConservedPositions(int lcon);      /// will be put at random positions
   void setStructure(struct3D* _folding, vector<int> _shieldedPositions = vector<int>());
   void setStructure(string structSeq, int startPosition, vector<int> _shieldedPositions = vector<int>());
   void setStructure(string structSeq, int startPosition, set<int> _shieldedPositions, int shieldingOptions = 0);

   // Data storage (AA content)
   string content;
   int size;                           /// Antigen and BCR may have different sizes ... so not static
   vector<bool> conservedPositions;
   struct3D* folding;                  /// for ligands only. For receptors there are many best foldings => put NULL
   vector<int>* shieldedPositions;     /// for ligands only. NULL for receptors.Can use '-1' or '-2' as first values
                                       /// for special shapes. Therefore, better to use vector than set.

   // Accessing data
   char operator [](int i);                     /// Note : can not be used for assigning values
   virtual void operator = (foldedFree& B) {content = B.content; size = B.size;}
   virtual bool operator == (foldedFree &b);

   // Smarter functions:
   virtual void randomize();                            /// Randomize every position (uniform chance for each character)
   virtual int mutateOnePosition();                    /// pick random position and changes character with uniform proba for other characters

   // default parameters: if not specified inside affinity, these default values will be used
   // please also call ligandDatabase::initialize() once for global parameters.
   enum {freeFoldBest, freeFoldStat}; // types of affinity
   static int typeAffinity;
   static double rescalingFactor;
   static void initializeDefaultParameters(int _typeAffinity, double _rescalingFactor);

   // affinity functions !
   //static double affinity(foldedFree * receptor, string ligandAASeq, string ligandStructureSeq, int startingPosition = -1);
   //static double affinity(foldedFree * receptor, struct3D* ligand);
   static double affinity(foldedFree * receptor, foldedFree *ligand);
   static double hamming(foldedFree *receptor1, foldedFree *receptor2);         /// min number of mutations to reach target. Symmetrical

   // Documentation / Test functions
   std::string print();
   static string test();


   static double retrieveEnergy(double aff);
   static double affinityFromEnergy(double E);

   virtual ~foldedFree(){delete folding;}
};

// everything the same as foldedFree, but mutates nucleotides and reconstitutes the AA sequence.
// Conserved positions are still AAs, and nucleotides should be sized 3x the AA sequence.
struct nucleotideFree : public foldedFree {
    // size is in AAs, inherited from foldedFree. nucleotideContent should be 3 times bigger
    string nucleotideContent;
    bool containsStop;
    nucleotideFree(int _sizeInAAS):             foldedFree(_sizeInAAS), containsStop(false) {nucleotideContent.resize(_sizeInAAS*3, '-');}
    nucleotideFree(string text):                foldedFree(text.size() / 3), containsStop(false) {nucleotideContent = text; if(size * 3 != (int) text.size()) cerr << "Size Mismatch" << endl;}
    nucleotideFree(nucleotideFree * toCopy):    foldedFree(toCopy), containsStop(toCopy->containsStop) {nucleotideContent = toCopy->nucleotideContent;}
    void operator = (nucleotideFree& B) {content = B.content; size = B.size; conservedPositions=B.conservedPositions; nucleotideContent = B.nucleotideContent; containsStop = B.containsStop;}
    bool operator == (nucleotideFree &b);

    // content is still updated to be the AA content => can reuse the mother affinity !
    static double affinity(nucleotideFree * receptor, nucleotideFree *ligand){
        return foldedFree::affinity((foldedFree*) receptor, (foldedFree*) ligand);
    }
    // but hamming is now defined as distance in nucleotide mutations
    static double hamming(nucleotideFree *receptor1, nucleotideFree *receptor2);
    //virtual ~nucleotideFree(){foldedFree::~foldedFree();}

    // only functions that are different than foldedFree:
    void randomize();
    int mutateOnePosition();
};

void testDistribsFoldedFree();
void FigureUpDown();
void generateLibrary(string agStruct, string agAAseq, int startPos, int LseqBCRs, int minInteract, double thresholdEnergy, vector<int> shieldedPositions, int nToParse = 1000);



#endif
