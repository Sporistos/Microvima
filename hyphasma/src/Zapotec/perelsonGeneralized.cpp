#include "perelsonGeneralized.h"
#include "../Tools/graph.h"
#include <sstream>
#include <algorithm> // reverse

/// The type of values for computing inside the program is 'long double' to avoid at maximum the 'nan' effect of big numbers.
/// please feel free to change it to other type like only double or more sophisticated type.
typedef long double valeur;

void perelsonGeneralized::initializeDefaultParameters(int _alphabetsize, double _specificity, vector<double> _AAstrength, vector<double> _AAaccessibility, clusterLCSaffinities _typeOfClusterAffinities){
    alphabetsize = _alphabetsize;
    specificity = _specificity;
    typeOfClusterAffinities = _typeOfClusterAffinities;
    if((int) _AAstrength.size() != alphabetsize) cerr << "ERR: perelsonGeneralized::initializeDefaultParameters, the AAstrength vector has different size than alphabetsize" << endl;
    //if((int) _AAaccessibility.size() != *size of ligands*) cerr << "ERR: perelsonGeneralized::initializeDefaultParameters, the AAstrength vector has different size than alphabetsize" << endl;
    AAstrength = _AAstrength;
    AAaccessibility = _AAaccessibility;
    initializedGeneralized = true;
    threshold = NAN;
    exponent = NAN;
}

vector<double> perelsonGeneralized::AAaccessibility = {};
vector<double> perelsonGeneralized::AAstrength = {};
double  perelsonGeneralized::specificity = NAN;
bool    perelsonGeneralized::initializedGeneralized = false;
clusterLCSaffinities     perelsonGeneralized::typeOfClusterAffinities = onlySpecificity;

double perelsonGeneralized::affinity(perelsonGeneralized *x, perelsonGeneralized *y, double _specificity, clusterLCSaffinities _typeOfClusterAffinities){
    if(! initializedGeneralized) cerr << "ERR: perelsonGeneralized::affinity, you need to initialize default values first !!" << endl;
    if(_specificity < 0) {_specificity = specificity;}
    if(_typeOfClusterAffinities == specUndefined) {_typeOfClusterAffinities = typeOfClusterAffinities;}
    // in LCSclusters, ligand is first, antibody is second
    pair<double,string> res = LCSclusters(y->content, x->content, _typeOfClusterAffinities, _specificity);
    return res.first;
}

double  perelsonGeneralized::hamming(perelsonGeneralized *x, perelsonGeneralized *y){
    return perelsonSequence::hamming((perelsonSequence*) x,(perelsonSequence*) y);
}

string pgPrintVector(vector<double> l){
    stringstream res;
    res << "[" << l.size() << "] ";
    for(int i = 0; i < (int) l.size(); ++i){
        res << l[i] << ((i == (int) l.size()-1) ? "" : " ");
    }
    return res.str();
}

string perelsonGeneralized::print() {
    // note: do not call the mother function, it's virtual !!
   stringstream out;
   for (size_t i = 0; i < size; ++i) {
      out << content[i];
   }
   return out.str();
}

void perelsonGeneralized::testUpgrade(){
    perelsonSequence::alphabetsize = 4;
    vector<double> AAstr = {1.0, 1.0, 0.5, 2.0};
    vector<double> AAaccess = {1.0, 1.0, 2.0, 0.0, 1.0, 0.1, 1.0, 1.0, 1.0, 2.0, 1.0, 0.5, 0.5, 1.0, 1.0 ,1.0, 1.0, 1.0};
    perelsonGeneralized::AAstrength = AAstr;
    perelsonGeneralized::AAaccessibility = AAaccess;
    perelsonSequence v1  ("ABCBADDBCABBCBAB");
    perelsonSequence mask("--11111--11-1111");

    for(int i = 0; i < 3; ++i){
        double spec = 2;
        cerr << "Testing the upgrade function,";
        clusterLCSaffinities typeAffinityTested = onlySpecificity;  cerr << " with specificity " << spec; if(i == 0) cerr << endl;
        if(i == 1) {typeAffinityTested = specificityAndAAStrength;    cerr << " with AAstrength, " << pgPrintVector(AAstrength) << "for sequence" << endl;}
        if(i == 2) {typeAffinityTested = specificityAndAccessibility;
            cerr << "\n      with AAaccessibility ";
            for(size_t i = 0; i < v1.size; ++i){
                cerr << v1[i] << ":" << AAaccessibility.at(i) << ", ";
            }
            cerr << "for sequence" << endl;
        }
        cerr << "      " << v1.print() << " with mask \n      " << mask.print() << endl;

        int currentSizeCluster = 0;
        double currentClusterWeight = 0;

        for(unsigned int pos = 0; pos < v1.content.size(); ++pos){
            double upgradeWeight = (mask[pos] == '1') ? perelsonGeneralized::upgrade(currentClusterWeight, currentSizeCluster, pos, v1.content, typeAffinityTested, spec) : 0;
            if(mask[pos] == '1') {
                currentSizeCluster++;
                currentClusterWeight += upgradeWeight;
                cerr << "   -> New cluster (pos " << pos - currentSizeCluster+1 << "-" << pos << "), sequence ";
                for(unsigned int j = pos - currentSizeCluster+1; j <= pos; ++j){
                    cerr << v1.content[j];
                }
                cerr << " upgrade : " << upgradeWeight << " new weight : " << currentClusterWeight << endl;
            } else {
                currentClusterWeight = 0;
                currentSizeCluster = 0;
            }
        }
    }

    // example with two LCS: AABBBBBBAA or AABBCCBBAA
    perelsonGeneralized s1("DAABBBBCCBBAACAA");
    perelsonGeneralized s2("CAABBCCBBBBAADAA");
    cerr << "Sequences of interest: " << s1.print() << "\n                       " << s2.print() << endl;
    vector<string> getAll = AllLCS(s1.content, s2.content);
    vector<double> AAstr2 = {1.0, 1.0, 0.5, 1.0, 1.0};

    for(int i = 0; i < 3; ++i){
        double spec = 2;
        cerr << "Testing the LCSclusters function,";
        clusterLCSaffinities typeAffinityTested = onlySpecificity;  cerr << " with specificity " << spec; if(i == 0) cerr << endl;
        if(i == 1) {typeAffinityTested = specificityAndAAStrength;    cerr << " with AAstrength, " << pgPrintVector(AAstrength) << "for sequence" << endl;}
        if(i == 2) {typeAffinityTested = specificityAndAccessibility;
            cerr << "\n      with AAaccessibility ";
            for(int i = 0; i < s1.size; ++i){
                cerr << s1[i] << ":" << AAaccessibility.at(i) << ", ";
            }
            cerr << "for sequence" << endl;
        }
        cerr << "      " << s1.print() << " and \n      " << s2.print() << endl;

        cerr << "   -> All best LCS according to size only are :" << endl;
        perelsonGeneralized::AAstrength = AAstr;
        for(unsigned int i = 0; i < getAll.size(); ++i){
            cerr << "      " << getAll[i] << endl; //" with strength " << perelsonGeneralized::clusterWeight( endl;
        }
        pair<double,string> res = perelsonGeneralized::LCSclusters(s1.content, s2.content, typeAffinityTested, spec);
        cerr << "   -> Best matching found: " << res.second << " with affinity " << res.first << endl;
    }

}



/// 1 - tool function: what is the increase of a weight of a cluster by adding a new AA depending on 1/ the existing cluster size
///     2/ the affinity of the existing cluster (with the specificity exponent), 3/ the type of affinity
///     this function implicitly use the AA profiles
// this is generalizable to any function for cluster affinity f(ABCBR) = ...
double perelsonGeneralized::upgrade(double previousAffinity, double sizeAlreadyStartedCluster, int positionToAddInV1, string & v1, int typeClusterAffinity, double specificityParameter){
    switch(typeClusterAffinity){
        case onlySpecificity: {
            return - pow(sizeAlreadyStartedCluster,specificityParameter) + pow(sizeAlreadyStartedCluster+1,specificityParameter);
        }
        case specificityAndAAStrength: {
            if((int) AAstrength.size() < alphabetsize) cerr << "ERR: the AAstrength is defined for only " << AAstrength.size() << " while alphabet has " << alphabetsize << " elements " << endl;
            double oldweight = pow(previousAffinity, 1. / specificityParameter);
            return - pow(oldweight,specificityParameter) + pow(oldweight + (AAstrength[v1[positionToAddInV1] - 'A']),specificityParameter);
        }
        case specificityAndAccessibility: {
            if(AAaccessibility.size() < v1.size()) cerr << "ERR: the AAaccessibility is defined for only " << AAaccessibility.size() << " while given sequence has " << v1.size() << " elements " << endl;
            double oldweight = pow(previousAffinity, 1. / specificityParameter);
            return - pow(oldweight,specificityParameter) + pow(oldweight + (AAaccessibility[positionToAddInV1]),specificityParameter);
        }
        default:   {return -1;}
    }
}


// 2 tables : one for the right way and oen for the left way.
// for each position, stores the affinity if would end up with an open cluster of 0,1,2,3,4,5 ...

// decisions : -X = move right from breaking a cluster of size X-1, +X: moving down from breaking cluster size X-1; 0:binding !!
// note: these are const, or define, not global variables

// mDiag(0) should never happen
#define mDiag(X) (X)
#define mStart 0
#define mLeft -1
#define mUp -2
#define mImpossible -3
#define mBind -4

string printDecision(int v){
    if(v == mBind) return string("B");
    if(v == mStart) return string("S");
    if(v == mImpossible) return string(".");
    if(v == mUp) return string("U");
    if(v == mLeft) return string("L");
    if(v > 0) {
        stringstream res; res << "D(" << v << ")";
        return res.str();
    }
    return string("?");
}
string printAff(double v){
    stringstream res;
    if(isnan(v)) return string("");
    res << /*setprecision(1) <<  */ v;
    //if(res.str().size() > 4) return res.str().substr(0,4);
    return res.str();
}




// V1 is the epitope, with possibility to have predefined accessibilities.
pair<double,string> perelsonGeneralized::LCSclusters(string & v1, string & v2, int typeClusterAffinity, double specificityParameter){

    int M = v1.size();
    int N = v2.size();
    int maxLengthCluster = LCSsize(v1, v2); // better than  min(M,N);

    // First table gives the best possible affinity by having already formed a cluster of size k for each [k=0..maxLengthCluster]
    vector< vector<double> > affinityPossibilities (M*N, vector<double>(maxLengthCluster, NAN));
    // second table says how the best choice was made. If by increase of cluster size, will be 'mBind'. If not, says
    // whether the best cluster of size k comes from up or left (positive number) or right (negative number).
    vector< vector<int> > affinityDecision (M*N, vector<int>(maxLengthCluster, mImpossible));

    // lines :   i, explore v1, [0..M-1]
    // columns : j, explore v2, [0..N-1]

    // starting position
    affinityPossibilities[0][0] = 0;
    affinityDecision[0][0] = mStart; // note: this state can become mBind as well.

    // first column + starting point
    for(int i = 0; i < M; ++i){
        int j = 0;
        if(i > 0){
            affinityPossibilities[i*N+j][0] = affinityPossibilities[(i-1)*N+j][0];
            affinityDecision[i*N+j][0] = mUp; // by default, best choice is upstairs
        }
        if(v1[i] == v2[j]){
            affinityPossibilities[i*N+j][1] = upgrade(0.,0, i, v1, typeClusterAffinity, specificityParameter);
            affinityDecision[i*N+j][1] = mBind ; // Bind !
        }

    }

    // first line
    for(int j = 1; j < N; ++j){
        int i = 0;
        affinityPossibilities[i*N+j][0] = affinityPossibilities[i*N+j-1][0];
        affinityDecision[i*N+j][0] = mLeft;  // by default, on first line, best choice comes from left

        if(v1[i] == v2[j]){
            affinityPossibilities[i*N+j][1] = upgrade(0.,0, i, v1, typeClusterAffinity, specificityParameter);
            affinityDecision[i*N+j][1] = mBind ; // Bind !
        }
    }


    for(int i = 1; i < M; ++i){
        for(int j = 1; j < M; ++j){

            // if matching characters, computes the increase of each previous cluster size.
            if(v1[i] == v2[j]){
                for(int k = 1; k < maxLengthCluster; ++k){
                    if((k == 1) || (affinityDecision[(i-1)*N+j-1][k-1] != mImpossible)){
                        affinityPossibilities[i*N+j][k] = affinityPossibilities[(i-1)*N+j-1][k-1] + upgrade(affinityPossibilities[(i-1)*N+j-1][k-1], k-1,  i, v1, typeClusterAffinity, specificityParameter);
                        affinityDecision[i*N+j][k] = mBind ;
                    }
                }
            }

            // now computes the choice of not taking anything at this position (even if they are equal).
            // will save where was the best choice (up or left, and how big was the cluster that was not extended)
            double maximize = 0;
            int currentBestDirection = mUp; // default if the affinity is 0. It's always up or left anyways
            //affinityDecision[i*N+j][0] = mUp;
            for(int k = 1; k < maxLengthCluster; ++k){ // k=0 is useless, you can move Up or Left to do the same.
                double bestAffBreak = affinityPossibilities[(i-1)*N+(j-1)][k];
                if((!isnan(bestAffBreak)) && (bestAffBreak > maximize)){
                    maximize = bestAffBreak;
                    currentBestDirection = mDiag(k);
                }
            }
            double bestAffUp = affinityPossibilities[(i-1)*N+j][0];
            if((!isnan(bestAffUp)) && (bestAffUp > maximize)){
                maximize = bestAffUp;
                currentBestDirection = mUp;
            }
            double bestAffLeft = affinityPossibilities[i*N+j-1][0];
            if((!isnan(bestAffLeft)) && (bestAffLeft > maximize)){
                maximize = bestAffLeft;
                currentBestDirection = mLeft;
            }
            affinityPossibilities[i*N+j][0] = maximize;
            affinityDecision[i*N+j][0] = currentBestDirection;
        }
    }

    // now from the ending point, decides what is best, between current clusters of each size or not taking the last character
    double bestTotalAffinity = 0;
    int indexOptimalSizeLastCluster = 0;
    for(int k = 0; k < maxLengthCluster; ++k){
        if((!isnan(affinityPossibilities[(M-1)*N+N-1][k])) && (affinityPossibilities[(M-1)*N+N-1][k] > bestTotalAffinity)){
            bestTotalAffinity = affinityPossibilities[M*N-1][k];
            indexOptimalSizeLastCluster = k;
        }
    }


    // now printing the table
    if(false){
        cerr << "v1 \\ v2\t";
        for(int j = 0; j < N; ++j){
            cerr << v2[j] << "\t";
        }
        cerr << endl;
        for(int i = 0; i < M; ++i){
            cerr << v1[i] << "\t";
            for(int j = 0; j < N; ++j){
                cerr << "[";
                for(int k = 0; k < maxLengthCluster; ++k){
                    cerr << printDecision(affinityDecision[i*N+j][k]) << printAff(affinityPossibilities[i*N+j][k]);// << setprecision(2);
                }
                cerr << "]\t";
            }
            cerr << endl;
        }
        cerr << "At the last position, the best choice is to take the cluster of size " << indexOptimalSizeLastCluster << endl;
    }


    // retrieving best matching !
    stringstream detailsBestCS;
    string oneBestCS;
    {
        int i = M-1;
        int j = N-1;
        int k = indexOptimalSizeLastCluster;
        int movement = affinityDecision[M*N-1][k];
        while((movement != mStart) && (movement != mImpossible) && (i >= 0) && (j >= 0)){ // only one or three at a time become negative
            movement = affinityDecision[i*N+j][k];
            switch(movement){
                case mBind: {
                    detailsBestCS << v1[i] << "[" << i << "/" << j << "]" << ",";
                    oneBestCS.append(string(1,v1[i]));
                    i--;
                    j--;
                    k--;
                    break;
                }
                case mLeft: { // Left
                    if(k != 0) cerr << "ERR: LCSclusters, Left Move with k>0 should not happen" << endl;
                    detailsBestCS << "[" << i << "," << j << "->Left ";
                    j--;
                    break;
                }
                case mUp:{ // Up
                    if(k != 0) cerr << "ERR: LCSclusters, Up Move with k>0 should not happen" << endl;
                    detailsBestCS << "[" << i << "," << j << "Up ";
                    i--;
                    break;
                }
                case mStart:{
                    break;
                }
                default: {
                    if(movement > 0){ // Diag
                        k = (int) movement;
                        detailsBestCS << "[" << i << "," << j << "Diag(" << k << ") ";
                        i--;
                        j--;
                    } else {
                        cerr << "This should not happen" << endl;
                        movement = mImpossible;
                    }
                    break;
                }
            }
        }
    }
    std::reverse(oneBestCS.begin(), oneBestCS.end());
    return pair<double, string> (bestTotalAffinity, oneBestCS);
}



/* this was wrong
 *             // now computes the choice of not taking anything at this position (even if they are equal).
            // will save where was the best choice (up or left, and how big was the cluster that was not extended)
            double maximize = 0;
            int currentBestDirection = mImpossible;
            for(int k = 0; k < maxLengthCluster; ++k){
                double bestAffUp = affinityPossibilities[(i-1)*N+j][k];
                double bestAffLeft = affinityPossibilities[i*N+j-1][k];
                if((!isnan(bestAffUp)) && (bestAffUp > maximize)){
                    maximize = bestAffUp;
                    currentBestDirection = mUpstairs(k);
                }
                if((!isnan(bestAffLeft)) && (bestAffLeft > maximize)){
                    maximize = bestAffLeft;
                    currentBestDirection = mLeft(k);
                }
            }
            */


//affinityDecision[0][0] = mStart; // might be mBind. Start is useless.





/* // Maybe a previous cluster of size 1 would have a better weight than making a new one from scratch,
double affIfPoint = affinityPossibilities[(i-1)*N+j][1]; // upstairs affinity of previous cluster size 1
if((!isnan(affIfPoint)) && (affIfPoint > affinityPossibilities[i*N+j][0])){
    affinityPossibilities[i*N+j][1] = affIfPoint;
    affinityDecision[i*N+j][1] = mDown(1); // having
}*/

// if the left position was making a cluster (size 1), might be better to break it instead of taking the affinity from a previous cluster
/* double affIfPoint = affinityPossibilities[i*N+j-1][1];
if((!isnan(affIfPoint)) && (affIfPoint > affinityPossibilities[i*N+j][0])){
    affinityPossibilities[i*N+j][0] = affinityPossibilities[i*N+j-1][1];
    affinityDecision[i*N+j][0] = mRight(1);
}*/



/*
void Microcanonique::Init(){
    SumUp();
    for(int i = 1; i < N ; ++i){
        if(interaction(i) < 0.0){cerr << "ERR : cannot launch microcanonical algorithm with an NEGATIVE INTERACTION POTENTIAL (ex: distance = " << i << ", value= " << interaction(i) << ")"<<  endl; 	exit(-1);}
        if(landscape(i) < 0.0){cerr << "ERR : cannot launch microcanonical algorithm with an NEGATIVE POTENTIALS (ex: position " << i << ", value = " << landscape(i) << ")" << endl; exit(-1);}
    }

   //  -------------- Creation du graphe ---------------------*/

        /* Formulation of the inputs inside this class :
                N is the number of possible positions for the left side of a particle
                    i.e. N = L - vol_exclus + 1
                K is the maximum possible number of particles on a configuration
                portee is defined after the excluded volume (particles stuck together = portee = 1)
                    and is defined so I(portee) = 0
                    it is mode convenient to define :
                Dmax : the distance where interaction is 0 and will stay at 0.
                    Dmax = Vex + portee - 1
                //int Dmax = vol_exclus + portee - 1;

    // size of the graph : N columns for the positions + 1 column for gathering separately the configurations with 1, 2, 3 ... particles + source and arrivee */



    /*cerr << "    -> Graphe pre-created(empty), with " << graphe.nb_sommets() << " sommets" << endl;
    if(graphe.nb_sommets() != (N+1) * (K) + 2) cerr << "ERR : wrong size for graph, should be (N[=" << N << "] - 1 ) * K [=" << K << "] + 2" << endl;

    // --------- to make sure that the weight of all edges will be positive, prepare a constant to add in the good place on the graph ... --------------------

    if(Mu <= 0) {CstAntiNeg = max((valeur) 1000.0, - K * Mu + 10);
        cerr << "    -> For information, " << CstAntiNeg << " will be added to every last edge for preserving positive edges even if negative mu.\n";}
    else CstAntiNeg = 0.0;

    // defining the grid (already created with size K+1 x N)

    G2D.source = G2D.ID(K+1,1);
    G2D.arrivee = G2D.ID(K+1,2);

    // ---- Choix du premier nucléosome : la source lance sur la première ligne, coût landscape(x)	----
    for(int j = 1; j <= N; ++j){
        graphe.add_to_graph(G2D.source, G2D.ID(1,j), landscape(j));
    }

    // ---- d'une ligne à l'autre, ID(i,j) pointe sur ID(i+1, j+k) avec poids potentiel(k) + landscape(j+k)
    for(int i = 1; i <= K-1; ++i){
        for(int j = 1; j <= N; ++j){
            if(j % 100 == 0) cerr << "        ." << (i-1) * (N) + j << " / " << N*(K-1) << endl;
            for(int k = vol_exclus; k <= min(N - j, Dmax); ++k){
                graphe.add_to_graph(G2D.ID(i,j), G2D.ID(i+1,j+k), interaction(k) + landscape(j+k));
            }
            for(int k = min(N - j, Dmax) +1; k <= N-j; ++k){
                graphe.add_to_graph(G2D.ID(i,j), G2D.ID(i+1,j+k), landscape(j+k));
            }
        }
    }
    //  et ID(I,J) pointe sur la fin de ligne avec poids i * mu
    for(int i = 1; i <= K; ++i){
        for(int j = 1; j <= N; ++j){
            graphe.add_to_graph(G2D.ID(i,j), G2D.ID(i,N+1),  CstAntiNeg + (Mu - mean_landscape) * (valeur) i);
        }
    }

    // -> la source pointe aussi sur la dest (pas de nucléosome), poids 0
    graphe.add_to_graph(G2D.source, G2D.arrivee, CstAntiNeg);
    for(int i = 1; i <= K; ++i){
        graphe.add_to_graph(G2D.ID(i, N+1), G2D.arrivee, 0.0);
    }

    cerr << "    -> Graphe construit.\n";

    all_paths_computed = false;
    nb_paths.clear();
    nb_paths_reverse.clear();
    big_table.clear();
    ensemble_solutions.clear();
    density.resize(N+2);

    cerr << "    -> Dijkstra ..." << endl;
    graphe.dijkstra(G2D.source, G2D.arrivee);
    cerr << "    -> Dijkstra reverse ..." << endl;
    graphe.dijkstra_reverse(G2D.arrivee, G2D.source);
    graphe.tri_topologique(G2D.source);
    cerr << "    -> Topological sorting ..." << endl;
    graphe.tri_topologique_reverse(G2D.arrivee);
    cerr << "    -> Reverse topological sorting ..." << endl;

    //graphe.show_graph(grille::showpostatic);
    compute_density_from_nb_paths();
    cerr << "------------------- ______________________________________________________ -------------------" << endl;

    show_density();

    cerr << endl << endl;
    show_one_best_solution();
    //export_graph();

    if(N < 201) {
        compute_density_from_all_paths();
        show_all_best_solutions();
        //show_density();
    }

    return;
}
*/







