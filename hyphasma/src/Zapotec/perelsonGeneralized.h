#ifndef PERELSONGENERALIZED_H
#define PERELSONGENERALIZED_H

#include "perelsonSequence.h"

enum clusterLCSaffinities{onlySpecificity /* default */, specificityAndAAStrength, specificityAndAccessibility, specUndefined};
/// NEGATIVE affinity VALUES / accessibility / strength are FORBIDDEN !!!!

struct perelsonGeneralized : public perelsonSequence {

   // Operators / functions to set up a new perelsonGeneralized
   perelsonGeneralized(int _size)                                : perelsonSequence(_size) {}
   perelsonGeneralized(string text, int _lcon = 0)               : perelsonSequence(text, _lcon) {}
   perelsonGeneralized(perelsonGeneralized* toCopy)              : perelsonSequence(toCopy) {}
   perelsonGeneralized(int _size, long long ID, int _lcon = 0)   : perelsonSequence(_size, ID, _lcon) {}

   // Data storage: inherited from perelsonSequence:
   // string content;
   // int size;
   // int lcon;

   // Accessing data
   // char operator [](int i);                   // inherited from perelsonSequence
   void operator = (perelsonGeneralized& B) {content = B.content; size = B.size; lcon = B.lcon;}
   bool operator == (perelsonGeneralized &b);

   // global parameters: alphabetsize, AAstrength & AAaccessibility + default parameters in case non specified inside affinity()
   static void initializeDefaultParameters(int _alphabetsize, double _specificity, vector<double> _AAstrength, vector<double> _AAaccessibility, clusterLCSaffinities _typeOfClusterAffinities);
   // static int alphabetsize;                   // inherited. note: inherited Threshold and exponent are useless.
   static std::vector<double> AAstrength;             // the AAstrength and AAaccessibility should be defined once for all
   static std::vector<double> AAaccessibility;
   static double specificity;
   static clusterLCSaffinities typeOfClusterAffinities;
   static bool initializedGeneralized;

   // Meaningful functions
   // void randomize();                            // inherited
   // void mutateOnePosition();                    // inherited
   static double affinity(perelsonGeneralized * x, perelsonGeneralized * y, double _specificity = -1, clusterLCSaffinities _typeOfClusterAffinities = specUndefined);
   static double hamming(perelsonGeneralized *x, perelsonGeneralized *y);

   // Documentation / Test functions
   string print();
   static string test();

   // tool functions:
   // 1 - what is the increase of weight by adding a new residue to an existing cluster ?
   static double upgrade(double previousAffinity, double sizeAlreadyStartedCluster, int positionToAddInV1, string & v1, int typeClusterAffinity, double specificityParameter);
   // 2 - test function for it
   static void testUpgrade();
   // 3 - giving the weight of having a particular cluster (from the epitope side, positions left and right) - function used for testing
   static double clusterWeight(perelsonGeneralized &a, int posLeft, int posRight, int typeClusterAffinity, double specificityParameter); // both positions are included
   // 3 - Find the weight of the optimal clusters.
   static pair<double, string> LCSclusters(string & v1, string & v2, int typeClusterAffinity, double specificityParameter);

   ~perelsonGeneralized(){}
};

/** Description: The perelson sequences is based on longuest common substrings (LCS) of matching residues
 *  Here, we want to include two additional effects:
 *      - that some matching residues could have higher binding
 *      - that successive matches (domains) could have stronger impact on affinity.
 *
 *  the different options are :
 *  in all cases, a specificity parameter (exponent to the cluster size) can be chosen. 1 = no impact of cluster size.
 *
 *  different options that modulate the strength of a cluster (before putting exponent specificity)
 *  1/ onlySpecificity: 1 point per matching residues, 0 for non-matcihing
 *  2/ specificityAndAAStrength: each residue match has a weight depending on the residue (wherever they are in the sequence)
 *  3/ specificityAndAccessibility: each position has a predefined accessibility on the EPITOPE side, not on the antibody. Not symmetrical !
 *
 *  The good thing is that all these cases can be solved by the same dynamic programming algorithm !
 *  the trick is to make a decision graph in 2D with always three choices: take a new residue, go down or go right in the LCS table.
 *  When taking a residue, the added value only depends on 1/the new residue (its accessibility or residue affinity) 2/ the exponent R
 *  and the size of the affinity of the cluster being built. the only information needed is the best cluster of every size reaching this position.
 */
#endif // PERELSONGENERALIZED_H
