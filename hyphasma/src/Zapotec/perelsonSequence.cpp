#include <cmath> // pow
#include "perelsonSequence.h"
#include "../Tools/zaprandom.h"
#include <set>

// tool functions, only in the .cpp (so this file can be used independently just along with random.h/cpp)
#include <sstream>
// note: these functions could be written as templates, but the output is different: char:no spacing, string:spacing, set<>: requires iterator
string pgPrintVector(vector<char> l){
    stringstream res;
    res << "[" << l.size() << "] ";
    for(int i = 0; i < (int) l.size(); ++i){
        res << l[i]; // << ((i == (int) l.size()-1) ? "" : "");
    }
    return res.str();
}
string pgPrintVector(vector<string> l){
    stringstream res;
    res << "[" << l.size() << "] ";
    for(int i = 0; i < (int) l.size(); ++i){
        res << l[i] << ((i == (int) l.size()-1) ? "" : " ");
    }
    return res.str();
}
template< typename T>
string pgPrintVector(set<T> l){
    stringstream res;
    res << "[" << l.size() << "] ";
    for(typename set<T>::iterator i = l.begin(); i != l.end(); ++i){
        res << *i << " ";
    }
    return res.str();
}




/// ============================ Part I: sequences and LCS-based affinity exactly according to Perelson papers =================================

perelsonSequence::perelsonSequence(string text, int _lcon){
   size = text.size();
   if(_lcon < 0) lcon = 0;
   if(_lcon > size) cerr << "ERR: perelsonSequence(int _size, long long ID, int _lcon), lcon is bigger than size" << endl;
   lcon = _lcon;
   content.resize(size, false);
   for (int i = 0; i < size; ++i){
      if((((int) (text[i] - 'A')) < 0) || (((int) (text[i] - 'A') >= alphabetsize))){
          cerr << "ERR:sequence(" << text << "), un-allowed character : " << text[i] <<
                  " for alphabetsize " << alphabetsize << endl;
      }
      content[i] = text[i];
   }
}

perelsonSequence::perelsonSequence(perelsonSequence *toCopy){
   size = toCopy->size;
   lcon = toCopy->lcon;
   content.resize(size);
   for (int i = 0; i < size; ++i){
      content[i] = toCopy->content[i];
   }
}
perelsonSequence::perelsonSequence(int _size){
   if (_size < 0) {_size = 0;}
   size = _size;
   lcon = size / 2;
   content.resize(_size, 'A');
}

char perelsonSequence::operator [](int i){
   if ((i < 0) || (i > size)){
      cerr << "sequence[" << i << "] out of bounds (size=" << size << ")\n";
      return '\0';
   }
   return content[i];
}

void perelsonSequence::initializeDefaultParameters(int _alphabetsize, double _threshold, double _exponent){
    alphabetsize = _alphabetsize;
    threshold = _threshold;
    exponent = _exponent;
    initialized = true;
}
int perelsonSequence::alphabetsize = 4;                     /// default 4, can be modified. Sequences will contain ABC... in THIS order !
double perelsonSequence::threshold = NAN;
double perelsonSequence::exponent = NAN;
bool perelsonSequence::initialized = false;



/// @brief Transform a long number into sequence in base alphabetsize, inside a given vector.
/// example: recursive_binary(2354, aboolvectortofill); Returns error if the number don't fit.
// Alternative is to use bitset, but size has to be known at compilation -> not convenient
void recursive_to_base(long ID, string &towrite, int base, int currentposition = -1){
   if (ID < 0) {cerr << "ERR::sequence::binary, negative number " << ID << endl; return;}
   if (currentposition < 0){                            // position to write next digit.
      currentposition = towrite.size() - 1;
   }
   if (currentposition >= (int) towrite.size()){
      cerr << "Err: sequence::binary(), currentposition is out of bounds ("
           << currentposition << "), while vector size is " << towrite.size() << endl;
      return;
   }
   // recursion is stopped when remainder is less than base
   if (ID < base) {towrite[currentposition] = 'A' + ID; return;}  // to stop recursion
   long remainder = ID % base;   
   towrite[currentposition] = 'A' + remainder;
   if(currentposition == 0) {cerr << "ERR: base " << base << " decomposition failed within "
                                  << towrite.size() << "positions. Number too big !" << endl;}
   recursive_to_base(ID / (long long) base, towrite, base, currentposition - 1);
}

perelsonSequence::perelsonSequence(int _size, long long ID, int _lcon) {
   // ': sequence(_size)' leads to a warning because only works in C++11, so copied constructor
   if (_size < 0) {_size = 0;}
   size = _size;
   if(_lcon < 0) lcon = 0;
   if(_lcon > size) cerr << "ERR: perelsonSequence(int _size, long long ID, int _lcon), lcon is bigger than size" << endl;
   lcon = _lcon;
   content.resize(_size, 'A');
   if(alphabetsize < 2) {cerr << "ERR: perelsonSequence(" << _size << "," << ID << ", alphabetsize should be >= 2" << endl; return; }
   recursive_to_base(ID, this->content, alphabetsize);
}


/// The affinity is defined as a formula from the size of the LCS between two sequences.
/// Here, usual algorithm to find LCS size (dynamical programming). return the size of LCS
/// use: LCSsize(seq1.content, seq2.content);
int LCSsize(string & v1, string & v2){
    int M = v1.size();
    int N = v2.size();
    vector<int> table(M*N, 0);
    for(int i = 0; i < M; ++i){
        table[i*N] = (v1[i] == v2[0]) ? 1 : 0;
    }
    for(int j = 1; j < N; ++j){
        table[j] = (v1[0] == v2[j]) ? 1 : 0;
    }
    for(int i = 1; i < M; ++i){
        for(int j = 1; j < N; ++j){
            if(v1[i] == v2[j]){
                table[i*N+j] = table[(i-1)*N + j-1] + 1;
            } else {
                table[i*N+j] = max(table[i*N+j-1], table[(i-1)*N+j]);
            }
        }
    }

    return table[(M-1)*N + N-1];
}

/// Same, but use: LCSsize(seq1, seq2);
int LCSsize(perelsonSequence &a, perelsonSequence &b){
    return LCSsize(a.content, b.content);
}


/// Tool function : same algorithm to find LCS but returns the table used to find the LCS.
// use: LCStable(seq1.content, seq2.content);
vector<int> LCStable(string & v1, string & v2){
    int M = v1.size();
    int N = v2.size();
    vector<int> table(M*N, 0);
    for(int i = 0; i < M; ++i){
        table[i*N] = (v1[i] == v2[0]) ? 1 : 0;
    }
    for(int j = 1; j < N; ++j){
        table[j] = (v1[0] == v2[j]) ? 1 : 0;
    }
    for(int i = 1; i < M; ++i){
        for(int j = 1; j < N; ++j){
            if(v1[i] == v2[j]){
                table[i*N+j] = table[(i-1)*N + j-1] + 1;
            } else {
                table[i*N+j] = max(table[i*N+j-1], table[(i-1)*N+j]);
            }
        }
    }
    if(false){ // if you want to print the table. if false, will be cut out by compiler
        cerr << "LCStable" << v1 << " , " << v2 << endl;
        for(int i = 0; i < M; ++i){
            for(int j = 0; j < N; ++j){
                cerr << table[i*N+j] << " ";
            }
            cerr << endl;
        }
        cerr << " LCS = " << table[(M-1)*N + N-1] << endl;
    }
    return table;
}

// Note: downstream you'll find a function that returns all possible LCS of 2 strings.



/// Affinity function for perelson representation of sequences (needs a threshold and exponent)
/// this function is not symmetrical !! it is the fraction of the AB which is bound, not the target !
double perelsonSequence::affinity(perelsonSequence * AB, perelsonSequence * y, double _threshold, double _exponent)
{
    if(!initialized) cerr << "ERR: perelsonSequence has not be given default parameters, especially alphabetsize";
    if(_exponent < 0){_exponent = exponent;}
    if(_threshold < 0){_threshold = threshold;}

    if(threshold > 1-1e-6) cerr << "ERR: perelson sequences can not be used with a threshold of 1 !!!" << endl;
    double LCS = LCSsize(AB->content, y->content);
    //cerr << "LCS = " << LCS << " fraction = " << (LCS / (double) AB->content.size()) << " Norm [0-1] " << ((LCS / (double) AB->content.size()) - threshold) / (1-threshold) << " with threshold " << threshold << " and exp " << exponent << endl;
    return pow(max(0., ((LCS / (double) AB->content.size()) - _threshold) / (1-_threshold)), _exponent);
}


/// here, will define hamming as the minimal number of mutations to get maximal binding.
/// I think this is max LCS - real LCS, but I am not sure it is always possible to reach
/// the optimal by only few mutations. Maybe need to move the clusters => more mutations.
/// how are increasing mutations ?? is there an optimal choice ?
double perelsonSequence::hamming(perelsonSequence * x, perelsonSequence * y) {
   int LCS = LCSsize(x->content, y->content);
   return min(x->content.size() , y->content.size()) - LCS;
}



// Be careful, here it is a Non Synonymous mutation, (proba (AlphaSize - 1) / AlphaSize return -1: fail, 0:silent, 1:success
int perelsonSequence::mutateOnePosition() {
   int position = random::uniformInteger(0,size-1);
   if ((position >= size) || (position < 0)) {
      cerr << "Random does shit, sequencespace.cpp::mutateOnePosition";
      return -1;
   }
   int currentChar = content[position] - 'A';
   // to avoid to mutate and get back to the previous char, move in a round from the previous character.
   int newChar = random::uniformInteger(0,alphabetsize-2);
   if(newChar >= currentChar) content[position] = 'A' + newChar + 1;
   else content[position] = 'A' + newChar;
   //cerr << (char) ('A' + currentChar) << content[position] << endl;
   //char newChar = 'A' + ((currentChar + irandom(alphabetsize-1)) % alphabetsize);
   //content[position] = newChar;
   return +1; // always success
}
// need a maximum and minimum size. Affinity 0 means die.
/*void perelsonSequence::randomInsertion(int _size) {
   //int positionStart = irandom(size);
}
void perelsonSequence::randomDeletion(int _size) {

}*/

void perelsonSequence::randomize() {
    for (int i = 0; i < size; ++i) {
        content[i] =  'A' + random::uniformInteger(0,alphabetsize-1); // hope it doesn't exceed ....
        if((content[i] - 'A') >=  alphabetsize) cerr << "perelsonSequence::randomize() Out of bounds for random generator" << endl;
    }
}

bool perelsonSequence::operator ==(perelsonSequence &b) {
    if(size != b.size) return false;
    if(lcon != b.lcon) return false; // controversial, but still, even if same sequence, should return false.
    for (int i = 0; i < size; ++i) {
       if((*this)[i] != b[i]) return false;
    }
    return true;
}

string perelsonSequence::print() {
   stringstream out;
   for (int i = 0; i < size; ++i) {
      out << content[i];
   }
   return out.str();
}






string printPerelsonSeqAff(perelsonSequence * S1, perelsonSequence *S2, double threshold, double exponent){
    static int lastSizeAlphabet= -1;
    stringstream res;
    if(perelsonSequence::alphabetsize != lastSizeAlphabet){
        lastSizeAlphabet = perelsonSequence::alphabetsize;
        res << "\nFrom now, alphabet size is : " << perelsonSequence::alphabetsize << endl;
    }
    res << "LCS Aff of " << S1->print() << " - \n"
        << "           " << S2->print() << " = "
        << perelsonSequence::affinity(S1, S2, threshold, exponent) << "\t with exp. " << exponent << " and threshold " << threshold << endl;
    res << "\n";
    return res.str();
}


set<string> AllLCSRecursive(vector<int>& table, string & v1, string & v2, int i, int j, vector< set<string>* >&  memory, bool takeAll);
vector<string> AllLCS(string v1, string v2, bool takeAll){
    // takeAll means: show all possible substrings, not only biggest. Will be important to check the more general case.
    int M = v1.size();
    int N = v2.size();
    // table should be precomputed because the recursive LCS does not follow the good order of calls.
    vector<int> table = LCStable(v1, v2);
    vector< set<string> *> memory = vector< set<string> *> (N*M, NULL);
    set <string> result1 = AllLCSRecursive(table, v1, v2, M-1, N-1, memory, takeAll);
    vector<string> result(result1.begin(), result1.end()); // apparently, this converts set to vector

    if(false){ // displaying information
        cout << "Computing all LCS between " << v1 << " and " << v2 << endl;
        // The table doesn't neeed to be computed
        for(int j = 0; j < N; ++j){
            cout << "\t" << j << v2[j];
        }
        cout << endl;
        for(int i = 0; i < M; ++i){
            cout << i << v1[i] << ":|";
            for(int j = 0; j < N ; ++j){
                cout << "\t" << table[i*N+j];
            }
            cout << endl;
        }
        cout << "Combinations of LCS : " << endl;
        for(int j = 0; j < N; ++j){
            cout << "\t\t" << j << v2[j];
        }
        cout << endl;
        for(int i = 0; i < M; ++i){
            cout << i << v1[i] << ":|";
            for(int j = 0; j < N ; ++j){
                cout << "\t\t" << (memory[i*N+j] ? pgPrintVector(*(memory[i*N+j])) : string("-"));
            }
            cout << endl;
        }
    }
    for(unsigned int i = 0; i < memory.size(); ++i){
        if(memory[i]) delete memory[i];
    }
    return result;
}

vector<string> filterSizeElements(vector<string> source, int sizeminincluded, int sizemaxincluded = 1e8){
    int L = source.size();
    vector<string> res;
    for(int i = 0; i < L; ++i){
        int LS = source[i].size();
        if((LS >= sizeminincluded) && (LS <= sizemaxincluded)){
            res.push_back(source[i]);
        }
    }
    return res;
}

// Note : this function is made slow, and memory consuming (memory), because only used for tests.
set<string> AllLCSRecursive(vector<int>& table, string & v1, string & v2, int i, int j, vector< set<string> * >&  memory, bool takeAll){
    // Note: all the blocks are not necessarily called, because the table says where to look for ! this is quite fast ...
    int N = v2.size();
    set<string> res = set<string>();
    if((i == -1) || (j == -1)){
        return res;
    }
    // Dynamic programing, to avoid calling two times the same (i,j). If not, becomes exponential.
    if(memory[i*N+j] != NULL) return (*(memory[i*N+j]));

    if(v1[i] == v2[j]){
        //if(i*j != 0) table[i*N+j] = table[(i-1)*N + j-1] + 1; // not for i==0 or j==0
        set<string> toFuse = AllLCSRecursive(table, v1, v2, i-1, j-1, memory, takeAll);
        if(toFuse.size() == 0) res.insert(string(&v1[i],1));
        else {
            for(set<string>::iterator k = toFuse.begin(); k != toFuse.end(); ++k){
                res.insert(*k + string(&(v1[i]),1)); // string takes a char*
            }
        }
        if(takeAll){
            res.insert(string(&(v1[i]),1));
        }
    }
    if((v1[i] != v2[j]) || takeAll){
        if(i + j > 0){
            // two cases: if both predecessors have same numbers, merge. If not, then only the biggest will contribute to the future LCS
            if((i == 0) || ((j > 0) && (table[i*N+j-1] >= table[(i-1)*N+j]))  || ((j > 0) && (takeAll))){   // if j = 0, copy anyways. Hope it does not enter the second part of ||
                set<string> toAdd = AllLCSRecursive(table, v1, v2, i, j-1, memory, takeAll);
                res.insert(toAdd.begin(), toAdd.end());
            }
            if((j == 0) || ((i > 0) && (table[(i-1)*N+j] >= table[i*N+j-1])) || ((i > 0) && (takeAll))){   // if i == 0 anyways will be ok to copy
                set<string> toAdd = AllLCSRecursive(table, v1, v2, i-1, j, memory, takeAll);
                res.insert(toAdd.begin(), toAdd.end());
            }
        }
    }

    memory[i*N+j] = new set<string>(res);
    //cerr << "i" << i << "j" << j << pgPrintVector(res) << endl;
    return res;
}






string perelsonSequence::test(){
#define MODE_OUT cerr
    MODE_OUT << "================ Test constructor / basic functions =================" << endl;
    perelsonSequence S1(5);
    MODE_OUT << "S1: Empty sequence, size 5 " << S1.print() << "\n\n";
    perelsonSequence S2(string("ABDBCBDABDCCADB"));
    MODE_OUT << "S2: Sequence from binary text ABDBCBDABDCCADB -> " << S2.print() << "\n\n";
    perelsonSequence S3(string("AABCGSTHDY01oAX5!="));
    MODE_OUT << "S3: Sequence from text with bad characters 'AABCGSTHDY01oAX5!=' ->" << S3.print() << "\n\n";
    perelsonSequence S4(&S2);
    MODE_OUT << "S4: Copy from previous sequence S2 " << S4.print() << "\n\n";
    perelsonSequence S4b(15);
    S4b = S2;
    MODE_OUT << "S4: = from previous sequence S2 " << S4b.print() << "\n\n";

    MODE_OUT << "Testing the IDs with different alphabetsizes" << endl;
    for(int i = 2; i < 6; ++i){
        perelsonSequence::alphabetsize = i;
        long long int x5 = 158230;
        int size5 = 10;
        perelsonSequence S5a(2*size5, x5); // size 10
        MODE_OUT << "S5a: Sequence in base " << i << ": " << S5a.print() << " from " << x5 << " with size " << 2*size5 << "\n\n";
        perelsonSequence S5b(size5, x5); // size 10
        MODE_OUT << "S5b: Sequence in base " << i << ": " << S5b.print() << " from " << x5 << " with size " << size5 << "\n\n";
    }
    perelsonSequence::alphabetsize  = 4;


    MODE_OUT << "Accessing positions of S5b one by one [] ";
    MODE_OUT << S2[0] << S2[1] << S2[2] << "..." << S2[S2.size-1] << "\n\n";
    MODE_OUT << "Now testing bad positions : ";
    MODE_OUT << S2[-1] << S2[S2.size] << "\n\n";
    MODE_OUT << "S6: Randomized sequence ";
    perelsonSequence S6(10);
    S6.randomize();
    MODE_OUT << S6.print() << "\n\n";
    MODE_OUT << "S7: Randomized sequence, from an existing one ";
    perelsonSequence S7(15,257);
    S7.randomize();
    MODE_OUT << S7.print() << "\n\n";
    MODE_OUT << "Consecutive mutations" << endl;
    perelsonSequence S8("BBBBBBBBBB");
    for(int i = 0; i < 20; ++i){
        S8.mutateOnePosition();
        MODE_OUT << S8.print() << endl;
    }
    perelsonSequence S9(0);
    S9 = S8;
    MODE_OUT << "Compare equal sequences : " << S8.print() << ((S8==S9) ? "=" : "!=")  << S9.print() << "\n\n";
    S9.mutateOnePosition();
    MODE_OUT << "Compare different sequences : " << S8.print() << ((S8==S9) ? "=" : "!=")  << S9.print() << "\n\n";
    S9.randomize();
    MODE_OUT << "Random sequence: " << S9.print() << endl;

    MODE_OUT << "Test affinity functions on single examples" << "\n";
    perelsonSequence::alphabetsize = 5;
    perelsonSequence S10("AAAAAAAAAA");   // reference sequence AG1
    perelsonSequence S11("AABCDDCBAA");   // reference sequence AG2
    perelsonSequence S21("AABBCCDDEE");   // match 4+5 to AG1, 4+3 to AG2
    perelsonSequence S22("EEDDCCBBAA");   // match 1+3+3 to AG1, 1+3+3 to AG2

    MODE_OUT << printPerelsonSeqAff(&S21, &S10, 0.5, 1.0) << endl;
    MODE_OUT << printPerelsonSeqAff(&S21, &S11, 0.5, 1.0) << endl;
    MODE_OUT << printPerelsonSeqAff(&S22, &S10, 0.5, 1.0) << endl;
    MODE_OUT << printPerelsonSeqAff(&S22, &S11, 0.5, 1.0) << endl;

    MODE_OUT << "Test LCS subfunctions" << "\n";
    perelsonSequence::alphabetsize = 4;

    MODE_OUT << "Show longuest substring after consecutive mutations" << endl;
    perelsonSequence S32("BBBBBBBBBB");
    perelsonSequence S32b = S32;
    for(int i = 0; i < 20; ++i){
        S32.mutateOnePosition();
        MODE_OUT << S32.print() << " with LCS to original " << LCSsize(S32, S32b) << "-" << LCSsize(S32b, S32)  << " and hamming (mut nr) " << perelsonSequence::hamming(&S32, &S32b) << endl;
    }
    MODE_OUT << "Now with a 2-char sequence " << endl;
    perelsonSequence S33("AABBAABBAABB");
    perelsonSequence S33b = S32;
    for(int i = 0; i < 20; ++i){
        S33.mutateOnePosition();
        MODE_OUT << S33.print() << " with LCS to original " << LCSsize(S33, S33b) << "-" << LCSsize(S33b, S33)  << " and hamming (mut nr) " << perelsonSequence::hamming(&S33, &S33b) << endl;
    }
    MODE_OUT << "And with a complex one " << endl;
    perelsonSequence S34("CADBCBADCCAB");
    perelsonSequence S34b = S34;
    for(int i = 0; i < 20; ++i){
        S34.mutateOnePosition();
        MODE_OUT << S34.print() << " with LCS to original " << LCSsize(S34, S34b) << "-" << LCSsize(S34b, S34)  << " and hamming (mut nr) " << perelsonSequence::hamming(&S34, &S34b) << endl;
    }

    perelsonSequence::alphabetsize = 5;
    MODE_OUT << "\nShowing the table of all possible LCS" << endl;
    perelsonSequence S35("ABCD");
    perelsonSequence S36("CBCAB");
    MODE_OUT << "All possible LCS for " << S35.print() << endl;
    MODE_OUT << "                 and " << S36.print() << " are " << pgPrintVector(AllLCS(S35.content, S36.content)) << endl;

    MODE_OUT << "\nShowing the table of all possible LCS" << endl;
    perelsonSequence S37("ABAEDBCBAC");
    perelsonSequence S38("BDCDBCCEA");
    MODE_OUT << "All possible LCS for " << S37.print() << endl;
    MODE_OUT << "                 and " << S38.print() << " are " << pgPrintVector(AllLCS(S37.content, S38.content)) << endl;

    MODE_OUT << "\nShowing the table of all possible LCS" << endl;
    perelsonSequence S39(25);
    S39.randomize();
    perelsonSequence S40(25);
    S40.randomize();
    MODE_OUT << "All possible LCS for " << S39.print() << endl;
    MODE_OUT << "                 and " << S40.print() << " are " << pgPrintVector(AllLCS(S39.content, S40.content)) << endl;

    MODE_OUT << "\nNow Showing the table of all possible CS (not longuest)" << endl;
    perelsonSequence S41(25);
    S41.randomize();
    perelsonSequence S42(25);
    S42.randomize();
    MODE_OUT << "All possible LCS for " << S41.print() << endl;
    MODE_OUT << "                 and " << S42.print() << " of size more than 9 are " << pgPrintVector(filterSizeElements(AllLCS(S41.content, S42.content, true), 9)) << endl;


    //vector<string> toPrint = AllLCS(S30.content, S31.content);
    //pgPrintVector(toPrint);
    return string(""); // if MODE_OUT is stringstream, return .str().
}
