#ifndef LCSperelson_h
#define LCSperelson_h

#include <iostream>
#include <vector>
#include <string>
using namespace std;

struct perelsonSequence {

   // Operators / functions to set up a new perelsonSequence
   perelsonSequence(int _size);                  /// sequence of 'A's of this size, half is variable
   perelsonSequence(string text, int _lcon = 0); /// string of characters (A-Z). Other characters become 'A'
   perelsonSequence(perelsonSequence * toCopy);
   perelsonSequence(int _size, long long ID, int _lcon = 0);    /// long long int to [Alphabet size]^N. Max 2^63

   // Data storage
   string content;
   size_t size;
   int lcon;

   // Accessing data
   char operator [](int i);                     /// Note : can not be used for assigning values
   void operator = (perelsonSequence& B) {content = B.content; size = B.size; lcon = B.lcon;}
   bool operator == (perelsonSequence &b);

   // global parameters: alphabetsize; + default parameters in case non specified inside affinity()
   static void initializeDefaultParameters(int _alphabetsize, double _threshold, double _exponent);
   static int alphabetsize;                     /// Note: only characters allowed are ABC... in this order !
   static double threshold;
   static double exponent;
   static bool initialized;

   // Meaningful functions:
   void randomize();                            /// Randomize every position (uniform chance for each character)
   int mutateOnePosition();                    /// pick random position and changes character with uniform proba for other characters
   static double affinity(perelsonSequence * x, perelsonSequence * y, double _threshold = -1, double _exponent = -1); // not virtual
   static double hamming(perelsonSequence *x, perelsonSequence *y);         /// min number of mutations to reach target. Symmetrical

   // Documentation / Test functions
   virtual std::string print();
   static string test();

   virtual ~perelsonSequence(){}

   // note: some functions are virtual because perelsonGeneralized extends this class with more options.
};

// tool function that could be used outside
int LCSsize(string &v1, string &v2);
vector<string> AllLCS(string v1, string v2, bool takeAll = false);



/** Infos: taken from paper : Competitive exclusion by autologous antibodies can prevent HIV-1
 * antibodies from arising - Luo 2015, PNAS
 * Idea : a sequence is a string wih an alphabet of size 4. A virus is 2 sequences : 8 chars of variable, and 8 chars of constant.
 *        an antibody is a sequence of size 8 as well. Affinity =
 *
 * Sequence parameters :
 *  k = 4      size of the alphabet
 *  la = 8     length of antibodies
 *  notation: 'vvar' and 'vcon' are the two parts of the virus
 *  lvar = 8   length of vvar
 *  lcon = 8   length of vcon
 *  => here, sequence have their own size and conserved part, so can have antigens and antibodies with diff. properties
 * Affinity : (LCSS ) longest common subsequence
 *    to the common part : AffCon =  max(0, 2*(lLCSS / lvar - 0.5))
 *    to the variable part : Affvar =  max(0, 2*(lLCSS / lcon - 0.5)^c)
 *       ie. affinity starts when more than 50% of the part is recognized
 *    Affinity = Affcon + Affvar
 *
 * affinity parameters :
 *  c = 1.5  (>1)       = hardness to recognize the conserved epitope.
 *                        they say value 1.5 means the match should be of 1 more
 *                        element to equal the variable region in affinity.
 *  tMatchMin = 0.5     = minimum to be recognized to give an affinity.
 */


#endif
