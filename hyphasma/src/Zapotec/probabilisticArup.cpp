//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///
///
///         3 - SEQUENCES IN ARUPS AFFINITY MODEL
///
///
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <cmath> //fabs
#include <sstream>
#include "../Tools/zaprandom.h"
#include "probabilisticArup.h"
/* ---- Constructors ---- */

int arupProtein::arup_length_sequences = 46;
int arupProtein::arup_N_conserved = 18;
int arupProtein::arup_N_mutates = 22;
int arupProtein::arup_N_shielded = 6;
double arupProtein::arup_alpha = 2.0;
double arupProtein::arup_h_min = -0.18;
double arupProtein::arup_h_max = 0.9;
double arupProtein::arup_hprime_min = -1.5;
double arupProtein::arup_hprime_max = 1.5;
double arupProtein::arup_hmut_min = -1.5;
double arupProtein::arup_hmut_max = 1e6;
probaLawFromTable* arupProtein::lawMutations = NULL;

/*void arupProtein::set_statics(Parameter &par, ofstream &ana) {
   arup_length_sequences = par.Value.arup_length_sequences;
   arup_N_conserved = par.Value.arup_N_conserved;
   arup_N_mutates = par.Value.arup_N_mutates;
   arup_N_shielded = par.Value.arup_N_shielded;
   arup_alpha = par.Value.arup_alpha;
   arup_hprime_min = par.Value.arup_hprime_min;
   arup_hprime_max = par.Value.arup_hprime_max;
   arup_hmut_min = par.Value.arup_hmut_min;
   arup_hmut_max = par.Value.arup_hmut_max;
   arup_h_min = par.Value.arup_h_min;
   arup_h_max = par.Value.arup_h_max;
   lawMutations = new probaLawFromTable(par.Value.arup_law_mut_Xs,
                                        par.Value.arup_law_mut_Densities,
                                        true);
   ana << "Initializing parameters for Arup Sequences : \n";
   ana << "   Number of residues : " << arup_length_sequences << " among which \n";
   ana << "    - " << arup_N_conserved << " conserved\n";
   ana << "    - " << arup_N_mutates << " mutables\n";
   ana << "    - " << arup_N_shielded << " shielded\n";
   ana << "   Unshielding coefficient (alpha) = " << arup_alpha << "\n";
   ana << "   Initial boundaries for each position in a sequence : [" << arup_h_min << " ; "
       << arup_h_max << "]\n";
   ana << "   Boundaries for mutable positions during mutation   : [" << arup_hmut_min << " ; "
       << arup_hmut_max << "]\n";
   ana << "   Boundaries for shielded positions during mutation  : [" << arup_hprime_min << " ; "
       << arup_hprime_max << "]\n";
   ana << "   Law of probabilities to generate mutations : " << lawMutations->print() << "\n\n";
}
*/
// I DO NOT UNDERSTAND THE FUCKING TARGET HERE
// check if irandom reaches the upper boundary
/* ---- Asking a arupProtein to mutate ---- */

int ArupBCR::mutateOnePosition(double targetChangeEnergy) {
   int posToMutate = random::uniformInteger(0,arup_length_sequences - 1); // check boundaries
   double deltaE = lawMutations->getRandValue();
   if(!isnan(targetChangeEnergy)) deltaE = targetChangeEnergy;
   double deltaHK = deltaE;   // because target is wild-type ???
   content[posToMutate] += deltaHK;
   if (posToMutate >= arup_N_conserved + arup_N_mutates) {
      // if in the shielded area,
      int posCompensation = random::uniformInteger(0,arup_N_conserved - 1);
      double hprime = content[posCompensation] - arup_alpha * (deltaHK);
      hprime = max(arup_hprime_min, hprime);
      hprime = min(arup_hprime_max, hprime);
      content[posCompensation] = hprime;
   }
   if ((posToMutate >= arup_N_conserved) && (posToMutate < (arup_N_conserved + arup_N_mutates))) {
      // if mutates in the variable area
      content[posToMutate] = max(arup_hmut_min, content[posToMutate]);
      content[posToMutate] = min(arup_hmut_max, content[posToMutate]);
   }
   return +1; // success
}
void ArupBCR::randomize(double targetEnergy, bool beExact) {
   arupProtein WildType = arupProtein();  // by default, only 1s (see empty constructor)
   // strategy 1 : generate randomly up to a good enough energy. In case it doesn't work, keeps the
   // best;
   arupProtein bestSoFar = arupProtein();
   double bestAffinity = -1e6;
   int count = 0;
   while ((count < 10000) && (bestAffinity < targetEnergy)) {
      for (int i = 0; i < size; ++i) {
         content[i] = arup_h_min + random::uniformDouble(0,1) * (arup_h_max - arup_h_min);
      }
      double newAffinity = (affinity(this, &WildType));
      if (newAffinity > bestAffinity) {
         bestAffinity = newAffinity;
         bestSoFar.copy(this);
      }
      ++count;
   }
   if ((!beExact) && (bestAffinity > targetEnergy)) {
      arupProtein::copy(&bestSoFar);   // this is a bit weird because you are a BCR , but you just
                                       // copy the ArupProtein part ...
      return;
   }
   // strategy 2 : Now mutates to reach the target (being exact and/or being high enough);
   double targetChange = targetEnergy - bestAffinity;
   int nbTries = 0;
   while ((nbTries < 10)
          && ((beExact && (fabs(targetChange) > 1e-4)) || ((!beExact) && (targetChange > 0)))) {
      mutateOnePosition(targetChange); // always success
      targetChange = targetEnergy - affinity(this, &WildType);
      nbTries++;
   }
}
void ArupAntigen::randomize(int nbResiduesToMutate) {
   if (nbResiduesToMutate < 0) {
      for (int i = arup_N_conserved; i < arup_N_conserved + arup_N_mutates; ++i) {
         if (random::uniformDouble(0,1) < 0.5) { content[i] = -content[i]; }
      }
   } else {
      vector<bool> mutatedPos = vector<bool> (arup_length_sequences, false);
      int nbTries = 0;
      int nbMuts = 0;
      while ((nbTries < 100) && (nbMuts < nbResiduesToMutate)) {
         int pos = arup_N_conserved + random::uniformInteger(0,arup_N_mutates - 1);
         if (!mutatedPos[pos]) {
            content[pos] = -content[pos];
            mutatedPos[pos] = true;
            nbMuts++;
         }
      }
   }
   // a stupid check, in case mutations occurs outside the area ...
   for (int i = 0; i < arup_N_conserved; ++i) {
      if (fabs(content[i] - 1.0)
          > 1e-9) {
         cerr
            <<
            "ERR : ArupAntigen::randomize, this antigen has problems with conserved residues that are not 1"
            << endl;
      }
   }
   for (int i = arup_N_conserved + arup_N_mutates; i < arup_length_sequences; ++i) {
      if (fabs(content[i] - 1.0)
          > 1e-9) {
         cerr
            <<
            "ERR : ArupAntigen::randomize, this antigen has problems with shielded residues that are not 1"
            << endl;
      }
   }
}
void ArupAntigen::mutate() {
   int pos = arup_N_conserved + random::uniformInteger(0,arup_N_mutates - 1);
   content[pos] = -content[pos];
}
void arupProtein::clear() {
   size = arup_length_sequences;
   content.resize(size, 1.0);   // this 1 is very important because the wild-type antigen is 111111
}
int arupProtein::operator [](int i) {
   if ((i < 0)|| (i > size)) {
      cerr << "arupProtein[" << i << "] out of bounds (size=" << size << ")\n";
      return 0;
   }
   return content[i];
}
arupProtein::arupProtein(vector<double> numbers) {
   clear();
   if ((int) numbers.size()
       != arup_length_sequences) {
      cerr
         << "ERR: arupProtein::arupProtein(vector), sequences should be of size "
         << arup_length_sequences << " while the input vector has " << numbers.size() << endl;
      return;
   }
   for (int i = 0; i < size; ++i) {
      content[i] = numbers[i];
   }
}
arupProtein::arupProtein(string numbersInText) {
   clear();
   stringstream transform(numbersInText);
   double buffer;
   content.clear();
   while ((transform >> buffer) && (transform.str().size() > 0)) {
      content.push_back(buffer);
   }
   if ((int) content.size() != arup_length_sequences) {
      cerr << "ERR: arupProtein::arupProtein(vector), sequences should be of size "
           << arup_length_sequences << " while the input string of numbers has " << content.size()
           << endl;
      content.resize(arup_length_sequences);
      return;
   }
}
arupProtein::arupProtein(arupProtein * toCopy) {
   size = toCopy->size;
   content.resize(size);
   for (int i = 0; i < size; ++i) {
      content[i] = toCopy->content[i];
   }
}
void arupProtein::copy(arupProtein * toCopy) {
   if (toCopy->size
       != size) { cerr << "ERR:  arupProtein::copy, copying sequences of different size" << endl; }
   for (int i = 0; i < size; ++i) {
      content[i] = toCopy->content[i];
   }
}
arupProtein::arupProtein() {
   clear();
}
arupProtein::~arupProtein() {
   clear();
   cerr << "~arupProtein() Should be reimplemented by daughter classes" << endl;
}
double arupProtein::affinity(arupProtein * BCR, arupProtein * Antigen) {
   if (BCR->size != Antigen->size) {
      cerr << "ERR: arupProtein::affinity, comparing sequences of different sizes" << endl;
   }
   double res = 0;
   for (int i = 0; i < BCR->size; ++i) {
      res += (*BCR)[i] * (*Antigen)[i];
      if ((i < arup_N_conserved) && (fabs(((*Antigen)[i]) - 1.0) > 1e-12)) {
         cerr << "ERR: arupProtein::affinity, conserved positions in the antigen should be 1. Antigen = "
              << Antigen->print() << endl;
      }
      if ((i >= (arup_N_conserved + arup_N_mutates)) && (fabs(((*Antigen)[i]) - 1.0) > 1e-12)) {
         cerr << "ERR: arupProtein::affinity, shielded positions in the antigen should be 1. Antigen = "
            << Antigen->print() << endl;
      }
   }
   return res;
}
double arupProtein::hamming(arupProtein * x, arupProtein * y) {
   if (x->size != y->size) {
      std::cout << "Err affinity: length of antigen and BCR are not identical!";
      return -1;
   }
   int length = x->size;
   int sum = 0;
   for (int i = 0; i < length; ++i) {
      sum += fabs((*x)[i] - (*y)[i]);
   }
   return sum;
}
bool arupProtein::operator ==(arupProtein &b) {
   bool res = (size == b.size);
   if (res) {
      for (int i = 0; i < size; ++i) {
         res = res && (content[i] == b[i]);
      }
   }
   return res;
}

string arupProtein::print() {
   stringstream out;
   for (int i = 0; i < size; ++i) {
      out << content[i] << " ";
   }
   return out.str();
}
/*string arupProtein::typeInString() {
   switch (getType()) {
      case typeArupAG:{
         return string("ArupAntigene");
         break;
      }
      case typeArupBCR:{
         return string("ArupBCR/Antibody");
         break;
      }
      case numberTypesArup:{
         return string("ArupUnknown");
         break;
      }
   }
   return string("Incorrect type");
}*/




string arupProtein::testeAffinityFunctions() {
/*
 *   stringstream out;
 *   out << "Testing the properties ot the affinity function for the following parameters : \n";
 *   out << "   ->     L= " << L << "\t(Size of arupProteins)" << endl;
 *   out << "   ->     R= " << R << "\t(specificity parameter)" << endl;
 *   out << "   -> maxCl= " << maxClusters << "\t(cluster size scale)" << endl;
 *   switch(typeAffinityFunction){
 *       case seqAff:{out << "   -> Using standard affinity (Saham's)\n"; break;}
 *       case seqAffNorm:{out << "   -> Using standard affinity normalized by maxCl^r\n"; break;}
 *       case seqAffWindow:{out << "   -> Using the maximum affinity of a sliding window\n"; break;}
 *   }
 *
 *   out << "==== Part 1 : enumerates all (if possible), or a lot of arupProteins and sort them by
 * affinity to get the best ones : ====" << endl;
 *
 #define resolutiondistrib 100
 #define maxarupProteinsToEnumeate 1100000
 *
 *   vector< pair<double, arupProtein*> > store;
 *   arupProtein* ref = new arupProtein(L);
 *   vector<double> distribution;
 *   distribution.resize(resolutiondistrib + 1);
 *
 *   int total = 0;
 *   int maxim = pow(2, L);
 *   if(L > 26) maxim = maxarupProteinsToEnumeate + 1;  // to avoid it to become negative ...
 *   bool enumerateAll = (maxim < maxarupProteinsToEnumeate);
 *   if(enumerateAll){
 *       for(int i = 0; i < maxim; ++i){
 *           arupProtein* a = new arupProtein(L, (long) i);
 *           double affi = arupProtein::seq_affinity(a, ref, R, maxClusters, typeAffinityFunction);
 *           distribution[(int) (((double) resolutiondistrib)*affi)] += 1.0;     // put into the
 * histogram classes for affinity
 *           store.push_back( pair<double, arupProtein*>(affi,a));
 *           total++;
 *       }
 *   } else {
 *       for(int i = 0; i < maxarupProteinsToEnumeate; ++i){
 *           arupProtein* a = new arupProtein(L);
 *           a->randomize();
 *           double affi = arupProtein::seq_affinity(a, ref, R, maxClusters, typeAffinityFunction);
 *           distribution[(int) (((double) resolutiondistrib)*affi)] += 1.0;
 *           store.push_back( pair<double, arupProtein*>(affi,a));
 *           total++;
 *       }
 *   }
 *
 *   out << "Distribution of affinities\n";
 *   for(int i = 0; i < (int) distribution.size(); ++i){
 *       distribution[i] /= (double) total;
 *       out << i << "\t" << distribution[i] << "\t" << double(i) * (1.0 / (double)
 * resolutiondistrib) << "\t" << double(i + 1) * (1.0 / (double) resolutiondistrib) << endl;
 *   }
 *
 *   out << "\narupProteins and affinity, " << ((enumerateAll) ? " in the order of ID\n" : "
 * randomly generated\n");
 *   for(int i = 0; i < 200; ++i){
 *       out << i << "\t" << store[i].second->print() << "\t" << store[i].first << "\n";
 *   }
 *
 *   out << "\narupProteins, sorted from the best, out of the " << min(maxim, (int)
 * maxarupProteinsToEnumeate) << " evaluated arupProteins\n";
 *   std::sort(store.begin(), store.end(), comparupProteins);
 *   for(int i = 0; i < 200; ++i){
 *       out << i << "\t" << store[i].second->print() << "\t" << store[i].first << "\n";
 *   }
 *   if(enumerateAll){
 *   out << "\nAffinity of arupProteins taken randomly\n";
 *       for(int i = 0; i < 100; ++i){
 *           arupProtein* seqtmp = new arupProtein(L);
 *           seqtmp->randomize();
 *           out << i << "\t" << arupProtein::seq_affinity(seqtmp, ref, R, maxClusters,
 * typeAffinityFunction) << "\t" << seqtmp->print() << "\n";
 *       }
 *   }
 *   for(int i = 0; i < (int) store.size(); ++i)
 *       delete store[i].second;
 *
 *
 *
 *   out << "==== Part 2 : Evaluating cross-reactivity in the system : ====" << endl;
 *
 *   int nbAntigens = 10;
 *   out << "Generating randomly " << nbAntigens << " antigens " << endl;
 *   vector<arupProtein*> ags;
 *   for(int i = 0; i < nbAntigens; ++i){
 *       arupProtein* seq = new arupProtein(L);
 *       seq->randomize();
 *       ags.push_back(seq);
 *       out << "\tAg nr " << i << "\t" << seq->print() << endl;
 *   }
 *   out << "\nNumber of antigens recognized by randomly generated arupProteins, based on
 * threshold\n";
 *
 *   out << "  -> (for the first 100 arupProteins : ) In the case of random arupProteins" << endl;
 *   total = 0;
 #define thresholdRecoAg 0.1
 *   int nbDiscardedSeq = 0; // arupProteins that don't recognize anything
 *   int countprint = 0;
 *   for(int k = 0; k < min(maxim, (int) maxarupProteinsToEnumeate); ++k){
 *       if(k == 100) out << "  -> (for the remaining arupProteins) for arupProteins recognizing at
 * least an antigen with affinity 0.1" << endl;
 *       total++;
 *
 *       // for each arupProtein,
 *       bool recoAtLeastOne = false;
 *       vector<double> nbRecoDepThresh(10, 0.0);
 *       vector<double> affinityEach(nbAntigens, 0.0);
 *       arupProtein* seqtmp = new arupProtein(L);
 *       seqtmp->randomize();
 *       for(int j = 0; j < nbAntigens; ++j){
 *           double thisAff = arupProtein::seq_affinity(seqtmp, ags[j], R, maxClusters,
 * typeAffinityFunction);
 *           if((thisAff > thresholdRecoAg) || (k < 100)) recoAtLeastOne = true; else nbDiscardedSeq
 * ++;
 *           affinityEach[j] = thisAff;
 *           for(int i = 0; i <= (int) (9.99 * thisAff); ++i){
 *               if(i < 10) nbRecoDepThresh[i] ++;
 *           }
 *       }
 *       if(recoAtLeastOne && (countprint < 5000)){
 *           countprint++;
 *           out << "RandSeq " << k << ", " << seqtmp->print() << " ";
 *           out << "nbAgPerThreshold:";
 *           for(int i = 0; i < 10; ++i){
 *               out << "\t" << nbRecoDepThresh[i];
 *           }
 *           out << "\taffPerAg:";
 *           for(int i = 0; i < nbAntigens; ++i){
 *               out << "\t" << affinityEach[i];
 *           }
 *           out << endl;
 *       }
 *       delete seqtmp;
 *   }
 *   out << "   ... Nb of arupProteins analyzed: " << total << endl;
 *   out << "   ... Nb of arupProteins discarded: " << nbDiscardedSeq << "(except the 100 first
 * ones, i.e. among the :" << total - 100 << " remaining)"<< endl;
 *
 *
 *
 *   out << "==== Part 3 : Evaluating the effect of mutations : ====" << endl;
 *
 *   arupProtein* start = new arupProtein(L);   // starting by '0000' : the best arupProtein
 *   out << "NbMut\tarupProtein\taffinity\n";
 *   for(int i = 0; i < 2*L; ++i){
 *       out << i << "\t" << start->print() << "\t" << arupProtein::seq_affinity(start, ref, R,
 * maxClusters, typeAffinityFunction) << endl;
 *       start->mutateOnePosition();
 *   }
 *
 *   out << "\tReaching a good affinity\t" << endl;
 *   start->randomize();
 *   double prevaff = arupProtein::seq_affinity(start, ref, R, maxClusters, typeAffinityFunction);
 *
 *   bool stop = false;
 *   for(int i = 0; (i < L) && (!stop); ++i){
 *       out << "arupProtein : " << start->print() << "\tAff:\t" << prevaff << "\t";
 *       out << "PossibleMut:";
 *       vector<int> posGoodMutations;
 *       for(int i = 0; i < L; ++i){
 *           arupProtein stmp = arupProtein(start);
 *           stmp.content[i] = !stmp.content[i];
 *           double newaff = arupProtein::seq_affinity(&stmp, ref, R, maxClusters,
 * typeAffinityFunction);
 *           out << "\t" << newaff;
 *           if(newaff > prevaff) posGoodMutations.push_back(i);
 *       }
 *       out << endl;
 *       if(posGoodMutations.size() > 0){
 *           int nextmut = irandom(posGoodMutations.size()-1);
 *           start->content[posGoodMutations[nextmut]] = !
 * start->content[posGoodMutations[nextmut]];
 *           prevaff = arupProtein::seq_affinity(start, ref, R, maxClusters, typeAffinityFunction);
 *       } else {
 *           stop = true;
 *       }
 *   }
 *
 *   return out.str();*/
   return string("");
}


