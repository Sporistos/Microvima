#ifndef ProbaArup_h
#define ProbaArup_h

#include <iostream>
#include <vector>
#include <string>
#include <cmath> // for NAN
#include <sstream>
#include "../Tools/distribution.h"
using namespace std;

// This class is not made to be general, but just reproduces the model of Arup Chakraborty (Cell paper)
// an antigen is a sequence of +1/-1. An antiboy is a sequence of double (h values)
enum typeArupSeq {
    typeArupBCR, typeArupAG, numberTypesArup
};

struct arupProtein {

    // global parameters that will not be changed between parallel simulations
    static int arup_length_sequences, arup_N_conserved, arup_N_mutates, arup_N_shielded;
    static double arup_alpha, arup_h_min, arup_h_max, arup_hprime_min, arup_hprime_max,
    arup_hmut_min, arup_hmut_max;
    static probaLawFromTable * lawMutations;

    // Operators / functions to set up a new binaryLineage
    arupProtein();
    arupProtein(vector<double> numbers);
    arupProtein(string numbersInText);   arupProtein(arupProtein * toCopy);
    void copy(arupProtein * toCopy);
    void operator =(arupProtein B) { content = B.content; size = B.size; }
    bool operator ==(arupProtein &b);
    void clear();

    // Data storage
    std::vector<double> content;
    int size;                            // alternately, put it static,
    int operator [](int i);

    // Meaningful functions
    static double affinity(arupProtein * BCR, arupProtein * Antigen);
    static double hamming(arupProtein * x, arupProtein * y);
    // Note: mutation and randomize are specific to sub-classes. Never use the mother class alone.

    // tests / tool functions
    static string testeAffinityFunctions();
    virtual std::string print();
    virtual ~arupProtein();

    virtual int mutateOnePosition(double targetChangeEnergy = NAN){cerr << "Mother function arupProtein::mutateOnePosition should not be called! Where is daughter function??" << endl; return -1;}
};

struct ArupBCR: public arupProtein {

    // All constructors redefined
    ArupBCR() : arupProtein() { }
    ArupBCR(string txt) : arupProtein(txt) { }
    ArupBCR(ArupBCR * anotherBCR) : arupProtein((arupProtein*) anotherBCR) { }
    ArupBCR(arupProtein * anotherSeq) : arupProtein(anotherSeq) { }
    typeArupSeq getType() { return typeArupBCR; }

    // The important functions
    int mutateOnePosition(double targetChangeEnergy = NAN);
    void randomize(double targetEnergy, bool beExact = false);

    // tools
    string print() {
        stringstream res;
        res << "ArupBCR:" << arupProtein::print();
        return res.str();
    }
};

struct ArupAntigen: public arupProtein {

    // All constructors redefined
    ArupAntigen() : arupProtein() { }
    ArupAntigen(string txt) : arupProtein(txt) { }
    ArupAntigen(ArupAntigen * anotherAntigen) : arupProtein((arupProtein*) anotherAntigen) { }
    ArupAntigen(arupProtein * anotherSeq) : arupProtein(anotherSeq) { }
    typeArupSeq getType() { return typeArupAG; }

    // The important functions
    void randomize(int nbResiduesToMutate = -1);
    void mutate();  // just one position

    // tools
    string print() {
        stringstream res;
        res << "ArupAntigen:" << arupProtein::print();
        return res.str();
    }

};


#endif

/* List of parameters for using Arup sequences
 *
 *  Use arup space (1/0) [use_arup_space]:
 *  0
 *  Length of sequences [arup_length_sequences]:
 *  46
 *  Nb conserved residues [arup_N_conserved]:
 *  18
 *  Nb mutated residues [arup_N_mutates]:
 *  22
 *  Nb shielded residues (the rest) [arup_N_shielded]:
 *  6
 *  Number of Initial Antigen sequences (int-type) [arup_nb_ini_antigens]:
 *  3
 *  Fix Antigen Sequence presentation. Order is Conserved(should be 1), Variable, shielded (should
 * be 1). max 1000 values [arup_ini_antigens[...]]:
 *  1111111111111111111111111111111111111111111111
 *  1111111111111111110000000000011111111111111111
 *  1111111111111111111111111111100000000000111111
 *  -1
 *  Fraction of Arup Ags (non-fixed Ag enter with same fraction) [arup_ag_fraction[...]]:
 *  -1
 *  Number of mutations for mutated strains (if more initial Ag have to be generated)
 * [arup_nb_mutations_gen_strains]:
 *  11
 *  Activation threshold (kcal/mol) [arup_threshold_activation]:
 *  10.8
 *  Initial interval for BCR sequences [arup_h_min, arup_h_max]:
 *  -0.18
 *  0.9
 *  Fix initial Repertoire distribution (max 1000 values) [arup_ini_bcrs[...]]:
 *  -1
 #would be 0.1 0.8 0.5 -0.2 0.1 ... with values
 *  Mutation rate per sequence per division per residue [arup_mutation]:
 *  0.003
 *  probability of a mutation being lethal [arup_proba_lethal_mut]:
 *  0.3
 *  probability of a mutation being affecting [arup_proba_affecting_mut]:
 *  0.2
 *  probability silent [arup_proba_silent_mut]:
 *  0.5
 *  distribution of affinity changes by mutation [nbLinesToRead, arup_law_mut_Xs[...],
 * arup_law_mut_Densities[...]]:
 *  40
 *  -6,4	0,019047619
 *  -6,2	0,003809524
 *  -6	0,003809524
 *  -5,8	0,003809524
 *  -5,6	0,003809524
 *  -5,4	0,024380952
 *  -5,2	0,042666667
 *  -5	0,028952381
 *  -4,8	0,03352381
 *  -4,6	0,03352381
 *  -4,4	0,058666667
 *  -4,2	0,084571429
 *  -4	0,089142857
 *  -3,8	0,054857143
 *  -3,6	0,09447619
 *  -3,4	0,095238095
 *  -3,2	0,079238095
 *  -3	0,14552381
 *  -2,8	0,11352381
 *  -2,6	0,134095238
 *  -2,4	0,095238095
 *  -2,2	0,179809524
 *  -2	0,234666667
 *  -1,8	0,089142857
 *  -1,6	0,134095238
 *  -1,4	0,204190476
 *  -1,2	0,290285714
 *  -1	0,249904762
 *  -0,8	0,215619048
 *  -0,6	0,170666667
 *  -0,4	0,340571429
 *  -0,2	0,361142857
 *  0	0,365714286
 *  0,2	0,316190476
 *  0,4	0,324571429
 *  0,6	0,043428571
 *  0,8	0,084571429
 *  1	0,048761905
 *  1,2	0,04952381
 *  1,4	0,03352381
 *  Coefficient of unshielding [arup_alpha]:
 *  2
 *  Bounding of the shielding h' [arup_hprime_min, arup_hprime_max]:
 *  -1.5
 *  1.5
 *  Bounding of the residues facing mutated h' [arup_hmut_min, arup_hmut_max]:
 *  -1.5
 *  1e6 */
