#include "sequencespaces.h"
#include "../Tools/zaprandom.h"
#include <cmath>                // for std::pow, seems that math.h doesn't have it
#include <sstream>
#include <fstream>
#ifndef NOTHERE
#include "../cellman.h" // to get the time, very stupidmaybe circular reference
#endif
#include "../setparam.h"
using namespace std;

#include <QCoreApplication>
// necessary for tests:
#include <bitset>
#include <algorithm>
#include <iomanip>

/// Pre-declaration of the available sequence classes (the template can only be used for those then):
struct hyBinaryLineage;
struct hyBinarySequence;
struct hyFoldedCubic;
struct hyFoldedFree;
struct hyNucleotideFree;
struct hyPerelsonSequence;
struct hyPerelsonGeneralized;
struct hyArupProtein;

/** @brief Generally, templates can not be implemented in the .cpp because everything has to be
 * known from the .h. Found two ways to go around :
 *  1/ explicit say here which T types are allowed, then the implementation can be kept in the .cpp
 **/
template class generalSequenceContainer<hyBinaryLineage>;
template class generalSequenceContainer<hyBinarySequence>;
template class generalSequenceContainer<hyFoldedCubic>;
template class generalSequenceContainer<hyFoldedFree>;
template class generalSequenceContainer<hyNucleotideFree>;
template class generalSequenceContainer<hyPerelsonSequence>;
template class generalSequenceContainer<hyPerelsonGeneralized>;
template class generalSequenceContainer<hyArupProtein>;

/** 2/ (that could be done) : put the implementation of the template class in a xxx.tpp file and do
 * #include "xxx.tpp" at the end of the .h file -> a bit complicated from now*/

double hyAffinity(hyBinaryLineage* s1, hyBinaryLineage* s2){
    return binaryLineage::affinity((binaryLineage*) s1, (binaryLineage*) s2);}
double hyAffinity(hyBinarySequence* s1, hyBinarySequence* s2){
    return binarySequence::affinity((binarySequence*) s1, (binarySequence*) s2);}
double hyAffinity(hyFoldedCubic* s1, hyFoldedCubic* s2){
    return foldedCubic::affinity((foldedCubic*) s1, (foldedCubic*) s2);}
double hyAffinity(hyFoldedFree* s1, hyFoldedFree* s2){
    return foldedFree::affinity((foldedFree*) s1, (foldedFree*) s2);}
double hyAffinity(hyNucleotideFree* s1, hyNucleotideFree* s2){
    return nucleotideFree::affinity((nucleotideFree*) s1, (nucleotideFree*) s2);}
double hyAffinity(hyPerelsonSequence* s1, hyPerelsonSequence* s2){
    return perelsonSequence::affinity((perelsonSequence*) s1, (perelsonSequence*) s2);}
double hyAffinity(hyPerelsonGeneralized* s1, hyPerelsonGeneralized* s2){
    return perelsonGeneralized::affinity((perelsonGeneralized*) s1, (perelsonGeneralized*) s2);}
double hyAffinity(hyArupProtein* s1, hyArupProtein* s2){
    return arupProtein::affinity((arupProtein*) s1, (arupProtein*) s2);}

string typeInString(typeSeq t){
    switch(t){
    case typeBCR: return string("BCR");
    case typeAG: return string("AG");
    case typeTCR: return string("TCR");
    default:{}
    }
    return string("?");
}

double currentTime(){
#ifdef NOTHERE
    return 0;
#else
    return cellman::time;
#endif
}

//template <typename T>
//double generalSequenceContainer<T>::pm_differentiation_rate = NAN; // to raise errors if not
                                                                   // instanciated ...
template <typename T> generalSequenceContainer<T>::generalSequenceContainer(){
   n_Sequences = 0;
   n_Seeders = 0;
   n_Antigens = 0;
   n_TCRs = 0;
   proba_lethal_mut = 0;
   proba_affecting_mut = 1.0;
   proba_silent_mut = 0;
   pm_differentiation_rate = NAN;
   initialized = false;
   GCgraph = new liste_adjacence();
}

template <typename T> void generalSequenceContainer<T>::initialize(double _mutation_rate, double _proba_lethal_mut, double _proba_affecting_mut, double _proba_silent_mut, double _pm_differentiation_rate){
    mutation_rate = _mutation_rate;
    proba_lethal_mut = _proba_lethal_mut;
    proba_affecting_mut = _proba_affecting_mut;
    proba_silent_mut = _proba_silent_mut;
    if(fabs(proba_lethal_mut + proba_affecting_mut + proba_silent_mut - 1.0) > 1e-4){
        cerr << "ERR: the different mutation rates (proba lethal, affecting and silent) do not sum up to 1" << endl;
        exit(-1);
        proba_affecting_mut = 1.0;
        proba_lethal_mut = 0.;
        proba_silent_mut = 0;
    }
    pm_differentiation_rate = _pm_differentiation_rate;
    initialized = true;
}

template <typename T> T* generalSequenceContainer<T>::getSequence(long id) {
   if ((id < 0) || (id >= n_Sequences)) {
      cerr << "ERR: sequenceSpace::getSequence(id=" << id << "), incorrect id, only "
           << n_Sequences << " defined\n";
      return nullptr;
   }
   return poolSequences[id];
}

template <typename T> long int generalSequenceContainer<T>::index_adding_sequence(T * toAdd, long int ID_of_mother_sequence) {
  if ((int) poolSequences.size() != n_Sequences) {
     cerr << "ERR: sequenceSpace::index_adding_sequence, n_Sequences management error !!" << endl;
  }
  if (toAdd == nullptr) {
     cerr << "ERR: sequenceSpace::index_adding_sequence(..., sequence* = NULL !!!)" << endl;
     return -1;
  }
  if (toAdd->getType() == undefined) {
     cerr << "ERR: sequenceSpace::index_adding_sequence(), forbidden sequence of undefined type" << endl;
     return -1;
  }
  poolSequences.push_back(toAdd);
  indexParentSeq.push_back(ID_of_mother_sequence);
  n_Sequences++;
  return n_Sequences - 1;
}

template <typename T>  generalSequenceContainer<T>::~generalSequenceContainer() {
   for (size_t i = 0; i < poolSequences.size(); ++i) {
      if (poolSequences[i]) { delete poolSequences[i]; }
   }
   delete GCgraph;
}

template <typename T> int generalSequenceContainer<T>::get_n_Sequences() { return n_Sequences; }
template <typename T> int generalSequenceContainer<T>::get_n_Antigens() { return n_Antigens; }
template <typename T> int generalSequenceContainer<T>::get_n_Seeders() { return n_Seeders; }
template <typename T> int generalSequenceContainer<T>::get_n_TCRs() { return n_TCRs; }

template <typename T> long int generalSequenceContainer<T>::get_Antigen() {
   return Antigens[random::uniformInteger(0,n_Antigens-1)];
}
template <typename T> long int generalSequenceContainer<T>::get_Seeder() {
   return Seeders[random::uniformInteger(0,n_Seeders-1)];
}
template <typename T> long int generalSequenceContainer<T>::get_TCR() {
   return TCRs[random::uniformInteger(0, n_Seeders-1)];
}
template <typename T> long int generalSequenceContainer<T>::get_Antigen(int i) {
   if ((i >= 0) && (i < n_Antigens)) { return Antigens[i]; } else { return -1; }
}
template <typename T> long int generalSequenceContainer<T>::get_Seeder(int i) {
   if ((i >= 0) && (i < n_Seeders)) { return Seeders[i]; } else { return get_Seeder(); }
}
template <typename T> long int generalSequenceContainer<T>::get_TCR(int i) {
   if ((i >= 0) && (i < n_TCRs)) { return TCRs[i]; } else { return get_TCR(); }
}
template <typename T> long int generalSequenceContainer<T>::add_Antigen(T * newAG) {
   if(newAG->getType() != typeAG)
       cerr << "ERR: adding a sequence as antigen while it has another type or undefined " << endl;
   Antigens.push_back(index_adding_sequence(newAG, -1));
   n_Antigens++;
   return Antigens[n_Antigens - 1];
}
template <typename T> long int generalSequenceContainer<T>::add_Seeder(T * newBCR) {
   if(newBCR->getType() != typeBCR){
       cerr << "ERR: adding a sequence as BCR while it has another type or undefined " << endl;
   }
   long int newIndex = index_adding_sequence(newBCR, -1);
   Seeders.push_back(newIndex);
   n_Seeders++;
   GCgraph->addCell(newIndex, -1, graphcells(currentTime(), newBCR->print(), best_affinity(newIndex)), best_affinity(newIndex));
   return Seeders[n_Seeders - 1];
}
template <typename T> long int generalSequenceContainer<T>::add_TCR(T * newTCR) {
   if(newTCR->getType() != typeTCR)
       cerr << "ERR: adding a sequence as TCR while it has another type or undefined " << endl;
   TCRs.push_back(index_adding_sequence(newTCR, -1));
   n_TCRs++;
   return TCRs[n_TCRs - 1];
}




enum {MUT_FAIL = -1, MUT_SILENT = 0, MUT_SUCCESS = 1};
template <typename T> long int generalSequenceContainer<T>::getMutation(long from_position) {
  double U = random::uniformDouble(0,1);
  if(U < proba_lethal_mut) return -1;
  if(U < proba_lethal_mut + proba_silent_mut) return from_position;

  T * theSeq = getSequence(from_position);
  if (theSeq == nullptr) {
     cerr << "affinitySpace::getMutation, NULL at " << from_position << "\n";
     return -1;
  }
  if (theSeq->getType() != typeBCR) {
     cerr << "affinitySpace::getMutation, you mutate something else than a BCR" << endl;
     return -1;
  }
  T * newBCR = new T(theSeq);  // will also copy its type, but empty number of cells in each of it
  int outcome = newBCR->mutateOnePosition(); // it was an error before, it was mutating the previous sequence
  if(outcome == MUT_FAIL) return -1;
  if(outcome == MUT_SILENT) return from_position;
  if(outcome != MUT_SUCCESS){cerr << "ERR: sequence space (general container)::mutate(), the mutateOnePosition() function should return -1, 0, or 1 !!!" << endl;}
  long res = index_adding_sequence(newBCR, from_position);

  GCgraph->addCell(res, from_position, graphcells(currentTime(), newBCR->print(), best_affinity(res)), best_affinity(res) -  best_affinity(from_position));

  //graph.addCell(res, from_position, graphcells(cellman::time, T->print(), IGM), 0.);
  return res;
}

// affinity with antibody feedback threshold (0 if no feedback)
template <typename T> double generalSequenceContainer<T>::affinity(long int seqId1, long int seqId2, double tr) {
   T* seq1 = getSequence(seqId1);
   T* seq2 = getSequence(seqId2);
   if((seq1->getType() != typeBCR) || (seq2->getType() != typeAG)) {
       cerr << "ERR: affinity only between 1/ BCR and 2/ Antigen\n";
   }
   double v = hyAffinity(seq1, seq2);
   if ((tr < 0) || (tr >= 1)) {
      cerr << "affinitySpace::affinity(..., ..., Threshold=" << tr << "), not in [0;1]\n";
      return 0.0;
   }
   return (v - tr) / (1 - tr);
}











template <typename T> double generalSequenceContainer<T>::best_affinity(long int seqID) {
   T* seq = getSequence(seqID);
   double aff = 0;
   for (int i = 0; i < get_n_Antigens(); ++i) {
      aff = max(aff, T::affinity((T*) seq, (T*) getSequence(get_Antigen(i))));
   }
   return aff;
}

template <typename T> int generalSequenceContainer<T>::get_nearest_Antigen(long n) {
   double aff = -1;
   int IDclosest = -1;
   T* seq = getSequence(n);
   for (int i = 0; i < n_Antigens; ++i) {
      double newAff = T::affinity((T*) seq, (T*) getSequence(get_Antigen(i)));
      if (newAff > aff) { aff = newAff; IDclosest = i; }
   }
   if (IDclosest < 0) {
      cerr << "affinitySpace::get_nearest_antigen(), could not find a closest antigen." << endl;
   }
   return IDclosest;                     // Might be more consistent to change it to return
                                         // get_antigen(IDclosest) !!!
}




template <typename T> void generalSequenceContainer<T>::put_Ab(long index,double d_ab) {
   getSequence(index)->antibody += d_ab;
   if (getSequence(index)->antibody < 0) {
      getSequence(index)->antibody = 0;
      cerr << "WARNING! antibody got negative at index " << index << " in sequence container space.\n";
   }
}
template <typename T> double generalSequenceContainer<T>::get_AbAmount(long index) {
   return getSequence(index)->antibody;
}







template <typename T> void generalSequenceContainer<T>::add_cell(cells typ, long int pos) {
   T* seq = getSequence(pos);
   if (typ == soutext) {  // to detect if a BCR sequence is reaching the soutext for the first time
      set_external_cell(pos);  // has to be done before n_cell[soutext] is increased,
   }                           // to detect when it goes from 0 to another number

   if (typ == soutextproduce) {
      cerr << "WRN: sequenceSpace::add_cell(soutextproduce), you are not supposed to add "
              "soutextproduce from the simulation. It should happen by differentiation by"
              "calling AffinitySpace::pm_differentiate()" << endl;
   }
   seq->n_cell[typ] += 1;
   ++sum_cell[typ];
}
template <typename T> void generalSequenceContainer<T>::rem_cell(cells typ, long int pos) {
   T* seq = getSequence(pos);
   if(seq){ // getSequence will raise an error anyways if wrong sequence
       seq->n_cell[typ] -= 1;
       --sum_cell[typ];
   }
   // note :  external cells is not updated if soutext/soutextproduce are removed
}








template <typename T>
int generalSequenceContainer<T>::get_n_ab_producers() {
   return ab_producers.size();
}
template <typename T>
double generalSequenceContainer<T>::get_AbProducingCells(int n) {
   if ((n < 0) || (n >= (int) ab_producers.size())) {
      cerr << "ERR:sequenceSpace::getAbProducingCells(" << n
           << "), index out of bounds [0.." << ab_producers.size() - 1 << "]" << endl;
      return 0;
   }
   T * bseq = getSequence(ab_producers[n]);
   return bseq->n_cell[soutextproduce];
}
template <typename T>
long int generalSequenceContainer<T>::get_AbProducingIndex(int n) {
   if ((n < 0) || (n >= (int) ab_producers.size())) {
      cerr << "ERR:sequenceSpace::getAbProducingIndex(" << n
           << "), index out of bounds [0.." << ab_producers.size() - 1 << "]" << endl;
      return 0;
   }
   return ab_producers[n];
}


template <typename T>
void generalSequenceContainer<T>::set_external_cell(long int pos) {
   T * seq = getSequence(pos);
   if(external_cells.find(pos) == external_cells.end()){ // if it is not found inside the list
       external_cells.insert(pos);
       if((seq->n_cell[soutext] + seq->n_cell[soutextproduce]) > 0 /* 1e-9 */) {
           cerr << "ERR: the first time a sequence is put into the set of external_cells, while it was already"
                   "containing cells before" << endl;
       }
   }
}

// thie function is a bit of cheating because it only works for a=soutext and b=soutextproduce
template <typename T>
void generalSequenceContainer<T>::PM_differentiate(cells a, cells b, double dt) {
   set<long int>::iterator it;
   for (it = external_cells.begin(); it != external_cells.end(); it++) {
      long n = *it;
      T* seq = getSequence(n);
      // if (poolSequences[n]->getType() == typeBCR) { // is sequence type dependent. external cells
      // should only contain BCR ids so it's ok
      double transit = seq->n_cell[a] * pm_differentiation_rate * dt;
      if ((transit > 0) && (seq->n_cell[b] == 0)) {
         // the first time there is a producer at this position
         ab_producers.push_back(n);
      }
      seq->n_cell[a] -= transit;
      sum_cell[a] -= transit;
      seq->n_cell[b] += transit;
      sum_cell[b] += transit;
      // }
   }
}

template <typename T>
double generalSequenceContainer<T>::get_sum_cell(cells celltype) { return sum_cell[celltype]; }
template <typename T>
double generalSequenceContainer<T>::get_oldsum_cell(cells celltype) {
   return oldsum_cell[celltype];
}
template <typename T>
short int generalSequenceContainer<T>::sum_check() {
   long int i;
   double dtmp;
   short int err = 0;
   cout << "Sequence/Arup space sum check ... ";
   for (int n = 0; n < number_cell_types; n++) {
      // Durchlaufe Typen
      dtmp = 0.;
      for (i = 0; i < n_Sequences; i++) {
         // Durchlaufe shape space Punkte
         if (getSequence(i)->n_cell[n] < 0) {
            cout << "ERR: NbCells in sequence[ID=" << i << "].[typeCell=" << n << "] = "
                 << getSequence(i)->n_cell[n] << " <0\n";
            exit(1);
         }
         // if (n!=soutext && n!= soutextproduce)
         dtmp += getSequence(i)->n_cell[n];
         // else if (n==soutext) { dtmp+=ssp[i].d_cell[n]; dtmp+=ssp[i].d_cell[n+1]; }
         // else if (n==soutext) { dtmp+=ssp[i].n_cell[n]; dtmp+=ssp[i].n_cell[n+1]; }
      }
      if ((dtmp - sum_cell[n] > 1.e-06) || (dtmp - sum_cell[n] < -1.e-06)) {
         cout << "Wrong ss sum for cell type " << n << " !\n";
         cout << "sum=" << sum_cell[n] << " but real sum=" << dtmp << "\n";
         // exit(1);
         err = 1;
      }
   }
   cout << "done\n";
   return err;
}


template <typename T> string generalSequenceContainer<T>::printGraph() {
    return GCgraph->print();
}
















template <typename T>
void generalSequenceContainer<T>::initializeAnalyzeFiles(ofstream &ana) {

   OUT_haffinity = 0;
   OUT_steepness = 0;
   CB_haffinity = 0;
   CC_haffinity = 0;

   ana << "Define sequence space output files ...";
   for (short j = 0; j < SSlogs; j++) {
      sum_cell[j] = 0.;
      oldsum_cell[j] = 0.;
   }

   // Opening the file streams for output during simulation - think of calling close_files(); later
   // to finish the job at the end of simulation
   logdata[sCB].open("ssumcb.out");
   logdata[sCB] << "! Sum of all the Centroblasts im the sequence space\n";
   logdata[sCC].open("ssumcc.out");
   logdata[sCC] << "! Sum of all the Centrocytes im the sequence space\n";
   logdata[sCCunselected].open("ssumccun.out");
   logdata[sCCunselected] << "! Sum of unselected Centrocytes im the sequence space\n";
   logdata[sCCcontact].open("ssumccco.out");
   logdata[sCCcontact] << "! ! Sum of antigen-selected Centrocytes im the sequence space\n";
   logdata[sCCselected].open("ssumccse.out");
   logdata[sCCselected] << "! ! Sum of positively (Tfh) selected Centrocytes im the sequence space\n";
   logdata[sCCapoptosis].open("ssumapo1.out");
   logdata[sCCapoptosis] << "! Sum of all dead cells in the grid\n";
   logdata[sallapoptosis].open("ssumapo2.out");
   logdata[sallapoptosis] << "! Sum of all cells that had died in the sequence space : increment\n";

   // Lasse FDC und Tcell weg! /// Philippe: TODO

   logdata[sout].open("ssumout.out");
   logdata[sout] << "! Sum of generated output cells in the sequence space (sout) "
                 << ": DEC205+ : fraction of DEC205+\n";
   logdata[soutext].open("ssumoute.out");
   logdata[soutext] << "! Sum of output cells that left the grid in the sequence space (soutext)\n";
   logdata[soutextproduce].open("ssumoutep.out");
   logdata[soutextproduce] << "! Sum of output cells that left the grid and produce antibodies, "
                           << "(soutextproduce)\n";
   logdata[total].open("ssum.out");
   logdata[total] << "! Sum of all the cells in the sequence space\n";

   logmeanaff.open("saffin.out");
   logmeanaff << "! Mean affinity of CB+CC : CB : CC : out\n";
   loghighaff.open("shaffin.out");
   loghighaff << "! Fraction of >30% affinity CB:CC:out\n";
   log048aff.open("s048aff.out");
   log048aff << "! Fraction of <40% affinity CB:CC:out : 40-80% : >=80%\n";

   logdiversity.open("diversity.out");
   logdiversity << "! Number of different encoded antibodies present in the GC\n"
                << "! time : total in GC : CB : CC : output in GC : output external\n";

// logdata[4].open("tbeta.out");                    logdata[4] << "! Beta fuer die Summe der
// praesentierten Epitope.\n";
// logdata[5].open("tomega.out");                   logdata[5] << "! Omega fuer die Summe der
// praesentierten Epitope.\n";
// logdata[6].open("tbpersum.out");                 logdata[6] << "! % Centroblasten an den
// praesentierten Epitopen.\n";
// logdata[7].open("topersum.out");                 logdata[7] << "! % Output-Zellen an den
// praesentierten Epitopen.\n";
// logdata[8].open("tfb.out");                      logdata[8] << "! % der selektierten
// Centroblasten.\n";
// logdata[9].open("tfo.out");                      logdata[9] << "! % der selektierten
// Output-Zellen.\n";

   // additional files with emphasis on multiple antigens:
   ofstream saffin_ag("saffin_ags.out");
   saffin_ag << "! time :  mean affinity of all CB+CC to each antigen(columns)\n";
   saffin_ag.close();
   ofstream saffin_t10("saffin_ags_t10.out");
   saffin_t10 << "! time : mean affinity of CB+CC with aff>0.1 to each antigen(columns)\n";
   saffin_t10.close();
   ofstream saffin_t20("saffin_ags_t20.out");
   saffin_t20 << "! time : mean affinity of CB+CC with aff>0.2 to each antigen(columns)\n";
   saffin_t20.close();
   ofstream saffin_out_ag("saffin_out_ags.out");
   saffin_out_ag << "! time :  mean affinity of accumulated output"
                 << " to each antigen(columns)\n";
   saffin_out_ag.close();
   ofstream saffin_out_t10("saffin_out_ags_t10.out");
   saffin_out_t10 << "! time : mean affinity of accumulated output"
                  << " with aff>0.1 to each antigen(columns)\n";
   saffin_out_t10.close();
   ofstream saffin_out_t20("saffin_out_ags_t20.out");
   saffin_out_t20 << "! time : mean affinity of accumulated output"
                  << " with aff>0.2 to each antigen(columns)\n";
   saffin_out_t20.close();
   ofstream shaffin_ag("shaffin_ags.out");
   shaffin_ag << "! time : fraction of all CB+CC with affinity >0.3 to each antigen(columns)"
              << " : sum of all fractions\n";
   shaffin_ag.close();
   ofstream shaffin_out_ag("shaffin_out_ags.out");
   shaffin_out_ag << "! time : fraction of accumulated output"
                  << " with affinity >0.3 to each antigen(columns) : sum of all fractions\n";
   shaffin_out_ag.close();
   ofstream cross_gcr("cross_reactivity_gcr.out");
   cross_gcr << "! time : cross-reactivity = "
             << "[mean affinity of CB+CC (all : aff>0.1 : >0.2) to all antigens]\n";
   cross_gcr.close();
   ofstream cross_out("cross_reactivity_out.out");
   cross_out << "! time : cross-reactivity = "
             << "[mean affinity of accumulated output (all : aff>0.1 : >0.2) to all antigens]\n";
   cross_out.close();

   // add a file for a histogram of GC-BCs versus Hamming distance to optimal clone
   ofstream gcbc_hamming("gcbc_hamming.out");
   gcbc_hamming << "! time[days] : time[hr] : Hamming distance : # of GC-BC nearest Ag "
                << ": # of GC-BC to mean Ag\n";
   gcbc_hamming.close();
   ofstream gcbc_affinity("gcbc_affinity.out");
   gcbc_affinity << "! time[days] : time[hr] : affinity-min : # of GC-BC nearest Ag "
                 << ": # of GC-BC to mean Ag\n";
   gcbc_affinity.close();

   ana << " done\n";
   ana << "Sequence space READY! (YEAAAAH !!)\n";
}

// common with ss.cpp
template <typename T>
void generalSequenceContainer<T>::close_files() {
   cout << "Close files in shape space ... ";
   for (short int i = 0; i < SSlogs; i++) {
      if ((i != sFDC) && (i != sTcell)) { logdata[i].close(); }
   }
   logmeanaff.close();
   loghighaff.close();
   logdiversity.close();
   log048aff.close();

   ofstream outSeqSpace;
   outSeqSpace.open("seqspace_Final.out");
   outSeqSpace << printStateAllSequences(true, false);
   outSeqSpace.close();

   outSeqSpace.open("sequences_Final.out");
   outSeqSpace << printSequences(cellman::time);
   outSeqSpace.close();

   outSeqSpace.open("GCgraph_Final.out");
   outSeqSpace << printGraph();
   outSeqSpace.close();

   cout << "done.\n";
}

template <typename T>
double generalSequenceContainer<T>::mean_affinity_norm(long pos) {
   if(get_n_Antigens() == 0) return NAN;
    double tmp = 0.;
   for (int j = 0; j < get_n_Antigens(); j++) {
      tmp += affinity_norm(pos, get_Antigen(j));
   }
   tmp /= double (get_n_Antigens());
   return tmp;
}

// now, affinities[15] is the average
template <typename T>
vector<double> generalSequenceContainer<T>::mean_affinity() {
    vector<double> affinities(16, 0.);
    for (long int i = 0; i < get_n_Sequences(); i++) {
       T* thisSeq = getSequence(i);
       if(thisSeq->getType() == typeBCR){
          // Find nearest antigen-type
          double aff = best_affinity_norm(i);
          // add corresponding affinity with weight #cb and #cc to ccaff and cbaff
          affinities[0] += aff * double (thisSeq->n_cell[sCB]);
          affinities[1] += aff * double (thisSeq->n_cell[sCC]);
          affinities[2] += aff * double (thisSeq->n_cell[sout]);
          if (aff > 0.3) {
             affinities[3] += double (thisSeq->n_cell[sCB]);
             affinities[4] += double (thisSeq->n_cell[sCC]);
             affinities[5] += double (thisSeq->n_cell[sout]);
          }
          if (aff < 0.4) {
             affinities[6] += double (thisSeq->n_cell[sCB]);
             affinities[7] += double (thisSeq->n_cell[sCC]);
             affinities[8] += double (thisSeq->n_cell[sout]);
          }
          if ((aff >= 0.4) && (aff < 0.8)) {
             affinities[9] += double (thisSeq->n_cell[sCB]);
             affinities[10] += double (thisSeq->n_cell[sCC]);
             affinities[11] += double (thisSeq->n_cell[sout]);
          }
          if (aff >= 0.8) {
             affinities[12] += double (thisSeq->n_cell[sCB]);
             affinities[13] += double (thisSeq->n_cell[sCC]);
             affinities[14] += double (thisSeq->n_cell[sout]);
          }
       }
   }
   // Divide through total cell numbers:
   double all = affinities[0] + affinities[1];
   if (sum_cell[sCB] > 0) {
      affinities[0] /= sum_cell[sCB];
      affinities[3] /= sum_cell[sCB];
      affinities[6] /= sum_cell[sCB];
      affinities[9] /= sum_cell[sCB];
      affinities[12] /= sum_cell[sCB];
   } else { affinities[0] = 0.; }
   if (sum_cell[sCC] > 0) {
      affinities[1] /= sum_cell[sCC];
      affinities[4] /= sum_cell[sCC];
      affinities[7] /= sum_cell[sCC];
      affinities[10] /= sum_cell[sCC];
      affinities[13] /= sum_cell[sCC];
   } else { affinities[1] = 0.; }
   if (sum_cell[sout] > 0) {
      // cout<<outs<<" "<<ha_out<<" "<<sum_cell[sout]<<"\n";
      affinities[2] /= sum_cell[sout];
      affinities[5] /= sum_cell[sout];
      affinities[8] /= sum_cell[sout];
      affinities[11] /= sum_cell[sout];
      affinities[14] /= sum_cell[sout];
      // cout<<outs<<" "<<ha_out<<"\n";
   } else { affinities[2] = 0.; }
   if (sum_cell[sCB] + sum_cell[sCC] > 0) {
      all /= (sum_cell[sCB] + sum_cell[sCC]);
   } else { all = 0.; }
   affinities[15] = all;
   return affinities;
}

template <typename T>
vector<double> generalSequenceContainer<T>::mean_affinities_ag(int ag_index) {
   vector<double> mean_affinities(8, 0.);
   vector<double> totalBC(8, 0.);

   // go through all points of the AffinitySpace
   for (long i = 0; i < get_n_Sequences(); i++) {
       T* thisSeq = getSequence(i);
       if(thisSeq->getType() == typeBCR){
           // load affinities[] with corresponding values
          /* [0]: mean affinity of CB+CC to Antigen[ag_index]
           *  [1]: as [0] but only including cells with aff>0.1
           *  [2]: as [0] but only including cells with aff>0.2
           *  [3-5]: as [0-2] for accummulated output
           *  [6]: fraction of CB+CC cells with aff>0.3
           *  [7]: fraction of OUT cells with aff>0.3
           */
          // determine the affinity of this point to the antigen denoted by ag_index
          double aff = affinity_norm(i, get_Antigen(ag_index));
          // get the total number of BCs at this point
          double nBCs = thisSeq->n_cell[sCB] + thisSeq->n_cell[sCC];
          // get the total number of accumulated output at this point
          double nOUT = thisSeq->n_cell[sout];
          // add this to the mean with all BC and OUT
          mean_affinities[0] += aff * nBCs;
          mean_affinities[3] += aff * nOUT;
          // add this to the total cell number
          totalBC[0] += nBCs;
          totalBC[3] += nOUT;
          // do the same for the subgroup of points with a threshold affinity:
          if (aff > 0.1) {
             mean_affinities[1] += aff * nBCs;
             mean_affinities[4] += aff * nOUT;
             totalBC[1] += nBCs;
             totalBC[4] += nOUT;
          }
          if (aff > 0.2) {
             mean_affinities[2] += aff * nBCs;
             mean_affinities[5] += aff * nOUT;
             totalBC[2] += nBCs;
             totalBC[5] += nOUT;
          }
          if (aff > 0.3) {
             totalBC[6] += nBCs;
             totalBC[7] += nOUT;
          }
       }
   }
   // normalise to get the mean affinities
   for (int i = 0; i < 6; i++) {
      if (totalBC[i] > 0) {
         mean_affinities[i] /= totalBC[i];
      } else {
         mean_affinities[i] = 0.;
      }
   }
   // get the fraction of aff>0.3 for BC
   if (totalBC[0] > 0) {
      mean_affinities[6] = totalBC[6] / totalBC[0];
   } else {
      mean_affinities[6] = 0.;
   }
   // ... and for accumulated output
   if (totalBC[3] > 0) {
      mean_affinities[7] = totalBC[7] / totalBC[3];
   } else {
      mean_affinities[7] = 0.;
   }
   return mean_affinities;
}

template <typename T>
void generalSequenceContainer<T>::to_multiag_files(double time) {
   ofstream
      saffin_ags, saffin_t10, saffin_t20,
      saffin_out_ags, saffin_out_t10, saffin_out_t20,
      shaffin_ag, shaffin_out_ag,
      cross_gcr, cross_out;
   saffin_ags.open("saffin_ags.out", ofstream::app);
   saffin_ags << time << "  ";
   saffin_t10.open("saffin_ags_t10.out", ofstream::app);
   saffin_t10 << time << "  ";
   saffin_t20.open("saffin_ags_t20.out", ofstream::app);
   saffin_t20 << time << "  ";
   saffin_out_ags.open("saffin_out_ags.out", ofstream::app);
   saffin_out_ags << time << "  ";
   saffin_out_t10.open("saffin_out_ags_t10.out", ofstream::app);
   saffin_out_t10 << time << "  ";
   saffin_out_t20.open("saffin_out_ags_t20.out", ofstream::app);
   saffin_out_t20 << time << "  ";
   shaffin_ag.open("shaffin_ags.out", ofstream::app);
   shaffin_ag << time << "  ";
   shaffin_out_ag.open("shaffin_out_ags.out", ofstream::app);
   shaffin_out_ag << time << "  ";
   cross_gcr.open("cross_reactivity_gcr.out", ofstream::app);
   cross_gcr << time << "  ";
   cross_out.open("cross_reactivity_out.out", ofstream::app);
   cross_out << time << "  ";
   double fracsum = 0., fracOUTsum = 0.;
   // variables for cross reactivity
   vector<double> cross_reactivity(6,0.);
   for (int a = 0; a < n_Antigens; a++) {
      vector<double> mean_affinities = mean_affinities_ag(a);
      saffin_ags << mean_affinities[0] << "  ";
      saffin_t10 << mean_affinities[1] << "  ";
      saffin_t20 << mean_affinities[2] << "  ";
      saffin_out_ags << mean_affinities[3] << "  ";
      saffin_out_t10 << mean_affinities[4] << "  ";
      saffin_out_t20 << mean_affinities[5] << "  ";
      shaffin_ag << mean_affinities[6] << "  ";
      shaffin_out_ag << mean_affinities[7] << "  ";
      fracsum += mean_affinities[6];
      fracOUTsum += mean_affinities[7];
      // make the sum of all antigens to get cross-reactivity (with thresholds) for BC and out
      for (int i = 0; i < 6; i++) {
         cross_reactivity[i] += mean_affinities[i];
      }
   }
   // add the sum of the fractions, which is a measure of cross-reactivity, to the fraction-file
   shaffin_ag << fracsum << "\n";
   shaffin_ag.close();
   shaffin_out_ag << fracOUTsum << "\n";
   shaffin_out_ag.close();
   saffin_ags << "\n";
   saffin_t10 << "\n";
   saffin_t20 << "\n";
   saffin_out_ags << "\n";
   saffin_out_t10 << "\n";
   saffin_out_t20 << "\n";
   saffin_ags.close();
   saffin_t10.close();
   saffin_t20.close();
   saffin_out_ags.close();
   saffin_out_t10.close();
   saffin_out_t20.close();
   // normalise cross-reactivities with the number of antigens
   for (int i = 0; i < 3; i++) {
      if (n_Antigens > 0) {
         cross_reactivity[i] /= n_Antigens;
         cross_reactivity[i + 3] /= n_Antigens;
      } else {
         cross_reactivity[i] = 0.;
         cross_reactivity[i + 3] = 0.;
      }
      cross_gcr << cross_reactivity[i] << "  ";
      cross_out << cross_reactivity[i + 3] << "  ";
   }
   cross_gcr << "\n";
   cross_out << "\n";
   cross_gcr.close();
   cross_out.close();

   //   stringstream name;
   //   name << "seqspaceNTree_t=" << time << ".out";
   //   ofstream outSeqSpace;
   //   outSeqSpace.open(name.str().c_str());
   //   outSeqSpace << printSequences(false, true);
   //   outSeqSpace.close();

}

template <typename T>
void generalSequenceContainer<T>::get_diversity(double time) {
   logdiversity << time << "   ";
   vector<int> diversity(number_cell_types, 0);
   for (long i = 0; i < n_Sequences; i++) {
      T* thisSeq = getSequence(i);
      if(thisSeq->getType() == typeBCR){
          if (thisSeq->n_cell[sCB] > 0.5) { ++diversity[sCB]; }
          if (thisSeq->n_cell[sCC] > 0.5) { ++diversity[sCC]; }
          if (thisSeq->n_cell[sout] > 0.5) { ++diversity[sout]; }
          if (thisSeq->n_cell[sCB] + thisSeq->n_cell[sCC]
              + thisSeq->n_cell[sout] > 0.5) { ++diversity[total]; }
          if (thisSeq->n_cell[soutext] + thisSeq->n_cell[soutextproduce]
              > 0.5) { ++diversity[soutext]; }
      }
   }
   logdiversity << diversity[total] << "   "
                << diversity[sCB] << "   "
                << diversity[sCC] << "   "
                << diversity[sout] << "   "
                << diversity[soutext] << "\n";
}
template <typename T>
void generalSequenceContainer<T>::to_ssfiles(double time) {
   // sum_check();
   for (short int i = 0; i < SSlogs; i++) {
      if ((i != sFDC) && (i != sTcell)) {
         if (i != soutdec) { logdata[i] << time << "     " << sum_cell[i]; }
         if (i == sout) {
            logdata[sout] << "    " << sum_cell[sout] - oldsum_cell[sout];
         } else if (i == soutdec) {
            logdata[sout] << "   " << sum_cell[soutdec] << "   ";
            if (sum_cell[sout]
                > 0) {
               logdata[sout] << sum_cell[soutdec] / sum_cell[sout] << "\n";
            } else { logdata[sout] << "0\n"; }
         } else if (i == sallapoptosis) {
            logdata[sallapoptosis] << "    " << sum_cell[sallapoptosis]
               - oldsum_cell[sallapoptosis] << "\n";
         } else { logdata[i] << "\n"; }
      }
      oldsum_cell[i] = sum_cell[i];
   }

   vector<double> affinities = mean_affinity(); // generates a table with affinity classes for CB, CC and out
   double a = affinities[15]; // the average affinity is the last value of the table

   logmeanaff << time << "   " << a << "   " << affinities[0] << "   "
              << affinities[1] << "   " << affinities[2] << "\n";
   loghighaff << time << "   " << affinities[3] << "   "
              << affinities[4] << "   " << affinities[5] << "\n";
   log048aff << time;
   for (short int i = 6; i < 15; i++) {
      log048aff << "   " << affinities[i];
   }
   log048aff << "\n";
   // for analysis
   CB_haffinity = affinities[3];
   CC_haffinity = affinities[4];
   OUT_haffinity = affinities[5];

   get_diversity(time);

   if ((time > 144. - 1.e-08) && (time < 144. + 1.e-08)) {
      OUT_steepness = double (sum_cell[sout]);
   }
   if ((time > 288. - 1.e-08) && (time < 288. + 1.e-08)) {
      OUT_steepness = double (sum_cell[sout]) / OUT_steepness;
   }
}


template <typename T>
string generalSequenceContainer<T>::printSequences(double currentTime) {
   stringstream ss;
   for (int i = 0; i < n_Sequences; ++i) {
      if (getSequence(i)->getType() == typeBCR) {
         if ((getSequence(i)->n_cell[sCB]) + (getSequence(i)->n_cell[sCC]) + (getSequence(i)->n_cell[sout]) > 0){
            T * seq = getSequence(i);
            ss << currentTime << "_" << i << "_" << getSequence(i)->n_cell[sCB] << "_" << getSequence(i)->n_cell[sCC] << "_" <<
                getSequence(i)->n_cell[sout] << "\t" << seq->print() << "\n";
          }
       }
    }
    return ss.str();
}

template <typename T>
string generalSequenceContainer<T>::printStateAllSequences(bool showSequencesWithNoAliveCells, bool showTree) {
   stringstream ss;
   ss << get_n_Antigens() << " sequences of antigens, size and sequence\n";
   for (int i = 0; i < get_n_Antigens(); ++i) {
      ss << "Ag nr " << i << " ,ID=\t" << get_Antigen(i) << "\t"
         << getSequence(get_Antigen(i))->size << "\t" << getSequence(get_Antigen(i))->print()
         << "\n";
   }
   ss << get_n_Seeders() << " sequences of seeders, their affinity to each antigen, best affinity, size and sequence\n";
   for (int i = 0; i < get_n_Seeders(); ++i) {
      ss << "Seeder BCR nr " << i << " ,ID=\t" << get_Seeder(i) << "\t";
      for (int j = 0; j < get_n_Antigens(); ++j) {
         double aff = T::affinity(getSequence(get_Seeder(i)), getSequence(get_Antigen(j)));
         ss << aff << "\t";
      }
      ss << getSequence(get_Seeder(i))->size << "\t"
      << best_affinity(get_Seeder(i)) << "\t" << getSequence(get_Seeder(i))->print() << "\n";
   }
   ss << get_n_TCRs() << " sequences of TCRs\n";
   for (int i = 0; i < get_n_TCRs(); ++i) {
      ss << "TCR nr " << i << " ,ID=\t" << get_TCR(i) << "\t" << getSequence(get_TCR(i))->size
         << "\t" << getSequence(get_TCR(i))->print() << "\n";
   }

   vector<vector<long> > tree;
   tree.resize(n_Sequences);
   for (int i = 0; i < n_Sequences; ++i) {
      if (indexParentSeq[i] >= 0) { tree[indexParentSeq[i]].push_back(i); } // will apply to B and T cells, anyways no impact
   }

   ss << n_Sequences - n_Antigens - n_TCRs << " sequences of BCRs (seeders or not)\n";
   if (!showSequencesWithNoAliveCells) {
      ss << "List of sequences with alive cells carrying them:\n";
   }
   for (int i = 0; i < n_Sequences; ++i) {
      if (getSequence(i)->getType() == typeBCR) {
         if (showSequencesWithNoAliveCells
             || ((getSequence(i)->n_cell[sCB]) + (getSequence(i)->n_cell[sCC])
                 + (getSequence(i)->n_cell[sout]) > 0)) {
            T * seq = getSequence(i);     // doesn't need the cast actually
            ss << "ID=\t" << i << "\tCellsTot-B-C-O\t" << getSequence(i)->n_cell[sCB]
               + getSequence(i)->n_cell[sCC] + getSequence(i)->n_cell[sout] << "\t"
               << getSequence(i)->n_cell[sCB] << "\t" << getSequence(i)->n_cell[sCC] << "\t"
               << getSequence(i)->n_cell[sout] << "\t(" << seq->size << "),affinity=\t";
                for (int j = 0; j < get_n_Antigens(); ++j) {
                   double aff = T::affinity(getSequence(i), getSequence(get_Antigen(j)));
                   ss << aff << "\t";
                }
               ss << best_affinity(i) << "\t" << seq->print() << "\tMotherID\t" << indexParentSeq[i] << "\n";
            if (showTree) {
               if (tree[i].size() > 0) {
                  ss << "\tDaughters are :\n";
                  for (int j = 0; j < (int) tree[i].size(); ++j) {
                     ss << "\t-->ID=\t" << tree[i][j] << "\t,nCC=\t"
                        << getSequence(tree[i][j])->n_cell[sCC]
                        << "\taffinity=\t" << best_affinity(tree[i][j]) << "\tImproving=\t"
                        << best_affinity(tree[i][j]) - best_affinity(i) << "\t" << (best_affinity(tree[i][j]) - best_affinity(i)) / max(1e-16, best_affinity(i)) << "\n";
                  }
               }
            }
         }
      }
   }

   ss << external_cells.size()
      << " Sequences IDs where there are external cells (producing or not) \n";
   set<long int>::iterator it;
   for (it = external_cells.begin(); it != external_cells.end(); ++it) {
      T* seq = getSequence(*it);
      ss << "ID=\t" << *it << "\t";
      if (seq->getType() != typeBCR) {
         ss << "ERR: this is of type "
            << typeInString(seq->getType()) << endl;
      } else {
         ss << "n_soutext=\t" << seq->n_cell[soutext] << "\tn_soutextproduce:\t"
            << seq->n_cell[soutextproduce] << endl;
      }
   }

   ss << ab_producers.size() << " Sequences IDs where there are producing cells \n";
   for (int i = 0; i < (int) ab_producers.size(); ++i) {
      T* seq = getSequence(ab_producers[i]);
      ss << "ProducerCel\t" << i << " ,ID=\t" << ab_producers[i] << "\t";
      if (seq->getType() != typeBCR) {
         ss << "ERR: this is of type "
            << typeInString(seq->getType()) << endl;
      } else {
         ss << "n_soutext=\t" << seq->n_cell[soutext] << "\tn_soutextproduce:\t"
            << seq->n_cell[soutextproduce] << endl;
      }
   }

   return ss.str();
}

template <typename T>
void generalSequenceContainer<T>::write_state_cells(double currentTime) {
    stringstream fname1; fname1 << "seqspace" << currentTime << ".out";
    stringstream fname2; fname2 << "GCgraph" << currentTime << ".out";
    stringstream fname3; fname3 << "sequences_" << currentTime << ".out";

    cout << "Wrting state of cells at time " << currentTime << endl;

    ofstream outSeqSpace;
    outSeqSpace.open(fname1.str().c_str());
    outSeqSpace << printStateAllSequences(true, false);
    outSeqSpace.close();

    outSeqSpace.open(fname3.str().c_str());
    outSeqSpace << printSequences(currentTime);
    outSeqSpace.close();

    outSeqSpace.open(fname2.str().c_str());
    outSeqSpace << printGraph();
    outSeqSpace.close();
}

template <typename T>
void generalSequenceContainer<T>::write_gcbc_affinity(double time) {
   // +++++++++++++++ OPTION ++++++++++++++++
   double logfactor = 3.0;
   int n_bins = 10;
   vector<double >affarray(n_bins, 0.);
   affarray[0] = 0;
   affarray[1] = 0.0002 / logfactor;
   for (int b = 2; b < n_bins; b++) {
      affarray[b] = logfactor * affarray[b - 1];
   }
   /* for (int b = 0; b < n_bins; b++) {
    *  cout << affarray[b] << ",";
    *  } cout << "\n";  */
   // +++++++++++ end OPTION ++++++++++++++++

   // double check that the whole range of binding probabilities is covered
   if (affarray[n_bins - 1] * logfactor < 1.0) {
      cerr << "In sequenceSpace::write_gcbc_affinity(double): largest affinity is smaller than 1.\n"
           << "Abort simulation.\n";
      exit(1);
   }
   // define a set of counters for each Hamming distance
   vector<int> nbc(n_bins, 0);
   // .. and for the mean Hamming distance for all antigens/epitopes
   vector<int> nbc_mean_ag(n_bins, 0);

   // Go through all points of the shape space and add to the counters:
   // Only should take into account BCR sequences
   for (long i = 0; i < n_Sequences; i++) {

       T* seq = getSequence(i);
       if(seq->getType() == typeBCR){


          // get the total number of GC-BCs at this point
          int nbchere = getSequence(i)->n_cell[sCB] + getSequence(i)->n_cell[sCC];
          // determine the Hamming distance of this point to the nearest antigen
          double affbest = best_affinity_norm(i);
          double affmean = mean_affinity_norm(i);
          // Add the number of GC-BC found to the counter of cells with Hamming-distance <hamming>
          int binbest = 0, binmean = 0;
          while (binbest<n_bins&&affbest> affarray[binbest]) {
             ++binbest;
          }   // now binbest is one too far, as affbest>0, binbest>0 as well
          --binbest;
          // this is the array position for nbc
          nbc[binbest] += nbchere;
          // repeat the same for affmean
          while (binmean<n_bins&&affmean> affarray[binmean]) {
             ++binmean;
          }
          --binmean;
          nbc_mean_ag[binmean] += nbchere;
        }
   }
   // open the output file (append)
   ofstream gcbc_affinity;
   gcbc_affinity.open("gcbc_affinity.out", ofstream::app);
   for (int n = 0; n < n_bins; n++) {
      gcbc_affinity << time / 24. << "   " << time << "   " << affarray[n] << "   "
                    << nbc[n] << "   " << nbc_mean_ag[n] << "\n";
   }
   gcbc_affinity.close();
}




template <typename T>
void generalSequenceContainer<T>::correct_average_affinity(cells celltyp, long &pos, double &average) {
   // Never call this routine before the first cell of type celltyp was created in SS!
   if (sum_cell[celltyp] == 1) {
      average = 0;
   }
   // cout<<"average vorher="<<average<<"; add aff="<<best_affinity(pos)<<"; ";
   average = ((sum_cell[celltyp] - 1) * average + best_affinity_norm(pos)) / (sum_cell[celltyp]);
   // cout<<"sum="<<sum_cell[celltyp]<<"; threshold="<<average<<"\n";
}

template <typename T>
double generalSequenceContainer<T>::Abstandquad(long int &n, long int &n2){
    static bool raised = false;
    if(!raised){cerr << "ERR: function Abstandquad not defined for sequence spaces" << endl; raised = true;}
    return 0;
}

template <typename T>
void generalSequenceContainer<T>::getStatistics(double &_OUT_haffinity,
                   double &_OUT_steepness,
                   double &_CB_haffinity,
                   double &_CC_haffinity) {
   _OUT_haffinity = OUT_haffinity;
   _OUT_steepness = OUT_steepness;
   _CB_haffinity = CB_haffinity;
   _CC_haffinity = CC_haffinity;
}

template <typename T>
void generalSequenceContainer<T>::write_gcbc_hamming(double time) {
    static bool raised = false;
    if(!raised){cerr << "ERR: function write_gcbc_hamming not defined for sequence spaces" << endl; raised = true;}
    return;

    /* code from the shape space. could be adapted when hamming functions are made for the sequence representations
    // +++++++++++++++ OPTION ++++++++++++++++
   int max_hamming = 12;
   // +++++++++++ end OPTION ++++++++++++++++
   // define a set of counters for each Hamming distance
   int nbc[max_hamming + 1];
   // .. and for the mean Hamming distance for all antigens/epitopes
   int nbc_mean_ag[max_hamming + 1];
   int nbchere = 0;
   for (int n = 0; n <= max_hamming; n++) {
      nbc[n] = 0;
      nbc_mean_ag[n] = 0;
   }
   int hamming = 0;
   double hamming_mean = 0.;
   // remove this line in the general case:
   // cout << "WARNING! Modified wrong code in SS::best_hamming_distance(long&)\n";
   // Go through all points of the shape space and add to the counters:
   for (long i = 0; i < PointsTotal; i++) {
      // get the total number of GC-BCs at this point
      nbchere = ssp[i].n_cell[sCB] + ssp[i].n_cell[sCC];
      // determine the Hamming distance of this point to the nearest antigen
      hamming = best_hamming_distance(i);
      hamming_mean = mean_hamming_distance(i);
      // Add the number of GC-BC found to the counter of cells with Hamming-distance <hamming>
      if (hamming >= max_hamming) {
         nbc[max_hamming] += nbchere;
      } else {
         nbc[hamming] += nbchere;
      }
      // now do rounding of the mean hamming distance of SS-point i:
      hamming = int (hamming_mean + 0.5);
      // ... and attribute to the
      if (hamming >= max_hamming) {
         nbc_mean_ag[max_hamming] += nbchere;
      } else {
         nbc_mean_ag[hamming] += nbchere;
      }
   }
   // open the output file (append)
   ofstream gcbc_hamming;
   gcbc_hamming.open("gcbc_hamming.out", ofstream::app);
   for (int n = 0; n <= max_hamming; n++) {
      gcbc_hamming << time / 24. << "   " << time << "   "
                   << n << "   "
                   << nbc[n] << "   "
                   << nbc_mean_ag[n]
                   << "\n";
   }
   gcbc_hamming.close();
   */
}








///§§§ Philippe: think of deleting the sequence pointers after converting to hyXXX


vector<bool> prepareMask(int ncon, int LseqsAG, string requestedMask, ofstream& ana){
    ana << "Settings for conservation between antigens:" << endl;
    ana << "   -> Number of conserved positions: " << ncon << endl;
    if(!requestedMask.compare(string("-1"))){ // -1 means empty
        requestedMask.clear();
    }
    int LM = requestedMask.size();
    ana << "   -> Optionally, conservation mask: " <<
           ((LM > 0)? requestedMask : string("not given")) << endl;

    vector<bool> mask(LseqsAG, false);
    if(LM > 0){
        if(LM != LseqsAG){
            cerr << "ERR: you gave a conservation mask " << requestedMask
                 << " while (antigen) sequences have size " << LseqsAG << endl;
            exit(-1); // important to exit this function, at least
        }
        int cptFix = 0;
        for(int i = 0; i < LM; ++i){
            char c = requestedMask[i];
            if(c == '1') {
                cptFix++;
                mask.at(i) = true;
            }
            else if (c != '0'){
                cerr << "ERR: unaccepted character " << c << " reading conservation mask "
                     << requestedMask << ". Use 0/1. " << endl;
            }
        }
        if(cptFix != ncon){
            cerr << "ERR: the conservation mask given does not fit the number of conserved residues" << endl;
            exit(-1);
        }
    }
    if(LM == 0){
        for(int i = 0; i < ncon; ++i){
            mask.at(i) = true;
        }
        random::shuffle<bool>(mask);
    }
    if(LM == 0) ana << "   -> Randomly generated mask: ";
    for(unsigned int i = 0; i < mask.size(); ++i){
        ana << ((mask[i]) ? '1' : '0');
    }
    ana << "\n";
    return mask;
}

template<typename T>
vector<T*> readFixedAntigens(vector<string> toRead, int Lseqs, int nbToPick, vector<bool> mask, ofstream& ana){
    ana << "   -> Taking " << nbToPick << "Antigens from a predefined set (random = false)\n ";
    vector<T*> res;
    //int Lseqs = mask.size();
    for(int i = 0; i < nbToPick; ++i){
        if(toRead[i].compare(string("-1"))){
            T* newAg = new T(toRead[i]);

            if(newAg->size > Lseqs){
                cerr << "ERR: predefined antigen " << toRead[i] << " is too long, please define the MAX length of antigens in the parameter file: " << Lseqs << endl;
                exit(-1);
            }
            if(newAg->size < Lseqs){
                //vector<bool> maskShorter = mask;
                //mask.resize(newAg->size);
            }
            newAg->setConservedPositions(mask); // for further possible mutations of antigen
            res.push_back(newAg);
        }
    }
    return res;
}

// applies for BCRs and TCRs, but not antigens (conservation constraints)
template<typename T>
vector<T*> readFixedSeqs(vector<string> toRead, int Lseqs, int NFix, ofstream& ana){
    // Make sure Nfix < toRead.size() before calling.
    vector<T*> res;
    ana << "   -> Taking " << NFix << "BCRs from a predefined set - will be added to the automatically generated \n ";
    for(int i = 0; i < NFix; ++i){
        if(toRead[i].compare(string("-1"))){
            T* newBCR = new T(toRead.at(i));
            if(newBCR->size != Lseqs){
                cerr << "ERR: predefined BCR " << toRead.at(i) << " has inconsistent size from requested: " << Lseqs << endl;
                exit(-1);
            }
            res.push_back(newBCR);
        }
    }
    return res;
}



template<typename T>
vector<T*> genRandomAntigens(int nbToPick, int Lseqs, vector<bool> mask, int minHamming, int maxHamming, ofstream& ana){
    vector<T*> res;
    T* firstAg = NULL;      // first antigen is random, then other should follow the conserved mask to it.
    ana << "   -> Generating " << nbToPick << " antigens randomly\n ";
    for(int i = 0; i < nbToPick; ++i){
        T * newAg;
        if(firstAg == NULL) {
            newAg = new T(Lseqs);
            newAg->randomize();
        } else {
            newAg = new T(firstAg);
        }
        newAg->setConservedPositions(mask);
        int tries = 0;
        bool correct = false;
        while ((tries < 10000) && (!correct)) {
            newAg->randomize();
            correct = true;
            for (unsigned int i = 1; i < res.size(); ++i) {
                correct = correct && (T::hamming(res.at(i), newAg)
                                      >= minHamming);
                correct = correct && (T::hamming(res.at(i), newAg)
                                      <= maxHamming);
            }
            tries++;
        }
        if (!correct) {
            cerr << "ERR: failed to create group of antigens zith hamming distance ["
                 << minHamming << ";" << maxHamming << "]" << endl;
            //cerr << " ... takes a random antigen sequence instead " << endl;
            //newAg->randomize();
            exit(-1);
        }
        res.push_back(newAg);
        if(firstAg == NULL) firstAg = newAg;
    }
    return res;
}

//generalSequenceContainer<T>::
template<typename T>
void checkSeederParameters(Parameter& par, ofstream& ana){
    int nAg = par.Value.nAntigenTypesSeqSpaces;
    if(nAg < 1){
        cerr << "ERR: you defined a simulation without antigen while using sequence representation (nAntigenTypesSeqSpaces)! " << endl;
        exit(-1);
    }
    ana << "   -> " << par.Value.nbBCRclasses << " different types(classes) of BCRs" << endl;
    ana << "   -> with hamming between " << par.Value.max_hamming_BCRs
        << " and " << par.Value.max_hamming_BCRs << "between them inside each class" << endl;
    int nClasses = par.Value.nbBCRclasses;
    if(nClasses < 1){
        cerr << "   -> No BCR seeder affinity class specified. Implicitely create one class with no constraints (compatibility mode)." << endl;
        par.Value.nbBCRclasses = 1;
        par.Value.freqBCRclasses.clear();
        par.Value.freqBCRclasses.resize(1, 1.0);
        par.Value.timePeriodsStartBCRclasses.clear();
        par.Value.timePeriodsStartBCRclasses.resize(1, -1e16);
        par.Value.timePeriodsStopBCRclasses.clear();
        par.Value.timePeriodsStopBCRclasses.resize(1, +1e16);
        par.Value.affinityRangesBCRClasses.clear();
        vector<double> lineAffinities;
        for(int i = 0; i < nAg; ++i){
            lineAffinities.push_back(-1e16);
            lineAffinities.push_back(+1e16);
        }
        par.Value.affinityRangesBCRClasses.push_back(lineAffinities);
    }
    if((int) par.Value.freqBCRclasses.size() < nClasses){
        cerr << "ERR: the frequency of BCR classes is only given for " << par.Value.freqBCRclasses.size() << " while " << nClasses << " classes requested" << endl;
        exit(-1);
    }
    // Renormalizes freqBCRclasses to 1 (for instance if was defined for more antigens)
    par.Value.freqBCRclasses.resize(nClasses);
    double totN = 0;
    for(int i = 0; i < nClasses; ++i){
        totN += par.Value.freqBCRclasses.at(i);
    }
    for(int i = 0; i < nClasses; ++i){
        par.Value.freqBCRclasses.at(i) /= (max(1e-16, totN));
    }
    int NSP = par.Value.timePeriodsStartBCRclasses.size();
    if((NSP == 0) || ((NSP == 1) && (par.Value.timePeriodsStartBCRclasses[0] == -1))){
        par.Value.timePeriodsStartBCRclasses.clear();
        par.Value.timePeriodsStartBCRclasses.resize(nClasses,0);
    }
    int NTP = par.Value.timePeriodsStopBCRclasses.size();
    if((NTP == 0) || ((NTP == 1) && (par.Value.timePeriodsStopBCRclasses[0] == -1))){
        par.Value.timePeriodsStopBCRclasses.clear();
        par.Value.timePeriodsStopBCRclasses.resize(nClasses,0);
    }
    if((int) par.Value.timePeriodsStartBCRclasses.size() < nClasses){
        cerr << "ERR: the time of starting for BCR classes is only given for " << par.Value.timePeriodsStartBCRclasses.size() << " while " << nClasses << " classes requested" << endl;
        exit(-1);
    }
    if((int) par.Value.timePeriodsStopBCRclasses.size() < nClasses){
        cerr << "ERR: the time of ending for BCR classes is only given for " << par.Value.timePeriodsStopBCRclasses.size() << " while " << nClasses << " classes requested" << endl;
        exit(-1);
    }
    if((int) par.Value.affinityRangesBCRClasses.size() < nClasses){
        cerr << "ERR: the affinity ranges for BCR classes is only given for " << par.Value.affinityRangesBCRClasses.size() << " while " << nClasses << " classes requested" << endl;
        exit(-1);
    }
    for(int i = 0; i < nClasses ; ++i){
        vector<double> minMaxAffinitiesToAg = par.Value.affinityRangesBCRClasses[i];
        if((int) minMaxAffinitiesToAg.size() < 2 * nAg){
            cerr << "ERR: Too many antigens, not enough values (columns) for max/min affinities of BCRs of class (line) " << i << endl;
            exit(-1);
        }
    }
}

string getBCRfromLibrary(vector<bool> suitable, vector<std::pair<double, vector<string> *> > loadedLibraries){
    // assumes at least one is suitable.
    int N = suitable.size();
    for(int i = 0; i < 100; ++i){   // bounded while written as a if
        int ix = random::uniformInteger(0, N-1);
        if(suitable.at(ix)){
            vector<string>* list = loadedLibraries.at(ix).second;
            if(!list) {cerr << "getBCRfromLibrary, Shit in the pipes clogging somewhere" << endl; return string("");}
            int ib = random::uniformInteger(0, list->size()-1);
            return (*list)[ib];
        }
    }
    cerr << "ERR: getBCRfromLibrary couldn't find anysuitable library" << endl;
    return string("");
}

template<typename T>
vector<T*> generateSeederClass(double nbToGenerate, int Lseqs, vector<double> minMaxAffinitiesToAg, vector<T*> antigens, int min_hamming_BCRs, int max_hamming_BCRs, ofstream& ana, vector<std::pair<double, vector<string> *> > loadedLibraries){
    // creates a seed sequence with a required affinity to the antigens + hamming constraints

    int tries2 = 0;
    int nbSuccess = 0;
    //binarySequence * seedSeq = NULL;  // the first sequence will be a reference for hamming distances.
    int NbToGen = (int) nbToGenerate;
    if(random::uniformDouble(0,1) <= nbToGenerate - (double) NbToGen){
        NbToGen++;
    }
    int nAg = antigens.size();

    bool modeUseBCRsFromLibrary = false;    // will be activated if fails to find good BCR sequences
    // prepare to use libraries
    vector<bool> suitableLibraries (nAg, false);
    int nSuitable = 0;
    cout << "\n     ... Defining suitable libraries for this class of seeders, in case needed " << endl;
    if((int) loadedLibraries.size() == nAg){
        for(int j = 0; j < nAg;++j){
            if(minMaxAffinitiesToAg[2*j] > loadedLibraries[j].first){
                suitableLibraries[j] = true;
                nSuitable++;
            }
            if(!loadedLibraries[j].first) cout << "          for Ag " << j << ", no Library loaded" << endl;
            else {
                cout << "          for Ag " << j << ", requested threshold: " << minMaxAffinitiesToAg[2*j] << ", library threshold: " << loadedLibraries[j].first;
                if(suitableLibraries[j]) cout << " -> suitable" << endl; else cout << " -> not suitable here " << endl;
            }

        }
    } else {cerr << "WRN Could not load BCR libraries, generateSeederClass should receive one per antigen" << endl;}

    cout << "     -> Start looking for random BCRs with requested affinities. If doesn't reach 10% in 200 tries, switch to libraries " << endl;
    vector<T*> poolInThisClass;
    while (nbSuccess < NbToGen){
        // 2021-03-27 Philippe: If we wish to use predefined seeder BCR sequences to be parse, this will happen here.
        T* test = new T(Lseqs);
        if(modeUseBCRsFromLibrary){             // if prefers to take from presorted good BCR sequences
            test->content = getBCRfromLibrary(suitableLibraries, loadedLibraries);
        } else {
            test->randomize();
        }
        bool correct = true;
        for(int j = 0; j < nAg; ++j){
            if(minMaxAffinitiesToAg[2*j] > 1e-16) correct = correct && (T::affinity(test, antigens.at(j)) >= minMaxAffinitiesToAg[2*j]); // speeding ....
            if(minMaxAffinitiesToAg[2*j+1] < 1.0)correct = correct && (T::affinity(test, antigens.at(j)) <= minMaxAffinitiesToAg[2*j + 1]);
        }
        if(nbSuccess > 0){
            int SP = poolInThisClass.size(); // could replace by hamming of seedSeq < max/2. Not sure for min.
            for(int j = 0; j < SP; ++j){
                int ham = T::hamming(test, poolInThisClass[j]);
                correct = correct && (ham >= min_hamming_BCRs);
                correct = correct && (ham <= max_hamming_BCRs);
            }
        }
        if((tries2 > 150) && ((tries2 % 100) == 0)){
            cout << "     ... Having trouble to find suitable BCR (test" << tries2 << "/25000, success " << nbSuccess << "/" << NbToGen << ").\n         Example: " << test->print() << "\n";
            for(int j = 0; j < nAg; ++j){
                cout << "         Ag" << j << ", aff " << T::affinity(test, antigens.at(j)) << ", wanted ["
                     << minMaxAffinitiesToAg[2*j] << "," << minMaxAffinitiesToAg[2*j + 1] << "]";
            }
            cout << "\n         Hamming[" << min_hamming_BCRs << "," << max_hamming_BCRs << "]: values with previous:\t";
            int SP = poolInThisClass.size();
            for(int j = 0; j < min(50,SP); ++j){
                cout << " " << T::hamming(test, poolInThisClass[j]);
            }
            cout << "\n\n";
            if(tries2 == 200){
                if(((double) nbSuccess / max(1.0, (double) NbToGen)) < 0.1){// if couldnt generate 10% in 200 shots, read from library
                    if(nSuitable > 0) {
                        modeUseBCRsFromLibrary = true;
                        cout << "     -> Will not be able to generate the seeders by random picking => Now reading from BCR libraries !" << endl;
                    } else {
                        cout << "     -> tried to read from BCR libraries, but none was compatible with the affinity threshold. \n      => no library, program will take long." << endl;
                    }
                } else {
                    cout << "     -> Will continue trying, should be reached fast. No need of library" << endl;
                }
            }
        }
        if(tries2 >= 25000) {
            cerr << "ERR: couldn't find appropriate seeder sequences, will abort" << endl;
            exit(-1);
            // or correct = true;     // If couldn't complete, just accept anything. Sorry...
        }
        if(correct){
            //if(nbSuccess == 0) seedSeq = new binarySequence(test);
            cout << "      ... took " << test->content << endl;
            poolInThisClass.push_back(test);
            nbSuccess++;
        }
        tries2++;
    }
    if (tries2 >= 9999) {
       cerr << "Warning: could not generate a seeding BCR sequence with required affinity to antigens\n"
            << "   -> Added random sequences to complete the filling" << endl;
    }
    cout << "      -> Seeders for this class successfully generated!" << endl;
    return poolInThisClass;
}




// =================== Constructors of the sequence spaces: ==============================
//      they have to set up the layout of sequences before the start of simulation

// Informations from the parameter file:

// 1- Generic information on what to set up

    /* enum {typeShapeSpace, typebinarySequences, typebinaryLineages, typefoldedCubic, typefoldedFree, typeDNAfoldedFree, typeperelson, typeperelsonGeneral, typeArup};
    int typeSeqSpace;                        // 349 Type of sequence representation
    double proba_lethal_mut;                 // 341 probability of a mutation being lethal [proba_lethal_mut]
    double proba_affecting_mut;              // 342 probability of a mutation being affecting [proba_affecting_mut]
    double proba_silent_mut;                 // 343 probability silent [proba_silent_mut]
    int nAntigenTypesSeqSpaces;              // 307 Number of antigen types [nAntigenTypesSeqSpaces]
    vector<double> freqAntigenClasses;       // 313 Frequency of each antigen type (sum will be renormalized to 1) [freqAntigenClasses]
    vector<double> appearanceTimeAntigenClasses;    // 314 Time appearance of each antigen type, in hours [appearanceTimeAntigenClasses]
    int typeAdditionAntigen;                 // 318 Mode of addition of an antigen (1:keep total antigen amount constant, 2:Add separately to reach total amount when all are here) [typeAdditionAntigen]

    /// !! Not used !! int nbBCRsSequencesToGenerate;           // 390 Total number of seeder sequences to pre-generate [nbBCRsSequencesToGenerate]
    int nbBCRclasses;                       // 391 Number of B cell seeder classes [nbBCRclasses]:
    vector<double> freqBCRclasses;          // 392 Frequency of each seeder class [freqBCRclasses]:
    vector< vector<double> > affinityRangesBCRClasses; //393  Minimum and maximum affinity of each seeder class to each antigen. You can put more antigen classes but not less. Affinities range between 0 and 1 (10^-4.5 to 10^-9) [affinityRangesBCRClasses]:
    vector<double> timePeriodsStartBCRclasses;   // 394 Min and max time period constraint for the appearance of seeder classes (min and max time in hours), put -1 for no constraint
    vector<double> timePeriodsStopBCRclasses;   // 395 Min and max time period constraint for the appearance of seeder classes (min and max time in hours), put -1 for no constraint
    int max_hamming_BCRs;                   // 312
    int min_hamming_BCRs;                   // New 320  */

//class binaryLineageSpace : public generalSequenceContainer<hyBinaryLineage> {
binaryLineageSpace::binaryLineageSpace(Parameter & p, ofstream & report){

    initializeAnalyzeFiles(report);

    /*/ BinaryLineages
    // ============
     // Probability of mutation, per base.
     double binaryLineage_mut_per_base;      // New 398
     // Length of sequences:
     int binaryLineage_size_sequences;       // New 399
     vector<double> binaryLineage_distribAccessValues; // New 400
     int binaryLineage_nb_conserved; // New 401
    // Fix Antigen Sequence presentation (max 1000 values):
    vector<string> binaryLineage_initAntigenSeqs;    // New 402
    // Fix initial Repertoire distribution (max 1000 values):
    vector<string> binaryLineage_initBCRSeqs;    // New 403
    // Fix initial Repertoire distribution for T cells:
    vector<string> binaryLineage_initTCRSeqs;        // New 404 */
}

//class binarySequenceSpace : public generalSequenceContainer<hyBinarySequence> {
binarySequenceSpace::binarySequenceSpace(Parameter & par, ofstream & ana){

    // create output files to be filled at each time-point
    initializeAnalyzeFiles(ana);

    // Reading and forwarding parameters to the sequence representation itself
    double maxSizeClusters = par.Value.binarySeq_max_affinity_cluster;
    if (maxSizeClusters < 0) { maxSizeClusters = par.Value.binarySeq_size_sequences; }
    double rescaling_exponent = par.Value.binarySeq_RescalingExponent;
    bool use_logarithmic_seq_affinity = (rescaling_exponent < 0);
    if(rescaling_exponent< 0) rescaling_exponent = 1.0;
    binarySequence::initializeDefaultParameters(par.Value.binarySeq_type_affinity_function,
                                                par.Value.binarySeq_R_affinity, maxSizeClusters,
                                                use_logarithmic_seq_affinity, rescaling_exponent);

     ana << "Settings of the binary sequence representation" << endl;
     ana << "  -> type of affinity: " << par.Value.binarySeq_type_affinity_function
         << ((par.Value.binarySeq_type_affinity_function == seqAff) ? ": standard affinity/seqAff(=0)" : "")
         << ((par.Value.binarySeq_type_affinity_function == seqAffNorm) ? ": affinity normalized by cluster size/seqAffNorm(=1)" : "")
         << ((par.Value.binarySeq_type_affinity_function == seqAffWindow) ? ": sliding window/seqAffWindow(=2)" : "") << endl;
     ana << "  -> specificity (R) : " << par.Value.binarySeq_R_affinity << "\n";
     ana << "  -> max cluster size: " << maxSizeClusters << "\n";
     ana << "  -> log affinity ?  : " << ((use_logarithmic_seq_affinity) ? "Yes" : "No") << "\n";
     ana << "  -> rescaling expon.: " << rescaling_exponent << "\n";
     ana << "\n";

     // Loading parameter for managing differentiation to PC and antibody production
     ana << "General Settings of the space of sequences for hyphasma simulations" << "\n";
     double _pm_differentiation_rate = log(2.) / par.Value.pm_differentiation_time;

     // Note: the mutation rate is defined per base and has to be transformed in a probability at the whole sequence size.
     initialize(par.Value.binarySeq_mut_per_base * par.Value.binarySeq_size_sequences,
                par.Value.proba_lethal_mut,
                par.Value.proba_affecting_mut,
                par.Value.proba_silent_mut,
                _pm_differentiation_rate);
     ana << "   -> (try) mutation rate = " << par.Value.binarySeq_mut_per_base * par.Value.binarySeq_size_sequences<< "\n";
     ana << "   -> proba_lethal_mut = " <<    par.Value.proba_lethal_mut << "\n";
     ana << "   -> proba_affecting_mut = " << par.Value.proba_affecting_mut << "\n";
     ana << "   -> proba_silent_mut = " <<    par.Value.proba_silent_mut << "\n";
     ana << "   -> pm differentiation rate = " << _pm_differentiation_rate << "\n";

     ana << "Specific options for this sequence representation (binarySequences)" << "\n";
     ana << "  -> All sequences will be of size: " << par.Value.binarySeq_size_sequences << "\n";
     int Lseqs =  par.Value.binarySeq_size_sequences;

     ana << "Initialize Antigen pool ...\n ";
     vector<bool> mask = prepareMask(par.Value.binarySeq_nb_conserved, Lseqs, par.Value.binarySeq_conserved_mask, ana);
     ana << "   -> Number of antigens required : " << par.Value.nAntigenTypesSeqSpaces << endl;
     ana << "      with hamming distance between each-other within " << par.Value.min_hamming_antigens
         << " and " << par.Value.max_hamming_antigens << endl;

     int nAg = par.Value.nAntigenTypesSeqSpaces;
     int predef = par.Value.binarySeq_initAntigenSeqs.size();
     if(par.Value.randomAgGeneration == false){
         ana << "   -> Taking " << nAg << "Antigens from a predefined set (random = false)\n ";
         for(int i = 0; i < min(predef, nAg); ++i){
             if(par.Value.binarySeq_initAntigenSeqs[i].compare(string("-1"))){
                binarySequence* newAg = new binarySequence(par.Value.binarySeq_initAntigenSeqs[i]);
                newAg->setConservedPositions(mask); // for further possible mutations of antigen
                int index = add_Antigen(new hyBinarySequence(newAg,typeAG));
                cout << "      + (ID=" << index << ", " << newAg->print() << endl;
                if(newAg->size != Lseqs){
                    cerr << "ERR: predefined antigen " << par.Value.binarySeq_initAntigenSeqs[i] << " has inconsistent size from requested: " << Lseqs << endl;
                    exit(-1);
                }
                delete newAg;
             }
         }
         if(get_n_Antigens() < nAg){
             cerr << "ERR: you didn't specify enough predefined antigens (" << get_n_Antigens() << ") in the parameter file, instead of " << nAg << " requested" << endl;
             exit(-1);  // note: i waited that the sequences are created to raise error, maybe good number of sequencea but incorrect or -1.
         }
     } else {
         ana << "   -> Generating " << nAg << " antigens randomly\n ";
         for(int i = 0; i < nAg; ++i){
             binarySequence * newAg = new binarySequence(Lseqs);
             newAg->setConservedPositions(mask);
             int tries = 0;
             bool correct = false;
             while ((tries < 10000) && (!correct)) {
                 newAg->randomize();
                 correct = true;
                 for (int i = 1; i < nAg; ++i) {
                     correct = correct && (binarySequence::hamming(getSequence(get_Antigen(i)), newAg)
                                            >= par.Value.min_hamming_antigens);
                     correct = correct && (binarySequence::hamming(getSequence(get_Antigen(i)), newAg)
                                               <= par.Value.max_hamming_antigens);
                 }
                 tries++;
             }
             if (!correct) {
                 cerr << "ERR: binarySequence, failed to create group of antigens zith hamming distance ["
                      << par.Value.min_hamming_antigens << ";" << par.Value.max_hamming_antigens << "]" << endl;
                 //cerr << " ... takes a random antigen sequence instead " << endl;
                 //newAg->randomize();
                 exit(-1);
             }
             int index = add_Antigen(new hyBinarySequence(newAg, typeAG));
             cout << "      + (ID=" << index << ", " << newAg->print() << endl;
             delete newAg;
         }
     }
     ana << "... done.\n";

     ana << "Now, Initialize Seeder cell pool ... \n";

     // first, check the consistency of parameters
     ana << "   with the requested constraints: \n";
     ana << "   -> " << par.Value.totalBss << " different possible BCR sequences" << endl;
     ana << "   -> " << par.Value.nbBCRclasses << " different types of BCRs" << endl;

     ana << "   -> with hamming between " << par.Value.max_hamming_BCRs
         << " and " << par.Value.max_hamming_BCRs << "between them inside each class" << endl;

     int nClasses = par.Value.nbBCRclasses;
     if((int) par.Value.freqBCRclasses.size() < nClasses){
         cerr << "ERR: the frequency of BCR classes is only given for " << par.Value.freqBCRclasses.size() << " while " << nClasses << " classes requested" << endl;
         exit(-1);
     }
     // Renormalizes freqBCRclasses to 1 (for instance if was defined for more antigens)
     par.Value.freqBCRclasses.resize(nAg);
     double totN = 0;
     for(int i = 0; i < nAg; ++i){
        totN += par.Value.freqBCRclasses.at(i);
     }
     for(int i = 0; i < nAg; ++i){
        par.Value.freqBCRclasses.at(i) /= (max(1e-16, totN));
     }

     int NSP = par.Value.timePeriodsStartBCRclasses.size();
     if((NSP == 0) || ((NSP == 1) && (par.Value.timePeriodsStartBCRclasses[0] == -1))){
         par.Value.timePeriodsStartBCRclasses.clear();
         par.Value.timePeriodsStartBCRclasses.resize(nClasses,0);
     }
     int NTP = par.Value.timePeriodsStopBCRclasses.size();
     if((NTP == 0) || ((NTP == 1) && (par.Value.timePeriodsStopBCRclasses[0] == -1))){
         par.Value.timePeriodsStopBCRclasses.clear();
         par.Value.timePeriodsStopBCRclasses.resize(nClasses,0);
     }

     if((int) par.Value.timePeriodsStartBCRclasses.size() < nClasses){
         cerr << "ERR: the time of starting for BCR classes is only given for " << par.Value.timePeriodsStartBCRclasses.size() << " while " << nClasses << " classes requested" << endl;
         exit(-1);
     }
     if((int) par.Value.timePeriodsStopBCRclasses.size() < nClasses){
         cerr << "ERR: the time of ending for BCR classes is only given for " << par.Value.timePeriodsStopBCRclasses.size() << " while " << nClasses << " classes requested" << endl;
         exit(-1);
     }
     if((int) par.Value.affinityRangesBCRClasses.size() < nClasses){
         cerr << "ERR: the affinity ranges for BCR classes is only given for " << par.Value.affinityRangesBCRClasses.size() << " while " << nClasses << " classes requested" << endl;
         exit(-1);
     }

     // Taking the predefined BCR sequences
     // no conserved positions for BCRs!
     int NFix = par.Value.binarySeq_initBCRSeqs.size();
     ana << "   -> Taking " << NFix << "BCRs from a predefined set - will be added to the automatically generated \n ";
     predef = par.Value.binarySeq_initBCRSeqs.size();
     for(int i = 0; i < min(predef, NFix); ++i){
         if(par.Value.binarySeq_initBCRSeqs[i].compare(string("-1"))){
            binarySequence* newBCR = new binarySequence(par.Value.binarySeq_initBCRSeqs[i]);
            int index = add_Seeder(new hyBinarySequence(newBCR, typeBCR));
            cout << "      + (ID=" << index << ", " << newBCR->print() << endl;
            if(newBCR->size != Lseqs){
                cerr << "ERR: predefined BCR " << par.Value.binarySeq_initBCRSeqs[i] << " has inconsistent size from requested: " << Lseqs << endl;
                exit(-1);
            }
            delete newBCR;
         }
     }

     // int nAg = par.Value.nAntigenTypesSeqSpaces
     for(int i = 0; i < nClasses ; ++i){
        ana << "Class " << i << " of BCRs, freq: " << 100. * par.Value.freqBCRclasses[i] << "%" << endl;
        vector<double> minMaxAffinitiesToAg = par.Value.affinityRangesBCRClasses[i];
        if((int) minMaxAffinitiesToAg.size() < 2 * nAg){
            cerr << "ERR: Too many antigens, not enough values (columns) for max/min affinities of BCRs of class (line) " << i << endl;
            exit(-1);
        }
        // creates a seed sequence with a required affinity to the antigens + hamming constraints
        int NbToGen = par.Value.freqBCRclasses[i] * par.Value.totalBss - NFix;
        int tries2 = 0;
        int nbSuccess = 0;
        //binarySequence * seedSeq = NULL;  // the first sequence will be a reference for hamming distances.
        vector<binarySequence*> poolInThisClass;
        while (nbSuccess < NbToGen){
            binarySequence* test = new binarySequence(Lseqs);
            test->randomize();
            bool correct = true;

            // check the affinities
            for(int j = 0; j < nAg; ++j){
                correct = correct && (binarySequence::affinity(test, getSequence(get_Antigen(j))) >= minMaxAffinitiesToAg[2*j]);
                correct = correct && (binarySequence::affinity(test, getSequence(get_Antigen(j))) <= minMaxAffinitiesToAg[2*j + 1]);
            }
            if(nbSuccess > 0){
                int SP = poolInThisClass.size(); // could replace by hamming of seedSeq < max/2. Not sure for min.
                for(int j = 0; j < SP; ++j){
                    int ham = binarySequence::hamming(test, poolInThisClass[j]);
                    correct = correct && (ham >= par.Value.min_hamming_BCRs);
                    correct = correct && (ham <= par.Value.max_hamming_BCRs);
                }
            }
            if((tries2 % 1000) == 0) cerr << ".";
            // If couldn't complete, just accept anything. Sorry...
            if(tries2 >= 10000) correct = true;
            if(correct){
                //if(nbSuccess == 0) seedSeq = new binarySequence(test);
                long int index = add_Seeder(new hyBinarySequence(test, typeBCR));
                cout << "   + BCR: ID=" << index << ", " << test->print() << ", Affs to Ags:";
                for(int j = 0; j < nAg; ++j){
                    cout << "\t" << j << " " << binarySequence::affinity(test, getSequence(get_Antigen(j)));
                }
                cout << endl;
                poolInThisClass.push_back(test);
                nbSuccess++;
            }
            delete test;
            tries2++;
        }
        if (tries2 >= 9999) {
           cerr << "Warning: SequenceSpaces, could not generate a seeding BCR sequence with required affinity to antigens\n"
                << "   -> Added random sequences to complete the filling" << endl;
        }
     }

     // note: nothing happens yet to TCR sequences, but the program is ready to read them (06/2019)
     ana << "Initialize TCR initial sequences... \n";
     ana << par.Value.totalTC << " sequences required" << endl;

     // Taking the predefined BCR sequences


     // Taking the predefined TCR sequences
     int NFixT = par.Value.binarySeq_initTCRSeqs.size();
     ana << "   -> Taking " << NFix << "BCRs from a predefined set - will be added to the automatically generated \n ";
     predef = par.Value.binarySeq_initTCRSeqs.size();
     for(int i = 0; i < min(predef, NFixT); ++i){
         if(par.Value.binarySeq_initTCRSeqs[i].compare(string("-1"))){
            binarySequence* newTCR = new binarySequence(par.Value.binarySeq_initTCRSeqs[i]);
            int index = add_TCR(new hyBinarySequence(newTCR, typeTCR));
            ana << "      + TCR (ID=" << index << ", " << newTCR->print() << endl;
            if(newTCR->size != Lseqs){
                cerr << "ERR: predefined TCR " << par.Value.binarySeq_initTCRSeqs[i] << " has inconsistent size from requested: " << Lseqs << endl;
                exit(-1);
            }
            delete newTCR;
         }
     }
     int remain = par.Value.totalTC - NFixT;
     // creates a seed sequence with a required affinity to the antigens.
     for(int i = 0; i < remain; ++i){
        binarySequence*  newTCR = new binarySequence(par.Value.binarySeq_size_sequences);
        newTCR->randomize();
        add_TCR(new hyBinarySequence(newTCR, typeTCR));
        delete newTCR;
     }


     ana << "... done.\n";
}

//class foldedCubicSpace : public generalSequenceContainer<hyFoldedCubic> {
foldedCubicSpace::foldedCubicSpace(Parameter & p, ofstream & report){

    initializeAnalyzeFiles(report);

    /*// foldedCubic
// ============
 // Probability of mutation, per base.
 double foldedCubic_mut_per_base;    // New 405
 double foldedCubic_rescale_log_affinitymax; // New 406
 int foldedCubic_nb_conserved;       // New 407
 string foldedCubic_conserved_mask;  // New 408
// Fix Antigen Sequence presentation (max 1000 values):
vector<string> foldedCubic_initAntigenSeqs;  // New 409
// Fix initial Repertoire distribution (max 1000 values):
vector<string> foldedCubic_initBCRSeqs;      // New 410
// Fix initial Repertoire distribution for T cells:
vector<string> foldedCubic_initTCRSeqs;      // New 411
*/
}



//class foldedFreeSpace : public generalSequenceContainer<foldedFree>{
foldedFreeSpace::foldedFreeSpace(Parameter & par, ofstream & ana){

    initializeAnalyzeFiles(ana);

    /* specific parameters for the foldedFreeSpace class
    double foldedFree_mut_per_base;             // New 412
    int foldedFree_size_antigens;               // New 413
    int foldedFree_size_sequences;              // New 414
    int foldedFree_typeAffinity;                // New 415
    int foldedFree_temperature;                 // New 416
    foldedFree_min_contacts                     // 428
    double foldedFree_rescale_log_affinitymax;  // New 417
    int foldedFree_nb_conserved;                // New 418
    string foldedFree_conserved_mask;           // New 419
    vector<std::pair<long int, string>> foldedFree_initAntigenStructures;    // New 420
    vector< vector<int>* >  foldedFree_listForbiddenPositionsPerAntigen         // Also 420
    vector<string> foldedFree_initAntigenSeqs;  // New 421
    vector<string> foldedFree_initBCRSeqs;      // New 422 */

    int LseqsAg =  par.Value.foldedFree_size_antigens;
    int LseqsBCR =  par.Value.foldedFree_size_sequences;
    ana << "Specific options for this sequence representation (foldedFree)" << "\n";
    ana << "  -> Antigens will be of size: " << LseqsAg << "\n";
    ana << "  -> BCRs will be of size: " << LseqsBCR << "\n";

    // Giving parameters to the sequence representation itself
    double rescaling_exponent = par.Value.foldedFree_rescale_log_affinitymax;
    if(rescaling_exponent< 0) rescaling_exponent = 1.0;
    foldedFree::initializeDefaultParameters(par.Value.foldedFree_typeAffinity, par.Value.foldedFree_rescale_log_affinitymax);
    // Important, we initialize the size of a receptor as its number of BOUNDS, so it's LseqsBCR - 1 (compared to AA size)
    ligandDatabase::initialize(LseqsBCR-1, par.Value.foldedFree_min_contacts, -1 /* default */, par.Value.foldedFree_temperature);

    ana << "Settings of the folded free sequence representation" << endl;
    ana << "  -> type of affinity: " << par.Value.foldedFree_typeAffinity
        << ((par.Value.foldedFree_typeAffinity == foldedFree::freeFoldBest) ? ": Best affinity of any structure" : "")
        << ((par.Value.foldedFree_typeAffinity == foldedFree::freeFoldStat) ? ": Statistical affinity (Bolzman weighted)" : "");
    ana << "  -> temperature     : " << par.Value.foldedFree_temperature << "\n";
    ana << "  -> rescaling factor: " << par.Value.foldedFree_rescale_log_affinitymax << "\n";
    ana << "  -> min contacts    : " << par.Value.foldedFree_min_contacts << "\n";
    ana << "\n";

    ana << "General Settings of the space of sequences for hyphasma simulations" << "\n";
    double _pm_differentiation_rate = log(2.) / par.Value.pm_differentiation_time;
    // Note: the mutation rate has to be transformed in a probability at the whole sequence size.
    initialize(par.Value.foldedFree_mut_per_base * LseqsBCR,
               par.Value.proba_lethal_mut,
               par.Value.proba_affecting_mut,
               par.Value.proba_silent_mut,
               _pm_differentiation_rate);
    ana << "   -> mutation rate =           " << par.Value.foldedFree_mut_per_base * LseqsBCR<< "\n";
    ana << "   -> proba_lethal_mut =        " << par.Value.proba_lethal_mut << "\n";
    ana << "   -> proba_affecting_mut =     " << par.Value.proba_affecting_mut << "\n";
    ana << "   -> proba_silent_mut =        " << par.Value.proba_silent_mut << "\n";
    ana << "   -> pm differentiation rate = " << _pm_differentiation_rate << "\n";


    vector<bool> mask = prepareMask(par.Value.foldedFree_nb_conserved, LseqsAg, par.Value.foldedFree_conserved_mask, ana);


    ana << "Initialize Antigen pool ...\n ";
    int nAg = par.Value.nAntigenTypesSeqSpaces;
    int predef = par.Value.foldedFree_initAntigenSeqs.size();
    ana << "   -> Number of antigens required : " << nAg << endl;
    ana << "      with hamming distance between each-other within " << par.Value.min_hamming_antigens
        << " and " << par.Value.max_hamming_antigens << endl;

    // Fof FoldedFree, the setting of antigens is a bit different from other representations.
    // First, for each antigen, a valid 3D structure should be given manually. There is no
    // other choice, as it is not easy to generate an epitope randomly.
    // However, the sequence of AAs inside the antigen can be generated randomly, included
    // conservation, for instance if the hidden or outside parts are conserved.
    // Possible uses:
    //      - give the same structure to all antigens, but they will have different sequences
    //      - or give different structures, for instance slight modifications or hindrance.


    vector<foldedFree*> listAgs;
    if(par.Value.randomAgGeneration == false){
         listAgs = readFixedAntigens<foldedFree>(par.Value.foldedFree_initAntigenSeqs, LseqsAg, min(nAg, predef), mask, ana);
         if(predef < nAg){
             cerr << "ERR: you didn't specify enough predefined antigen sequences (" << predef << ") in the parameter file, instead of " << nAg << " requested. Suggestion: activate random generation of antigens then." << endl;
             exit(-1);  // note: i waited that the sequences are created to raise error, maybe looks like a good number of sequencea but incorrect or -1 inside.
         }
    } else {
        listAgs = genRandomAntigens<foldedFree>(nAg, LseqsAg, mask, par.Value.min_hamming_antigens, par.Value.max_hamming_antigens, ana);
    }
    // Now insert them into hyphasma, with a type typeAG,
    // *** and applies the structure of the antigens  ***
    if(par.Value.foldedFree_listForbiddenPositionsPerAntigen.size() < listAgs.size()){
        cerr << "ERR: foldedFree_listForbiddenPositionsPerAntigen was not given for enough antigens" << endl;
    }
    if(par.Value.foldedFree_initAntigenStructures.size() < listAgs.size()){
        cerr << "ERR: foldedFree_initAntigenStructures was not given for enough antigens" << endl;
    }
    for(unsigned int i = 0; i < listAgs.size(); ++i){
        foldedFree* newAg = listAgs[i];
        vector<int>* listF = par.Value.foldedFree_listForbiddenPositionsPerAntigen.at(i);
        newAg->setStructure(par.Value.foldedFree_initAntigenStructures[i].second, par.Value.foldedFree_initAntigenStructures[i].first, *listF);
        int index = add_Antigen(new hyFoldedFree(newAg,typeAG));
        cout << "      + Ag,ID=" << index << ", " << newAg->print() << endl;
        //delete newAg later, we use it to create the BCRs
    }
    ana << "... done.\n";

    ana << "Now, Initialize Seeder cell pool ... \n";
    ana << "   with the requested constraints: \n";
    ana << "   -> " << par.Value.totalBss << " different possible BCR sequences" << endl;

    // Taking the predefined BCR sequences. Note: no conserved positions/constraints for BCRs
    int NFixB = min((int) par.Value.totalBss, (int) par.Value.foldedFree_initBCRSeqs.size());
    vector<foldedFree*> fixedBCRs = readFixedSeqs<foldedFree>(par.Value.foldedFree_initBCRSeqs, LseqsBCR, NFixB, ana);

    checkSeederParameters<foldedFree>(par, ana);



    // opens libraries of good BCRs for the antigens,if they exist. Note: they should exist for all antigens in the simulation
    // double thresholdEnergy = -79; // right now libraries were made manually for threshold -79
    double thresholdEnergy = foldedFree::retrieveEnergy(0.000099);

    double eauivAff = foldedFree::affinityFromEnergy(thresholdEnergy);
    int foundLibraries = 0;
    vector<std::pair<double, vector<string> *> > loadedLibraries;  // one library per AG (in same index), then threshold and list of AA sequences
    for(int i = 0; i < (int) listAgs.size(); ++i){
        loadedLibraries.push_back(std::pair<double, vector<string> *> (NAN, NULL));
        foldedFree* newAg = listAgs[i];
        string fileToLookFor = fnameLibrary(newAg->folding->sequence, newAg->content, LseqsBCR-1, thresholdEnergy, par.Value.foldedFree_min_contacts, *(newAg->shieldedPositions));
        cout << "   -> look for library: " << fileToLookFor;
        ifstream testWhere(fileToLookFor.c_str());
        if(testWhere) testWhere.close();
        else {
            fileToLookFor = string("../") + fileToLookFor;
            ifstream testWhere2(fileToLookFor.c_str());
            if(testWhere2) testWhere2.close();
            else {
                cerr << "CANT LOCATE libraries " << QCoreApplication::applicationDirPath().toStdString() << endl;
                if(par.Value.foldedFreeForceLibrary > 0){
                    // strategie: need a separate parameter file with a parameter to force generating libraries.
                    cout << "Now generating libraries for this antigen and threshold" << endl;
                    generateLibrary(newAg->folding->sequence, newAg->content, -1, LseqsBCR, par.Value.foldedFree_min_contacts, thresholdEnergy, *(newAg->shieldedPositions), 5*par.Value.foldedFreeForceLibrary);
                    exit(-1);
                }
            }
        }
        ifstream testRead(fileToLookFor.c_str());
        if(testRead){
            cout << "\n   => Library has been found!\n" << endl;
            vector<string>* listAAseqs = new vector<string>();
            int cpt = 0;
            foundLibraries++;
            double energy = 0; string AAseq;
            while((testRead >> energy) && (cpt < 1e10)){
                if((cpt % 1000) == 0) cerr << "   "  << cpt << " read ... ";
                testRead >> AAseq;
                listAAseqs->push_back(AAseq);
                // random test that affinities match
                if((cpt < 10) || (random::uniformDouble(0,1) < 0.0001)){
                    cerr << " | check | ";
                    foldedFree* convert = new foldedFree(AAseq);
                    double test = foldedFree::retrieveEnergy(foldedFree::affinity(convert, newAg));
                    if(fabs(test - energy) > 0.1) {
                        cerr << "ERR: testing of the library failed. For whatever reason, the affinities stored do not match anymore. Stopping program." << endl;
                        cerr << "    example: BCR " << AAseq << ", stored affinity " << energy << ", recalculated " << test << endl;
                        cerr << "    did you change antigen shielded positions or starting position of the antigens after calculating the library ? " << endl;
                        exit(-1);
                    }
                }
                cpt++;
            }
            cout << "       -> finished reading " << cpt << " BCR AA sequences " << endl;
            if(cpt < par.Value.foldedFreeForceLibrary){
                cout << "We do not wish to use libraries of less than 10000 sequences, thereby adding sequences to the library for the next simulation.\n";
                generateLibrary(newAg->folding->sequence, newAg->content, -1, LseqsBCR, par.Value.foldedFree_min_contacts, thresholdEnergy, *(newAg->shieldedPositions), 5*(par.Value.foldedFreeForceLibrary-cpt+1));
                exit(-1);
            }

            loadedLibraries[i].first = eauivAff;
            loadedLibraries[i].second = listAAseqs;
            testRead.close();
        } else cout << " -> not found" << endl;
    }



    // Now generating randomly the other seeders according different constraints (classes of BCRs)
    int nClasses = par.Value.nbBCRclasses;
    vector<hyFoldedFree*> seederBCRsForHyphasma;
    for(int i = 0; i < nClasses ; ++i){
        ana << "Class " << i << " of BCRs, freq: " << 100. * par.Value.freqBCRclasses[i] << "%" << endl;
        vector<double> minMaxAffinitiesToAg = par.Value.affinityRangesBCRClasses[i];
        // the remaining number to generate for this class is:
        double NbToGen = par.Value.freqBCRclasses[i] * ((double) par.Value.totalBss - (double)NFixB);
        vector<foldedFree*> poolThisClass = generateSeederClass(NbToGen, LseqsBCR, minMaxAffinitiesToAg, listAgs, par.Value.min_hamming_BCRs, par.Value.max_hamming_BCRs, ana,  loadedLibraries);
        for(unsigned int j = 0; j < poolThisClass.size(); ++j){
            hyFoldedFree* seederBCR = new hyFoldedFree(poolThisClass[j], typeBCR);
            seederBCR->timeBoundMin = par.Value.timePeriodsStartBCRclasses[i];
            seederBCR->timeBoundMax = par.Value.timePeriodsStopBCRclasses[i];
            seederBCRsForHyphasma.push_back(seederBCR);
            delete poolThisClass[j];
        }
    }

    for(int i = 0; i < (int) loadedLibraries.size(); ++i){
        if(loadedLibraries[i].second) delete loadedLibraries[i].second;
    }

    ana << "   BCR seeder sequences will be used in the following way:\n";
    if(par.Value.respectSeederOrder){
        ana << "    -> First, given seeder sequences are taken in the same order (respectSeederOrder = true)\n";
        ana << "       then, the randomly generated BCRs from classes are taken randomly provided their time-window" << endl;
    } else {
        ana << "    -> All BCR seeder sequences (inclued given ones) are taken in random order according to their time-window\n";
    }
    ana << "Note: the time-windows of the first classe apply to given BCR seeder sequences, but not their affinity constraints. \n";

    // Merge the Fixed and generated sequences and adds them to Hyphasma.
    // now, if keep order, inject fixed BCRs first. If not, shuffle everything including random
    for(unsigned int i = 0; i < fixedBCRs.size(); ++i){
        hyFoldedFree* seederBCR = new hyFoldedFree(fixedBCRs[i], typeBCR);
        seederBCR->timeBoundMin = par.Value.timePeriodsStartBCRclasses[0];
        seederBCR->timeBoundMax = par.Value.timePeriodsStopBCRclasses[0];
        // if want to respect order, put directly as sequence n+1,
        if(par.Value.respectSeederOrder){
            long int index = add_Seeder(seederBCR);
            cout << "   + Fix BCR: ID=" << index << ", " << seederBCR->print() << ", Affs to Ags:";
            for(int j = 0; j < nAg; ++j){
                cout << "\t" << j << " " << affinity(index, get_Antigen(j));
            }
            cout << endl;
        } else { // if not, creates a list of BCRs that will be shuffled before using add_Seeders
            seederBCRsForHyphasma.push_back(seederBCR); // puts in the list to be shuffled.
        }
        delete fixedBCRs[i];
    }

    random::shuffle<hyFoldedFree*>(seederBCRsForHyphasma);
    for(unsigned int i = 0; i < seederBCRsForHyphasma.size(); ++i){
        long int index = add_Seeder(seederBCRsForHyphasma[i]);
        cout << "   + RandBCR: ID=" << index << ", " << seederBCRsForHyphasma[i]->print() << ", Affs to Ags:";
        for(int j = 0; j < nAg; ++j){
            cout << "\t" << j << " " << affinity(index, get_Antigen(j));
        }
        cout << endl;
    }

    for(unsigned int i = 0; i < listAgs.size(); ++i){
        delete listAgs[i];
    }

    /* folded Free does not support TCR sequences
    ana << "Initialize TCR initial sequences... \n";
    ana << par.Value.totalTC << " sequences required" << endl;
    int NFix = min((int) par.Value.totalTC, (int) par.Value.foldedFree_initTCRSeqs.size());
    ana << "   -> Taking " << NFix << "BCRs from a predefined set - will be added to the automatically generated \n ";

    vector<foldedFree*> listTCRs = readFixedSeqs<foldedFree>(par.Value.foldedFree_initTCRSeqs, LseqsBCR, NFix, ofstream& ana);
    int remain = par.Value.totalTC - NFix;
    for(int i = 0; i < remain; ++i){
        hyFoldedFree*  newTCR = new foldedFree(Lseqs);
        newTCR->randomize();
        listTCRs.push_back(newTCR);
    }
    for(int i = 0; i < fixedTCRs.size(); ++i){
        foldedFree* newTCR = new foldedFree(fixedTCRs[i]);
        int index = add_TCR(new hyFoldedFree(newTCR, typeTCR));
        ana << "      + TCR (ID=" << index << ", " << newTCR->print() << endl;
        delete newTCR;
    }*/

    ana << "... done.\n";
}



//class foldedFreeSpace : public generalSequenceContainer<foldedFree>{
nucleotideFreeSpace::nucleotideFreeSpace(Parameter & par, ofstream & ana){

    initializeAnalyzeFiles(ana);

    // a bit too much copy paste ...

    // ================================================= copy pasted from foldedFree... don't touch ! ====================================
                                /* specific parameters for the foldedFreeSpace class
                                double foldedFree_mut_per_base;             // New 412
                                int foldedFree_size_antigens;               // New 413
                                int foldedFree_size_sequences;              // New 414
                                int foldedFree_typeAffinity;                // New 415
                                int foldedFree_temperature;                 // New 416
                                foldedFree_min_contacts                     // 428
                                double foldedFree_rescale_log_affinitymax;  // New 417
                                int foldedFree_nb_conserved;                // New 418
                                string foldedFree_conserved_mask;           // New 419
                                vector<std::pair<long int, string>> foldedFree_initAntigenStructures;    // New 420
                                vector< vector<int>* >  foldedFree_listForbiddenPositionsPerAntigen         // Also 420
                                vector<string> foldedFree_initAntigenSeqs;  // New 421      // this stays in AAs...
                                vector<string> foldedFree_initBCRSeqs;      // New 422 */

                                int LseqsAg =  par.Value.foldedFree_size_antigens;
                                int LseqsBCR =  par.Value.foldedFree_size_sequences;
                                ana << "Specific options for this sequence representation (foldedFree)" << "\n";
                                ana << "  -> Antigens will be of size: " << LseqsAg << "\n";
                                ana << "  -> BCRs will be of size: " << LseqsBCR << "\n";

                                // Giving parameters to the sequence representation itself
                                double rescaling_exponent = par.Value.foldedFree_rescale_log_affinitymax;
                                if(rescaling_exponent< 0) rescaling_exponent = 1.0;
                                foldedFree::initializeDefaultParameters(par.Value.foldedFree_typeAffinity, par.Value.foldedFree_rescale_log_affinitymax);
                                // Important, we initialize the size of a receptor as its number of BOUNDS, so it's LseqsBCR - 1 (compared to AA size)
                                ligandDatabase::initialize(LseqsBCR-1, par.Value.foldedFree_min_contacts, -1 /* default */, par.Value.foldedFree_temperature);

                                ana << "Settings of the folded free sequence representation" << endl;
                                ana << "  -> type of affinity: " << par.Value.foldedFree_typeAffinity
                                    << ((par.Value.foldedFree_typeAffinity == foldedFree::freeFoldBest) ? ": Best affinity of any structure" : "")
                                    << ((par.Value.foldedFree_typeAffinity == foldedFree::freeFoldStat) ? ": Statistical affinity (Bolzman weighted)" : "");
                                ana << "  -> temperature     : " << par.Value.foldedFree_temperature << "\n";
                                ana << "  -> rescaling factor: " << par.Value.foldedFree_rescale_log_affinitymax << "\n";
                                ana << "  -> min contacts    : " << par.Value.foldedFree_min_contacts << "\n";
                                ana << "\n";

                                ana << "General Settings of the space of sequences for hyphasma simulations" << "\n";
                                double _pm_differentiation_rate = log(2.) / par.Value.pm_differentiation_time;
                                // Note: the mutation rate has to be transformed in a probability at the whole sequence size.
                                initialize(par.Value.foldedFree_mut_per_base * LseqsBCR,
                                           par.Value.proba_lethal_mut,
                                           par.Value.proba_affecting_mut,
                                           par.Value.proba_silent_mut,
                                           _pm_differentiation_rate);
                                ana << "   -> mutation rate =           " << par.Value.foldedFree_mut_per_base * LseqsBCR<< "\n";
                                ana << "   -> proba_lethal_mut =        " << par.Value.proba_lethal_mut << "\n";
                                ana << "   -> proba_affecting_mut =     " << par.Value.proba_affecting_mut << "\n";
                                ana << "   -> proba_silent_mut =        " << par.Value.proba_silent_mut << "\n";
                                ana << "   -> pm differentiation rate = " << _pm_differentiation_rate << "\n";


                                vector<bool> mask = prepareMask(par.Value.foldedFree_nb_conserved, LseqsAg, par.Value.foldedFree_conserved_mask, ana);


                                ana << "Initialize Antigen pool ...\n ";
                                int nAg = par.Value.nAntigenTypesSeqSpaces;
                                int predef = par.Value.foldedFree_initAntigenSeqs.size();
                                ana << "   -> Number of antigens required : " << nAg << endl;
                                ana << "      with hamming distance between each-other within " << par.Value.min_hamming_antigens
                                    << " and " << par.Value.max_hamming_antigens << endl;

                                // Fof FoldedFree, the setting of antigens is a bit different from other representations.
                                // First, for each antigen, a valid 3D structure should be given manually. There is no
                                // other choice, as it is not easy to generate an epitope randomly.
                                // However, the sequence of AAs inside the antigen can be generated randomly, included
                                // conservation, for instance if the hidden or outside parts are conserved.
                                // Possible uses:
                                //      - give the same structure to all antigens, but they will have different sequences
                                //      - or give different structures, for instance slight modifications or hindrance.


                                vector<foldedFree*> listAgs;
                                if(par.Value.randomAgGeneration == false){
                                     listAgs = readFixedAntigens<foldedFree>(par.Value.foldedFree_initAntigenSeqs, LseqsAg, min(nAg, predef), mask, ana);
                                     if(predef < nAg){
                                         cerr << "ERR: you didn't specify enough predefined antigen sequences (" << predef << ") in the parameter file, instead of " << nAg << " requested. Suggestion: activate random generation of antigens then." << endl;
                                         exit(-1);  // note: i waited that the sequences are created to raise error, maybe looks like a good number of sequencea but incorrect or -1 inside.
                                     }
                                } else {
                                    listAgs = genRandomAntigens<foldedFree>(nAg, LseqsAg, mask, par.Value.min_hamming_antigens, par.Value.max_hamming_antigens, ana);
                                }
                                // Now insert them into hyphasma, with a type typeAG,
                                // *** and applies the structure of the antigens  ***
                                if(par.Value.foldedFree_listForbiddenPositionsPerAntigen.size() < listAgs.size()){
                                    cerr << "ERR: foldedFree_listForbiddenPositionsPerAntigen was not given for enough antigens" << endl;
                                }
                                if(par.Value.foldedFree_initAntigenStructures.size() < listAgs.size()){
                                    cerr << "ERR: foldedFree_initAntigenStructures was not given for enough antigens" << endl;
                                }
    // =========================== Now, a couple of things are different for nucleotideFree ========================================
    // basically, replaced foldedFree by nucleotideFree and hyFoldedFree by hyNucleotideFree

                                /*
    for(unsigned int i = 0; i < listAgs.size(); ++i){
        foldedFree* newAg = listAgs[i];
        vector<int>* listF = par.Value.foldedFree_listForbiddenPositionsPerAntigen.at(i);
        newAg->setStructure(par.Value.foldedFree_initAntigenStructures[i].second, par.Value.foldedFree_initAntigenStructures[i].first, *listF);
        int index = add_Antigen(new hyNucleotideFree(newAg,typeAG)); // it will not have a nucleotide sequence, it's fine. mutating can not be done to antigen without manually giving nucleotides
        cout << "      + Ag,ID=" << index << ", " << newAg->print() << endl;
        //delete newAg later, we use it to create the BCRs
    }
    ana << "... done.\n";

    ana << "Now, Initialize Seeder cell pool ... \n";
    ana << "   with the requested constraints: \n";
    ana << "   -> " << par.Value.totalBss << " different possible BCR sequences" << endl;

    // Taking the predefined BCR sequences. Note: no conserved positions/constraints for BCRs
    int NFixB = min((int) par.Value.totalBss, (int) par.Value.nucleotideFree_initBCRSeqs.size());
    vector<nucleotideFree*> fixedBCRs = readFixedSeqs<nucleotideFree>(par.Value.nucleotideFree_initBCRSeqs, LseqsBCR, NFixB, ana);

    checkSeederParameters<foldedFree>(par, ana);

    // Now generating randomly the other seeders according different constraints (classes of BCRs)
    int nClasses = par.Value.nbBCRclasses;
    vector<hyFoldedFree*> seederBCRsForHyphasma;
    for(int i = 0; i < nClasses ; ++i){
        ana << "Class " << i << " of BCRs, freq: " << 100. * par.Value.freqBCRclasses[i] << "%" << endl;
        vector<double> minMaxAffinitiesToAg = par.Value.affinityRangesBCRClasses[i];
        double NbToGen = par.Value.freqBCRclasses[i] * ((double) par.Value.totalBss - (double)NFixB);
        vector<nucleotideFree*> poolThisClass = generateSeederClass(NbToGen, LseqsBCR, minMaxAffinitiesToAg, listAgs, par.Value.min_hamming_BCRs, par.Value.max_hamming_BCRs, ana);
        for(unsigned int j = 0; j < poolThisClass.size(); ++j){
            hyNucleotideFree* seederBCR = new hyNucleotideFree(poolThisClass[j], typeBCR);
            seederBCR->timeBoundMin = par.Value.timePeriodsStartBCRclasses[i];
            seederBCR->timeBoundMax = par.Value.timePeriodsStopBCRclasses[i];
            seederBCRsForHyphasma.push_back(seederBCR);
            delete poolThisClass[j];
        }
    }

    ana << "   BCR seeder sequences will be used in the following way:\n";
    if(par.Value.respectSeederOrder){
        ana << "    -> First, given seeder sequences are taken in the same order (respectSeederOrder = true)\n";
        ana << "       then, the randomly generated BCRs from classes are taken randomly provided their time-window" << endl;
    } else {
        ana << "    -> All BCR seeder sequences (inclued given ones) are taken in random order according to their time-window\n";
    }
    ana << "Note: the time-windows of the first classe apply to given BCR seeder sequences, but not their affinity constraints. \n";

    // Merge the Fixed and generated sequences and adds them to Hyphasma.
    // now, if keep order, inject fixed BCRs first. If not, shuffle everything including random
    for(unsigned int i = 0; i < fixedBCRs.size(); ++i){
        hyNucleotideFree* seederBCR = new hyNucleotideFree(fixedBCRs[i], typeBCR);
        seederBCR->timeBoundMin = par.Value.timePeriodsStartBCRclasses[0];
        seederBCR->timeBoundMax = par.Value.timePeriodsStopBCRclasses[0];
        if(par.Value.respectSeederOrder){
            long int index = add_Seeder(seederBCR);
            cout << "   + Fix BCR: ID=" << index << ", " << seederBCR->print() << ", Affs to Ags:";
            for(int j = 0; j < nAg; ++j){
                cout << "\t" << j << " " << affinity(index, get_Antigen(j));
            }
            cout << endl;
        } else {
            seederBCRsForHyphasma.push_back(seederBCR); // puts in the list to be shuffled.
        }
        delete fixedBCRs[i];
    }
    cerr << "Ags5-";

    random::shuffle<hyFoldedFree*>(seederBCRsForHyphasma);
    for(unsigned int i = 0; i < seederBCRsForHyphasma.size(); ++i){
        long int index = add_Seeder(seederBCRsForHyphasma[i]);
        cout << "   + RandBCR: ID=" << index << ", " << seederBCRsForHyphasma[i]->print() << ", Affs to Ags:";
        for(int j = 0; j < nAg; ++j){
            cout << "\t" << j << " " << affinity(index, get_Antigen(j));
        }
        cout << endl;
    }
    cerr << "Ags6-";

    for(unsigned int i = 0; i < listAgs.size(); ++i){
        delete listAgs[i];
    }
    cerr << "Ags7-";

    folded Free does not support TCR sequences
    ana << "Initialize TCR initial sequences... \n";
    ana << par.Value.totalTC << " sequences required" << endl;
    int NFix = min((int) par.Value.totalTC, (int) par.Value.foldedFree_initTCRSeqs.size());
    ana << "   -> Taking " << NFix << "BCRs from a predefined set - will be added to the automatically generated \n ";

    vector<foldedFree*> listTCRs = readFixedSeqs<foldedFree>(par.Value.foldedFree_initTCRSeqs, LseqsBCR, NFix, ofstream& ana);
    int remain = par.Value.totalTC - NFix;
    for(int i = 0; i < remain; ++i){
        hyFoldedFree*  newTCR = new foldedFree(Lseqs);
        newTCR->randomize();
        listTCRs.push_back(newTCR);
    }
    for(int i = 0; i < fixedTCRs.size(); ++i){
        foldedFree* newTCR = new foldedFree(fixedTCRs[i]);
        int index = add_TCR(new hyFoldedFree(newTCR, typeTCR));
        ana << "      + TCR (ID=" << index << ", " << newTCR->print() << endl;
        delete newTCR;
    }
*/

    ana << "... done.\n";
}

//class perelsonSequenceSpace : public generalSequenceContainer<perelsonSequence>{
perelsonSequenceSpace::perelsonSequenceSpace(Parameter & p, ofstream & report){

    initializeAnalyzeFiles(report);

    /* // perelson:
 // ============
  // Probability of mutation, per base.
  double perelson_mut_per_base;  // New 424
  // Length of sequences:
  int perelson_size_sequences;   // New 425
  int perelson_alphabet_size;     // New 426
  double perelson_threshold;  // New 427
  double perelson_exponent;       // New 428
  int perelson_nb_conserved;  // New 429
 // Fix Antigen Sequence presentation (max 1000 values):
 vector<string> perelson_initAntigenSeqs; // New 430
 // Fix initial Repertoire distribution (max 1000 values):
 vector<string> perelson_initBCRSeqs; // New 431 */
}

//class perelsonGeneralizedSpace : public generalSequenceContainer<perelsonGeneralized> {
perelsonGeneralizedSpace::perelsonGeneralizedSpace(Parameter & p, ofstream & report){

    initializeAnalyzeFiles(report);

    /*
     *  // perelson generalized:        // Philippe 01-02-2016
 // ============
  // Probability of mutation, per base.
  double perelsonGeneral_mut_per_base; // New 432
  // Length of sequences:
  int perelsonGeneral_size_sequences;// New 433
  int perelsonGeneral_alphabet_size;// New 434
  vector<double> perelsonGeneral_AAstrength;  // New  435          // the AAstrength and AAaccessibility should be defined once for all
  vector<double> perelsonGeneral_AAaccessibility;// New 436
  double perelsonGeneral_specificity;// New 437
  int perelsonGeneral_typeOfClusterAffinities;// New 438
  int perelsonGeneral_nb_conserved;// New 439
 // Fix Antigen Sequence presentation (max 1000 values):
 vector<string> perelsonGeneral_initAntigenSeqs; // New 440
 // Fix initial Repertoire distribution (max 1000 values):
 vector<string> perelsonGeneral_initBCRSeqs;     // New 441
 */
}


/* List of parameters for using Arup sequences
 *
 *  Use arup space (1/0) [use_arup_space]:
 *  0
 *  Length of sequences [arup_length_sequences]:
 *  46
 *  Nb conserved residues [arup_N_conserved]:
 *  18
 *  Nb mutated residues [arup_N_mutates]:
 *  22
 *  Nb shielded residues (the rest) [arup_N_shielded]:
 *  6
 *  Number of Initial Antigen sequences (int-type) [arup_nb_ini_antigens]:
 *  3
 *  Fix Antigen Sequence presentation. Order is Conserved(should be 1), Variable, shielded (should
 * be 1). max 1000 values [arup_ini_antigens[...]]:
 *  1111111111111111111111111111111111111111111111
 *  1111111111111111110000000000011111111111111111
 *  1111111111111111111111111111100000000000111111
 *  -1
 *  Fraction of Arup Ags (non-fixed Ag enter with same fraction) [arup_ag_fraction[...]]:
 *  -1
 *  Number of mutations for mutated strains (if more initial Ag have to be generated)
 * [arup_nb_mutations_gen_strains]:
 *  11
 *  Activation threshold (kcal/mol) [arup_threshold_activation]:
 *  10.8
 *  Initial interval for BCR sequences [arup_h_min, arup_h_max]:
 *  -0.18
 *  0.9
 *  Fix initial Repertoire distribution (max 1000 values) [arup_ini_bcrs[...]]:
 *  -1
 #would be 0.1 0.8 0.5 -0.2 0.1 ... with values
 *  Mutation rate per sequence per division per residue [arup_mutation]:
 *  0.003
 *  probability of a mutation being lethal [arup_proba_lethal_mut]:
 *  0.3
 *  probability of a mutation being affecting [arup_proba_affecting_mut]:
 *  0.2
 *  probability silent [arup_proba_silent_mut]:
 *  0.5
 *  distribution of affinity changes by mutation [nbLinesToRead, arup_law_mut_Xs[...],
 * arup_law_mut_Densities[...]]:
 *  40
 *  -6,4	0,019047619
 *  -6,2	0,003809524
 *  -6	0,003809524
 *  -5,8	0,003809524
 *  -5,6	0,003809524
 *  -5,4	0,024380952
 *  -5,2	0,042666667
 *  -5	0,028952381
 *  -4,8	0,03352381
 *  -4,6	0,03352381
 *  -4,4	0,058666667
 *  -4,2	0,084571429
 *  -4	0,089142857
 *  -3,8	0,054857143
 *  -3,6	0,09447619
 *  -3,4	0,095238095
 *  -3,2	0,079238095
 *  -3	0,14552381
 *  -2,8	0,11352381
 *  -2,6	0,134095238
 *  -2,4	0,095238095
 *  -2,2	0,179809524
 *  -2	0,234666667
 *  -1,8	0,089142857
 *  -1,6	0,134095238
 *  -1,4	0,204190476
 *  -1,2	0,290285714
 *  -1	0,249904762
 *  -0,8	0,215619048
 *  -0,6	0,170666667
 *  -0,4	0,340571429
 *  -0,2	0,361142857
 *  0	0,365714286
 *  0,2	0,316190476
 *  0,4	0,324571429
 *  0,6	0,043428571
 *  0,8	0,084571429
 *  1	0,048761905
 *  1,2	0,04952381
 *  1,4	0,03352381
 *  Coefficient of unshielding [arup_alpha]:
 *  2
 *  Bounding of the shielding h' [arup_hprime_min, arup_hprime_max]:
 *  -1.5
 *  1.5
 *  Bounding of the residues facing mutated h' [arup_hmut_min, arup_hmut_max]:
 *  -1.5
 *  1e6 */

//class arupProteinSpace : public generalSequenceContainer<arupProtein>{
arupProteinSpace::arupProteinSpace(Parameter & par, ofstream & ana){

    initializeAnalyzeFiles(ana);


    /*// Arup space:
// ============
int use_arup_space;  // deprecated, can be removed later
int arup_length_sequences;
int arup_N_conserved;
int arup_N_mutates;
int arup_N_shielded;
vector<string> arup_ini_antigens;
int arup_nb_mutations_gen_strains;
double arup_threshold_activation;
double arup_h_min;
double arup_h_max;
vector<string> arup_ini_bcrs;
double arup_mutation;
vector<double> arup_law_mut_Xs;
vector<double> arup_law_mut_Densities;
double arup_alpha;
double arup_hprime_min;
double arup_hprime_max;
double arup_hmut_min;
double arup_hmut_max;

*/



    arupProtein::arup_length_sequences = par.Value.arup_length_sequences;
    arupProtein::arup_N_conserved = par.Value.arup_N_conserved;
    arupProtein::arup_N_mutates = par.Value.arup_N_mutates;
    arupProtein::arup_N_shielded = par.Value.arup_N_shielded;
    arupProtein::arup_alpha = par.Value.arup_alpha;
    arupProtein::arup_hprime_min = par.Value.arup_hprime_min;
    arupProtein::arup_hprime_max = par.Value.arup_hprime_max;
    arupProtein::arup_hmut_min = par.Value.arup_hmut_min;
    arupProtein::arup_hmut_max = par.Value.arup_hmut_max;
    arupProtein::arup_h_min = par.Value.arup_h_min;
    arupProtein::arup_h_max = par.Value.arup_h_max;
    arupProtein::lawMutations = new probaLawFromTable(par.Value.arup_law_mut_Xs,
                                         par.Value.arup_law_mut_Densities,true);
    ana << "Initializing parameters for Arup Sequences : \n";
    ana << "   Number of residues : " << arupProtein::arup_length_sequences << " among which \n";
    ana << "    - " << arupProtein::arup_N_conserved << " conserved\n";
    ana << "    - " << arupProtein::arup_N_mutates << " mutables\n";
    ana << "    - " << arupProtein::arup_N_shielded << " shielded\n";
    ana << "   Unshielding coefficient (alpha) = " << arupProtein::arup_alpha << "\n";
    ana << "   Initial boundaries for each position in a sequence : [" << arupProtein::arup_h_min << " ; "
        << arupProtein::arup_h_max << "]\n";
    ana << "   Boundaries for mutable positions during mutation   : [" << arupProtein::arup_hmut_min << " ; "
        << arupProtein::arup_hmut_max << "]\n";
    ana << "   Boundaries for shielded positions during mutation  : [" << arupProtein::arup_hprime_min << " ; "
        << arupProtein::arup_hprime_max << "]\n";
    ana << "   Law of probabilities to generate mutations : " << arupProtein::lawMutations->print() << "\n\n";

    ana << "General Settings of the space of sequences for hyphasma simulations" << "\n";
    double _pm_differentiation_rate = log(2.) / par.Value.pm_differentiation_time;
    initialize(par.Value.arup_mutation,
               par.Value.proba_lethal_mut,
               par.Value.proba_affecting_mut,
               par.Value.proba_silent_mut,
               _pm_differentiation_rate);
    ana << "   -> (try) mutation rate = " << par.Value.binarySeq_mut_per_base << "\n";
    ana << "   -> proba_lethal_mut = " <<    par.Value.proba_lethal_mut << "\n";
    ana << "   -> proba_affecting_mut = " << par.Value.proba_affecting_mut << "\n";
    ana << "   -> proba_silent_mut = " <<    par.Value.proba_silent_mut << "\n";
    ana << "   -> pm differentiation rate = " << _pm_differentiation_rate << "\n";




    int arup_nb_ini_antigens = par.Value.nAntigenTypesSeqSpaces;

    double arup_threshold_activation;

    // Antigen::number_of_bins = par.Value.antibodies_resolution + 2;
       long int n;
       ana << "Initialize arupProtein Space fields ... \n";

       arup_threshold_activation = par.Value.arup_threshold_activation;
       //arup_nb_ini_antigens = par.Value.arup_nb_ini_antigens;
       ana << "   Threshold of activation = " << arup_threshold_activation << "\n";
       //ana << "   Initial number of antigens defined = " << arup_nb_ini_antigens << "\n";
       ana << "   In case antigens have to be generated automatically, they will have "
           << par.Value.arup_nb_mutations_gen_strains << " mutations " << endl;

       // vector<double> arup_ag_fraction;

       ana << "Initialize Antigen pool (Arup)...\n ";
       int nbAg = par.Value.arup_ini_antigens.size();
       for (n = 0; n < min(nbAg, arup_nb_ini_antigens); n++) {
          int index = add_Antigen(new hyArupProtein(new arupProtein(par.Value.arup_ini_antigens[n])));
          ana << "   ... predefined seq nr" << n << " index= " << index << "("
              << getSequence(index)->size << "):" << getSequence(index)->print() << endl;
       }
       for (n = nbAg; n < arup_nb_ini_antigens; n++) {
          ArupAntigen * AAG = new ArupAntigen();
          AAG->randomize(par.Value.arup_nb_mutations_gen_strains);
          int index = add_Antigen(new hyArupProtein(AAG));
          ana << "   ... Generated antigen " << n << " index= " << index << "("
              << getSequence(index)->size << "):" << getSequence(index)->print() << endl;
       }
       ana << "... done.\n";

       ana << "Initialize Seeder cell pool ... \n";
       ana << par.Value.totalBss << " seeders required" << endl;
       int nbBCRs = par.Value.arup_ini_bcrs.size();
       for (n = 0; n < min(nbBCRs, (int) par.Value.totalBss); n++) {
          int index = add_Seeder(new hyArupProtein( new arupProtein(par.Value.arup_ini_bcrs[n])));
          ana << "   ... predefined seq nr" << n << " index= " << index << "("
              << getSequence(index)->size << "):" << getSequence(index)->print() << endl;
       }
       for (n = nbBCRs; n < (int) par.Value.totalBss; n++) {
          ArupBCR * ABCR = new ArupBCR();
          ABCR->randomize(arup_threshold_activation, false);
          int index = add_Seeder(new hyArupProtein(ABCR));
          ana << "   ... Generated BCR " << n << " index= " << index << "("
              << getSequence(index)->size << "):" << getSequence(index)->print() << endl;
       }
       ana << "... done.\n";

       // Opening the file streams for output during simulation - think of calling close_files(); later
       // to finish the job at the end of simulation




}

//void arupSpace::testarupSpace() { }
/*
 *   //use cerr to catch errors and avoid delays between cout/cerr
 #define OutputTest cerr
 *   OutputTest << "============ Testing arupProteins ... ============\n" << endl;
 *   arupProtein* a = new arupProtein(10);
 *   OutputTest << "Empty arupProtein  a :" << a->print() << endl;
 *   a->mutateOnePosition();
 *   OutputTest << "One Mutation    a :" << a->print() << endl;
 *   a->mutateOnePosition();
 *   OutputTest << "Another one     a :" << a->print() << endl;
 *   a->randomize();
 *   OutputTest << "Randomized :    a :" << a->print() << endl;
 *   arupProtein* b = new arupProtein(string("01111111111110"));
 *   OutputTest << "New arupProtein    b :" << b->print() << endl;
 *   b->mutate(0.5);
 *   OutputTest << "Mutate 50%/base b:" << b->print() << endl;
 *   arupProtein* c = new arupProtein(b);
 *   OutputTest << "New arupProtein  c=b :" << c->print() << endl;
 *   OutputTest << ((arupProtein::compSeq(b,c)) ? "c equals b" : "problem : c != b") << endl;
 *   OutputTest << "affinity b-c (r=2): " << arupProtein::seq_affinity(b, c, 2.0, -1, seqAff) <<
 * endl;
 *   arupProtein* d = new arupProtein(string("1111100000"));
 *   arupProtein* e = new arupProtein(string("1111011111"));
 *   OutputTest << "affinity d-e (r=3): " << arupProtein::seq_affinity(d, e, 3.0, -1, seqAff) << "
 * between " << d->print() << "\t" << e->print() << endl;
 *   OutputTest << "hamming(d,e) = " << arupProtein::hamming(d,e) << endl;
 *
 *   //Antigen::number_of_bins = 10;
 *   OutputTest << "Getting type of a arupProtein : in enum, " << e->getType() << " and as string: "
 * << e->typeInString() << "\t" << e->print() << endl;
 *   BCR* s1 = new BCR(10);
 *   OutputTest << "Getting type of a BCR      : in enum, " << s1->getType() << " and as string: "
 * << s1->typeInString() << "\t" << s1->print() << endl;
 *   TCR* s2 = new TCR(10);
 *   OutputTest << "Getting type of a TCR      : in enum, " << s2->getType() << " and as string: "
 * << s2->typeInString() << "\t" << s2->print() << endl;
 *   Antigen* s3 = new Antigen(10);
 *   OutputTest << "Getting type of an Antigen : in enum, " << s3->getType() << " and as string: "
 * << s3->typeInString() << "\t" << s3->print() << endl;
 *
 *   s1->add_producer(0.15, 4);
 *   s1->add_producer(0.35, 5);
 *   s1->rem_producer(0.31, 2);
 *   s1->rem_producer(0.0, 1);
 *   s1->add_producer(0.0, 2);
 *   s1->printNbCells();
 *   s1->print();*/

/*
 *
 *   OutputTest << "============ Testing arupSpace ... ============\n" << endl;
 *
 *   ofstream ana("testFile.out");
 *   Parameter par;
 *   par.Value.size_arupProteins = 10;
 *   par.Value.init_antigen_Sequences = 8;
 *   par.Value.initAntigenSeqs.resize(100, string(""));
 *   par.Value.initAntigenSeqs[0] = string("0000000001");
 *   par.Value.initAntigenSeqs[0] = string("-1");
 *   par.Value.max_hamming_antigens = 2;
 *   par.Value.min_hamming_antigens = 1;
 *   par.Value.totalBss = 5; // nb seeder cells
 *   par.Value.initBCRSeqs.resize(100, string(""));
 *   par.Value.initBCRSeqs[0] = string("1111111110");
 *   par.Value.initBCRSeqs[0] = string("-1");
 *   par.Value.max_hamming_BCRs = 2;
 *   par.Value.min_initial_affinity_BCRs = 0.1;
 *   par.Value.max_initial_affinity_BCRs = 1.0;
 *   par.Value.initTCRSeqs.resize(100, string(""));
 *   par.Value.initTCRSeqs[0] = string("1111111110");
 *   par.Value.initTCRSeqs[0] = string("-1");
 *   par.Value.min_initial_affinity_TCRs = 0.1;
 *   par.Value.max_initial_affinity_TCRs = 1.0;
 *   par.Value.R_affinity = 2.0;
 *   par.Value.pm_differentiation_time = 0.5;
 *   ana << "hi" << endl;
 *   arupSpace sp(par, ana);
 *
 *
 *   OutputTest << "arupProtein space at initialisation : "<< endl;
 *   OutputTest << sp.printarupProteins() << endl;
 *
 *   arupProtein* newseq = new arupProtein("0001111110");
 *   BCR* aBCR = new BCR(newseq);
 *   BCR* bBCR = new BCR("1100111000");
 *
 *   OutputTest << "when trying to add a arupProtein (without type) to the arupSpace, should raise
 * an error. " << endl;
 *   sp.index_adding_arupProtein(newseq);  // should raise an error
 *
 *   OutputTest << endl;
 *   long newIda = sp.index_adding_arupProtein(aBCR);  // should raise an error
 *   long newIdb = sp.index_adding_arupProtein(bBCR);  // should raise an error
 *   OutputTest << "inserting two BCRs to the space, and got the IDs, and best affinity to antigens
 * (automatically updated :" << endl;
 *   OutputTest << sp.getSequence(newIda)->print() << " with ID " << newIda << "\t" <<
 * sp.best_affinity(newIda) << endl;
 *   OutputTest << sp.getSequence(newIdb)->print() << " with ID " << newIdb << "\t" <<
 * sp.best_affinity(newIdb) << endl;
 *
 *   sp.add_cell(soutext, newIda);
 *   sp.add_cell(soutext, newIda);
 *   sp.add_cell(soutext, newIda);
 *   OutputTest << "After adding three cell to the first BCR arupProtein, now list of cells (n_cell)
 * for this arupProtein:" << endl;
 *   OutputTest << sp.getSequence(newIda)->printNbCells() << endl;
 *   cout << "differentiating the three cells for dt = 0.5 with pm_differentiation_time = 0.5\n";
 *   sp.PM_differentiate(soutext, soutextproduce, 0.5);
 *   OutputTest << "new state of the arupProtein space : " << endl;
 *   OutputTest << sp.printarupProteins(true) << endl;
 *
 *   Antigen* newA = new Antigen(string("01000001010"));
 *   Antigen* newB = new Antigen(string("01111001010"));
 *   long idNewA = sp.add_Antigen(newA);
 *   long idNewB = sp.add_Antigen(newB);
 *   OutputTest << "Now, adding two antigens : " << newA->print() << " ID=" << idNewA << "\t" <<
 * newB->print() << " ID=" << idNewB << endl;
 *   OutputTest << sp.printarupProteins(true) << endl;
 *
 *  }*/

/*template<typename T>
vector<T*> readFixedAntigens(vector<string> toRead, int nbToPick, vector<bool> mask, ofstream& ana){
    ana << "   -> Taking " << nbToPick << "Antigens from a predefined set (random = false)\n ";
    vector<T*> res;
    int Lseqs = mask.size();
    for(int i = 0; i < nbToPick; ++i){
        if(toRead[i].compare(string("-1"))){
            T* newAg = new T(toRead[i]);
            newAg->setConservedPositions(mask); // for further possible mutations of antigen
            if(newAg->size != Lseqs){
                cerr << "ERR: predefined antigen " << toRead[i] << " has inconsistent size from requested: " << Lseqs << endl;
                exit(-1);
            }
            res.push_back(newAg);
        }
    }
    return res;
}*/
