#ifndef SEQUENCESPACES_H
#define SEQUENCESPACES_H

#include <set>
#include <fstream>
#include <string>
#include "GCgraph.h"
using namespace std;

/// A sequence space is a storage for all the antibody and antigen sequences in a simulation,
///     -> it can be used with different sequence representations (one space per representation)
///     -> it takes care of doing mutations or taking random sequences,
///     -> and it takes care of getting an affinity
/// => Therefore, the main hyphasma simulation doesn't care how the sequence is represented,
///     but just asks: give me the ID of a new BCR, generates a new BCR by mutation, etc.

#include "affinityspace.h"

/// The way hyphasma was programmed requires specific functions to be implemented,
///     and they are all described inside ***AffinitySpace.h***
///     So, a sequence space needs to reimplement them.
///     The functions do management of a list of sequences for Antigen, BCR (and TCR if needed),
///     together with calculating the number of plasma cells, produced antibodies, and outputing files.
///     this is done by the 'general sequence container' class.
/// => all sequence spaces will be daughter classes from generalSequence container,
///     -> no need to take care of most functions, already inherited.
///     will need to take care of the set up of antigens and seeder B cells,
///     and reimplement if wanted everything specific to a type of sequence.

/// Also, the way hyphasma was implemented requires each sequence to store information on howm
/// many cells (of each type) carry this sequence and how much antibody of this sequence has been secreted.
/// -> each sequence representation is extended with the two fields:
///        double n_cells[]
///        double antibody
/// -> each sequence could have different properties / subclasses depending if it's a BCR or antigen, so we add a function:
///        typeSeq getType()

enum typeSeq {typeBCR, typeTCR, typeAG, undefined};
string typeInString(typeSeq t);

/// Extension of any sequence representation for hyphasma, to store additional infos.
struct hySequencesExtension {
    double n_cell[number_cell_types];       // Nb of cells of each type
    double antibody;                        // amount of produced antibody

    typeSeq type;                           // Ag, BCR or TCR
    virtual typeSeq getType() { return type; }

    double timeBoundMin, timeBoundMax;      // one can request that some seeder sequences appear under a specific time-window.
    hySequencesExtension() : antibody(0), type(undefined), timeBoundMin(-1e18), timeBoundMax(1e18) {
        for(int i = 0; i < number_cell_types; ++i){n_cell[i] = 0;}
    }

     vector<double> affinitiesToAgs;
};


/// Now extending all sequence types with the hySequencesExtension ... (plus default constructor)

#include "binaryLineages.h"
struct hyBinaryLineage: public binaryLineage, public hySequencesExtension {
    hyBinaryLineage(hyBinaryLineage* toCopy) :   binaryLineage((binaryLineage*) toCopy), hySequencesExtension(){type = toCopy->type;}
    hyBinaryLineage(binaryLineage* toCopy, typeSeq _type = undefined) :     binaryLineage(toCopy), hySequencesExtension() {type = _type;}
    virtual ~hyBinaryLineage(){}
};
#include "binarySequences.h"
struct hyBinarySequence: public binarySequence, public hySequencesExtension {
    hyBinarySequence(hyBinarySequence* toCopy) : binarySequence((binarySequence*) toCopy), hySequencesExtension(){type = toCopy->type;}
    hyBinarySequence(binarySequence* toCopy, typeSeq _type = undefined) :   binarySequence(toCopy), hySequencesExtension() {type = _type;}
    virtual ~hyBinarySequence(){}
};
#include "foldedCubic.h"
struct hyFoldedCubic: public foldedCubic, public hySequencesExtension {
    hyFoldedCubic(hyFoldedCubic* toCopy) : foldedCubic((foldedCubic*) toCopy), hySequencesExtension(){type = toCopy->type;}
    hyFoldedCubic(foldedCubic* toCopy, typeSeq _type = undefined) :   foldedCubic(toCopy), hySequencesExtension() {type = _type;}
    virtual ~hyFoldedCubic(){}
};
#include "foldedFree.h"
struct hyFoldedFree: public foldedFree, public hySequencesExtension {
    hyFoldedFree(hyFoldedFree* toCopy) :   foldedFree((foldedFree*) toCopy), hySequencesExtension(){type = toCopy->type;}
    hyFoldedFree(foldedFree* toCopy, typeSeq _type = undefined) :     foldedFree(toCopy), hySequencesExtension() {type = _type;}
    virtual ~hyFoldedFree(){}
};
struct hyNucleotideFree: public nucleotideFree, public hySequencesExtension {
    hyNucleotideFree(hyNucleotideFree* toCopy) :   nucleotideFree((nucleotideFree*) toCopy), hySequencesExtension(){type = toCopy->type;}
    hyNucleotideFree(nucleotideFree* toCopy, typeSeq _type = undefined) :     nucleotideFree(toCopy), hySequencesExtension() {type = _type;}
    virtual ~hyNucleotideFree(){}
};
#include "perelsonSequence.h"
struct hyPerelsonSequence: public perelsonSequence, public hySequencesExtension {
    hyPerelsonSequence(hyPerelsonSequence* toCopy) :  perelsonSequence((perelsonSequence*) toCopy), hySequencesExtension(){type = toCopy->type;}
    hyPerelsonSequence(perelsonSequence* toCopy, typeSeq _type = undefined) :    perelsonSequence(toCopy), hySequencesExtension() {type = _type;}
    virtual ~hyPerelsonSequence(){}
};
#include "perelsonGeneralized.h"
struct hyPerelsonGeneralized: public perelsonGeneralized, public hySequencesExtension {
    hyPerelsonGeneralized(hyPerelsonGeneralized* toCopy) :  perelsonGeneralized((perelsonGeneralized*) toCopy), hySequencesExtension(){type = toCopy->type;}
    hyPerelsonGeneralized(perelsonGeneralized* toCopy, typeSeq _type = undefined) :    perelsonGeneralized(toCopy), hySequencesExtension() {type = _type;}
    virtual ~hyPerelsonGeneralized(){}
};
#include "probabilisticArup.h"
struct hyArupProtein: public arupProtein, public hySequencesExtension {
    hyArupProtein(hyArupProtein* toCopy) :  arupProtein((arupProtein*) toCopy), hySequencesExtension(){type = toCopy->type;}
    hyArupProtein(arupProtein* toCopy, typeSeq _type = undefined) :    arupProtein(toCopy), hySequencesExtension(){type = _type;}
    virtual ~hyArupProtein(){}
};

// This part is a bit dirty: I want to keep the sequence types in a separate library,
// meaning they can not know about the hy-sequence extension (n_cells, antibody and type).
// Everything works with the template below, except the sequence functions that take
// other sequences, like affinity (the extended hyXXX classes are not known there).
// solution: redefine sequenceType::affinity(hyXXX,hyXXX) by hyAffinity() for each type.
// couldn't find better ... Speed should be ok, the compiler will replace the function
// in an inline way automatically.
// another solution would be to use double template : template <typename T, typename T2>
// class generalSequenceContainer, T = hyBinaryLineage and T2=binaryLineage for instance,
// but too complicated

double hyAffinity(hyBinaryLineage* s1, hyBinaryLineage* s2);
//    return binaryLineage::affinity((binaryLineage*) s1, (binaryLineage*) s2);}
double hyAffinity(hyBinarySequence* s1, hyBinarySequence* s2);
//    return binarySequence::affinity((binarySequence*) s1, (binarySequence*) s2);}
double hyAffinity(hyFoldedCubic* s1, hyFoldedCubic* s2);
//    return foldedCubic::affinity((foldedCubic*) s1, (foldedCubic*) s2);}
double hyAffinity(hyFoldedFree* s1, hyFoldedFree* s2);
//    return foldedFree::affinity((foldedFree*) s1, (foldedFree*) s2);}
double hyAffinity(hyNucleotideFree* s1, hyNucleotideFree* s2);
//    return foldedFree::affinity((foldedFree*) s1, (foldedFree*) s2);}
double hyAffinity(hyPerelsonSequence* s1, hyPerelsonSequence* s2);
//    return perelsonSequence::affinity((perelsonSequence*) s1, (perelsonSequence*) s2);}
double hyAffinity(hyPerelsonGeneralized* s1, hyPerelsonGeneralized* s2);
//    return perelsonGeneralized::affinity((perelsonGeneralized*) s1, (perelsonGeneralized*) s2);}
double hyAffinity(hyArupProtein* s1, hyArupProtein* s2);
//    return arupProtein::affinity((arupProtein*) s1, (arupProtein*) s2);}




template <typename T>
class generalSequenceContainer: public AffinitySpace {
  public:
   generalSequenceContainer();             // note : think about calling void initializeAnalyzeFiles(ofstream &ana);

   /// Part 1: Storage of sequences of whatever type, and retrieving seeders, antigens and TCRs.
   T* getSequence(long id);
   int get_n_Sequences();
   int get_n_Seeders();
   long int add_Seeder(T* newSequence);    // will check it is a BCR
   long int get_Seeder();
   long int get_Seeder(int i);
   int get_n_Antigens();
   long int add_Antigen(T* newSequence);   // will check it is an antigen/ligand
   long int get_Antigen();                 // cannot change it, reimplemented for mother class
   long int get_Antigen(int i);            // cannot change it, reimplemented for mother class
   int get_n_TCRs();
   long int add_TCR(T * newSequence);      // will check it is a TCR
   long int get_TCR();
   long int get_TCR(int i);
   ~generalSequenceContainer();            // Warning: !! will delete all sequences !!

  private:
   // adds a sequence in poolSequences. Called from add_Seeder, add_Antigen or add_TCR
   long int index_adding_sequence(T * toAdd, long int ID_of_mother_sequence);

  protected:
   vector<T*> poolSequences;            /// Stores all sequences at their ID as position.
   vector<long> indexParentSeq;
   long n_Sequences;
   vector<long> Seeders;
   int n_Seeders;
   vector<long> Antigens;
   int n_Antigens;
   vector<long> TCRs;
   int n_TCRs;


  /// Part 2: Getting an affinity and a new sequence by mutation (will call the specific sequence type functions)
  public:
   virtual long getMutation(long from_position);    // can still be reimplemented, but already does the job
   virtual double affinity(long int n, long int m, double abFeedbackThreshold = 0);

   /// for a BCR, gives best affinity and which antigen it is
   virtual double best_affinity(long seqID);
   virtual int get_nearest_Antigen(long n);


   /// Part 3: storing parameters for the setting/simulation, common to each sequence representation
   double mutation_rate;        // this will be specific to the sequence representation
                                // this is a probability to mutate, at the scale of the sequence (not perb base anymore)
   double proba_lethal_mut;
   double proba_affecting_mut;
   double proba_silent_mut;
   double pm_differentiation_rate;
   void initialize(double _mutation_rate, double _proba_lethal_mut, double _proba_affecting_mut, double _proba_silent_mut, double _pm_differentiation_rate);
   bool initialized;

   double getMutationRate(){
       if(initialized) return mutation_rate;
       else cerr << "ERR: calling sequence space::getMutationRate() called before initializing the class with those values" << endl;
       exit(-1);
   }

  /// Part 4: Management of information stored inside each (hyphasma extended) sequence type: n_cells and antibody
  public:
   /** @brief writes or read in the field 'antibody' of a sequence in the AffinitySpace */
   void put_Ab(long index,double d_ab);     /// **add** d_ab inside this sequence antibody amounts
   double get_AbAmount(long index);

   /** @brief writes(+1/-1) in the field n_cell[] of a sequence in the AffinitySpace
    * set_external_cell is called automatically */
   void add_cell(cells typ, long int pos);
   void rem_cell(cells typ, long int pos);

   /// generates the graph of mutations in the GC
   liste_adjacence* GCgraph;
   string printGraph();

  /// automatically maintains information on the total number of cells in the system (+ checking)
  private:
   double sum_cell[number_cell_types];
   double oldsum_cell[number_cell_types];
  public:
   double get_sum_cell(cells celltype);
   double get_oldsum_cell(cells celltype);
   short int sum_check();

   /// To be programmed
   bool add_new_Antigen() {return false;}              // antigen chosen by AffinitySpace
   bool add_new_Antigen(long ag_index) {return false;} // antigen index provided by the calling routine

   /// Part 5: Stores how many cells are producing each antibody.

   /// three steps in the system: 1/ type sout, will leave the GC, 2/ type soutext, left the GC
   /// but doesn't produce antibodies, and 3/ type soutextproduce, left GC and are secreting plasma cells.
   /// -> differentiation to plasma cells is done by applying an ODE to all cells that are soutext
   /// to become soutextproduce over time (pm_differentiation_rate).
   void PM_differentiate(cells a, cells b, double dt); // use only with a=soutext and b=soutextproduce
  private:
   set<long int> external_cells;            /// list of ID of sequences that are carried by outext cells (external)
   void set_external_cell(long int pos);    /// checks if a sequence is already secreted (if not, adds)
   vector<long int> ab_producers;           /// List of ID of sequences that are currently getting secreted.
  public:
   int get_n_ab_producers();                /// number of sequences being secreted by Plasma Cells
   long get_AbProducingIndex(int n);        /// ID of the nth secreted sequence
   double get_AbProducingCells(int n);      /// number of Plasma Cells producing the n-th secreted sequence
                                            /// note: it might is of double type because the nr of producing cells
                                            /// follows an ODE

   /// With multiple antigens, affinity could be analyzed normalized to the maximum affinity for this antigen.
   /// -> will depend on the layout of antigens => to be reimplemented in the specific sequence space.
   /// -> by default, it just returns the usual affinity.
   virtual double affinity_norm(long int seqId1, long int seqId2) {
      return affinity(seqId1, seqId2);
   }
   virtual double best_affinity_norm(long int pos){
       return best_affinity(pos);
   }



   /// -------------------------------- Analysis output files -------------------------- !!

   /// returns a full description of all the sequences present in the system.
   string printStateAllSequences(bool showSequencesWithNoAliveCells = false, bool showTree = false);

   /// (simpler) returns a parsable description of all sequences at this time-point, two columns: an ID and the sequence
   string printSequences(double currentTime);

   /** files are opened inside the initialize function, and closed when calling close files. */
   ofstream logdata[number_cell_types];
   ofstream logmeanaff,loghighaff,log048aff,logdiversity;

   /// open or close the output files that will be filled at multiple time-points
   void initializeAnalyzeFiles(ofstream &ana);
   void close_files();

   /// append information in the output files for a particular time-point
   void to_multiag_files(double time);      // saffin_ags_..., saffin_out_..., crossreactivity... files
   void get_diversity(double time);         // diversity.out
   void to_ssfiles(double time);            // all logData[] files, and call get_diversity
   void write_gcbc_affinity(double time);   // gcbc_affinity.out
   void write_gcbc_hamming(double time);    // not implemented for sequence spaces

   /// write the state of all cells (sequence, affinity), will create seqspaceTIME.out and GCgraphTIME.out
   void write_state_cells(double time);

   // tool functions that return a vector (size 16) with: 1/ avg affinity of CB, CC and out,
   // and 2/ the amount of cells in each affinity class for CB, CC and out, and 3/ average total affinity
   vector<double> mean_affinity();
   // similar function, for a particular antigen, 1/ avg affinity of GC (CB+CC) and out,
   // and 2/ the amount of cell in each affinity class for (CB+CC) and out.
   vector<double> mean_affinities_ag(int ag_index);
   double mean_affinity_norm(long int pos);

   // Calculates and returns a corrected average affinity (3rd argument) for a cell type.
   void correct_average_affinity(cells celltyp, long &pos, double &average);
   double Abstandquad(long int &n, long int &n2);

   /// @brief  Indicators that are updated when to_ss files is called , and that are retreived by get_statistics
   double OUT_haffinity,OUT_steepness,CB_haffinity,CC_haffinity;
   void getStatistics(double &_OUT_haffinity,double &_OUT_steepness,
                      double &_CB_haffinity,double &_CC_haffinity) ;

   // not programmed: virtual double get_affinity2ag(long pos, int i) = 0; // No !
};


class Parameter;

struct binaryLineageSpace : public generalSequenceContainer<hyBinaryLineage> {
    binaryLineageSpace(Parameter & p, ofstream & report);
};
struct binarySequenceSpace : public generalSequenceContainer<hyBinarySequence> {
    binarySequenceSpace(Parameter & p, ofstream & report);
};
struct foldedCubicSpace : public generalSequenceContainer<hyFoldedCubic> {
    foldedCubicSpace(Parameter & p, ofstream & report);
};
struct foldedFreeSpace : public generalSequenceContainer<hyFoldedFree>{
    foldedFreeSpace(Parameter & p, ofstream & report);
};
struct nucleotideFreeSpace : public generalSequenceContainer<hyNucleotideFree>{
    nucleotideFreeSpace(Parameter & p, ofstream & report);
};
struct perelsonSequenceSpace : public generalSequenceContainer<hyPerelsonSequence>{
    perelsonSequenceSpace(Parameter & p, ofstream & report);
};
struct perelsonGeneralizedSpace : public generalSequenceContainer<hyPerelsonGeneralized> {
    perelsonGeneralizedSpace(Parameter & p, ofstream & report);
};
struct arupProteinSpace : public generalSequenceContainer<hyArupProtein>{
    arupProteinSpace(Parameter & p, ofstream & report);
};


#endif // SEQUENCESPACES_H
