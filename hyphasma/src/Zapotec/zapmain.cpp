#include <iostream>
using namespace std;


//---------- mixed pieces of code (not needed) ---------------------------

#include "../Ymir/ymir.h"
#include "../Tools/md5.h"
#include "../Tools/zaprandom.h"

#ifdef USE_MPI
#include "mpi.h"
#endif

int mainZapForTests(int argc, char** argv){

#ifdef USE_MPI
    MPI_Init(nullptr, nullptr);
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    cout << "Parallel simulation, starting process nr " << world_rank << " out of " << world_size << " totel threads" << endl;
#endif




    // Sanity check, do not touch
    if(true){
        string s2 = string("SURRDDLLURUSDSSLDSL");
        int testsizeR = 6; //133152
        affinityOneLigand a = affinityOneLigand(s2, "AGRHGRAHARGRAGRTWHSW" , -1, testsizeR, 7, -1, 1.0);
        cout << a.affinity("AGNTWPL",false).first << endl;
        // answer should be -29.36
        //sanityCheckEasyRotate();
        return 0;
    }

    // A more detailed way to display a single chain structure
    if(false){
        string simpleAccessible = string("DUUSURUSLDDUUDDUDDSUUSRDDRUDDUUDDUDDSUULS");
        string AAsimple = "YFHGCARRATLNTTISWEYVSVDMEKIRVGGNEWFNHTMYVT"; //randomProt(simpleAccessible.size()+1);
        int receptorSize = 8;
    //  struct3D* ligand = new struct3D(simpleAccessible, UnDefined, lattice::idFromPosisition(31,30,34));
        vector<int> forbOptions = {POSITIONS_SQUARE_BLOCKED};
    //  set<int>* forb =  generateForbidden(forbOptions);

        /* glDisplay(argc, argv);
           addToDisplay(ligand, true);
           addToDisplay(forb);
         //glutMainLoop();*/

        affinityOneLigand T1 = affinityOneLigand(simpleAccessible, AAsimple, lattice::idFromPosisition(31,30,34), receptorSize, 4, 4, 0.4, forbOptions);
        cout << "Details of the structures and affinities for " << simpleAccessible << " (" << AAsimple << "), receptors " << receptorSize << " minI=4" << endl;
        for(int i = 0; i < 1; ++i){
            string Px = randomProt(receptorSize+1);
            std::pair<double, double> res = T1.affinity(Px, true);
            cout << "affinity(" << Px << ") = \t" << res.first << "\t" << res.second << endl;
        }
    }





//    //struct3D start = struct3D("");
//    //protein empty = protein(start);
//    superProtein v0 = superProtein();
//    struct3D S01 = struct3D("SUSSDUSSDSSDRRUSSLUDDLRDLSSDSUSDDRUULDSDLUDRDLRSSSDUSRDLUDLDUSDLLURSLUUDDRSSURRSSULRDLUSSRUSURRDDLUURRLRSRUDUSDSUSSSRLDSDDUSLSDRUDDLLURLLRRUULDDUDSRLRDRRLDRSRDSLURSUDUUSSSULDSDLSSDUDURLUUDSUSSDRSRLLUSRSUDULSRLUDUSRLDUDUDSDRLSDLSURLLUUSULSSSSLRRSRURLSDDLLUURSLDDUULRUDULRLRDURSRLUDLULRURDLLDUSSDUDSDRRDLLDDRDLLURDRUDURRDLURRDD", UnDefined, 133152);
//    superProtein v1 = insert(v0, S01, 0);
//    //protein v1 = insert(empty, S01, 1000);
//    struct3D S02 = struct3D("DRUDLRUDUSRSDLRUSSULUSLRSUSSRLRLURLSURRLURDDLDLRUDUSDRSSSULDRRSDSLUURDLUURRDLUSUSLRLRLRSSUURDLURDLLUUSUSRLSLUDRLURDUDULRUUDRULRDDLLURDLUUDRRLSURULDRSSURUSSUDLLUDRUSLSLSLURRDLSDSLSSRLRRUDRLRLSSSDLRLULRRSDUSLRSUUDSSSRLSSSSDDUDLRSLRDLLULUSSLDUDURSLLRLUDSLLUURRDLURRDRDSUSSRLSSDRDSLRUSDRLDURDSRSDUSSLUSDDLLUURSDLUUDLSDUDDLLRRLURR", UnDefined, 165399);
//    superProtein v2 = insert(v1, S02, 1000);
//    struct3D S03 =  struct3D("LUDSSSLRSDSDDLSLRSLDSLDUDLRDLSLRLDRDSLULSRUULDRLRUDSSRLRDLDLSRDDLLULRRLLDDRDUSDSDSRLRLUDSLUDDLLUURDDLLSSDULLDUDSRULSSSDULLSURRDDLUSDLUUSDDULRLLRLSDUSDLDSDULDSLRLDUSSLLSSSDULRRSRLSRLRLULLRUDURDLRUSSSLSDDRDURSDURLSRDUSSLRLSDLSSDLLUUDDUUSUSSSLRSSRDUURLSRULLDLURDLLUULRUSLRDULSDRDSSSRUDDLRSURDRSURDLSSDRDDLLUURRDSLRRDSUUSRDDUULSR", UnDefined, 189536);
//    superProtein v3 = insert(v2, S03, 2000);
//    v3.setAAs(randomProt(v3.size()));
//    int sizeR = 3;
//    //cout << print(v3) << endl;
//    affinityOneLigand zz = affinityOneLigand(&v3, sizeR, 7, -1, 1.0);

//    return -1;


//    string s2 = string("SURRDDLLURUSDSSLDSLRURUSURLSULUDSUSSLLRLURRDDLLUURUSUDLLRSLSLURSDLLRRUDLLUURRDDLSDLLRSDLUUDRUURDUSRDDLLUURULURRDDULLDDUURRDSRURRDSLRRLLSUURDLSDDUURDDLUUDDLSDRLLUUR");
//    int sizeR = 6; //133152
//    affinityOneLigand a = affinityOneLigand(s2, randomProt(s2.size()+1) , -1, sizeR, 7, -1, 1.0);
//    cout << a.affinity(randomProt(sizeR+1),true).first;
//    return 0;

    //    // sanity test of the struct3D constructor is not copying pointers
    //    struct3D* v10 = new struct3D("SUUL");
    //    struct3D* v20 = new struct3D(*v10);
    //    delete v20;
    //    cout << *(v10->occupiedPositions.begin()) << endl;

    //    superProtein* v11 = new superProtein("SUUL");
    //    superProtein* v21 = new superProtein(*v11);
    //    delete v21;
    //    cout << *(v11->structure->occupiedPositions.begin()) << endl;
    //    return 0;




    //Figure1(1,8);
    //testMultichain();
    //return 0;

    //testFastAffinity();
    //testGenerateProteins();

    //Figure2(5,8);


//    string s2 = string("SUSSDUSSDSSDLLUSSLSDDSLSSSDRLSUDDLURDSLDLUURRLUSRLSSSUDLSRDRLSRDDLUSLSRRDLLRSULRUSLRLRLRDURDUURDSURDDSLSLDSLRSSLRSSDSUUDRRSSLURRDDLSRLLUURLUURDSDSSSSRDRSRLRSURRLDUSSUUDUSLRLSDDSUSLSRDRRLRDLRLRLSDSSUURSRSSSLSUDLRULRLDUSDSURRSRULLRRDDLLSULLRSDUSSLSUUDSUULLRSDSRDLLULRLURDSSLURDDSURLRSRDSSUSSRSSUSDUUDLLSRDLLURRLLDDUDSDDUUDLSURDLSSSSSUSSRSSSSULRLRSSDULDSLRSDURURRSRDDLSLSRDDUDUDSRULDSSDRRDLRSRDDUULLDDRRUULRRLRSSUDUDDRRLLDSUURRDLUUDURLULRDULDRLUSDSRDDRLLUUSRDLUURRLSRRULLUULRUSLSDDSSDRURRLDURDUSURULRLSRLLDLUSSSRSRRULRLSSSUSLSRDSSLSSSUDDUDDUSSSUDSSSRRULDDUUDSRUULURLURLSDURSUSULDRDDLUULLDLURRSDUSRLRLRLDSDSSSURDDSRULDULSSSLRRLDSDDURDLLUUDDRSDRLRRDDLUSDLURSSSSUSSSSLRSSLRSSRUSUSUSRSLUSSRDUULSRDDUULDDLSRDUDUSDUSRSLLURDDSLUDDUURRDDLLRRDDSURLSDRLRSDLLDDLSRSDLLUDSLDSUDSSUDSDRURDRSRLLRRDDLDRUURDSSDLLRDUDSRDLURUUSSRLLUSSDLULDLSLRDULDDSDUSSLRSRSRSSLRDULRDSSLUURLRLRSDLDSRSLUDSRLURSLSUULDDUURRDDSLDSUDSSSSDDLUDSRRDDLUDSLUURRSRSLRSLURDURRULRUDRLDSLURRSSRLSUSSLSDLLURDLLUURDDLRSURRDLLUURRSSSSLSUDSSLSRSLRLRDSSLSSURSSSSSDUURLSRRDLDRRSSDUDRLRUDSSSRUSRLSRRLSRDDLLRRLLURDDUDSSRLSSLRDDLURDLSUUDDUSUSSULRDSUSSLSDSSRDDRLSURDDLLURDDLLUDUSLSUSLDSUSLSLSSLRDDUDUDLRSDRDSUDUDLLRLSRLSRDURUDRLSSSSDSLUUDRSDUSSUSSUDSUDUDRLSUDDSLRDDLLUSRDLDLSSSRSDSLSRDRSLRDDLLUURRDDLUUSSSRLUDSSRRSSLRLUSRSURDLULSSRLSDUDDRLURRDDLLUUDDLSRSRDDLUULLD");
//    struct3D s1 = struct3D(string("SUSSDUSSDSSDLLUSSLSDDSLSSSDRLSUDDLURDSLDLUURRLUSRLSSSUDLSRDRLSRDDLUSLSRRDLLRSULRUSLRLRLRDURDUURDSURDDSLSLDSLRSSLRSSDSUUDRRSSLURRDDLSRLLUURLUURDSDSSSSRDRSRLRSURRLDUSSUUDUSLRLSDDSUSLSRDRRLRDLRLRLSDSSUURSRSSSLSUDLRULRLDUSDSURRSRULLRRDDLLSULLRSDUSSLSUUDSUULLRSDSRDLLULRLURDSSLURDDSURLRSRDSSUSSRSSUSDUUDLLSRDLLURRLLDDUDSDDUUDLSURDLSSSSSUSSRSSSSULRLRSSDULDSLRSDURURRSRDDLSLSRDDUDUDSRULDSSDRRDLRSRDDUULLDDRRUULRRLRSSUDUDDRRLLDSUURRDLUUDURLULRDULDRLUSDSRDDRLLUUSRDLUURRLSRRULLUULRUSLSDDSSDRURRLDURDUSURULRLSRLLDLUSSSRSRRULRLSSSUSLSRDSSLSSSUDDUDDUSSSUDSSSRRULDDUUDSRUULURLURLSDURSUSULDRDDLUULLDLURRSDUSRLRLRLDSDSSSURDDSRULDULSSSLRRLDSDDURDLLUUDDRSDRLRRDDLUSDLURSSSSUSSSSLRSSLRSSRUSUSUSRSLUSSRDUULSRDDUULDDLSRDUDUSDUSRSLLURDDSLUDDUURRDDLLRRDDSURLSDRLRSDLLDDLSRSDLLUDSLDSUDSSUDSDRURDRSRLLRRDDLDRUURDSSDLLRDUDSRDLURUUSSRLLUSSDLULDLSLRDULDDSDUSSLRSRSRSSLRDULRDSSLUURLRLRSDLDSRSLUDSRLURSLSUULDDUURRDDSLDSUDSSSSDDLUDSRRDDLUDSLUURRSRSLRSLURDURRULRUDRLDSLURRSSRLSUSSLSDLLURDLLUURDDLRSURRDLLUURRSSSSLSUDSSLSRSLRLRDSSLSSURSSSSSDUURLSRRDLDRRSSDUDRLRUDSSSRUSRLSRRLSRDDLLRRLLURDDUDSSRLSSLRDDLURDLSUUDDUSUSSULRDSUSSLSDSSRDDRLSURDDLLURDDLLUDUSLSUSLDSUSLSLSSLRDDUDUDLRSDRDSUDUDLLRLSRLSRDURUDRLSSSSDSLUUDRSDUSSUSSUDSUDUDRLSUDDSLRDDLLUSRDLDLSSSRSDSLSRDRSLRDDLLUURRDDLUUSSSRLUDSSRRSSLRLUSRSURDLULSSRLSDUDDRLURRDDLLUUDDLSRSRDDLUULLD"), UnDefined, 133152);
//    if(s1.properlyFolded) cout << "Properly folded, size " << s1.sequence.size() << endl;
//    else {cerr << "ERR: the structure entered is self-colliding!!\n   ... " << s1.sequence << endl; return(-1);}


    //    set<int> emptyset = set<int>();
//    receptorLigand rl = receptorLigand(s1, 2, 1, emptyset);
//    rl.generateReceptors();

    //testBigReceptorLigand(2, 1, s2, 133152);

//

    //testRotate3D();
    //testDiscretize();

    //testDistribsFoldedFree();
    //return 0;


    //testVectorsDirections();

    //cerr << 8940696716. / 18446744073. << endl;

//printVector(generateRandomSeqSize(6));
//return 0;


/*
    // Epitope 1
    if(0) displayEpitope("SSRLLRLLRLLRS", lattice::idFromPosisition(32,32,33));
    testBigReceptorLigand(9, 4, "SSRLLRLLRLLRS", lattice::idFromPosisition(32,32,33));

    // Epitope 2
    if(0){
        set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(30,33,33)).occupiedPositions;
        vector<int> blockV = vector<int>(1, -2); // -2 inserts the flat square
        std::copy(block.begin(), block.end(), std::back_inserter(blockV));
        //for(int i = 0; i < blockV.size(); ++i){
        //    cout << blockV[i] << endl;
        //}
        displayEpitope("RRUUSURRDSSRSRSSDDSSRS", lattice::idFromPosisition(30,32,33), blockV);
    }

    // Epitope 3
    if(1){
        vector<int> blockV = vector<int>(1, -2); // -2 inserts the flat square
        set<int> block = struct3D("SSSSSUUSSSS", UnDefined, lattice::idFromPosisition(31,34,33)).occupiedPositions;
        std::copy(block.begin(), block.end(), std::back_inserter(blockV));
        set<int> block2 = struct3D("RSSSUUSSS", UnDefined, lattice::idFromPosisition(36,33,33)).occupiedPositions;
        std::copy(block2.begin(), block2.end(), std::back_inserter(blockV));
        //for(int i = 0; i < blockV.size(); ++i){
        //    cout << blockV[i] << endl;
        //}
        displayEpitope("DRRDSRSRSDSDSRSRRLLRSSRSSRSSDDSSRSSRS", lattice::idFromPosisition(32,31,35), blockV);
    }
    */



    // Using getopt to parse arguments.
    //print(argc, argv);
    // short version: https://stackoverflow.com/questions/9642732/parsing-command-line-arguments
    /*int mode = -1;
    int opt;
    while ((opt = getopt(argc, argv, "ilw")) != -1) {
         switch (opt) {
         case 'i': isCaseInsensitive = true; break;
         case 'l': mode = 1; break;
         case 'w': mode = 0; break;
         default:
             fprintf(stderr, "Usage: %s [-ilw] [file...]\n", argv[0]);
             exit(EXIT_FAILURE);
         }
     }*/

    // long version:See https://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Option-Example.html#Getopt-Long-Option-Example

    /*
    static int verbose_flag;        // Flags
    int c = 0;
    while (c != -1){
        static struct option long_options[] =
        {
          {"verbose", no_argument,       &verbose_flag, 1}, // Options to set a flag.
          {"brief",   no_argument,       &verbose_flag, 0},
          {"add",     no_argument,       0, 'a'},   // These options don’t set a flag. We distinguish them by their indices ??.
          {"append",  no_argument,       0, 'b'},
          {"delete",  required_argument, 0, 'd'},
          {"create",  required_argument, 0, 'c'},
          {"file",    required_argument, 0, 'f'},
          {0, 0, 0, 0}
        };

      int option_index = 0;     // getopt_long stores the option index here.
      c = getopt_long (argc, argv, "abc:d:f:", long_options, &option_index);
      switch (c){
        case 0:
          if (long_options[option_index].flag != 0)            // If this option set a flag, do nothing else now.
            break;
          //printf ("option %s", long_options[option_index].name);
          if (optarg)
            //printf (" with arg %s", optarg);
          //printf ("\n");
          break;

        case 'a':
          cout << "-a detected " << endl;
          //puts ("option -a\n");
          break;

        case 'b':
          cout << "-b detected " << endl;
          //puts ("option -b\n");
          break;

        case 'c':
          cout << "-c detected with " << optarg << endl;
          //printf ("option -c with value `%s'\n", optarg);
          break;

        case 'd':
          cout << "-d detected with " << optarg << endl;
          //printf ("option -d with value `%s'\n", optarg);
          break;

        case 'f':
          cout << "-e detected with " << optarg << endl;
          //printf ("option -f with value `%s'\n", optarg);
          break;

        case '?':

          // getopt_long already printed an error message.
          break;

        default:
          // no arguments
          cout << "no argument" << endl;
          //abort ();
          break;
        }
    }

  // Print any remaining command line arguments (not options).
  if (optind < argc)
    {
      cout << "Detected additional parameter: ";
      while (optind < argc){
        cout << argv[optind++] << "\t";
      }
    }
    //return 0;
*/


    //test();
    //cout << printVector(resized(generateProteinsSizeLAndLess(5),6));
    //cout << intToID(0,0,0,0,16384) << endl;

    //test2();
    //testCodingOnExampleSequences();
//    testVectorsDirections();
  // testLattice();
  //  testProteins();
    //testGenerateProteins();


    // test functions one by one

//testEncoding();
    //testRotations();
    //testBigReceptorLigand();
    //showStructures("../xiv/cubicStructuresL26.txt", false);//, 100000, 101000);
    //showStructures("../xiv/cubicStructuresL26NonRedundant.txt", true);//, 100000, 101000);
    //readCubes("../xiv/cubicStructuresL26.txt");
    //testCubicAffinities();
    //testCubicFoldings();
    //testEnsProts();
    //testFoldingStructures();

  //testBigReceptorLigand();
  //testFastAffinity();
  //testProteins();
    //testAAaffinities();

    //testFaces();

    cerr << "continued " << endl;
     //testRecepLigands();


    #ifdef USE_MPI
    MPI_Finalize();
    #endif

    return 0;
}


















void print(int argc, char** argv){
    cout << argc << " arguments:\n";
    for(int i = 0; i < argc; ++i){
        cout << "   " << argv[i] << endl;
    }
    cout << endl;
}






















/*
// extending the protein with '-' on the left to reach size 30
char buf[30];
buf[29] = '\0';
for(int i = 0; i < 29; ++i){
    if(i < 29-L) buf[i] = '-';
    else buf[i] = prot[i-29+L];
}

char part1[6], part2[7], part3[7], part4[7], part5[7];
part1[5] = '\0';
part2[6] = '\0';
part3[6] = '\0';
part4[6] = '\0';
part5[6] = '\0';
for(int i = 0; i < 5; ++i){
    part1[i] = buf[i];
}
for(int i = 0; i < 6; ++i){

}
*/
/*
string print(protein& op){
    stringstream res;
    res << "Protein:(ID=" << op.ID << ", Size=" << op.size() << ")\n";
    res << "Residues : " << endl;
    for(unsigned int i = 0; i < op.points.size(); ++i){
        res << "\t" << print(op.points[i]) << endl;
    }
    res << print(op.structure);
    for(int i = 0; i < op.size(); ++i){
        res << AAname(op.points[i].TypeResidue);
    }
    res << "\n\tIDres\tResidue\tIDpos\tx\ty\tz\n";
    for(int i = 0; i < op.size(); ++i){
        res << "\t" << print(op.points[i]) << "\n";
    }
    res << "\nOccupiedPositions\n";
    std::set<int>::iterator it;
    for(it = op.occupiedPositions.begin(); it != op.occupiedPositions.end(); ++it){
        res << *it << " ";
    }

    return res.str();
}*/
