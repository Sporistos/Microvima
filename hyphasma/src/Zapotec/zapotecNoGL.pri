# This is the includable project file for Zapotec (sequence & structural spaces) including Ymir structural foldings.

# this function allows to do include(Moonfit.pri) from another project
INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD
message("Info: Zapotec Library included from folder :")
message($$PWD)

include("../Ymir/YmirNoGL.pri")


HEADERS += \
    $$PWD/affinityspace.h \
    $$PWD/binaryLineages.h \
    $$PWD/binarySequences.h \
    $$PWD/cubes.h \
    $$PWD/figures.h \
    $$PWD/foldedCubic.h \
    $$PWD/foldedFree.h \
    $$PWD/perelsonSequence.h \
    $$PWD/perelsonGeneralized.h \
    $$PWD/probabilisticArup.h \
    $$PWD/sequencespaces.h \
    $$PWD/../Tests/common.h

SOURCES += \
    $$PWD/binaryLineages.cpp \
    $$PWD/binarySequences.cpp \
    $$PWD/cubes.cpp \
    $$PWD/figures.cpp \
    $$PWD/foldedCubic.cpp \
    $$PWD/foldedFree.cpp \
    $$PWD/perelsonSequence.cpp \
    $$PWD/perelsonGeneralized.cpp \
    $$PWD/probabilisticArup.cpp \
    $$PWD/sequencespaces.cpp \
    $$PWD/../Tests/common.cpp \
    $$PWD/zapmain.cpp

win32: TARGET = Zapotec.exe
unix: TARGET = Zapotec

QMAKE_CXXFLAGS += "-Wno-old-style-cast" "-Wno-shorten-64-to-32" "-Wno-sign-conversion" "-Wno-old-style-cast" "-Wno-implicit-int-float-conversion -Wno-unused-parameter -Wno-sign-compare -Wno-implicit-float-conversion"

CONFIG -= QT

#https://gcc.gnu.org/onlinedocs/gcc-4.9.2/gcc/Warning-Options.html
CONFIG += warn_off
#https://gcc.gnu.org/onlinedocs/gcc-4.9.2/gcc/Warning-Options.html these are different
QMAKE_CXXFLAGS += -std=c++11 -Wno-extra
# -Wno-oldstyle-case -Wno-unused-parameter -Wno-shorten-64-to-32 -Wno-sign-conversion -Wno-ignored-qualifiers -Wno-type-limits -Wno-misleading-indentation -Wno-sign-compare -Wno-implicit-float-conversion
# QMAKE_CXXFLAGS += -Wall -Wextra -Wunsafe-loop-optimizations -pedantic -Wfloat-equal -Wundef -Wpointer-arith -Wcast-align -Wunreachable-code

