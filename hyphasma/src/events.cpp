#include "events.h"
#include <string>
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
using namespace std;

double currentEventsTime = 0;
vector<string*> storage;
ofstream* eventsOut = NULL;
ofstream* eventsDead = NULL;

/// Philippe 2018-04-24
string nameEvent(int typeEvent){
    switch(typeEvent){
    case event_catch_antigen: {return string("catch_antigen");}
    case event_start_TC_contact:{return string("start_TC_contact");}
    case event_stop_TC_contact: {return string("stop_TC_contact");}
    case event_start_TC_signaling: {return string("\tstart_TC_signaling");}
    case event_stop_TC_signaling:{return string("\tstop_TC_signaling");}
    case event_start_selection:{return string("start_selection");}
    case event_tc_selected:{return string("tc_selected");}
    case event_born:{return string("born");}
    case event_divide: {return string("divide");}
    case event_die:{return string("die");}
    case event_become_out:{return string("become_out");}
    case event_mutate: {return string("event_mutate");}
    case NB_types_events:{return string("Not_an_event");}
    }
    return string("Not_an_event");
}

void extendStorage(int ID){
    static bool firstTime = true;
    if(firstTime){
        storage.resize(100000, NULL);
        firstTime = false;
        eventsOut = new ofstream("historyOut.txt", ios::out);
        eventsDead = new ofstream("historyDead.txt", ios::out);
    }
    if(ID >= (int) storage.size()){
        storage.resize(ID+1,NULL);
    }
    if(!storage[ID]) storage[ID] = new string();
}

void writeEvents(){
    if(eventsOut) eventsOut->close();
    if(eventsDead) eventsDead->close();
}

void recordEvent(int ID, int typeEvent, double t, double info){
    return;
    if(t < 0) t = currentEventsTime;
    extendStorage(ID);

    stringstream res;
    res << t << "\t" << nameEvent(typeEvent) << endl;
    storage[ID]->append(res.str());

    if((typeEvent == event_divide) && (info > -0.5)){
        int IDdaughter = (int) (info + 1e-5);
        if(IDdaughter < 0) return;
        stringstream res2add;
        //res2add << "#---- Infos on mother cell (ID=" << ID << ") ----\n";
        res2add << *(storage[ID]);
        //res2add << "#---- End Infos on mother cell (ID=" << ID << ") ----\n";
        extendStorage(IDdaughter);
        *(storage[IDdaughter]) = res2add.str() + (*(storage[IDdaughter]));
    }

    if(typeEvent == event_become_out) {
        (*eventsOut) << ID << "\t" << *(storage[ID]) << endl;

    }
    if(typeEvent == event_die) {
        (*eventsDead) << ID << "\t" << *(storage[ID]) << endl;
    }
    //}
}

void eventSetTime(double t){
    currentEventsTime = t;
}
