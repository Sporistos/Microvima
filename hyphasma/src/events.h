#ifndef i_events
#define i_events



/// Philippe 2018-04-25
int get_new_ID();
enum {event_catch_antigen, event_start_TC_contact, event_stop_TC_contact, event_start_TC_signaling, event_stop_TC_signaling, event_start_selection, event_tc_selected, event_born, event_divide, event_die, event_become_out, event_mutate, NB_types_events};
void recordEvent(int ID, int typeEvent, double t = -1, double info = -1);
void eventSetTime(double newTime);
void writeEvents();


#endif

