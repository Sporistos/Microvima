#include "graph.h"

void TestGraph(){
    cerr << "0" << endl;
    liste_adjacence graph = liste_adjacence();
    cerr << "a" << endl;
    graph.addCell((long) 45, (long) 20, graphcells(170, string("A"), IGG),0.);
    graph.addCell((long) 25, (long) 15, graphcells(130, string("B"),IGG), 0.);
    graph.addCell((long) 30, (long) 25, graphcells(145, string("C"),IGG), 0.);
    graph.addCell((long) 30, (long) 25, graphcells(150, string("D"),IGG), 0.);
    graph.addCell((long) 15, (long) 10, graphcells(120, string("E"),IGG), 0.);
    graph.addCell((long) 20, (long) 15, graphcells(140, string("F"),IGG), 0.);
    graph.addCell((long) 20, (long) 15, graphcells(150, string("G"),IGG), 0.);
    graph.addCell((long) 20, (long) 15, graphcells(300, string("H"),IGG), 0.);
    graph.addCell((long) 40, (long) 20, graphcells(250, string("I"),IGG), 0.);
    graph.addCell((long) 35, (long) 25, graphcells(150, string("J"),IGG), 0.);
    graph.addCell((long) 50, (long) 25, graphcells(200, string("K"),IGG), 0.);
    graph.addCell((long) 10, (long) -1, graphcells(0,   string("L"),IGG), 0.);
    graph.addCell((long) 60, (long) 65, graphcells(100, string("M"),IGG), 0.);   // no founder in this case
    cerr << "b" << endl;
    graph.FindFounders();
    cerr << graph.print();
}
