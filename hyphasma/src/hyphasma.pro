# changed printing sequence in the middle
include("Zapotec/zapotecNoGL.pri")

HEADERS += \
    cell.h \
    cellman.h \
    cellthis.h \
    dynarray.h \
    grid.h \
    gridpoint.h \
    kinetics.h \
    ode.h \
    odelist.h \
    random.h \
    setparam.h \
    signals.h \
    space.h \
    ss.h \
    track.h \
    brainbow.h \
    antibody.h \
    events.h


SOURCES += \
    cell.cpp \
    cellman.cpp \
    cellthis.cpp \
    grid.cpp \
    gridpoint.cpp \
    hyphasma.cpp \
    kinetics.cpp \
    ode.cpp \
    odelist.cpp \
    random.cpp \
    setparam.cpp \
    signals.cpp \
    space.cpp \
    ss.cpp \
    track.cpp \
    brainbow.cpp \
    antibody.cpp \
    events.cpp \
    trackball.cpp \

QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter -Wno-reorder

#TARGET = hySeq1.09
#CONFIG += static
