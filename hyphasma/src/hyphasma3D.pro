DISTFILES += \
    ../bcinflow09rand0.par \
    ../leda-cellrep12.par \
    ../parameter_files/bcinflow02.par \
    ../parameter_files/bcinflow09.par \
    ../parameter_files/bcinflow09rand0.par \
    ../parameter_files/bcinflow09rand0abtest.par \
    ../parameter_files/bcinflow09rand0multiag001.par \
    ../parameter_files/bcinflow12.par \
    ../parameter_files/bcinflow12rand0.par \
    ../parameter_files/bcinflow13.par \
    ../parameter_files/bcinflow14.par \
    ../parameter_files/bcinflow15.par \
    ../parameter_files/bcinflow16.par \
    ../parameter_files/leda-cellrep12.par \
    ../parameter_files/leda-cr12_bcinflow.par \
    ../parameter_files/tmp.par

HEADERS += \
    cell.h \
    cellman.h \
    cellthis.h \
    dynarray.h \
    grid.h \
    gridpoint.h \
    kinetics.h \
    ode.h \
    odelist.h \
    random.h \
    setparam.h \
    signals.h \
    space.h \
    ss.h \
    track.h \
    brainbow.h \
    antibody.h \
    GC3D.h \
    trackball.h \
    events.h \
    Zapotec/affinityspace.h \
    Zapotec/binaryLineages.h \
    Zapotec/binarySequences.h \
    Zapotec/foldedCubic.h \
    Zapotec/perelsonGeneralized.h \
    Zapotec/foldedFree.h \
    Zapotec/perelsonSequence.h \
    Zapotec/probabilisticArup.h \
    Zapotec/sequencespaces.h \
    Zapotec/Tools/distribution.h \
    Zapotec/Tools/graph.h \
    Zapotec/Tests/common.h \
    Zapotec/Ymir/compact.h \
    Zapotec/Ymir/fastaffinity.h \
    Zapotec/Ymir/receptorligand.h \
    Zapotec/Ymir/proteins.h \
    Zapotec/Ymir/plot3d.h \
    Zapotec/Ymir/lattice.h \
    soil/SOIL.h \
    Zapotec/Tools/zaprandom.h \
    Zapotec/Ymir/zaptrackball.h

SOURCES += \
    cell.cpp \
    cellman.cpp \
    cellthis.cpp \
    grid.cpp \
    gridpoint.cpp \
    hyphasma.cpp \
    kinetics.cpp \
    ode.cpp \
    odelist.cpp \
    random.cpp \
    setparam.cpp \
    signals.cpp \
    space.cpp \
    ss.cpp \
    track.cpp \
    brainbow.cpp \
    antibody.cpp \
    GC3D.cpp \
    trackball.cpp \
    events.cpp \
    Zapotec/binaryLineages.cpp \
    Zapotec/binarySequences.cpp \
    Zapotec/foldedCubic.cpp \
    Zapotec/foldedFree.cpp \
    Zapotec/perelsonGeneralized.cpp \
    Zapotec/perelsonSequence.cpp \
    Zapotec/probabilisticArup.cpp \
    Zapotec/sequencespaces.cpp \
    Zapotec/Tools/distribution.cpp \
    Zapotec/Tools/graph.cpp \
    Zapotec/Tests/common.cpp \
    Zapotec/Tests/sequenceTests.cpp \
    Zapotec/Ymir/compact.cpp \
    Zapotec/Ymir/fastaffinity.cpp \
    Zapotec/Ymir/lattice.cpp \
    Zapotec/Ymir/receptorligand.cpp \
    Zapotec/Ymir/proteins.cpp \
    Zapotec/Ymir/plot3d.cpp \
    Zapotec/Ymir/main.cpp \
    Zapotec/Tools/zaprandom.cpp \
    Zapotec/Ymir/zaptrackball.cpp

QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter

DEFINES += ALLOW_GRAPHICS

DEFINES += ALLOW_GC3D

TARGET = hypv1.71
#CONFIG += static

unix: LIBS += -lglut -lGLU -lGL -lSOIL

win32: LIBS += -L$$PWD/freeglut/lib/ -lfreeglut -lopengl32
INCLUDEPATH += $$PWD/freeglut/include
#DEPENDPATH += $$PWD/freeglut/lib

win32: LIBS += -L$$PWD/soil/ -lSOIL
INCLUDEPATH += $$PWD/soil
DEPENDPATH += $$PWD/soil

win32: LIBS += -L$$PWD/glStatic/ -lglu32
INCLUDEPATH += $$PWD/glStatic
DEPENDPATH += $$PWD/glStatic

win32: LIBS += -L$$PWD/glStatic/ -lopengl32
INCLUDEPATH += $$PWD/glStatic
DEPENDPATH += $$PWD/glStatic

#OTHER_FILES += $$PWD/../../dllFilesFor3D/freeglut.dll \
#                 $$PWD/../../dllFilesFor3D/glu32.dll \
#                 $$PWD/../../dllFilesFor3D/opengl32.dll


#QMAKE_EXTRA_TARGETS += customtarget1
#customtarget1.target = dummy
#customtarget1.commands = set PATH=$$PWD/../dllFilesFor3D;$(PATH)
# copy \"$$PWD\..\..\dllFilesFor3D\*.DLL\" .
# set PATH=L$$PWD/../../dllFilesFor3D/;$(PATH)
#PRE_TARGETDEPS += dummy


