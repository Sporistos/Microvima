##################################################################################################################
# additional analysis done only for a subset
##################################################################################################################
# General settings used in all graphs
# the number of species and the time frame should correspond to the values in the parameter file:
Nruns=50
# set the x range
tmax=12
# set the y range
ymin=0
ymax=100
# for more than one species, these can be shown as different colors in each plot:
colors<-c("red","green","blue","magenta","cyan")
# plotdata should take values of enum experiments { } as defined in difevolve.h:
plotdata=-1
# remove the message that some data were not plotted
options(warn=-1)
##################################################################################################################
# rainbow48_stain40_20cols.eps
file="rainbow48_stain40_20cols.eps"
file
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality48_stain40_20cols.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  if (count==1) {
     # set the margins and set the line width
     #par(mai=c(1,1,0.5,0.5),col="black", pch=16) 
     #par(mai=c(1,1,0.5,0.5), col="black", pch=16) 
     par(mai=c(1,1,0.5,0.5), col="black") 
     # generate the graph and plot the first curve
     plot(jitter(sim[,2]/24), 100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex.axis=1.2, 
                 cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="% of the dominant", 
                 xlim=c(0,tmax), ylim=c(ymin,ymax))
     title(main = "Tamoxifen (20 colours) at day 2 after GC onset staining 40% of the cells")
  } else {
     # prevent opening of a new graph and set the color
     par(new=TRUE, col="black")
     #par(new=TRUE)
     # plot without remaking axes
     plot(jitter(sim[,2]/24),100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  }
}
dev.off()
##################################################################################################################
# rainbow48_stain40_20cols_02.eps
file="rainbow48_stain40_20cols_02.eps"
file
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality48_stain40_20cols_02.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  if (count==1) {
     # set the margins and set the line width
     #par(mai=c(1,1,0.5,0.5),col="black", pch=16) 
     #par(mai=c(1,1,0.5,0.5), col="black", pch=16) 
     par(mai=c(1,1,0.5,0.5), col="black") 
     # generate the graph and plot the first curve
     plot(jitter(sim[,2]/24), 100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex.axis=1.2, 
                 cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="% of the dominant", 
                 xlim=c(0,tmax), ylim=c(ymin,ymax))
     title(main = "Tamoxifen (20 colours) at day 2 after GC onset staining 40% of the cells")
  } else {
     # prevent opening of a new graph and set the color
     par(new=TRUE, col="black")
     #par(new=TRUE)
     # plot without remaking axes
     plot(jitter(sim[,2]/24),100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  }
}
dev.off()
##################################################################################################################
# rainbow48_stain40_20cols_03.eps
file="rainbow48_stain40_20cols_03.eps"
file
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality48_stain40_20cols_03.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  if (count==1) {
     # set the margins and set the line width
     #par(mai=c(1,1,0.5,0.5),col="black", pch=16) 
     #par(mai=c(1,1,0.5,0.5), col="black", pch=16) 
     par(mai=c(1,1,0.5,0.5), col="black") 
     # generate the graph and plot the first curve
     plot(jitter(sim[,2]/24), 100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex.axis=1.2, 
                 cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="% of the dominant", 
                 xlim=c(0,tmax), ylim=c(ymin,ymax))
     title(main = "Tamoxifen (20 colours) at day 2 after GC onset staining 40% of the cells")
  } else {
     # prevent opening of a new graph and set the color
     par(new=TRUE, col="black")
     #par(new=TRUE)
     # plot without remaking axes
     plot(jitter(sim[,2]/24),100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  }
}
dev.off()
##################################################################################################################
# rainbow48_stain100_20cols.eps
file="rainbow48_stain100_20cols.eps"
file
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality48_stain100_20cols.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  if (count==1) {
     # set the margins and set the line width
     #par(mai=c(1,1,0.5,0.5),col="black", pch=16) 
     #par(mai=c(1,1,0.5,0.5), col="black", pch=16) 
     par(mai=c(1,1,0.5,0.5), col="black") 
     # generate the graph and plot the first curve
     plot(jitter(sim[,2]/24), 100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex.axis=1.2, 
                 cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="% of the dominant", 
                 xlim=c(0,tmax), ylim=c(ymin,ymax))
     title(main = "Tamoxifen (20 colours) at day 2 after GC onset staining 100% of the cells")
  } else {
     # prevent opening of a new graph and set the color
     par(new=TRUE, col="black")
     #par(new=TRUE)
     # plot without remaking axes
     plot(jitter(sim[,2]/24),100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  }
}
dev.off()
##################################################################################################################
