##################################################################################################################
# additional analysis done only for a subset
##################################################################################################################
# General settings used in all graphs
# the number of species and the time frame should correspond to the values in the parameter file:
Nruns=50
# set the x range
tmax=12
# set the y range
ymin=0
ymax=100
# set the minimum fraction of stained cells required to be included in the plots
min_stained_fraction=0.0
# for more than one species, these can be shown as different colors in each plot:
colors<-c("red","green","blue","magenta","cyan")
# plotdata should take values of enum experiments { } as defined in difevolve.h:
plotdata=-1
# remove the message that some data were not plotted
options(warn=-1)
##################################################################################################################
# rainbow48stain100_real01.eps
file="rainbow48stain100_real01.eps"
file
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality48stain100.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  rfile<-paste(i,"/clonality_real.out", sep="")
  real<- read.table(rfile, header=FALSE, skip=1)
  if (count==1) {
     # set the margins and set the line width
     par(mai=c(1,1,0.5,0.5),col="black", pch=16) 
     # generate the graph and plot the first curve
     plot(sim[,2]/24-0.4, 100*sim[,4], col=(sim[,5]+10), pch=15+sim[,5], type="p", cex.axis=1.2, 
                 cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="% of the dominant", 
                 xlim=c(0,tmax), ylim=c(ymin,ymax))
     title(main = "Tamoxifen at day 2 after GC onset staining 100% of the cells with 4 colors")
  } else {
     # prevent opening of a new graph and set the color
     par(new=TRUE, col="black")
     # plot without remaking axes
     plot(sim[,2]/24-0.4,100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  }
  par(new=TRUE, col="black")
  plot(real[,2]/24+0.4,100*real[,4], col=real[,5]+10, pch=0+real[,5], type="p", cex=1.2, 
        xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  segments(sim[,2]/24-0.4, 100*sim[,4], real[,2]/24+0.4, 100*real[,4], lwd=0.5)
}
dev.off()
##################################################################################################################
# rainbow48stain100_real02.eps
file="rainbow48stain100_real02.eps"
file
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality48allstained2.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  rfile<-paste(i,"/clonality_real.out", sep="")
  real<- read.table(rfile, header=FALSE, skip=1)
  if (count==1) {
     # set the margins and set the line width
     par(mai=c(1,1,0.5,0.5),col="black", pch=16) 
     # generate the graph and plot the first curve
     plot(sim[,2]/24-0.4, 100*sim[,4], col=(sim[,5]+10), pch=15+sim[,5], type="p", cex.axis=1.2, 
                 cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="% of the dominant", 
                 xlim=c(0,tmax), ylim=c(ymin,ymax))
     title(main = "Tamoxifen at day 2 after GC onset staining 100% of the cells with 4 colors")
  } else {
     # prevent opening of a new graph and set the color
     par(new=TRUE, col="black")
     # plot without remaking axes
     plot(sim[,2]/24-0.4,100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  }
  par(new=TRUE, col="black")
  plot(real[,2]/24+0.4,100*real[,4], col=real[,5]+10, pch=0+real[,5], type="p", cex=1.2, 
        xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  segments(sim[,2]/24-0.4, 100*sim[,4], real[,2]/24+0.4, 100*real[,4], lwd=0.5)
}
dev.off()
##################################################################################################################
min_list = seq(0.0,0.35,by=0.05)
for (i in 1:length(min_list)) {
min_stained = min_list[i]
print(paste("Run with min_stained=",min_stained,"..."))
  ######################################
# rainbow48_stain8_4cols_real01.eps
file<-paste("rainbow48_stain8_4cols_real01_min",100*min_stained,".eps", sep="")
print(file)
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality48_stain8_4cols_01.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  rfile<-paste(i,"/clonality_real.out", sep="")
  real<- read.table(rfile, header=FALSE, skip=1)
  if (count==1) {
     # set the margins and set the line width
     par(mai=c(1,1,0.5,0.5),col="black", pch=16) 
     # generate the graph and plot the first curve
     plot(sim[,2]/24-0.4, 100*sim[,4], col=(sim[,5]+10), pch=15+sim[,5], type="p", cex.axis=1.2, 
                 cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="% of the dominant", 
                 xlim=c(0,tmax), ylim=c(ymin,ymax))
     title(main = "Tamoxifen at day 2 after GC onset staining 8% of the cells with 4 colors")
  } else {
     # prevent opening of a new graph and set the color
     par(new=TRUE, col="black")
     # plot without remaking axes
     plot(sim[,2]/24-0.4,100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  }
  par(new=TRUE, col="black")
  plot(real[,2]/24+0.4,100*real[,4], col=real[,5]+10, pch=0+real[,5], type="p", cex=1.2, 
        xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  if (min_stained>0) {
    segments(sim[,2]/24-0.4, 100*sim[,4]*round(((sim[,3]/real[,3])-min_stained)+0.5), 
	    real[,2]/24+0.4, 100*real[,4]*round(((sim[,3]/real[,3])-min_stained)+0.5), lwd=0.5)
  } else {
    segments(sim[,2]/24-0.4, 100*sim[,4], real[,2]/24+0.4, 100*real[,4], lwd=0.5)
  }
}
dev.off()
##################################################################################################################
# rainbow48_stain8_4cols_real02.eps
file<-paste("rainbow48_stain8_4cols_real02_min",100*min_stained,".eps", sep="")
#file="rainbow48_stain8_4cols_real02.eps"
print(file)
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality48_stain8_4cols_02.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  rfile<-paste(i,"/clonality_real.out", sep="")
  real<- read.table(rfile, header=FALSE, skip=1)
  if (count==1) {
     # set the margins and set the line width
     par(mai=c(1,1,0.5,0.5),col="black", pch=16) 
     # generate the graph and plot the first curve
     plot(sim[,2]/24-0.4, 100*sim[,4], col=(sim[,5]+10), pch=15+sim[,5], type="p", cex.axis=1.2, 
                 cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="% of the dominant", 
                 xlim=c(0,tmax), ylim=c(ymin,ymax))
     title(main = "Tamoxifen at day 2 after GC onset staining 8% of the cells with 4 colors")
  } else {
     # prevent opening of a new graph and set the color
     par(new=TRUE, col="black")
     # plot without remaking axes
     plot(sim[,2]/24-0.4,100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  }
  par(new=TRUE, col="black")
  plot(real[,2]/24+0.4,100*real[,4], col=real[,5]+10, pch=0+real[,5], type="p", cex=1.2, 
        xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  if (min_stained>0) {
    segments(sim[,2]/24-0.4, 100*sim[,4]*round(((sim[,3]/real[,3])-min_stained)+0.5), 
	    real[,2]/24+0.4, 100*real[,4]*round(((sim[,3]/real[,3])-min_stained)+0.5), lwd=0.5)
  } else {
    segments(sim[,2]/24-0.4, 100*sim[,4], real[,2]/24+0.4, 100*real[,4], lwd=0.5)
  }
}
dev.off()
##################################################################################################################
# rainbow48_stain8_4cols_real03.eps
file<-paste("rainbow48_stain8_4cols_real03_min",100*min_stained,".eps", sep="")
#file="rainbow48_stain8_4cols_real03.eps"
print(file)
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality48_stain8_4cols_03.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  rfile<-paste(i,"/clonality_real.out", sep="")
  real<- read.table(rfile, header=FALSE, skip=1)
  if (count==1) {
     # set the margins and set the line width
     par(mai=c(1,1,0.5,0.5),col="black", pch=16) 
     # generate the graph and plot the first curve
     plot(sim[,2]/24-0.4, 100*sim[,4], col=(sim[,5]+10), pch=15+sim[,5], type="p", cex.axis=1.2, 
                 cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="% of the dominant", 
                 xlim=c(0,tmax), ylim=c(ymin,ymax))
     title(main = "Tamoxifen at day 2 after GC onset staining 8% of the cells with 4 colors")
  } else {
     # prevent opening of a new graph and set the color
     par(new=TRUE, col="black")
     # plot without remaking axes
     plot(sim[,2]/24-0.4,100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  }
  par(new=TRUE, col="black")
  plot(real[,2]/24+0.4,100*real[,4], col=real[,5]+10, pch=0+real[,5], type="p", cex=1.2, 
        xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  if (min_stained>0) {
    segments(sim[,2]/24-0.4, 100*sim[,4]*round(((sim[,3]/real[,3])-min_stained)+0.5), 
	    real[,2]/24+0.4, 100*real[,4]*round(((sim[,3]/real[,3])-min_stained)+0.5), lwd=0.5)
  } else {
    segments(sim[,2]/24-0.4, 100*sim[,4], real[,2]/24+0.4, 100*real[,4], lwd=0.5)
  }
}
dev.off()
   ###########################################################
} # for min_stained
##################################################################################################################
min_list = seq(0.0,0.13,by=0.01)
for (i in 1:length(min_list)) {
min_stained = min_list[i]
print(paste("Run with min_stained=",min_stained,"..."))
# rainbow48stain1_real01.eps
file<-paste("rainbow48stain1_real01_min",100*min_stained,".eps", sep="")
#file="rainbow48stain1_real01.eps"
print(file)
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality48stain1.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  rfile<-paste(i,"/clonality_real.out", sep="")
  real<- read.table(rfile, header=FALSE, skip=1)
  if (count==1) {
     # set the margins and set the line width
     par(mai=c(1,1,0.5,0.5),col="black", pch=16) 
     # generate the graph and plot the first curve
     plot(sim[,2]/24-0.4, 100*sim[,4], col=(sim[,5]+10), pch=15+sim[,5], type="p", cex.axis=1.2, 
                 cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="% of the dominant", 
                 xlim=c(0,tmax), ylim=c(ymin,ymax))
     title(main = "Tamoxifen at day 2 after GC onset staining 1% of the cells with 4 colors")
  } else {
     # prevent opening of a new graph and set the color
     par(new=TRUE, col="black")
     # plot without remaking axes
     plot(sim[,2]/24-0.4,100*sim[,4], col=sim[,5]+10, pch=15+sim[,5], type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  }
  par(new=TRUE, col="black")
  plot(real[,2]/24+0.4,100*real[,4], col=real[,5]+10, pch=0+real[,5], type="p", cex=1.2, 
        xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
  if (min_stained>0) {
    segments(sim[,2]/24-0.4, 100*sim[,4]*round(((sim[,3]/real[,3])-min_stained)+0.5), 
	    real[,2]/24+0.4, 100*real[,4]*round(((sim[,3]/real[,3])-min_stained)+0.5), lwd=0.5)
  } else {
    segments(sim[,2]/24-0.4, 100*sim[,4], real[,2]/24+0.4, 100*real[,4], lwd=0.5)
  }
}
dev.off()
} # for min_stained
##################################################################################################################
