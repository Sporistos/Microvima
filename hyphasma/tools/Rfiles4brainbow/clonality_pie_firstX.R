##################################################################################################################
# additional analysis done only for a subset
#library(ggplot2)
##################################################################################################################
# General settings used in all graphs
# set the y range
ymin=0
ymax=100
# for more than one species, these can be shown as different colors in each plot:
colors<-c("red","green","blue","magenta","cyan")
# plotdata should take values of enum experiments { } as defined in difevolve.h:
plotdata=-1
# remove the message that some data were not plotted
options(warn=-1)
count=0
stopat=8
##################################################################################################################
# clonality_pie_dayX.eps
# go through all runs
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
for (i in dir_list) { 
  if (count<stopat) {
  # set the output file name
  ifile<-paste("./",basename(i),"/clone_fractions.out", sep="")
  #ifile<-paste("./clone_fractions.out", sep="")
  # read next data set
  clone_fractions<- read.table(ifile, header=FALSE, skip=1)
  bla <- dim(clone_fractions[,])
  Ntimes <- bla[1]
  for (j in 1:Ntimes) {
      time<-clone_fractions[j,1]/24
      cellnum<-clone_fractions[j,2]
      founder<-clone_fractions[j,3]
      file=paste("./",basename(i),"/clonality_pie_day",time,".eps", sep="")
      print(file)
      cairo_ps(width=3, height=3, file)
      #print(clone_fractions[1,])
      #print(length(clone_fractions[1,]))
      x <- clone_fractions[j,4:(founder+3)]
      remove <- c(0)
      x <- x[! x %in% remove]
      #print(x)
      #print(length(x))
      clonenum<-length(x)
      if (clonenum>0) {
        # set the margins and set the line width
        par(mai=c(0.2,0.2,0.2,0.2), col="black") 
        pie(t(x), labels="", col=sample(rainbow(clonenum)), radius=1.0, clockwise=FALSE)
        title(main = paste("day ",time,", ",clonenum," clones, ",cellnum," cells",sep=""))
      }
      dev.off()
  }
  }
  count=count+1
}
##################################################################################################################
# clonality_pie_subset_dayX.eps
# go through all runs
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  if (count<stopat) {
  # set the output file name
  ifile<-paste("./",basename(i),"/clone_fractions_rand.out", sep="")
  #ifile<-paste("./clone_fractions_rand.out", sep="")
  # read next data set
  clone_fractions<- read.table(ifile, header=FALSE, skip=1)
  bla <- dim(clone_fractions[,])
  Ntimes <- bla[1]
  for (j in 1:Ntimes) {
      time<-clone_fractions[j,1]/24
      cellnum<-clone_fractions[j,2]
      founder<-clone_fractions[j,3]
      file=paste("./",basename(i),"/clonality_pie_subset_day",time,".eps", sep="")
      print(file)
      cairo_ps(width=3, height=3, file)
      #print(clone_fractions[1,])
      #print(length(clone_fractions[1,]))
      x <- clone_fractions[j,4:(founder+3)]
      remove <- c(0)
      x <- x[! x %in% remove]
      #print(x)
      #print(length(x))
      clonenum<-length(x)
      if (clonenum>0) {
        # set the margins and set the line width
        par(mai=c(0.2,0.2,0.2,0.2), col="black") 
        pie(t(x), labels="", col=sample(rainbow(clonenum)), radius=1.0, clockwise=FALSE)
        title(main = paste("day ",time,", ",clonenum," clones, ",cellnum," cells",sep=""))
      }
      dev.off()
  }
  }
  count=count+1
}
##################################################################################################################

