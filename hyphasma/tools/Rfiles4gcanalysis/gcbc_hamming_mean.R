##################################################################################################
# generates a plot of GC-BC numbers over Hamming distance to the nearest antigen-epitope
# every time point is represented by a separate line
##################################################################################################
get_data <- function(pathname,filename) {
  dataset <- read.table(paste(pathname,filename,sep=""), header=FALSE, skip=1)
  names(dataset) <- c("days","hours","Hamming","BCnumber","BCnumbermean")
  data.frame(Time=dataset$days,Hamming=dataset$Hamming,BCnumber=dataset$BCnumbermean)
}

# remove the message that some data were not plotted
# options(warn=-1)
# set the output file name
file <- "gcbc_hamming_mean.eps"
cairo_ps(width=10, height=10, file)
# read the data
alldata <- get_data("./","gcbc_hamming.out")
#print("data read")
#print(alldata)
# get the range of times which will be the number of lines to plot
times <- unique(alldata$Time)
ntimes <- length(times)
#print(times)
# define a corresponding colour range for the lines
color <- rainbow(ntimes)
#print(color)
# get the range of Hamming distances
hammings <- unique(alldata$Hamming)
nhammings <- length(hammings)
#print(hammings)
# for each time draw an extra line of BCnumber(Hamming)
for (i in 1:ntimes) {
  timenow <- times[i]
  print(paste("Run time = ",timenow," days ...",sep=""))
  # reduce the full data set to the one with a particular time
  dataoft <- alldata[alldata$Time==timenow,]
  #print(dataoft)
  cellsum <- sum(dataoft[,3])
  if (cellsum>0) {
    dataoft[,3] <- dataoft[,3]/cellsum
  } else {
    dataoft[,3] <- dataoft[,3]-dataoft[,3]
  }
  #print(dataoft)	
  #print(paste("sum of all cells = ",cellsum,sep=""))
  if (i==1) {
    # set the margins and set the line width
    par(mai=c(1,1,0.5,0.5), col="black", pch=1) 
    # generate the graph and plot the first curve
    plot(dataoft[,2], dataoft[,3], type="l", col=color[i],
	 cex.axis=1.5, cex.lab=2.0, cex=1.2, 
	 xlab="Mean Hamming distance to all antigens", 
         ylab="% of total germinal centre B cells", 
	 xlim=c(hammings[nhammings],hammings[1]), ylim=c(0,1))
    #title(main = "Affinity maturation of germinal centre B cells")
  } else {
    # prevent opening of a new graph and set the color
    par(new=TRUE, col=color[i], pch=0)
    # plot without remaking axes
    plot(dataoft[,2],dataoft[,3], type="l", cex=1.2,
         xlab="", ylab="", axes=FALSE, xlim=c(hammings[nhammings],hammings[1]), ylim=c(0,1))
  }
  par(new=TRUE, col=color[i], pch=0)
  plot(dataoft[,2],dataoft[,3], type="p", cex=1.5,
       xlab="", ylab="", axes=FALSE, xlim=c(hammings[nhammings],hammings[1]), ylim=c(0,1))
}
legend(nhammings-2, 1.0, times, lty=1, lwd=2, pch=0, cex=1.0, col=color, title="day", 
       text.col="black")
dev.off()
###################################################################################################
