if NOT exist %1 do (
echo "This script expects one argument:" 
echo "The path and name of the result directory relative to the tools directory."
echo "exit analysis".
) else (
set directory=%1
echo "Simulation analysis in " %directory%
echo "Prepare analysis files ... "
g++ -o average average.cpp
g++ -o average-all average-all.cpp
copy average %directory%
copy average-all %directory%
copy Rfiles4gcanalysis\*.R %directory%
copy ..\gle\*.gle %directory%%
copy ..\data\*.exp %directory%
copy mkfigure.bat %directory%
copy resultpage.tex %directory%
echo "Run analysis ... "
cd %directory%
mkfigure.bat
echo "Remove analysis files ... "
REM rm ./mkfigure
del *.R
del *.gle
del *.exp
REM #rm ./*.eps
echo " ... done."
REM evince resultpage.pdf &
echo "Figures associated with the analysis can be viewed by <evince resultpage.pdf>"
echo "in the directory of the analysed simulation "$directory
)

EXIT \B 0