// #include <stdlib.h>
// #include <ctime>
// #include <stdio.h>
// #include <math.h>
#include "track.h"

/* Author: Michael Meyer-Hermann 2nd September 2007
 *
 * Uses the file trackraw.out in the same directory and repeats the
 * tracking analysis. Some properties might be imposed by hand, thus,
 * the analysis might be done with different parameters than originally.
 * This is possible because all movements are tracked, such that within
 * the registration period any kind of analysis might be done.
 */

int main() {
  // Define an tracking object
  TRACK trackdata;
  // Read the data from trackraw.out
  int n_objects_per_type = -1; // chose -1 for using all tracked objects
  trackdata.get_trackraw(n_objects_per_type);
  double from = TRACK::TRACKFROM;
  double til = TRACK::TRACKUNTIL;
  // Change some parameter if needed
  // TRACK::TRACKFROM=80.0;
  // TRACK::TRACKUNTIL=90.0;
  // TRACK::DELTA_T=0.03333333;
  // TRACK::V_RESOLUTION=1;
  // TRACK::DELTA_V=;
  // TRACK::ALPHA_RESOLUTION=;
  // TRACK::DELTA_ALPHA=;
  // TRACK::S_RESOLUTION=;
  // TRACK::DELTA_S=;
  // direction of plane for zone transition analysis
  // TRACK::nhess[0]=0.; TRACK::nhess[1]=1.; TRACK::nhess[2]=0.;
  /* Activation of INCLUDE_INCONTACT = true is standard and correct.
   * If it is set false, BC in contact to FDC or TC are not included in the analysis.
   * However, this is an approximation as the detection frequency does not match
   * the frequency of contact durations and is not synchronised. Thus, it every
   * case one has to test, whether this provides useful results.
   */
  TRACK::INCLUDE_INCONTACT = true;


  if ((TRACK::TRACKFROM < from) || (TRACK::TRACKFROM > til)
      || (TRACK::TRACKUNTIL < from) || (TRACK::TRACKUNTIL > til)) {
    cout << "ERROR: REQUIRED TRACKING TIMES NOT COVERED BY THE DATA!\n";
    exit(1);
  }

  cout << "\nData as used for this tracking analysis:\n";
  cout << "TRACKFROM=" << TRACK::TRACKFROM << "\n"
       << "TRACKUNTIL=" << TRACK::TRACKUNTIL << "\n"
       << "DELTA_T=" << TRACK::DELTA_T << "\n"
       << "V_RESOLUTION=" << TRACK::V_RESOLUTION << "\n"
       << "DELTA_V=" << TRACK::DELTA_V << "\n"
       << "INCLUDE_INCONTACT (set in anatrack.cpp)=" << TRACK::INCLUDE_INCONTACT << "\n"
       << "ALPHA_RESOLUTION=" << TRACK::ALPHA_RESOLUTION << "\n"
       << "DELTA_ALPHA=" << TRACK::DELTA_ALPHA << "\n"
       << "S_RESOLUTION=" << TRACK::S_RESOLUTION << "\n"
       << "DELTA_S=" << TRACK::DELTA_S << "\n"
       << "nhess=(" << TRACK::nhess[0] << "," << TRACK::nhess[1] << "," << TRACK::nhess[2] << ")\n";

  // Perform the actual analysis
  trackdata.Write_files(false);

  return 1;
}
