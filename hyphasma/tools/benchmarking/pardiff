#!/usr/bin/env python
# Parse + compare parameter files. Parsing the format is a nightmare,
# should be changed to ini, yaml, json, ... Plenty of sane formats
# are available.
import sys

class bcolors:
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    
def is_numeric(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
    
def parse_parfile(fh):
    kenntext = ""
    parameters = {}
    for i,line in enumerate(fh):
        line = line.strip()
        if not line or line.startswith("#") or line.startswith("---") or line.startswith("="):
            continue
        elif is_numeric(line):
            if kenntext and kenntext in ["Fraction of Ags (non-fixed Ag enter with same fraction):",
                                         "Fix Antigen Sequence presentation (max 1000 values):",
                                         "Fix initial Repertoire distribution (max 1000 values):",
                                         "Fix initial Repertoire distribution for T cells:",
                                         "Fix initial Centroblast distribution (max 10 values):",
                                         "Fix initial Centroblast position (max 10 values):",
                                         "Fix initial blast2 position (max 10 values):",
                                         "Class switch probabilities (sum of each line must be 1):",
                                         "Fix Antigen Sequence presentation. Order is Conserved(should be 1), Variable, shielded (should be 1). max 1000 values [arup_ini_antigens[...]]:",
                                         "Fraction of Arup Ags (non-fixed Ag enter with same fraction) [arup_ag_fraction[...]]:",
                                         "Fix initial Repertoire distribution (max 1000 values) [arup_ini_bcrs[...]]:"]:
                if line=="-1":
                    kenntext=""
                else:
                    try:
                        parameters[kenntext][1] += ", " + line
                    except KeyError:
                        parameters[kenntext] = [i,line]
            elif kenntext:
                parameters[kenntext] = [i,line]
                kenntext = ""
        else:
            kenntext = line.strip()
    return parameters

def pad(text):
    print "---"
    print text
    print "---\n"

def added(text):
    return "%s> %s%s" % (bcolors.GREEN, text, bcolors.ENDC)

def deleted(text):
    return "%s< %s%s" % (bcolors.RED, text, bcolors.ENDC)

def diff(origfn, newfn):
    with open(origfn, "r") as origfh:
        orig = parse_parfile(origfh)
    with open(newfn, "r") as newfh:
        new = parse_parfile(newfh)
    keys = set(orig.keys() + new.keys())
    additions, deletions, changes, unchanged = 0, 0, 0, 0
    for k in keys:
        if not orig.has_key(k):
            pad(added("%i %s %s" % (new[k][0], k, new[k][1])))
            additions += 1
        elif not new.has_key(k):
            pad(deleted("%i %s %s" % (orig[k][0], k, orig[k][1])))
            deletions += 1
        elif orig[k][1]!=new[k][1]:
            pad(deleted("%i %s %s\n" % (orig[k][0],k, orig[k][1])) + added("%i %s %s" % (new[k][0], k, new[k][1])))
            changes += 1
        else:
            unchanged += 1
    print '''%s                         %s
%sStats:                   %s
New parameters:       %i
Deleted parameters:   %i
Changed parameters:   %i
%sUnchanged parameters: %i%s''' % (bcolors.UNDERLINE, bcolors.ENDC,
                                       bcolors.BOLD + bcolors.UNDERLINE,bcolors.ENDC,
                                       additions, 
                                       deletions, 
                                       changes, 
                                       bcolors.UNDERLINE, unchanged, bcolors.ENDC)
    
    
diff(sys.argv[1], sys.argv[2])