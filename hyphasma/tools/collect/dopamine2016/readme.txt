The gle-files in this directory need out-files in the subdirectories of 
the hyphasma16.05.1/results and the hyphasma/results directory containing 
simulation results. In order to
generate these out-files, the analysis files in the directory statfiles
have to be copied into these respective subdirectories, they need to
be run by evoking mkstat-out, and the resulting out-files are interpreted
with the gle-files in the present directory. 

The process is automatic by evoking <mkcollect_dopamine>.
This also generates a page <resultpage.pdf> which summarises all results.

The present version is made for the dopamine experiments with Carola.
The figures have a nomenclature _leda, _icos, _recy, which refers to
different models:
_leda refers to the LEDA model published in Cell Reports 2012
_icos refers to the new model generated in v20161122 with ICOSL regulation 
      and multiple T-B-interactions with signal integration
_recy refers to the _icos model but not using the LEDA as a basis but
      the textbook recycling model with 80% recycling probability

