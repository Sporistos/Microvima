/* xvolume_stat.C, 23.12.2004; last update 21.06.2016
   Code zum einlesen von GC-readouts aus strukturell identischen files 
   in nummerierten Unterverzeichnissen.
   Die files und Parameter (Spalten) werden im Quellcode angegeben,
   der Code compiliert,
   die Ausfuehrung des Codes liest die Daten ein,
   berechnet Mittelwerte und Standardabweichung,
   und schreibt die Daten in das gewuenschte file raus.
*/

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <math.h>
#include <string.h>

const short maxcols=5;

const int fn=100; // ### insert number of files to read
const int valn=504; // ### insert number of values to read in each file
const short xcol=1; // ### insert the column for the x-value
//const short ycol[]; // ### insert the column for the y-value
const short y0col=4; // ### insert the column for the first y-value
const short y1col=4;
const short y2col=4;
const short y3col=5;
const short y4col=6;
const short nycols=1; // ### insert number of y-values to read
const int ignore=1; // ### insert number of lines to ignore at file start
const char outputfilename[30]="xvolume_stat.out"; // ### insert the output filename

const short ndigits = 3;
/** @brief Type definition for suffixes with increasing ID each time (0000, 0001 etc ...)*/
typedef char suffix[ndigits + 1];
/** @brief to increase the suffix by 1. */
void addchar(suffix &tmp) {
  // works for numbers with 3 digits
  int i = ndigits - 1;
  char weiter = 1;
  while (i >= 0 && weiter == 1) {
    if (tmp[i] != '9') {
      ++tmp[i];
      weiter = 0;
    } else {
      tmp[i] = '0';
      --i;
    }
  }
  if (weiter == 1) {
    std::cout << "Too large number in addchar(..) !\n";
  }
}
void get_charnum(int &num, suffix &charnum) {
  //charnum = "000";
  for (int i = 0; i < num; i++) { addchar(charnum); }
}

void getname(int& i, char *datname) { // ### insert filename here
  suffix charnum = "000";
  get_charnum(i, charnum);
  std::cout<<charnum<<".";
  strcat(datname, "./");
  strcat(datname, charnum);
  strcat(datname, "/xvolume.out");
}

std::ifstream in[fn];

void nextline(std::ifstream& s) {
  char d='a';
  int i=0;
  while (int(d)!=10 && i<200) {
    s.get(d); 
    i++;
  }
}

void openall() {
  const char* filename = "";
  std::cout<<"open files ";
  for (int i=0; i<fn; i++) {
    char datname[50] = "";
    getname(i, datname);
    filename = datname;
    in[i].open(filename);
    for (int j=0; j<ignore; j++) nextline(in[i]);
    //std::cout<<"open file "<<filename<<".\n";
  }
  std::cout<<" done.\n";
}

void closeall() {
  for (int i=0; i<fn; i++) in[i].close();
}

int main()
{
  int i,n;
  short cols,thiscol,abcol;
  // initialize x-value
  double x,x2,tmp;
  // initialize y-values
  double y[maxcols];
  double sigma[maxcols];
  double val[maxcols][fn];
  short ycol[maxcols];
  ycol[0]=y0col;
  ycol[1]=y1col;
  ycol[2]=y2col;
  ycol[3]=y3col;
  ycol[4]=y4col;
  if (fn<=1) { 
    std::cout<<"Es sind mehr als "<<fn<<" Dateien gefordert!\n";
    exit(1);
  }

  // open the necessary files
  openall();
  std::ofstream result(outputfilename);
  std::cout<<"Write in file "<<outputfilename<<" ... ";

  // read values from all files
  for (i=0; i<valn; i++) {
    for (cols=0; cols<maxcols; cols++) {
      y[cols]=0.0;
      sigma[cols]=0.0;
      for (n=0; n<fn; n++) val[cols][n]=0.0;
    }
    x=0.0; x2=0.0;
    for (n=0; n<fn; n++) {
      for (cols=1; cols<xcol; cols++) in[n]>>tmp;
      in[n]>>x2;
      if (n==0) { x=x2; }
      else if (x!=x2) {
	std::cout<<"Error: Inconsistent x-values in file 0 and "<<"n"<<":"
		 <<"x="<<x<<" x2="<<x2
		 <<"; at value number "<<i<<"\n";
	exit(1);
      }
      abcol=xcol;
      for (thiscol=0; thiscol<nycols; thiscol++) {
	for (cols=abcol+1; cols<ycol[thiscol]; cols++) in[n]>>tmp;
	if (in[n].eof()==1)
  	  std::cout<<"Unexpected end of file in file n="<<n<<"!!!\n";
	else in[n]>>val[thiscol][n];
	y[thiscol]+=val[thiscol][n];
	//std::cout<<thiscol<<" "<<y[thiscol]<<";  ";
	abcol=ycol[thiscol];
      }
      nextline(in[n]);
    }
    // schreibe x Wert in das neue file
    result<<x<<"   ";
    // jetzt ist der x-Wert in x und die y-Werte aus allen files in val[][] drin
    for (cols=0; cols<nycols; cols++) {
      // berechne Mittelwerte
      y[cols]/=fn;
      //std::cout<<"cols="<<cols<<" y="<<y[cols]<<"\n";
      // berechne Standardabweichung
      for (n=0; n<fn; n++) sigma[cols]+=pow(val[cols][n]-y[cols],2.);
      sigma[cols]/=double(fn-1);
      sigma[cols]=sqrt(sigma[cols]);
      // schreibe y Wert in das neue file
      result<<y[cols]<<"   "<<sigma[cols]<<"   ";
    }
    // Zeilenwechsel im neuen file
    result<<"\n";
  }
  
  // close all files
  std::cout<<"done.\n";
  result.close();
  closeall();
  return 1;
}


