The gle-files in this directory need out-files in the subdirectories of 
the hyphasma9.09.1 directory containing simulation results. In order to
generate these out-files, the analysis files in the directory statfiles
have to be copied into these respective subdirectories, they need to
be run by evoking mkstat-out, and the resulting out-files are interpreted
with the gle-files in the present directory.

The so far last version of statistical evaluation of differently seeded
runs was done in hyphasma14.10.1 in the context of miR-155.
I now deleted all gle-files here because of a smooth switch to R.
However, the programmes in statfiles would still be relevant in
many context in order to generate some statistical properties
from the files in subdirectories of hyphasmaxxx.

