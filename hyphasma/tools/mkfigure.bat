#cp ../liu91.txt .
#cp ../hol92.txt .
Rscript xlogbcaff.R
Rscript gcbc_hamming.R
Rscript gcbc_hamming_lessdays.R
Rscript gcbc_hamming_mean.R
Rscript gcbc_affinity.R
Rscript gcbc_affinity_mean.R
Rscript mutation_histo.R
Rscript mutation_histo_lessdays.R
gle ag_collected.gle
gle ag_endhisto.gle
gle aff_bc.gle
#gle aff_cb.gle
#gle aff_cc.gle
gle aff_out.gle
#gle antibodies.gle
gle antigen.gle
#gle cbha_c.gle
gle cellcycle_durations.gle
gle dec205.gle
gle dz2lzdata.gle
#gle dz2lz.gle
#gle dz2lz_ig.gle
average-all xsumbc_ig.out 1 25 24
mv averaged.out xsumbc_ig_smooth.out
gle dz2lz_ig_smooth.gle
gle dz2lznoapoz.gle
gle dz2lznoapoz-hd-log.gle
#gle dzout2lz.gle
#gle dzsel2lz.gle
#gle dzsel2lznoapo.gle
#
average ssumapo1.out 1 2 24
mv averaged.out ssumapo1_smooth.out
average ssumcc.out 1 2 24
mv averaged.out ssumcc_smooth.out
average xvolume.out 1 9 24
mv averaged.out xvolume_smooth.out
average ssumout.out 1 5 24
mv averaged.out ssumout_smooth.out
gle fracapobc_smooth.gle
#gle fracapo_smooth.gle
#
gle fdcencount.gle
gle fracFDCco.gle
gle fracTFHco.gle
#gle frac-divide.gle
gle mutation.gle
gle mutation_out_histo.gle
gle mutation_set.gle
gle mutation_time.gle
gle ndivtime.gle
gle ndivhisto.gle
gle nhaffin_c.gle
#gle outfrac.gle
gle recycling.gle
#gle s40aff.gle
#gle s4080aff.gle
#gle s80aff.gle
gle saffin.gle
gle selected.gle
gle shaffin_c.gle
gle sh40affin.gle
#gle ssumapo1.gle
#gle ssumapo2.gle
gle ssumout.gle
#gle ssumoute.gle
gle tc.gle
#gle vdist.gle
#gle vdistp.gle
#gle vol3d_data.gle
#gle vol_data.gle
#gle vol_korr.gle
gle voldec205.gle
#gle xlogouts.gle
gle xaff_ig.gle
#gle xapo_ig.gle
average-all xapo_ig.out 1 11 24
mv averaged.out xapo_ig_smooth.out
average-all xapo_ig_dz.out 1 11 24
mv averaged.out xapo_ig_dz_smooth.out
average-all xapo_ig_lz.out 1 11 24
mv averaged.out xapo_ig_lz_smooth.out
gle xapo_ig_smooth.gle
gle xapo_ig_zones_smooth.gle
gle xsumbc_ig.gle
gle xsumbc_igfrac.gle
#gle xsumbco_ig.gle
gle xsumout_ig.gle
gle xsumcb_c.gle
#gle xsumcbcc.gle
gle xsumcc_c.gle
#gle xvolume.gle
gle zone_da_c.gle
#gle zonercb.gle

gle *.gle
############################
#gv aff_bc.eps &
##gv aff_cb.eps &
##gv aff_out.eps &
##gv antibodies.eps &
##gv antigen.eps &
##gv cbha_c.eps &
##gv dec205.eps &
##gv dz2lz.eps &
#gv dz2lz_ig.eps &
#gv dz2lz_ig_smooth.eps &
#gv dz2lznoapoz.eps &
##gv dz2lznoapoz-hd-log.eps &
##gv dzout2lz.eps &
##gv dzsel2lz.eps &
##gv dzsel2lznoapo.eps &
##gv fdcencount.eps &
##gv frac-divide.eps &
##gv fracapo_smooth.eps &
##gv fracapobc_smooth.eps &
##gv fracFDCco.eps &
#gv nhaffin_c.eps &
##gv outfrac.eps &
##gv recycling.eps &
#gv saffin.eps &
##gv selected.eps &
##gv sh40affin.eps &
##gv ssumout.eps &
##gv tc.eps &
##gv vol3d_data.eps &
##gv vol_data.eps &
##gv voldec205.eps &
##gv xlogouts.eps &
#gv xaff_ig.eps &
#gv xapo_ig.eps &
#gv xapo_ig_smooth.eps &
#gv xapo_ig_zones_smooth.eps &
#gv xsumbc_ig.eps &
##gv xsumbco_ig.eps &
##gv xsumcb_c.eps &
##gv xsumcbcc.eps &
##gv xsumcc_c.eps &
##gv zone_da_c.eps &
##gv zonercb.eps &
latex -interaction=nonstopmode resultpage.tex
latex -interaction=nonstopmode resultpage.tex
dvips resultpage.dvi
ps2pdf resultpage.ps
#evince resultpage.pdf &

