echo "Clearing all sub-folders from big and non-used output files + signal files."
find . -type f -name 'xlogbcaff.out' -delete
find . -type f -name 'xlogouts.out' -delete
find . -type f -name 'cxcl*.dat' -delete
find . -type f -name 'xy*.pov' -delete
find . -type f -name 'xy*.ppm' -delete
find . -type f -name 'sigs_cxcl*.out' -delete
find . -type f -name 'cxcl*ini.dat' -delete

#for j in `find . -type d -name 'bla'`; do