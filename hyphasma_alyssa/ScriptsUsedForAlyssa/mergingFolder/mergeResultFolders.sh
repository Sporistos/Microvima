

function merge {
	rm allOutputs/*
	mkdir allOutputs
	rm -r mergedOutputs
		
	#cd $1
	#LIST=find -maxdepth 1 -type d
	#echo list
	#for f in $(find -maxdepth 1 -type d);
	#for f in $(ls -d */);
	
	#echo "\\begin{verbatim}" >> allOutputs/index.tex  
	set var = 0
	for f in */;
	do
	# note : leave spaces inside the []  https://stackoverflow.com/questions/4277665/how-do-i-compare-two-string-variables-in-an-if-statement-in-bash
		if [ "$f" == "allOutputs/" ]
		then
			echo "Discarding $f"
		else {
			echo "Copying files from $f ($var)"
			echo "$var = ${f%Done*}" >> allOutputs/index.tex  
			cd $f
			for g in *.out;
			do
				
				cp "$g" "../allOutputs/${g%.*}$var.out"
			done
			cd ..
			
			#echo "${f%/}"
			#cd ../hyphasma/tools/
			#./analysis ../../scripts/$f #&> trash.txt
			#cd ../../scripts/
			#cp $f/resultpage.pdf ${f%/}.pdf
			}
		fi
	var=$((var+1))
	done	
	
}

# merged output should not exist, so it is not taken as a simulation
#rm mergedOutputs/*
#rm -rf mergedOutputs

merge

Rscript mergeFolders.R
R --vanilla < mergeGTI.R
R --vanilla < mergeDeath.R
R --vanilla < mergeVol.R
R --vanilla < mergeAlyssa.R

# rm -rf allOutputs/
cd ../hyphasma/tools/
./analysis ../../scripts/mergedOutputs/ #&> trash.txt
cd ../../scripts
#if [ ! -f "$NEWFOLDER/resultpage.pdf" ] ; then
#  rm -rf $NEWFOLDER
#fi
#cp mergedOutputs/resultpage.pdf mergedOutputs.pdf