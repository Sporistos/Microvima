
#this function is to make it parallel
function mergeAndCopyUp {
	cd $1
	./mergeResultFolders.sh
	cp -r mergedOutputs ../merged$1
	cd ..
}

function mergeSubFolders {
	for f in */;
	do
		echo "${f%/}"
		cp mergeDeath.R $f/mergeDeath.R -f
		cp mergeFolders.R $f/mergeFolders.R -f
		cp mergeGTI.R $f/mergeGTI.R -f
		cp mergeVol.R $f/mergeVol.R -f
		cp mergeResultFolders.sh $f/mergeResultFolders.sh -f
		# old way (non parallel)
		#cd $f
		#./mergeResultFolders.sh
		#cd ..
		#cp -r $f/mergedOutputs merged$f
		trap 'kill %1' SIGINT	#to stopp all subroutines when killing the original one
		mergeAndCopyUp $f &
	done
}


rm -rf merged*
mergeSubFolders

