#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
using namespace std;


// .glex means the out file is not here
int main(int argc, char** argv){

    if(argc < 2) {
        cerr << "This script expect one argument : the number of files to be compared.";
        return 0;
    }

    vector<string> col = {"useless", "red", "indigo", "dodgerblue", "seagreen", "darkorange", "orchid", "maroon", "gray", "lime", "gray20", "burlywood"};

    if(argc == 3){
        ifstream index (argv[2]);
        char buf[2048];

        {
            int i = 0;
            ofstream f("index.gle"); // OK
            f << "size 15 15\n"
                 "amove 2.5 2\n"
                 "begin graph\n"
                 "    size 12 12\n"
                 "    fullsize\n"
                 "    xtitle \" Index \" hei .7 font TEXCMR\n"
                 "    xaxis font TEXCMR\n"
                 "    yaxis font TEXCMR\n"
                 "end graph\n"
                 "set font texcmss\n"
                 "begin key\n"
                 "      hei .4\n"
                 "      position tr\n";
            while(index.getline(buf, 2047)){
                if(!(buf[1] == '=')){ // failed number
                    i++;
                    f << "      text \"" << buf << "\" lstyle 1 color " << col[i] << " lwidth 0.07\n";
                }
            }
            f << "end key";
            f.close();
        }
    }

    int nb = atoi(argv[1]);
    {
        ofstream f("ab-frac-ha.glex"); // OK
        f << "size 15 15\n"
             "amove 2.5 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" t [h] \" hei .7 font TEXCMR\n"
             "    ytitle \" endogenous high affinity antibody fraction \" hei .7 font TEXCMR\n"
             "    xaxis font TEXCMR\n"
             "    yaxis font TEXCMR\n"
             "    xaxis min 0 max 504\n"
             "!	yaxis log\n"
             "!	yaxis min 0.0000000001\n"
             "    yaxis min 0 max 1\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  antibody" << i << ".out  d" << i << "=c1,c15\n"
             "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.05\n";
             }
        f << "end graph\n";
        f.close();
    }


    vector<int> bins = {0,1,3,6,8,9,10,11};
    for(unsigned int bin = 0; bin < bins.size(); ++bin)
    {
        stringstream fname;
        fname << "aff_bc_bin" << bins[bin] << ".gle";
        ofstream f(fname.str().c_str());
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" time [days] \" hei .7 font texcmss\n"
             "    ytitle \" fraction of BC in affinity bin " << bins[bin] << "\" hei .7 font texcmss\n"
             "        xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 max 1\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data aff_cb_lz" << i << ".out d" << i << "01=c1,c2\n"
             "    data aff_cb_lz" << i << ".out d" << i << "11=c1,c3\n"
             "    data aff_cb_lz" << i << ".out d" << i << "02=c1,c4\n"
             "    data aff_cb_lz" << i << ".out d" << i << "12=c1,c5\n"
             "    data aff_cb_lz" << i << ".out d" << i << "03=c1,c8\n"
             "    data aff_cb_lz" << i << ".out d" << i << "13=c1,c9\n"
             "    data aff_cb_lz" << i << ".out d" << i << "04=c1,c14\n"
             "    data aff_cb_lz" << i << ".out d" << i << "14=c1,c15\n"
             "    data aff_cb_lz" << i << ".out d" << i << "05=c1,c18\n"
             "    data aff_cb_lz" << i << ".out d" << i << "15=c1,c19\n"
             "    data aff_cb_lz" << i << ".out d" << i << "06=c1,c20\n"
             "    data aff_cb_lz" << i << ".out d" << i << "16=c1,c21\n"
             "    data aff_cb_dz" << i << ".out d" << i << "21=c1,c2\n"
             "    data aff_cb_dz" << i << ".out d" << i << "31=c1,c3\n"
             "    data aff_cb_dz" << i << ".out d" << i << "22=c1,c4\n"
             "    data aff_cb_dz" << i << ".out d" << i << "32=c1,c5\n"
             "    data aff_cb_dz" << i << ".out d" << i << "23=c1,c8\n"
             "    data aff_cb_dz" << i << ".out d" << i << "33=c1,c9\n"
             "    data aff_cb_dz" << i << ".out d" << i << "24=c1,c14\n"
             "    data aff_cb_dz" << i << ".out d" << i << "34=c1,c15\n"
             "    data aff_cb_dz" << i << ".out d" << i << "25=c1,c18\n"
             "    data aff_cb_dz" << i << ".out d" << i << "35=c1,c19\n"
             "    data aff_cb_dz" << i << ".out d" << i << "26=c1,c20\n"
             "    data aff_cb_dz" << i << ".out d" << i << "36=c1,c21\n"
             "    data dec205" << i << ".out d" << i << "38=c1,c11\n"
             "    data xsumcb" << i << ".out d" << i << "39=c1,c2\n"
             "!\n"
             "    data aff_cc_lz" << i << ".out d" << i << "61=c1,c2\n"
             "    data aff_cc_lz" << i << ".out d" << i << "71=c1,c3\n"
             "    data aff_cc_lz" << i << ".out d" << i << "62=c1,c4\n"
             "    data aff_cc_lz" << i << ".out d" << i << "72=c1,c5\n"
             "    data aff_cc_lz" << i << ".out d" << i << "63=c1,c8\n"
             "    data aff_cc_lz" << i << ".out d" << i << "73=c1,c9\n"
             "    data aff_cc_lz" << i << ".out d" << i << "64=c1,c14\n"
             "    data aff_cc_lz" << i << ".out d" << i << "74=c1,c15\n"
             "    data aff_cc_lz" << i << ".out d" << i << "65=c1,c18\n"
             "    data aff_cc_lz" << i << ".out d" << i << "75=c1,c19\n"
             "    data aff_cc_lz" << i << ".out d" << i << "66=c1,c20\n"
             "    data aff_cc_lz" << i << ".out d" << i << "76=c1,c21\n"
             "    data aff_cc_dz" << i << ".out d" << i << "81=c1,c2\n"
             "    data aff_cc_dz" << i << ".out d" << i << "91=c1,c3\n"
             "    data aff_cc_dz" << i << ".out d" << i << "82=c1,c4\n"
             "    data aff_cc_dz" << i << ".out d" << i << "92=c1,c5\n"
             "    data aff_cc_dz" << i << ".out d" << i << "83=c1,c8\n"
             "    data aff_cc_dz" << i << ".out d" << i << "93=c1,c9\n"
             "    data aff_cc_dz" << i << ".out d" << i << "84=c1,c14\n"
             "    data aff_cc_dz" << i << ".out d" << i << "94=c1,c15\n"
             "    data aff_cc_dz" << i << ".out d" << i << "85=c1,c18\n"
             "    data aff_cc_dz" << i << ".out d" << i << "95=c1,c19\n"
             "    data aff_cc_dz" << i << ".out d" << i << "86=c1,c20\n"
             "    data aff_cc_dz" << i << ".out d" << i << "96=c1,c21\n"
             "    data dec205" << i << ".out d" << i << "98=c1,c14\n"
             "    data xsumcc" << i << ".out d" << i << "99=c1,c2\n"
             "!\n"
             "    let d" << i << "40 = (d" << i << "01+d" << i << "21+d" << i << "61+d" << i << "81)/(d" << i << "39+d" << i << "99)\n"
             "    let d" << i << "41 = (d" << i << "02+d" << i << "22+d" << i << "62+d" << i << "82)/(d" << i << "39+d" << i << "99)\n"
             "    let d" << i << "43 = (d" << i << "03+d" << i << "23+d" << i << "63+d" << i << "83)/(d" << i << "39+d" << i << "99)\n"
             "    let d" << i << "46 = (d" << i << "04+d" << i << "24+d" << i << "64+d" << i << "84)/(d" << i << "39+d" << i << "99)\n"
             "    let d" << i << "48 = (d" << i << "05+d" << i << "25+d" << i << "65+d" << i << "85)/(d" << i << "39+d" << i << "99)\n"
             "    let d" << i << "49 = (d" << i << "06+d" << i << "26+d" << i << "66+d" << i << "86)/(d" << i << "39+d" << i << "99)\n";
        switch(bins[bin]){
             case 0: f << "    d" << i << "40 lstyle 1 color " << col[i] << " lwidth 0.05\n"; break;      // was cyan
             case 1: f << "    d" << i << "41 lstyle 1 color " << col[i] << " lwidth 0.05\n"; break;     // was brown
             case 3: f << "    d" << i << "43 lstyle 1 color " << col[i] << " lwidth 0.05\n"; break;   // was magenta
             case 6: f << "    d" << i << "46 lstyle 1 color " << col[i] << " lwidth 0.05\n"; break;      // was blue
             case 8: f << "    d" << i << "48 lstyle 1 color " << col[i] << " lwidth 0.05\n"; break;     // was green
             case 9: f << "    d" << i << "49 lstyle 1 color " << col[i] << " lwidth 0.05\n"; break;       // was red
             default: break;
             }
        /*f << "    let d" << i << "50 = (d" << i << "11+d" << i << "31+d" << i << "71+d" << i << "91)/(d" << i << "38+d" << i << "98)\n"
             "    let d" << i << "51 = (d" << i << "12+d" << i << "32+d" << i << "72+d" << i << "92)/(d" << i << "38+d" << i << "98)\n"
             "    let d" << i << "53 = (d" << i << "13+d" << i << "33+d" << i << "73+d" << i << "93)/(d" << i << "38+d" << i << "98)\n"
             "    let d" << i << "56 = (d" << i << "14+d" << i << "34+d" << i << "74+d" << i << "94)/(d" << i << "38+d" << i << "98)\n"
             "    let d" << i << "58 = (d" << i << "15+d" << i << "35+d" << i << "75+d" << i << "95)/(d" << i << "38+d" << i << "98)\n"
             "    let d" << i << "59 = (d" << i << "16+d" << i << "36+d" << i << "76+d" << i << "96)/(d" << i << "38+d" << i << "98)\n";
        switch(bins[bin]){
             case 0: f << "    d" << i << "50 lstyle 2 color " << col[i] << " lwidth 0.05\n"; break;
             case 1: f << "    d" << i << "51 lstyle 2 color " << col[i] << " lwidth 0.05\n"; break;
             case 3: f << "    d" << i << "53 lstyle 2 color " << col[i] << " lwidth 0.05\n"; break;
             case 6: f << "    d" << i << "56 lstyle 2 color " << col[i] << " lwidth 0.05\n"; break;
             case 8: f << "    d" << i << "58 lstyle 2 color " << col[i] << " lwidth 0.05\n"; break;
             case 9: f << "    d" << i << "59 lstyle 2 color " << col[i] << " lwidth 0.05\n"; break;
             default: break;
        }*/
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .4\n"
             "      position tr\n"
             "      text \" bin " << bins[bin] << "\" lstyle 1 color cyan lwidth 0.07\n";
             /* "      text \" bin 1 \" lstyle 1 color brown lwidth 0.07\n"
             "      text \" bin 3 \" lstyle 1 color magenta lwidth 0.07\n"
             "      text \" bin 6 \" lstyle 1 color blue lwidth 0.07\n"
             "      text \" bin 8 \" lstyle 1 color green lwidth 0.07\n"
             "      text \" bin 9 \" lstyle 1 color red lwidth 0.07\n"
             "      text \" all BC \" lstyle 1 color black lwidth 0.07\n"
             "      text \" DEC205+ BC \" lstyle 2 color black lwidth 0.07\n"*/
             f << "end key";
        f.close();
    }




/*

    {
        ofstream f("aff_cb.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " time [days] " hei .7 font texcmss
                 ytitle " fraction of dividing BC in affinity bins " hei .7 font texcmss
                     xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data aff_cb_lz.out d1=c1,c2
                 data aff_cb_lz.out d11=c1,c3
                 data aff_cb_lz.out d2=c1,c4
                 data aff_cb_lz.out d12=c1,c5
                 data aff_cb_lz.out d3=c1,c8
                 data aff_cb_lz.out d13=c1,c9
                 data aff_cb_lz.out d4=c1,c14
                 data aff_cb_lz.out d14=c1,c15
                 data aff_cb_lz.out d5=c1,c18
                 data aff_cb_lz.out d15=c1,c19
                 data aff_cb_lz.out d6=c1,c20
                 data aff_cb_lz.out d16=c1,c21
                 data aff_cb_dz.out d21=c1,c2
                 data aff_cb_dz.out d31=c1,c3
                 data aff_cb_dz.out d22=c1,c4
                 data aff_cb_dz.out d32=c1,c5
                 data aff_cb_dz.out d23=c1,c8
                 data aff_cb_dz.out d33=c1,c9
                 data aff_cb_dz.out d24=c1,c14
                 data aff_cb_dz.out d34=c1,c15
                 data aff_cb_dz.out d25=c1,c18
                 data aff_cb_dz.out d35=c1,c19
                 data aff_cb_dz.out d26=c1,c20
                 data aff_cb_dz.out d36=c1,c21
                 data dec205.out d38=c1,c11
                 data xsumcb.out d39=c1,c2
                 let d40 = (d1+d21)/d39
                 let d41 = (d2+d22)/d39
                 let d43 = (d3+d23)/d39
                 let d46 = (d4+d24)/d39
                 let d48 = (d5+d25)/d39
                 let d49 = (d6+d26)/d39
                 d40 lstyle 1 color cyan lwidth 0.05
                 d41 lstyle 1 color brown lwidth 0.05
                 d43 lstyle 1 color magenta lwidth 0.05
                 d46 lstyle 1 color blue lwidth 0.05
                 d48 lstyle 1 color green lwidth 0.05
                 d49 lstyle 1 color red lwidth 0.05
                 let d50 = (d11+d31)/d38
                 let d51 = (d12+d32)/d38
                 let d53 = (d13+d33)/d38
                 let d56 = (d14+d34)/d38
                 let d58 = (d15+d35)/d38
                 let d59 = (d16+d36)/d38
                 d50 lstyle 2 color cyan lwidth 0.05
                 d51 lstyle 2 color brown lwidth 0.05
                 d53 lstyle 2 color magenta lwidth 0.05
                 d56 lstyle 2 color blue lwidth 0.05
                 d58 lstyle 2 color green lwidth 0.05
                 d59 lstyle 2 color red lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " bin 0 " lstyle 1 color cyan lwidth 0.07
                   text " bin 1 " lstyle 1 color brown lwidth 0.07
                   text " bin 3 " lstyle 1 color magenta lwidth 0.07
                   text " bin 6 " lstyle 1 color blue lwidth 0.07
                   text " bin 8 " lstyle 1 color green lwidth 0.07
                   text " bin 9 " lstyle 1 color red lwidth 0.07
                   text " all dividing BC " lstyle 1 color black lwidth 0.07
                   text " DEC205+ dividing BC " lstyle 2 color black lwidth 0.07
             end key
";
        f.close();
    }

    {
        ofstream f("aff_cc.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " time [days] " hei .7 font texcmss
                 ytitle " fraction of non-dividing BC in affinity bins " hei .7 font texcmss
                     xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data aff_cc_lz.out d1=c1,c2
                 data aff_cc_lz.out d11=c1,c3
                 data aff_cc_lz.out d2=c1,c4
                 data aff_cc_lz.out d12=c1,c5
                 data aff_cc_lz.out d3=c1,c8
                 data aff_cc_lz.out d13=c1,c9
                 data aff_cc_lz.out d4=c1,c14
                 data aff_cc_lz.out d14=c1,c15
                 data aff_cc_lz.out d5=c1,c18
                 data aff_cc_lz.out d15=c1,c19
                 data aff_cc_lz.out d6=c1,c20
                 data aff_cc_lz.out d16=c1,c21
                 data aff_cc_dz.out d21=c1,c2
                 data aff_cc_dz.out d31=c1,c3
                 data aff_cc_dz.out d22=c1,c4
                 data aff_cc_dz.out d32=c1,c5
                 data aff_cc_dz.out d23=c1,c8
                 data aff_cc_dz.out d33=c1,c9
                 data aff_cc_dz.out d24=c1,c14
                 data aff_cc_dz.out d34=c1,c15
                 data aff_cc_dz.out d25=c1,c18
                 data aff_cc_dz.out d35=c1,c19
                 data aff_cc_dz.out d26=c1,c20
                 data aff_cc_dz.out d36=c1,c21
                 data dec205.out d38=c1,c14
                 data xsumcc.out d39=c1,c2
                 let d40 = (d1+d21)/d39
                 let d41 = (d2+d22)/d39
                 let d43 = (d3+d23)/d39
                 let d46 = (d4+d24)/d39
                 let d48 = (d5+d25)/d39
                 let d49 = (d6+d26)/d39
                 d40 lstyle 1 color cyan lwidth 0.05
                 d41 lstyle 1 color brown lwidth 0.05
                 d43 lstyle 1 color magenta lwidth 0.05
                 d46 lstyle 1 color blue lwidth 0.05
                 d48 lstyle 1 color green lwidth 0.05
                 d49 lstyle 1 color red lwidth 0.05
                 let d50 = (d11+d31)/d38
                 let d51 = (d12+d32)/d38
                 let d53 = (d13+d33)/d38
                 let d56 = (d14+d34)/d38
                 let d58 = (d15+d35)/d38
                 let d59 = (d16+d36)/d38
                 d50 lstyle 2 color cyan lwidth 0.05
                 d51 lstyle 2 color brown lwidth 0.05
                 d53 lstyle 2 color magenta lwidth 0.05
                 d56 lstyle 2 color blue lwidth 0.05
                 d58 lstyle 2 color green lwidth 0.05
                 d59 lstyle 2 color red lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " bin 0 " lstyle 1 color cyan lwidth 0.07
                   text " bin 1 " lstyle 1 color brown lwidth 0.07
                   text " bin 3 " lstyle 1 color magenta lwidth 0.07
                   text " bin 6 " lstyle 1 color blue lwidth 0.07
                   text " bin 8 " lstyle 1 color green lwidth 0.07
                   text " bin 9 " lstyle 1 color red lwidth 0.07
                   text " all non-dividing BC " lstyle 1 color black lwidth 0.07
                   text " DEC205+ non-dividing BC " lstyle 2 color black lwidth 0.07
             end key
";
        f.close();
    }

    {
        ofstream f("aff_out.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Tme [days] " hei .7 font texcmss
                 ytitle " Faction of in-GC-output in affinity bins " hei .7 font texcmss
                     xaxis hei .5 font texcmss dticks 72
                 yaxis hei .5 font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 1.0
                 data aff_out_lz.out d1=c1,c2
                 data aff_out_lz.out d2=c1,c3
                 data aff_out_lz.out d3=c1,c5
                 data aff_out_lz.out d4=c1,c8
                 data aff_out_lz.out d5=c1,c10
                 data aff_out_lz.out d6=c1,c11
                 data aff_out_dz.out d21=c1,c2
                 data aff_out_dz.out d22=c1,c3
                 data aff_out_dz.out d23=c1,c5
                 data aff_out_dz.out d24=c1,c8
                 data aff_out_dz.out d25=c1,c10
                 data aff_out_dz.out d26=c1,c11
                 let d37 = (d21+d22+d23+d24+d25+d26)
                 let d38 = (d1+d2+d3+d4+d5+d6)
                 let d39 = d37+d38
                 let d40 = ((d1+d21)/d39)
                 let d41 = ((d2+d22)/d39)
                 let d43 = ((d3+d23)/d39)
                 let d46 = ((d4+d24)/d39)
                 let d48 = ((d5+d25)/d39)
                 let d49 = ((d6+d26)/d39)
                 d40 lstyle 1 color cyan lwidth 0.05
                 d41 lstyle 1 color brown lwidth 0.05
                 d43 lstyle 1 color magenta lwidth 0.05
                 d46 lstyle 1 color blue lwidth 0.05
                 d48 lstyle 1 color green lwidth 0.05
                 d49 lstyle 1 color red lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .5
                   position tl
                   text " bin 0 " lstyle 1 color cyan lwidth 0.07
                   text " bin 1 " lstyle 1 color brown lwidth 0.07
                   text " bin 3 " lstyle 1 color magenta lwidth 0.07
                   text " bin 6 " lstyle 1 color blue lwidth 0.07
                   text " bin 8 " lstyle 1 color green lwidth 0.07
                   text " bin 9 " lstyle 1 color red lwidth 0.07
             end key
";
        f.close();
    }
*/


    {
        ofstream f("ag_brdu.gle");
        f << "size 15 15\n"
             "amove 2.0 2.0\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "!	xtitle \" BrdU \" hei .7 font texcmss\n"
             "    xtitle \" CTV \" hei .7 font texcmss\n"
             "    ytitle \" Loaded antigen \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss\n"
             "    yaxis hei .5 font texcmss\n"
             "    ynames 0 1.7 3.7 7.6 16 32 64 128 256\n"
             "    xaxis log min 0.1 max 200\n"
             "    yaxis min -0.1 max 8\n"
             "!	yaxis log min 0.1 max 200\n";
         for(int i = 1; i < nb+1; ++i){
         f << "    data  ag_brdu_ini" << i << ".out  d1" << i << "=c1,c3\n"
              "    data  ag_brdu" << i << ".out  d2" << i << "=c1,c3\n"
              "    d" << i << "1 marker fcircle msize 0.1 color dark" << col[i] << "\n"
			  "    d" << i << "2 marker fcircle msize 0.1 color " << col[i] << "\n";
             }
         f <<"    data  ag_brdu_ini.out  d1=c1,c3\n"
             "    data  ag_brdu.out  d2=c1,c3\n"
             "end graph";
        f.close();
    }



/*
    {
        ofstream f("ag_brdu_final.gle");
        f << "size 15 15\n"
             "amove 2.0 2.0\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" BrdU \" hei .7 font texcmss\n"
             "    x2title \"Final configuration \" hei .7 font texcmss\n"
             "    ytitle \" Loaded antigen \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss\n"
             "    yaxis hei .5 font texcmss\n"
             "    ynames 0 1.7 3.7 7.6 16 32 64 128 256\n"
             "    xaxis log min 0.1 max 200\n"
             "    yaxis min 0 max 8\n"
             "!	yaxis log min 0.01 max 200\n"
             for(int i = 1; i < nb+1; ++i){
        f << "    data  ag_brdu" << i << ".out  d" << i << "=c1,c3\n"
             "    d"<< i << " marker fcircle color " << col[i] << " msize 0.05";
             }
        f << "end graph";
        f.close();
    }

    {
        ofstream f("ag_brdu_ini.gle");
        f << "size 15 15
             "amove 2.0 2.0
             "begin graph
             "    size 12 12
             "    fullsize
             "    xtitle \" BrdU \" hei .7 font texcmss
             "    x2title \" Initial configuration \" hei .7 font texcmss
             "    ytitle \" Loaded antigen \" hei .7 font texcmss
             "    xaxis hei .5 font texcmss
             "    yaxis hei .5 font texcmss
             "    ynames 0 1.7 3.7 7.6 16 32 64 128 256
             "    xaxis log min 0.1 max 200
             "    yaxis min 0 max 8
             "!	yaxis log min 0.01 max 200
             for(int i = 1; i < nb+1; ++i){
        f << "    data  ag_brdu_ini" << i << ".out  d" << i << "=c1,c3\n"
             "    d"<< i << " marker fcircle color " << col[i] << " msize 0.05";
             }
        f << "end graph";
        f.close();
    }
*/




    {
        ofstream f("ag_collected_untilContact.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Number of successful FDC contacts per BC \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min -1 max 8\n";
             for(int i = 1; i < nb+1; ++i){
        f << "    data ag_collected" << i << ".out  d" << i << "1=c1,c3\n"
             "    data ag_collected" << i << ".out  d" << i << "2=c1,c4\n"
             "    data ag_collected" << i << ".out  d" << i << "3=c1,c6\n"
             "    data ag_collected" << i << ".out  d" << i << "4=c1,c7\n"
             "    data ag_collected" << i << ".out  d" << i << "5=c1,c9\n"
             "    data ag_collected" << i << ".out  d" << i << "6=c1,c10\n"
             //"    d" << i << "1 err d" << i << "2 color grey10 errwidth 0.1\n"
             "    let d" << i << "6 = d" << i << "1\n"
             "    d" << i << "6 lstyle 1 lwidth 0.06 color " << col[i] << "\n";
             }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tl\n"
             "      text \" until TC contact \" lstyle 1 color black lwidth 0.07\n"
             "end key\n";
        f.close();
    }


    {
        ofstream f("ag_collected_include_selected.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Number of successful FDC contacts per BC \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min -1 max 8\n";
             for(int i = 1; i < nb+1; ++i){
        f << "    data ag_collected" << i << ".out  d" << i << "1=c1,c3\n"
             "    data ag_collected" << i << ".out  d" << i << "2=c1,c4\n"
             "    data ag_collected" << i << ".out  d" << i << "3=c1,c6\n"
             "    data ag_collected" << i << ".out  d" << i << "4=c1,c7\n"
             "    data ag_collected" << i << ".out  d" << i << "5=c1,c9\n"
             "    data ag_collected" << i << ".out  d" << i << "6=c1,c10\n"
             //"    d" << i << "3 errup d" << i << "4 color lightgreen errwidth 0.1\n"
             "    let d" << i << "7 = d" << i << "3\n"
             "    d" << i << "7 lstyle 1 lwidth 0.06 color " << col[i] << "\n";
               }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tl\n"
             "      text \" include selected \" lstyle 1 color green lwidth 0.07\n"
             "end key";
        f.close();
    }


    {
        ofstream f("ag_collected_include_apo.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Number of successful FDC contacts per BC \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min -1 max 8\n";
             for(int i = 1; i < nb+1; ++i){
        f << "    data ag_collected" << i << ".out  d" << i << "1=c1,c3\n"
             "    data ag_collected" << i << ".out  d" << i << "2=c1,c4\n"
             "    data ag_collected" << i << ".out  d" << i << "3=c1,c6\n"
             "    data ag_collected" << i << ".out  d" << i << "4=c1,c7\n"
             "    data ag_collected" << i << ".out  d" << i << "5=c1,c9\n"
             "    data ag_collected" << i << ".out  d" << i << "6=c1,c10\n"
             //"    d" << i << "5 errdown d" << i << "6 color lightblue errwidth 0.1\n"
             "    let d" << i << "8 = d" << i << "5\n"
             "    d" << i << "8 lstyle 1 lwidth 0.06 color " << col[i] << "\n";
               }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tl\n"
             "      text \" include apoptotic \" lstyle 1 color blue lwidth 0.07\n"
             "end key";
        f.close();
    }

    {
        ofstream f("ag_endhisto.gle");
        f << "size 15 15\n"
             "amove 2.0 2.0\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Number of Ag-collection events per BC \" hei .7 font texcmss\n"
             "    ytitle \" BC counts \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss\n"
             "    yaxis hei .5 font texcmss\n"
             "    xaxis min -1 max 15\n"
             "    yaxis min 0 max 40000\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  ag_endhisto" << i << ".out  d" << i << "1=c1,c2  ! selected BC\n"
             "    data  ag_endhisto" << i << ".out  d" << i << "2=c1,c3  ! all BC\n"
             "    d" << i << "2 lstyle 1 lwidth 0.05 color " << col[i] << "\n"
             "    d" << i << "1 lstyle 2 lwidth 0.05 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tr\n"
             "      text \" all BCs \" color black lstyle 1 lwidth 0.3\n"
             "      text \" selected BCs \" color grey30 lstyle 2 lwidth 0.3\n"
             "end key";
        f.close();
    }


    {
        ofstream f("ag_loaded.gle");
        f << "size 15 15\n"
             "amove 2.0 2.0\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Loaded antigen \" hei .7 font texcmss\n"
             "    ytitle \" Number of cells \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss\n"
             "    yaxis hei .5 font texcmss\n"
             "    xaxis log min 0.05 max 200\n"
             "!	  xaxis min 0.01 max 200\n"
             "    yaxis min 0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  ag_loaded" << i << ".out  d" << i << "=c1,c2\n"
             "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.05\n"
             "!   d"<< i << " marker ftriangle color " << col[i] << " msize 0.5\n";
        }
        f << "end graph\n";
        f.close();
    }
/*
    {
        ofstream f("antibodies.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " concentrations [M] " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis log
                 yaxis min 0.0000000001 max 0.000001
                 data  antibody.out  d1=c1,c2
                 data  antibody.out  d2=c1,c3
                 data  antibody.out  d3=c1,c4
                 data  antibody.out  d4=c1,c5
                 data  antibody.out  d5=c1,c6
                 data  antibody.out  d6=c1,c7
                 data  antibody.out  d7=c1,c8
                 data  antibody.out  d8=c1,c9
                 data  antibody.out  d9=c1,c10
                 data  antibody.out  d10=c1,c11
                 data  antibody.out  d11=c1,c12
                 data  antibody.out  d12=c1,c13
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 5 color red lwidth 0.05
                 d3 lstyle 5 color blue lwidth 0.05
                 d4 lstyle 5 color green lwidth 0.05
                 d5 lstyle 5 color orange lwidth 0.05
                 d6 lstyle 5 color cyan lwidth 0.05
                 d7 lstyle 5 color yellow lwidth 0.05
                 d8 lstyle 2 color red lwidth 0.05
                 d9 lstyle 2 color blue lwidth 0.05
                 d10 lstyle 2 color green lwidth 0.05
                 d11 lstyle 2 color orange lwidth 0.05
                 d12 lstyle 2 color cyan lwidth 0.05
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tl
                   text " total " lstyle 1 color black lwidth 0.06
                   text " bin 0 " lstyle 5 color red lwidth 0.06
                   text " bin 1 " lstyle 5 color blue lwidth 0.06
                   text " bin 2 " lstyle 5 color green lwidth 0.06
                   text " bin 3 " lstyle 5 color orange lwidth 0.06
                   text " bin 4 " lstyle 5 color cyan lwidth 0.06
                   text " bin 5 " lstyle 5 color yellow lwidth 0.06
                   text " bin 6 " lstyle 2 color red lwidth 0.06
                   text " bin 7 " lstyle 2 color blue lwidth 0.06
                   text " bin 8 " lstyle 2 color green lwidth 0.06
                   text " bin 9 " lstyle 2 color orange lwidth 0.06
                   text " bin 10 " lstyle 2 color cyan lwidth 0.06
             end key
";
        f.close();
    }

    {
        ofstream f("antibody.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " Antibody-Concentrations [arbitrary units] " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
             !	xaxis min 0 max 504
                 yaxis min 0
                 data  siglog.out  d1=c1,c18
                 data  siglog.out  d2=c1,c21
             ! 	data  antigen.out  d3=c1,c4
             !	let d4 = d1+d2
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 2 color red lwidth 0.05
             !  	d3 lstyle 1 color black lwidth 0.05
             !	d4 lstyle 1 color green lwidth 0.05
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " total concentration in GC " lstyle 1 lwidth 0.06
                   text " production " lstyle 2 color red lwidth 0.06
             !      text " total antigen on FDC " lstyle 1 color green lwidth 0.06
             !      text " free antibody in GC " lstyle 1 color black lwidth 0.06
             end key
";
        f.close();
    }
*/




    /*{
        ofstream f("antigen.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Concentrations [antigen-portions] " hei .7 font texcmss
                 xaxis hei .5 font texcmss dticks 72 hei .6
                 yaxis hei .5 font texcmss hei .6
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis log
                 yaxis min 1e-02 max 1e+07
             !	max 22000
                 data  antigen.out  d1=c1,c2
                 data  antigen.out  d2=c1,c3
                 data  antigen.out  d3=c1,c4
                 let d4 = d1+d2
                 d1 lstyle 1 color blue lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color black lwidth 0.05
                 d4 lstyle 1 color green lwidth 0.05
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " free antigen on FDC " lstyle 1 color blue lwidth 0.06
                   text " immune complex on FDC " lstyle 1 color red lwidth 0.06
                   text " total antigen on FDC " lstyle 1 color green lwidth 0.06
                   text " free antibody in GC " lstyle 1 color black lwidth 0.06
             end key
";
        f.close();
    }*/

    {
        ofstream f("axis.glex");
        f << "size 15 8\n"
             "amove 2.0 1.5\n"
             "set font TEXCMR\n"
             "begin graph\n"
             "    size 12 6\n"
             "    fullsize\n"
             "    xtitle \" t [min] \" hei .7 font TEXCMR\n"
             "    ytitle \" shape [long2short axis] \" hei .7 font TEXCMR\n"
             "    xaxis hei .5 font TEXCMR\n"
             "    yaxis hei .5 font TEXCMR\n"
             "    xaxis min 0\n"
             "    yaxis min 1\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                 "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04\n"
                 "    d"<< i << " marker fcircle color " << col[i] << "\n";
                 }
        f << "end graph";
        f.close();
    }


/*

    {
        ofstream f("axis_histo_l2r.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " elongation " hei .7 font TEXCMR
                 x2title " long axis to radius histogram " hei .7 font TEXCMR
                 ytitle " counts/tracked cell " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min -2 max 40
                 yaxis min 0
                 data  axishisto.out  d1=c1,c9
                 data  axishisto.out  d2=c1,c11
                 data  axismean_all.out d3=c3,c5
                 bar d1 width 1.8 fill grey40
                     d1 err d2 errwidth .2 lwidth .06
                 d3 marker star msize 0.7
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }


    {
        ofstream f("axis_histo_l2r_sum.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " elongation " hei .7 font TEXCMR
                 x2title " long axis to radius histogram " hei .7 font TEXCMR
                 ytitle " counts " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min -2 max 40
                 yaxis min 0
                 data  axishisto.out  d1=c1,c3
             !	data  axishisto.out  d2=c1,c11
                 data  axismean_all.out d3=c3,c5
                 bar d1 width 1.8 fill grey40
             !        d1 err d2 errwidth .2 lwidth .06
                 d3 marker star msize 0.7
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }

    {
        ofstream f("axis_histo_l2s.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " elongation " hei .7 font TEXCMR
                 x2title " long to short axis histogram " hei .7 font TEXCMR
                 ytitle " counts/tracked cell " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0.5 max 4.0
                 yaxis min 0
                 data  axishisto.out  d1=c1,c8
                 data  axishisto.out  d2=c1,c10
                 data  axismean_all.out d3=c1,c5
             !  	bar d1 width 1.8 fill grey40
                 bar d1 fill grey40
                     d1 err d2 errwidth .1 lwidth .05
                 d3 marker star msize 0.7
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("axis_l2r.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             set font TEXCMR
             begin graph
                 size 12 6
                 fullsize
                 xtitle " t [min] " hei .7 font TEXCMR
                 x2title " long axis to radius " hei .7 font TEXCMR
                 ytitle " elongation [microns/min] " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0
                 yaxis min 0
             ! Insert any odd y-column here:
                 data  axis_t.out  d1=c1,c3
                 d1 marker fcircle
                 d1 lstyle 1 lwidth 0.04
             end graph
";
        f.close();
    }
    {
        ofstream f("axis_l2s.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             set font TEXCMR
             begin graph
                 size 12 6
                 fullsize
                 xtitle " t [min] " hei .7 font TEXCMR
                 x2title " long to short axis " hei .7 font TEXCMR
                 ytitle " elongation " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0
                 yaxis min 0
             ! Insert any even y-column here:
                 data  axis_t.out  d1=c1,c2
                 d1 marker fcircle
                 d1 lstyle 1 lwidth 0.04
             end graph
";
        f.close();
    }
    {
        ofstream f("axis_mean_t.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             set font TEXCMR
             begin graph
                 size 12 6
                 fullsize
                 xtitle " t [min] " hei .7 font TEXCMR
                 x2title " mean elongation " hei .7 font TEXCMR
                 ytitle " elongation " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0
                 yaxis min 0
                 data  axismean_t.out  d1=c1,c2
                 data  axismean_t.out  d2=c1,c3
                 data  axismean_t.out  d3=c1,c4
                 data  axismean_t.out  d4=c1,c5
                 let d5 = d1+d2
                 let d6 = d3-d4
                 d1 marker fcircle color red
                 d1 lstyle 1 lwidth 0.04 color red
                 d3 marker ftriangle color blue
                 d3 lstyle 1 lwidth 0.04 color blue
             !        d1 errup d2 errwidth .2 lwidth .06 color red
             !        d3 errdown d4 errwidth .2 lwidth .06 color blue
                 fill d1,d5 color grey10
                 fill d3,d6 color grey10
             end graph
             begin key
                   hei .4
                   position tr
                   text " long to short axis " marker fcircle color red
                   text " long axis to radius " marker ftriangle color blue
             end key
";
        f.close();
    }
    */

    {
        ofstream f("brdu.gle");
        f << "size 15 15\n"
             "amove 2.0 2.0\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" CTV \" hei .7 font texcmss\n"
             "!	xtitle \" BrdU \" hei .7 font texcmss\n"
             "    ytitle \" Number of cells \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss\n"
             "    yaxis hei .5 font texcmss\n"
             "    xaxis log min 0.1 max 200\n"
             "    yaxis min 0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  brdu" << i << ".out  d" << i << "=c1,c2\n"
             "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04\n";
        }
        f << "end graph\n";
        f.close();
    }

    /* later
    {
        ofstream f("brdu_cell.gle");
        f << "size 15 15
             amove 2.0 2.0
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Generation  " hei .7 font texcmss
                 ytitle " Number of cells " hei .7 font texcmss
                 xaxis hei .5 font texcmss
                 yaxis hei .5 font texcmss
                 xaxis min 0 max 6
                 yaxis min 0
                 data  brdu_cell.out  d1=c1,c3
                 data  brdu_cell.out  d2=c1,c4
                 data  brdu_cell.out  d3=c1,c5
                 data  brdu_cell.out  d4=c1,c6
                 let d5=d1-d3
                     d5 lstyle 1 lwidth .05 color black
                 d2 lstyle 1 lwidth .05 color blue
             !	d3 lstyle 1 lwidth .05 color green
                 d4 lstyle 1 lwidth .05 color red
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tl
                   text " all BC " lstyle 1 lwidth 0.07 color black
                   text " low antigen " lstyle 1 lwidth 0.07 color blue
             !      text " mid antigen " lstyle 1 lwidth 0.07 color green
                   text " high antigen " lstyle 1 lwidth 0.07 color red
             end key
";
        f.close();
    }

    {
        ofstream f("cb2ccnoapo.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Ratio " hei .7 font texcmss
                 xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 6
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 data xsumcc.out  d7=c1,c6  ! apoptotic CC
                 let d8 = (d4)/(d5-d7)  ! all BC div/(nondiv-apoptotic)
                 d8 lstyle 1 color black lwidth 0.05
             !
             !	let d3 = d1/(d2-d7)  ! all BC DZ/LZ
             !  	d3 lstyle 1 color black lwidth 0.05
             !
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
             !	let d23 = (d11+d21)/(d12+d22) ! DZ/LZ (all BC)
             !	d23 lstyle 1 color green lwidth 0.08
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 data dec205.out  d28=c1,c20 ! dec+ CC-apoptotic
                 let d29 = (d25)/(d26-d28) ! dec205+ div/non-div
                 d29 lstyle 5 color red lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 data dec205.out  d34=c1,c21 ! dec- CC-apoptotic
                 let d35 = (d31)/(d32-d34) ! dec205- div/non-div
                 d35 lstyle 2 color green lwidth 0.05
             !
             !	let d13 = d11/(d12-d28) ! dec205+ DZ/LZ
             !	d13 lstyle 1 color red lwidth 0.05
             !	let d24 = d21/(d22-d34) ! dec205- DZ/LZ
             !	d24 lstyle 2 color red lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
             !      text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
                   text " all BC div/non-div " lstyle 1 color black lwidth 0.07
             !      text " dec205+ DZ/LZ " lstyle 5 color red lwidth 0.07
                   text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
             !      text " dec205- DZ/LZ " lstyle 2 color green lwidth 0.07
                   text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("cbha.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " fraction of high affinity cells " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data  cbha.out  d1=c1,c2
                 data  cbha.out  d2=c1,c3
                 data  cbha.out  d3=c1,c4
                 d1 lstyle 1 lwidth 0.04
                 d2 lstyle 2 lwidth 0.04
                 d3 lstyle 3 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tl
                   text " all centroblasts " lstyle 1 lwidth 0.04
                   text " not-recycled centroblasts " lstyle 2 lwidth 0.04
                   text " recycled centroblasts " lstyle 3 lwidth 0.04
             end key
";
        f.close();
    }
    {
        ofstream f("cbha_c.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " time [days] " hei .7 font TEXCMR
                 ytitle " fraction of high affinity CBs " hei .7 font TEXCMR
                 xaxis font TEXCMR dticks 72
                 yaxis font TEXCMR
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data  cbha.out  d1=c1,c2
                 data  cbha.out  d2=c1,c3
                 data  cbha.out  d3=c1,c4
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color blue lwidth 0.05
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tl
                   text " all CBs " lstyle 1 color black lwidth 0.06
                   text " not-recycled CBs " lstyle 1 color red lwidth 0.06
                   text " recycled CBs " lstyle 1 color blue lwidth 0.06
             end key
";
        f.close();
    }
    {
        ofstream f("cbsel2cc.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Ratio " hei .7 font texcmss
                 xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 6
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 data xsumcc.out  d7=c1,c6  ! apoptotic CC
                 let d8 = (d4+d6)/(d5-d6)  ! all BC (div+selected)/(nondiv-selected)
                 d8 lstyle 1 color black lwidth 0.05
             !
             !	let d3 = d1/(d2-d7)  ! all BC DZ/LZ
             !  	d3 lstyle 1 color black lwidth 0.05
             !
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
             !	let d23 = (d11+d21)/(d12+d22) ! DZ/LZ (all BC)
             !	d23 lstyle 1 color green lwidth 0.08
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 data dec205.out  d28=c1,c20 ! dec+ CC-apoptotic
                 let d29 = (d25+d27)/(d26-d27) ! dec205+ (div+sel)/(non-div-sel)
                 d29 lstyle 5 color red lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 data dec205.out  d34=c1,c21 ! dec- CC-apoptotic
                 let d35 = (d31+d33)/(d32-d33) ! dec205- (div+sel)/(non-div-sel)
                 d35 lstyle 2 color green lwidth 0.05
             !
             !	let d13 = d11/(d12-d28) ! dec205+ DZ/LZ
             !	d13 lstyle 1 color red lwidth 0.05
             !	let d24 = d21/(d22-d34) ! dec205- DZ/LZ
             !	d24 lstyle 2 color red lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
             !      text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
                   text " all BC div/non-div " lstyle 1 color black lwidth 0.07
             !      text " dec205+ DZ/LZ " lstyle 5 color red lwidth 0.07
                   text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
             !      text " dec205- DZ/LZ " lstyle 2 color green lwidth 0.07
                   text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }
    */

    /* not a priority
    {
        ofstream f("cellcycle_durations.gle");
        f << "size 15 15
             amove 2.0 2.0
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Cell cycle duration [hours] " hei .7 font texcmss
                 ytitle " Frequency " hei .7 font texcmss
                 xaxis hei .5 font texcmss
                 yaxis hei .5 font texcmss
                 xaxis min -1 max 10
                 yaxis min 0
                 data  cellcycle_duration_g1.out d1=c1,c2
                 data  cellcycle_duration_s.out  d2=c1,c2
                 data  cellcycle_duration_g2.out d3=c1,c2
                 data  cellcycle_duration_m.out  d4=c1,c2
                 bar d1 width 0.2 fill magenta
                 bar d3 width 0.2 fill green
                 bar d2 width 0.07 fill cyan
                 bar d4 width 0.02 fill red
             end graph
             set font texcmss
             begin key
                   hei .5
                   position tr
                   text " G1 " color magenta lstyle 1 lwidth 0.3
                   text " S " color cyan lstyle 1 lwidth 0.3
                   text " G2 " color green lstyle 1 lwidth 0.3
                   text " M " color red lstyle 1 lwidth 0.3
             end key
";
        f.close();
    } */

    /*
    {
        ofstream f("cellsum.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " number of cells " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 288
                 yaxis min 0
                 data  cellsum.out  d1=c1,c2
                 data  cellsum.out  d2=c1,c3
                 data  cellsum.out  d3=c1,c4
                 let d4 = d2+d3
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color cyan lwidth 0.05
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tl
                   text " all cells " color black lstyle 1 lwidth 0.06
                   text " cell1 " color red lstyle 1 lwidth 0.06
                   text " cell2 " color cyan lstyle 1 lwidth 0.06
             end key
";
        f.close();
    }
    {
        ofstream f("chemotaxis.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " CXCL13-Concentrations [arbitrary units] " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0
                 data  siglog.out  d1=c1,c13
                 data  siglog.out  d2=c1,c16
             ! 	data  antigen.out  d3=c1,c4
             !	let d4 = d1+d2
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 2 color red lwidth 0.05
             !  	d3 lstyle 1 color black lwidth 0.05
             !	d4 lstyle 1 color green lwidth 0.05
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " total concentration in GC " lstyle 1 lwidth 0.06
                   text " production " lstyle 2 color red lwidth 0.06
             !      text " total antigen on FDC " lstyle 1 color green lwidth 0.06
             !      text " free antibody in GC " lstyle 1 color black lwidth 0.06
             end key
";
        f.close();
    }
    {
        ofstream f("corr_v_ax.gle");
        f << "size 15 8
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " time [min] " hei .7 font TEXCMR
                 ytitle " correlation (v, shape index) " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0 max 60
                 data  corr_v_ax.out  d1=c1,c5
                 data  corr_mean.out  d2=c1,c3
                 data  null.txt  d3=c1,c2
                 data  polarities.out  d4=c2,c3
                 d1 marker fcircle
                 d1 lstyle 1 lwidth 0.04
                 d2 marker star msize 0.7
                 d4 marker star msize 0.7
                 d3 lstyle 1 lwidth 0.04
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("corrs.gle");
        f << "size 17 17\n"
             "amove 1.0 1.5\n"
             "begin graph\n"
             "    size 14 14\n"
             "    fullsize\n"
             "    xtitle \" t [h] \" hei .7 font TEXCMR\n"
             "    ytitle \" diffusion correction factor \" hei .7 font TEXCMR\n"
             "    xaxis font TEXCMR\n"
             "    yaxis font TEXCMR\n"
             "    xaxis min 0\n"
             "    yaxis min 0\n"
            for(int i = 1; i < nb+1; ++i){
            f << "    data  corrs" << i << ".out  d" << i << "=c1,c2\n"
                 "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                 }
            f << "end graph";
            f.close();
    }
    */

    /* not now
    {
        ofstream f("dec205.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " time [days] " hei .7 font texcmss
                 ytitle " fraction of DEC205+/+ BC " hei .7 font texcmss
                 xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data dec205.out  d1=c1,c2
                 data dec205.out  d2=c1,c3
                 let d3 = d1/(d1+d2)
                 d3 lstyle 1 color black lwidth 0.05
                 data dec205.out  d4=c1,c5
                 data dec205.out  d5=c1,c6
                 let d6 = d4/(d4+d5)
                 d6 lstyle 1 color blue lwidth 0.05
                 data dec205.out  d7=c1,c8
                 data dec205.out  d8=c1,c9
                 let d9 = d7/(d7+d8)
                 d9 lstyle 1 color green lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all BC " lstyle 1 color black lwidth 0.07
                   text " DZ-BC " lstyle 1 color blue lwidth 0.07
                   text " LZ-BC " lstyle 1 color green lwidth 0.07
             end key
";
        f.close();
    }

    {
        ofstream f("diff2cc.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " CB differentiation signal [arbitrary units] " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0
                 data  siglog.out  d1=c1,c3
                 data  siglog.out  d2=c1,c6
                 data  siglog.out  d3=c1,c5
             !	let d4 = d1+d2
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 2 color red lwidth 0.05
                 d3 lstyle 3 color blue lwidth 0.05
             !	d4 lstyle 1 color green lwidth 0.05
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " total concentration in GC " lstyle 1 lwidth 0.06
                   text " production " lstyle 2 color red lwidth 0.06
                   text " used molecules " lstyle 3 color blue lwidth 0.06
             !      text " free antibody in GC " lstyle 1 color black lwidth 0.06
             end key
";
        f.close();
    }
    */


    /* later
    {
        ofstream f("diversity.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " number of antibody types " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0
                 data  diversity.out  d1=c1,c2
                 data  diversity.out  d2=c1,c3
                 data  diversity.out  d3=c1,c4
                 data  diversity.out  d4=c1,c5
                 data  diversity.out  d5=c1,c6
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color blue lwidth 0.05
                 d4 lstyle 1 color green lwidth 0.05
                 d5 lstyle 1 color cyan lwidth 0.05
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tl
                   text " total " lstyle 1 color black lwidth 0.06
                   text " CB " lstyle 1 color red lwidth 0.06
                   text " CC " lstyle 1 color blue lwidth 0.06
                   text " output " lstyle 1 color green lwidth 0.06
                   text " external " lstyle 1 color cyan lwidth 0.06
             end key
";
        f.close();
    }
    {
        ofstream f("diversityn.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " number of antibody types / cell number " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data  diversity.out  d1=c1,c2
                 data  diversity.out  d2=c1,c3
                 data  diversity.out  d3=c1,c4
                 data  diversity.out  d4=c1,c5
             ! 	data  diversity.out  d5=c1,c6
                 data  xsumcb.out  d6=c1,c2
                 data  xsumcc.out  d7=c1,c2
                 data  ssumout.out  d8=c1,c2
                 let d9 = d6+d7+d8
                 let d11 = d1/(d9+1)
                 let d12 = d2/(d6+1)
                 let d13 = d3/(d7+1)
                 let d14 = d4/(d8+1)
                 d11 lstyle 1 color black lwidth 0.05
                 d12 lstyle 1 color red lwidth 0.05
                 d13 lstyle 1 color blue lwidth 0.05
                 d14 lstyle 1 color green lwidth 0.05
             !  	d5 lstyle 1 color cyan lwidth 0.05
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " total " lstyle 1 color black lwidth 0.06
                   text " CB " lstyle 1 color red lwidth 0.06
                   text " CC " lstyle 1 color blue lwidth 0.06
                   text " output " lstyle 1 color green lwidth 0.06
             !      text " external " lstyle 1 color cyan lwidth 0.06
             end key
";
        f.close();
    }
    */
    {
        ofstream f("dz2lz.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" time [days] \" hei .7 font texcmss\n"
             "    ytitle \" ratio \" hei .7 font texcmss\n"
             "    xaxis font texcmss dticks 72\n"
             "    yaxis font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 max 6\n";
         for(int i = 1; i < nb+1; ++i){
         f <<"    data  zone_da" << i << ".out  d" << i << "1=c1,c7   ! all in DZ\n"
             "    data  zone_li" << i << ".out  d" << i << "2=c1,c7   ! all in LZ\n"
             "    let d" << i << "3 = d" << i << "1/d" << i << "2  ! all BC DZ/LZ\n"
             "    d" << i << "3 lstyle 1 color " << col[i] << " lwidth 0.05\n";
        /*     "    data xsumcb.out  d4=c1,c2  ! all CB\n"
             "    data xsumcc.out  d5=c1,c2  ! all CC\n"
             "    let d6=d4/d5  ! all BC div/non-div\n"
             "    d6 lstyle 1 color blue lwidth 0.05\n"
             "    data dec205.out  d11=c1,c5 ! dec+ in dz\n"
             "    data dec205.out  d12=c1,c8 ! dec+ in lz\n"
             "    data dec205.out  d21=c1,c6 ! dec- in dz\n"
             "    data dec205.out  d22=c1,c9 ! dec- in lz\n"
             "    let d13 = d11/d12 ! dec205+ DZ/LZ\n"
             "    d13 lstyle 1 color red lwidth 0.05\n"
             "    let d24 = d21/d22 ! dec205- DZ/LZ\n"
             "    d24 lstyle 2 color red lwidth 0.05\n"
             "    let d23 = (d11+d21)/(d12+d22) ! DZ/LZ (all BC)\n"
             "!	d23 lstyle 1 color green lwidth 0.08\n"
             "    data dec205.out  d25=c1,c11 ! dec+ CB\n"
             "    data dec205.out  d26=c1,c14 ! dec+ CC\n"
             "    let d27 = d25/d26 ! dec205+ div/non-div\n"
             "    d27 lstyle 1 color green lwidth 0.05\n"
             "    data dec205.out  d31=c1,c12 ! dec- CB\n"
             "    data dec205.out  d32=c1,c15 ! dec- CC\n"
             "    let d33 = d31/d32 ! dec205- div/non-div\n"
             "    d33 lstyle 2 color green lwidth 0.05"  */
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .4\n"
             "      position tr\n"
             "      text \" all BC DZ/LZ \" lstyle 1 color black lwidth 0.07\n"
             "!     text \" all BC div/non-div \" lstyle 1 color blue lwidth 0.07\n"
             "!     text \" dec205+ DZ/LZ \" lstyle 1 color red lwidth 0.07\n"
             "!     text \" dec205+ div/non-div \" lstyle 1 color green lwidth 0.07\n"
             "!     text \" dec205- DZ/LZ \" lstyle 2 color red lwidth 0.07\n"
             "!     text \" dec205- div/non-div \" lstyle 2 color green lwidth 0.07\n"
             "end key\n";
        f.close();
    }

    /*
    {
        ofstream f("dz2lz_ig.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " DZ/LZ ratio " hei .7 font texcmss
                 xaxis font texcmss dticks 72 hei .6
                 yaxis font texcmss hei .6
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 4
                 data  xsumbc_ig.out  d1=c1,c5
                 data  xsumbc_ig.out  d2=c1,c10
                 data  xsumbc_ig.out  d3=c1,c15
                 data  xsumbc_ig.out  d4=c1,c20
                 data  xsumbc_ig.out  d5=c1,c25
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color green lwidth 0.05
                 d4 lstyle 1 color blue lwidth 0.05
             !	d5 lstyle 1 color magenta lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .5
                   position tr
                   text " all BC " lstyle 1 color black lwidth 0.07
                   text " IgM-BC " lstyle 1 color red lwidth 0.07
                   text " IgG-BC " lstyle 1 color green lwidth 0.07
                   text " IgE-BC " lstyle 1 color blue lwidth 0.07
             !      text " IgA-BC " lstyle 1 color magenta lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("dz2lz_ig_smooth.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " DZ/LZ ratio " hei .7 font texcmss
                 xaxis font texcmss dticks 72 hei .6
                 yaxis font texcmss hei .6
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 10
                 data  xsumbc_ig_smooth.out  d1=c1,c5
                 data  xsumbc_ig_smooth.out  d2=c1,c10
                 data  xsumbc_ig_smooth.out  d3=c1,c15
                 data  xsumbc_ig_smooth.out  d4=c1,c20
                 data  xsumbc_ig_smooth.out  d5=c1,c25
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color green lwidth 0.05
                 d4 lstyle 1 color blue lwidth 0.05
             !	d5 lstyle 1 color magenta lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .5
                   position tr
                   text " all BC " lstyle 1 color black lwidth 0.07
                   text " IgM-BC " lstyle 1 color red lwidth 0.07
                   text " IgG-BC " lstyle 1 color green lwidth 0.07
                   text " IgE-BC " lstyle 1 color blue lwidth 0.07
             !      text " IgA-BC " lstyle 1 color magenta lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("dz2lzdata.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " DZ to LZ ratio " hei .7 font texcmss
                 xaxis hei .5 font texcmss dticks 12
                 yaxis hei .5 font texcmss
                 xnames "120" "132" "144" "156" "168" "180\n"
                 xaxis min 120 max 188
                 yaxis min 0 max 20
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 data xsumcc.out  d7=c1,c6  ! apoptotic CC
                 let d8 = (d4)/(d5-d7)  ! all BC div/(nondiv-apoptotic)
             !  	d8 lstyle 1 color blue lwidth 0.05
             !
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
             !
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 data dec205.out  d28=c1,c20 ! dec+ CC-apoptotic
                 data dec205.out  d41=c1,c23 ! dec+ CC-apoptotic-DZ
                 let d29 = (d25)/(d26-d28+d41) ! dec205+ div/non-div
             !	d29 lstyle 1 color green lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 data dec205.out  d34=c1,c21 ! dec- CC-apoptotic
                 data dec205.out  d42=c1,c24 ! dec- CC-apoptotic-DZ
                 let d35 = (d31)/(d32-d34+d42) ! dec205- div/non-div
             !	d35 lstyle 2 color green lwidth 0.05
             !
                 let d3 = (d1-d41-d42)/(d2-d28-d34+d41+d42)  ! all BC (DZ-apo)/(LZ-apo)
                 d3 lstyle 1 color black lwidth 0.05
                 let d23 = (d11+d21-d41-d42)/(d12+d22-d28-d34+d41+d42) ! all BC (DZ-apo)/(LZ-apo)
                 d23 lstyle 2 color grey30 lwidth 0.05
                 let d13 = (d11-d41)/(d12-d28+d41) ! dec205+ (DZ-apo)/(LZ-apo)
                 d13 lstyle 1 color red lwidth 0.05
                 let d24 = (d21-d42)/(d22-d34+d42) ! dec205- (DZ-apo)/(LZ-apo)
                 d24 lstyle 1 color green lwidth 0.05

                     data  /home/mehe/Work/hyphasma/hyphasma/data/victora10cell-fig6c.exp d70=c1,c2
                     data  /home/mehe/Work/hyphasma/hyphasma/data/victora10cell-fig6c.exp d71=c1,c4
                     d70 marker ftriangle color black msize 0.6
                     d71 marker fcircle color black msize 0.5
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tl
                   text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
             !      text " all BC div/non-div " lstyle 1 color blue lwidth 0.07
                   text " dec205+ DZ/LZ " lstyle 1 color red lwidth 0.07
             !      text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
                   text " dec205- DZ/LZ " lstyle 1 color green lwidth 0.07
             !      text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("dz2lznoapo-hd.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Ratio " hei .7 font texcmss
                 xaxis font texcmss dticks 12 hei .4
                 yaxis font texcmss hei .4
                 xnames "-24" "-12" "0" "12" "24" "36" "48" "60" "72" "84" "96" "108" "120\n"
                 xaxis min 96 max 240
                 yaxis min 0 max 20
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 data xsumcc.out  d7=c1,c6  ! apoptotic CC
                 let d8 = (d4)/(d5-d7)  ! all BC div/(nondiv-apoptotic)
             !  	d8 lstyle 1 color blue lwidth 0.05
             !
                 let d3 = d1/(d2-d7)  ! all BC DZ/LZ
             !  	d3 lstyle 1 color black lwidth 0.05
             !
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
             !	let d23 = (d11+d21)/(d12+d22) ! DZ/LZ (all BC)
             !	d23 lstyle 1 color green lwidth 0.08
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 data dec205.out  d28=c1,c20 ! dec+ CC-apoptotic
                 let d29 = (d25)/(d26-d28) ! dec205+ div/non-div
             !	d29 lstyle 1 color green lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 data dec205.out  d34=c1,c21 ! dec- CC-apoptotic
                 let d35 = (d31)/(d32-d34) ! dec205- div/non-div
             !	d35 lstyle 2 color green lwidth 0.05
             !
                 let d13 = d11/(d12-d28) ! dec205+ DZ/LZ
                 d13 lstyle 5 color red lwidth 0.05
                 let d24 = d21/(d22-d34) ! dec205- DZ/LZ
                 d24 lstyle 2 color green lwidth 0.05
                 let d36 = (d11+d21)/(d12+d22-d28-d34) ! DZ/LZ (all BC)
                 d36 lstyle 1 color black lwidth 0.08
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
             !      text " all BC div/non-div " lstyle 1 color blue lwidth 0.07
                   text " dec205+ DZ/LZ " lstyle 5 color red lwidth 0.07
             !      text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
                   text " dec205- DZ/LZ " lstyle 2 color green lwidth 0.07
             !      text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("dz2lznoapo.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Ratio " hei .7 font texcmss
                 xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 6
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 data xsumcc.out  d7=c1,c6  ! apoptotic CC
                 let d8 = (d4)/(d5-d7)  ! all BC div/(nondiv-apoptotic)
                 d8 lstyle 1 color blue lwidth 0.05
             !
                 let d3 = d1/(d2-d7)  ! all BC DZ/LZ
                 d3 lstyle 1 color black lwidth 0.05
             !
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
             !	let d23 = (d11+d21)/(d12+d22) ! DZ/LZ (all BC)
             !	d23 lstyle 1 color green lwidth 0.08
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 data dec205.out  d28=c1,c20 ! dec+ CC-apoptotic
                 let d29 = (d25)/(d26-d28) ! dec205+ div/non-div
                 d29 lstyle 1 color green lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 data dec205.out  d34=c1,c21 ! dec- CC-apoptotic
                 let d35 = (d31)/(d32-d34) ! dec205- div/non-div
                 d35 lstyle 2 color green lwidth 0.05
             !
                 let d13 = d11/(d12-d28) ! dec205+ DZ/LZ
                 d13 lstyle 1 color red lwidth 0.05
                 let d24 = d21/(d22-d34) ! dec205- DZ/LZ
                 d24 lstyle 2 color red lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
                   text " all BC div/non-div " lstyle 1 color blue lwidth 0.07
                   text " dec205+ DZ/LZ " lstyle 1 color red lwidth 0.07
                   text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
                   text " dec205- DZ/LZ " lstyle 2 color red lwidth 0.07
                   text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("dz2lznoapoz-hd-log.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " log2(Ratio) " hei .7 font texcmss
                 xaxis font texcmss dticks 12 hei .4
                 yaxis font texcmss hei .4
                 xnames "-24" "-12" "0" "12" "24" "36" "48" "60" "72" "84" "96\n"
                 xaxis min 96 max 216
                 yaxis min -2 max 10
                 data  victora10cell-fig6c.exp d70=c1,c3
                 data  victora10cell-fig6c.exp d71=c1,c5
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 data xsumcc.out  d7=c1,c6  ! apoptotic CC
                 let d8 = (d4)/(d5-d7)  ! all BC div/(nondiv-apoptotic)
             !  	d8 lstyle 1 color blue lwidth 0.05
             !
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
             !
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 data dec205.out  d28=c1,c20 ! dec+ CC-apoptotic
                 data dec205.out  d41=c1,c23 ! dec+ CC-apoptotic-DZ
                 let d29 = (d25)/(d26-d28+d41) ! dec205+ div/non-div
             !	d29 lstyle 1 color green lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 data dec205.out  d34=c1,c21 ! dec- CC-apoptotic
                 data dec205.out  d42=c1,c24 ! dec- CC-apoptotic-DZ
                 let d35 = (d31)/(d32-d34+d42) ! dec205- div/non-div
             !	d35 lstyle 2 color green lwidth 0.05
             !
                 let d3 = log10((d1-d41-d42)/(d2-d28-d34+d41+d42))/log10(2)  ! all BC (DZ-apo)/(LZ-apo)
                 d3 lstyle 1 color black lwidth 0.05
                 let d23 = log10((d11+d21-d41-d42)/(d12+d22-d28-d34+d41+d42))/log10(2) ! all BC (DZ-apo)/(LZ-apo)
                 d23 lstyle 2 color grey30 lwidth 0.05
                 let d13 = log10((d11-d41)/(d12-d28+d41))/log10(2) ! dec205+ (DZ-apo)/(LZ-apo)
                 d13 lstyle 1 color red lwidth 0.05
                 let d24 = log10((d21-d42)/(d22-d34+d42))/log10(2) ! dec205- (DZ-apo)/(LZ-apo)
                 d24 lstyle 1 color green lwidth 0.05
                 d70 marker fdiamond color red msize 0.5
                 d71 marker fdiamond color green msize 0.5
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
             !      text " all BC div/non-div " lstyle 1 color blue lwidth 0.07
                   text " dec205+ DZ/LZ " lstyle 1 color red lwidth 0.07
             !      text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
                   text " dec205- DZ/LZ " lstyle 1 color green lwidth 0.07
             !      text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("dz2lznoapoz-hd.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Ratio " hei .7 font texcmss
                 xaxis font texcmss dticks 12 hei .4
                 yaxis font texcmss hei .4
                 xnames "-24" "-12" "0" "12" "24" "36" "48" "60" "72" "84" "96\n"
                 xaxis min 96 max 216
                 yaxis min 0 max 20
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 data xsumcc.out  d7=c1,c6  ! apoptotic CC
                 let d8 = (d4)/(d5-d7)  ! all BC div/(nondiv-apoptotic)
             !  	d8 lstyle 1 color blue lwidth 0.05
             !
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
             !
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 data dec205.out  d28=c1,c20 ! dec+ CC-apoptotic
                 data dec205.out  d41=c1,c23 ! dec+ CC-apoptotic-DZ
                 let d29 = (d25)/(d26-d28+d41) ! dec205+ div/non-div
             !	d29 lstyle 1 color green lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 data dec205.out  d34=c1,c21 ! dec- CC-apoptotic
                 data dec205.out  d42=c1,c24 ! dec- CC-apoptotic-DZ
                 let d35 = (d31)/(d32-d34+d42) ! dec205- div/non-div
             !	d35 lstyle 2 color green lwidth 0.05
             !
                 let d3 = (d1-d41-d42)/(d2-d28-d34+d41+d42)  ! all BC (DZ-apo)/(LZ-apo)
                 d3 lstyle 1 color black lwidth 0.05
                 let d23 = (d11+d21-d41-d42)/(d12+d22-d28-d34+d41+d42) ! all BC (DZ-apo)/(LZ-apo)
                 d23 lstyle 2 color grey30 lwidth 0.05
                 let d13 = (d11-d41)/(d12-d28+d41) ! dec205+ (DZ-apo)/(LZ-apo)
                 d13 lstyle 1 color red lwidth 0.05
                 let d24 = (d21-d42)/(d22-d34+d42) ! dec205- (DZ-apo)/(LZ-apo)
                 d24 lstyle 1 color green lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
             !      text " all BC div/non-div " lstyle 1 color blue lwidth 0.07
                   text " dec205+ DZ/LZ " lstyle 1 color red lwidth 0.07
             !      text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
                   text " dec205- DZ/LZ " lstyle 1 color green lwidth 0.07
             !      text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }

    {
        ofstream f("dz2lznoapoz.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " DZ to LZ ratio " hei .7 font texcmss
                 xaxis hei .5 font texcmss dticks 72
                 yaxis hei .5 font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 10
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 data xsumcc.out  d7=c1,c6  ! apoptotic CC
                 let d8 = (d4)/(d5-d7)  ! all BC div/(nondiv-apoptotic)
             !  	d8 lstyle 1 color blue lwidth 0.05
             !
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
             !
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 data dec205.out  d28=c1,c20 ! dec+ CC-apoptotic
                 data dec205.out  d41=c1,c23 ! dec+ CC-apoptotic-DZ
                 let d29 = (d25)/(d26-d28+d41) ! dec205+ div/non-div
             !	d29 lstyle 1 color green lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 data dec205.out  d34=c1,c21 ! dec- CC-apoptotic
                 data dec205.out  d42=c1,c24 ! dec- CC-apoptotic-DZ
                 let d35 = (d31)/(d32-d34+d42) ! dec205- div/non-div
             !	d35 lstyle 2 color green lwidth 0.05
             !
                 let d3 = (d1-d41-d42)/(d2-d28-d34+d41+d42)  ! all BC (DZ-apo)/(LZ-apo)
                 d3 lstyle 1 color black lwidth 0.05
                 let d23 = (d11+d21-d41-d42)/(d12+d22-d28-d34+d41+d42) ! all BC (DZ-apo)/(LZ-apo)
                 d23 lstyle 2 color grey30 lwidth 0.05
                 let d13 = (d11-d41)/(d12-d28+d41) ! dec205+ (DZ-apo)/(LZ-apo)
                 d13 lstyle 1 color red lwidth 0.05
                 let d24 = (d21-d42)/(d22-d34+d42) ! dec205- (DZ-apo)/(LZ-apo)
                 d24 lstyle 1 color green lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
             !      text " all BC div/non-div " lstyle 1 color blue lwidth 0.07
                   text " dec205+ DZ/LZ " lstyle 1 color red lwidth 0.07
             !      text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
                   text " dec205- DZ/LZ " lstyle 1 color green lwidth 0.07
             !      text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }

    {
        ofstream f("dz2lztmp.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Ratio " hei .7 font texcmss
                 xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 6
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 let d3 = d1/d2  ! all BC DZ/LZ
                 d3 lstyle 1 color black lwidth 0.05
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 data xsumcc.out  d7=c1,c6  ! apoptotic CC
                 let d8 = (d4+(d6/2))/(d5-(d6/2)-d7)  ! all BC div/(nondiv-apoptotic)
                 d8 lstyle 1 color blue lwidth 0.05
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
                 let d13 = d11/d12 ! dec205+ DZ/LZ
                 d13 lstyle 1 color red lwidth 0.05
                 let d24 = d21/d22 ! dec205- DZ/LZ
                 d24 lstyle 2 color red lwidth 0.05
                 let d23 = (d11+d21)/(d12+d22) ! DZ/LZ (all BC)
             !	d23 lstyle 1 color green lwidth 0.08
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 data dec205.out  d28=c1,c20 ! dec+ CC-apoptotic
                 let d29 = (d25+d27)/(d26-d27-d28) ! dec205+ div/non-div
                 d29 lstyle 1 color green lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 data dec205.out  d34=c1,c21 ! dec- CC-apoptotic
                 let d35 = (d31+d33)/(d32-d33-d34) ! dec205- div/non-div
                 d35 lstyle 2 color green lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
                   text " all BC div/non-div " lstyle 1 color blue lwidth 0.07
                   text " dec205+ DZ/LZ " lstyle 1 color red lwidth 0.07
                   text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
                   text " dec205- DZ/LZ " lstyle 2 color red lwidth 0.07
                   text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }

    {
        ofstream f("dz2lztmp2.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Ratio " hei .7 font texcmss
                 xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 6
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 let d3 = d1/d2  ! all BC DZ/LZ
                 d3 lstyle 1 color black lwidth 0.05
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 data xsumcc.out  d7=c1,c6  ! apoptotic CC
                 data  ssumout.out d1=c1,c2 ! all produced output
                 data  ssumoute.out d2=c1,c2 ! all ejected output
                 let d8 = (d4+(d6/2)+d1-d2)/(d5-(d6/2)-d7)  ! all BC div/(nondiv-apoptotic)
                 d8 lstyle 1 color blue lwidth 0.05
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
                 let d13 = d11/d12 ! dec205+ DZ/LZ
                 d13 lstyle 1 color red lwidth 0.05
                 let d24 = d21/d22 ! dec205- DZ/LZ
                 d24 lstyle 2 color red lwidth 0.05
                 let d23 = (d11+d21)/(d12+d22) ! DZ/LZ (all BC)
             !	d23 lstyle 1 color green lwidth 0.08
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 data dec205.out  d28=c1,c20 ! dec+ CC-apoptotic
                 let d29 = (d25+d27)/(d26-d27-d28) ! dec205+ div/non-div
                 d29 lstyle 1 color green lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 data dec205.out  d34=c1,c21 ! dec- CC-apoptotic
                 let d35 = (d31+d33)/(d32-d33-d34) ! dec205- div/non-div
                 d35 lstyle 2 color green lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
                   text " all BC div/non-div " lstyle 1 color blue lwidth 0.07
                   text " dec205+ DZ/LZ " lstyle 1 color red lwidth 0.07
                   text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
                   text " dec205- DZ/LZ " lstyle 2 color red lwidth 0.07
                   text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }

    {
        ofstream f("dzout2lz.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " (DZ-BC + plasma cells)/LZ-BC " hei .7 font texcmss
                 xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 6
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 data  tc.out d3=c1,c2 ! all TC
                 data  xvolume.out d4=c1,c3 ! all nodes occupied with BC, output and TC
                 data  xvolume.out d5=c1,c4 ! all BC
                 let d6 = (d1+d4-d3-d5)/d2  ! all BC DZ+output/LZ
                 d6 lstyle 1 color black lwidth 0.05
                 data xsumcb.out  d7=c1,c2  ! all CB
                 data xsumcc.out  d8=c1,c2  ! all CC
                 let d9=(d7+d4-d3-d5)/d8  ! all BC div/non-div
                 d9 lstyle 1 color blue lwidth 0.05
             !	data dec205.out  d11=c1,c5 ! dec+ in dz
             !	data dec205.out  d12=c1,c8 ! dec+ in lz
             !	data dec205.out  d21=c1,c6 ! dec- in dz
             !	data dec205.out  d22=c1,c9 ! dec- in lz
             !	let d13 = d11/d12 ! dec205+ DZ/LZ
             !	d13 lstyle 1 color red lwidth 0.05
             !	let d24 = d21/d22 ! dec205- DZ/LZ
             !	d24 lstyle 2 color red lwidth 0.05
             !	let d23 = (d11+d21)/(d12+d22) ! DZ/LZ (all BC)
             !!	d23 lstyle 1 color green lwidth 0.08
             !	data dec205.out  d25=c1,c11 ! dec+ CB
             !	data dec205.out  d26=c1,c14 ! dec+ CC
             !	let d27 = d25/d26 ! dec205+ div/non-div
             !	d27 lstyle 1 color green lwidth 0.05
             !	data dec205.out  d31=c1,c12 ! dec- CB
             !	data dec205.out  d32=c1,c15 ! dec- CC
             !	let d33 = d31/d32 ! dec205- div/non-div
             !	d33 lstyle 2 color green lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
                   text " all BC div/non-div " lstyle 1 color blue lwidth 0.07
             !      text " dec205+ DZ/LZ " lstyle 1 color red lwidth 0.07
             !      text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
             !      text " dec205- DZ/LZ " lstyle 2 color red lwidth 0.07
             !      text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }

    {
        ofstream f("dzsel2lz.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Ratio " hei .7 font texcmss
                 xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 6
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 let d3 = d1/d2  ! all BC DZ/LZ
                 d3 lstyle 1 color black lwidth 0.05
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 let d7 = (d4+d6)/(d5-d6)  ! all BC div/non-div
                 d7 lstyle 1 color blue lwidth 0.05
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
                 let d13 = d11/d12 ! dec205+ DZ/LZ
                 d13 lstyle 1 color red lwidth 0.05
                 let d24 = d21/d22 ! dec205- DZ/LZ
                 d24 lstyle 2 color red lwidth 0.05
                 let d23 = (d11+d21)/(d12+d22) ! DZ/LZ (all BC)
             !	d23 lstyle 1 color green lwidth 0.08
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 let d28 = (d25+d27)/(d26-d27) ! dec205+ div/non-div
                 d28 lstyle 1 color green lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 let d34 = (d31+d33)/(d32-d33) ! dec205- div/non-div
                 d34 lstyle 2 color green lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
                   text " all BC div/non-div " lstyle 1 color blue lwidth 0.07
                   text " dec205+ DZ/LZ " lstyle 1 color red lwidth 0.07
                   text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
                   text " dec205- DZ/LZ " lstyle 2 color red lwidth 0.07
                   text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }

    {
        ofstream f("dzsel2lznoapo.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Ratio " hei .7 font texcmss
                 xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 6
                 data  zone_da.out  d1=c1,c7   ! all in DZ
                 data  zone_li.out  d2=c1,c7   ! all in LZ
                 let d3 = d1/d2  ! all BC DZ/LZ
                 d3 lstyle 1 color black lwidth 0.05
                 data xsumcb.out  d4=c1,c2  ! all CB
                 data xsumcc.out  d5=c1,c2  ! all CC
                 data xsumcc.out  d6=c1,c5  ! selected CC
                 data xsumcc.out  d7=c1,c6  ! apoptotic CC
                 let d8 = (d4+d6)/(d5-d6-d7)  ! all BC div/(nondiv-apoptotic)
                 d8 lstyle 1 color blue lwidth 0.05
                 data dec205.out  d11=c1,c5 ! dec+ in dz
                 data dec205.out  d12=c1,c8 ! dec+ in lz
                 data dec205.out  d21=c1,c6 ! dec- in dz
                 data dec205.out  d22=c1,c9 ! dec- in lz
                 let d13 = d11/d12 ! dec205+ DZ/LZ
                 d13 lstyle 1 color red lwidth 0.05
                 let d24 = d21/d22 ! dec205- DZ/LZ
                 d24 lstyle 2 color red lwidth 0.05
                 let d23 = (d11+d21)/(d12+d22) ! DZ/LZ (all BC)
             !	d23 lstyle 1 color green lwidth 0.08
                 data dec205.out  d25=c1,c11 ! dec+ CB
                 data dec205.out  d26=c1,c14 ! dec+ CC
                 data dec205.out  d27=c1,c17 ! dec+ CC-selected
                 data dec205.out  d28=c1,c20 ! dec+ CC-apoptotic
                 let d29 = (d25+d27)/(d26-d27-d28) ! dec205+ div/non-div
                 d29 lstyle 1 color green lwidth 0.05
                 data dec205.out  d31=c1,c12 ! dec- CB
                 data dec205.out  d32=c1,c15 ! dec- CC
                 data dec205.out  d33=c1,c18 ! dec- CC-selected
                 data dec205.out  d34=c1,c21 ! dec- CC-apoptotic
                 let d35 = (d31+d33)/(d32-d33-d34) ! dec205- div/non-div
                 d35 lstyle 2 color green lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all BC DZ/LZ " lstyle 1 color black lwidth 0.07
                   text " all BC div/non-div " lstyle 1 color blue lwidth 0.07
                   text " dec205+ DZ/LZ " lstyle 1 color red lwidth 0.07
                   text " dec205+ div/non-div " lstyle 1 color green lwidth 0.07
                   text " dec205- DZ/LZ " lstyle 2 color red lwidth 0.07
                   text " dec205- div/non-div " lstyle 2 color green lwidth 0.07
             end key
";
        f.close();
    }
    */


    /* ???? no out file
    {
        ofstream f("fdccontact.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " contact time [min] " hei .7 font TEXCMR
                 x2title " CC-FDC contact times " hei .7 font TEXCMR
                 ytitle " counts/total measurements " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min -1 max 60
                 yaxis min 0
                 data  fdccontact.out  d1=c1,c4
                 data  fdccmean.out d3=c1,c3
                 bar d1 width 0.9 fill grey40
             ! Reactivate this line to show error bars:
             !        d1 err d2 errwidth .2 lwidth .06
                 d3 marker star msize 0.7
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }

    {
        ofstream f("fdccontact_low.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " contact time [min] " hei .7 font TEXCMR
                 x2title " CC-FDC contact times " hei .7 font TEXCMR
                 ytitle " counts/total measurements " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min -1 max 60
                 yaxis min 0 max 0.003
                 data  fdccontact.out  d1=c1,c4
                 data  fdccmean.out d3=c1,c3
                 bar d1 width 0.9 fill grey40
             ! Reactivate this line to show error bars:
             !        d1 err d2 errwidth .2 lwidth .06
                 d3 marker star msize 0.7
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    */


    {
        ofstream f("fdcencount.gle");
        f << "size 15 15\n"
             "amove 2.0 2.0\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" # of FDC-tests by (dying) B-cells \" hei .7 font texcmss\n"
             "    ytitle \" Counts per GCR \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss\n"
             "    yaxis hei .5 font texcmss\n"
             "    xaxis min 0 max 50\n"
             "    yaxis min 0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  fdcencount" << i << ".out  d" << i << "=c1,c2\n"
             "    d"<< i << " lstyle 1  lwidth 0.05 color " << col[i] << "\n";
        }
        f <<"end graph";
        f.close();
    }

    {
        ofstream f("fracapo_smooth.glex");
        f << "size 15 15\n"
             "amove 2.5 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" time [days] \" hei .7 font TEXCMR\n"
             "    ytitle \" fraction of apoptotic CC \" hei .7 font TEXCMR\n"
             "        xaxis font TEXCMR dticks 72\n"
             "    yaxis font TEXCMR\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12`\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 max 0.01\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  ssumcc_smooth" << i << ".out  d" << i << "1=c1,c2\n"
             "    data  ssumapo1_smooth" << i << ".out  d" << i << "2=c1,c2\n"
             "    let d" << i << "3 = d" << i << "2/(d" << i << "1+1)\n"
             "    d" << i << "3 lstyle 1 color " << col[i] << " lwidth 0.05\n";
        }
        f << "end graph";
        f.close();
    }

    {
        ofstream f("fracapobc_smooth.glex");
        f << "size 15 15\n"
             "amove 2.5 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Fraction of apoptotic BC \" hei .7 font texcmss\n"
             "        xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 ! max 1.0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  xvolume_smooth" << i << ".out  d" << i << "1=c1,c8\n"
             "    data  ssumapo1_smooth" << i << ".out  d" << i << "2=c1,c2\n"
             "!	data  ssumout_smooth" << i << ".out d" << i << "3=c1,c3\n"
             "    let d" << i << "4 = d" << i << "2/(d" << i << "1+1)\n"
             "    d" << i << "4 lstyle 1 color " << col[i] << " lwidth 0.05\n";
        }
        f << "end graph";
        f.close();
    }

    {
        ofstream f("fracFDCcoAll.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "!	x2title \" fraction of BC in contact to FDC \" hei .7 font texcmss\n"
             "    ytitle \" BC fraction in contact to FDC\" hei .7 font texcmss\n"
             "        xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 !max 0.04\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  xsumcc" << i << ".out  d" << i << "1=c1,c2\n"
             "    data  xsumcb" << i << ".out  d" << i << "2=c1,c2\n"
             "    data  xsumcc" << i << ".out  d" << i << "3=c1,c4 ! CC in contact\n"
             "    data  xsumcc" << i << ".out  d" << i << "4=c1,c8 ! apoptotic\n"
             "    let d" << i << "5 = d" << i << "3/(d" << i << "1+d" << i << "2-d" << i << "4)\n"
             "    let d" << i << "6 = d" << i << "3/(d" << i << "1+d" << i << "2)\n"
             "    d" << i << "6 lstyle 2 lwidth 0.05 color " << col[i] << "\n"
             "!    d" << i << "5 lstyle 1 lwidth 0.05 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tr\n"
             "      text \" including apoptotic \" lstyle 2 lwidth 0.06 color grey10\n"
             "!      text \" excluding apoptotic \" lstyle 1 lwidth 0.06 color black\n"
             //"!      text " CCs in contact to FDCs " lstyle 3 lwidth 0.06 color blue\n"
             //"!      text " selected CCs " lstyle 4 lwidth 0.06 color green\n"
             //"!      text " apoptotic cells " lstyle 5 lwidth 0.06 color red\n"
             "end key";
        f.close();
    }

    {
        ofstream f("fracFDCcoAlive.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "!	x2title \" fraction of BC in contact to FDC \" hei .7 font texcmss\n"
             "    ytitle \" BC fraction in contact to FDC\" hei .7 font texcmss\n"
             "        xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 !max 0.04\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  xsumcc" << i << ".out  d" << i << "1=c1,c2\n"
             "    data  xsumcb" << i << ".out  d" << i << "2=c1,c2\n"
             "    data  xsumcc" << i << ".out  d" << i << "3=c1,c4 ! CC in contact\n"
             "    data  xsumcc" << i << ".out  d" << i << "4=c1,c8 ! apoptotic\n"
             "    let d" << i << "5 = d" << i << "3/(d" << i << "1+d" << i << "2-d" << i << "4)\n"
             "    let d" << i << "6 = d" << i << "3/(d" << i << "1+d" << i << "2)\n"
             "!    d" << i << "6 lstyle 2 lwidth 0.05 color light" << col[i] << "\n"
             "    d" << i << "5 lstyle 1 lwidth 0.05 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tr\n"
             "!      text \" including apoptotic \" lstyle 2 lwidth 0.06 color grey10\n"
             "      text \" excluding apoptotic \" lstyle 1 lwidth 0.06 color black\n"
             //"!      text " CCs in contact to FDCs " lstyle 3 lwidth 0.06 color blue\n"
             //"!      text " selected CCs " lstyle 4 lwidth 0.06 color green\n"
             //"!      text " apoptotic cells " lstyle 5 lwidth 0.06 color red\n"
             "end key";
        f.close();
    }
    {
        ofstream f("fracTFHco.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "!	x2title \" fraction of BC in contact to TFH \" hei .7 font texcmss\n"
             "    ytitle \" BC fraction in contact to TFH\" hei .7 font texcmss\n"
             "        xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 !max 0.04\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  xsumcc"<< i << ".out  d"<< i << "1=c1,c2\n"
             "    data  xsumcb"<< i << ".out  d"<< i << "2=c1,c2\n"
             "    data  xsumcc"<< i << ".out  d"<< i << "3=c1,c6 ! CC in contact to TFH\n"
             "    data  xsumcc"<< i << ".out  d"<< i << "4=c1,c8 ! apoptotic\n"
             "    let d"<< i << "5 = d"<< i << "3/(d"<< i << "1+d"<< i << "2-d"<< i << "4)\n"
             "    let d"<< i << "6 = d"<< i << "3/(d"<< i << "1+d"<< i << "2)\n"
             "    d"<< i << "6 lstyle 2 lwidth 0.05 color " << col[i] << "\n"
             "    d"<< i << "5 lstyle 1 lwidth 0.05 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tr\n"
             "      text \" including apoptotic \" lstyle 2 lwidth 0.06 color grey10\n"
             "      text \" excluding apoptotic \" lstyle 1 lwidth 0.06 color black\n"
             //"!      text " CCs in contact to FDCs " lstyle 3 lwidth 0.06 color blue\n"
             //"!      text " selected CCs " lstyle 4 lwidth 0.06 color green\n"
             //"!      text " apoptotic cells " lstyle 5 lwidth 0.06 color red\n"
             "end key";
        f.close();
    }

        /*
    {
        ofstream f("miller_vd.gle");
        f << "size 15 8
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " v [microns/min] " hei .7 font TEXCMR
                 ytitle " counts " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0 max 40
                 yaxis min 0
                 data  vdistd.out  d1=c2,c5
                 data  vmean.out d2=c6,c5
                 bar d1 width 1.8 fill grey40
                 d2 marker star msize 0.7
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("miller_vt.gle");
        f << "size 15 8
             amove 2.0 1.5
             set font TEXCMR
             begin graph
                 size 12 6
                 fullsize
                 xtitle " t [min] " hei .7 font TEXCMR
                 ytitle " velocity [microns/min] " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0
                 yaxis min 0
                 data  move_vdt.out  d1=c1,c6
                 d1 marker fcircle
                 d1 lstyle 1 lwidth 0.04
             end graph
";
        f.close();
    } */

        /* No File ???
    {
        ofstream f("move_dist.gle");
        f << "size 17 17
             amove 2.5 2.0
             set font TEXCMR
             begin graph
                 size 14 14
                 fullsize
                 xtitle " Square root of t [sqrt(min)] " hei .7 font TEXCMR
                 ytitle " distance from initial position in microns " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0
                 yaxis min 0
                 data  movement.out  d1=c2,c6
                 d1 lstyle 1 lwidth 0.06
             end graph
             !begin key
             !      hei .4
             !      position tr
             !      text " all CBs " lstyle 1 lwidth 0.04
             !      text " not recycled CBs " lstyle 2 lwidth 0.04
             !      text " recycled CBs " lstyle 3 lwidth 0.04
             !end key
";
        f.close();
    }
    {
        ofstream f("move_distt.gle");
        f << "size 17 17
             amove 2.5 2.0
             set font TEXCMR
             begin graph
                 size 14 14
                 fullsize
                 xtitle " t [min] " hei .7 font TEXCMR
                 ytitle " distance from initial position in microns " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0
                 yaxis min 0
                 data  movement.out  d1=c1,c6
                 d1 lstyle 1 lwidth 0.06
             end graph
             !begin key
             !      hei .4
             !      position tr
             !      text " all CBs " lstyle 1 lwidth 0.04
             !      text " not recycled CBs " lstyle 2 lwidth 0.04
             !      text " recycled CBs " lstyle 3 lwidth 0.04
             !end key
";
        f.close();
    }
    {
        ofstream f("move_trace.gle");
        f << "size 17 17
             amove 2.5 2.0
             set font TEXCMR
             begin graph
                 size 14 14
                 fullsize
                 xtitle " x [microns]" hei .7 font TEXCMR
                 ytitle " y [microns]" hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min -150 max 150
                 yaxis min -150 max 150
                 data  movement.out  d1=c8,c9
                 d1 lstyle 1 lwidth 0.06
             end graph
             !begin key
             !      hei .4
             !      position tr
             !      text " all CBs " lstyle 1 lwidth 0.04
             !      text " not recycled CBs " lstyle 2 lwidth 0.04
             !      text " recycled CBs " lstyle 3 lwidth 0.04
             !end key
";
        f.close();
    }
    {
        ofstream f("move_v.gle");
        f << "size 17 17
             amove 2.5 2.0
             set font TEXCMR
             begin graph
                 size 14 14
                 fullsize
                 xtitle " t [min] " hei .7 font TEXCMR
                 ytitle " single move-velocity [microns/min] " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0
                 yaxis min 0
                 data  movement.out  d1=c1,c5
                 d1 marker triangle
             end graph
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("move_vdt.gle");
        f << "size 15 8
             amove 2.0 1.5
             set font TEXCMR
             begin graph
                 size 12 6
                 fullsize
                 xtitle " t [min] " hei .7 font TEXCMR
                 ytitle " velocity [microns/min] " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0
                 yaxis min 0
                 data  move_vdt.out  d1=c1,c5
                 d1 marker fcircle
                 d1 lstyle 1 lwidth 0.04
             end graph
";
        f.close();
    }

    */

    {
        ofstream f("mutation.gle");
        f << "size 15 15\n"
             "amove 2.0 2.0\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "!   x2title \" Number of mutations per output cell \" hei .7 font texcmss\n"
             "    ytitle \" Mutations per cell \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 max 12\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  mutation" << i << ".out  d" << i << "1=c1,c5\n"
             "    data  mutation" << i << ".out  d" << i << "2=c1,c6\n"
             "    data  mutation" << i << ".out  d" << i << "3=c1,c8\n"
             "    d" << i << "1 lstyle 1 color " << col[i] << " lwidth 0.06\n"
             "!	d2 lstyle 1 color red lwidth 0.06\n"
             "!	d3 lstyle 1 color green lwidth 0.06\n";
        }
        f << "end graph\n"
             "!set font texcmss\n"
             "!begin key\n"
             "!      hei .5\n"
             "!      position tl\n"
             "!      text \" in any cell \" lstyle 1 color blue lwidth 0.07\n"
             "!      text \" in recycled cells \" lstyle 1 color green lwidth 0.07\n"
             "!end key";
        f.close();
    }
    {
        ofstream f("mutation_out_histo.gle");
        f << "size 15 15\n"
             "amove 2.0 2.0\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Number of mutations in output cells \" hei .7 font texcmss\n"
             "    ytitle \" Frequency \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss\n"
             "    yaxis hei .5 font texcmss\n"
             "    xaxis min -1 max 20\n"
             "    yaxis min 0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  mutation_out_histo" << i << ".out  d" << i << "1=c1,c2\n"
             "    d" << i << "1 lstyle 1 lwidth 0.05 color " << col[i] << "\n";
        }
        f << "end graph";
        f.close();
    }
    {
        ofstream f("mutation_set.gle");
        f << "size 15 15\n"
             "amove 2.0 2.0\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Mutation probability \" hei .7 font texcmss\n"
             "    ytitle \" Selected BC count \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss\n"
             "    yaxis hei .5 font texcmss\n"
             "!	  xaxis min -0.05 max 0.6\n"
             "    yaxis min 0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  mutation_set" << i << ".out  d" << i << "1=c2,c3\n"
             "    d" << i << "1 lstyle 1 lwidth 0.04 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "!set font texcmss\n"
             "!begin key\n"
             "!      hei .5\n"
             "!      position tr\n"
             "!      text \" all BCs \" color black lstyle 1 lwidth 0.3\n"
             "!      text \" selected BCs \" color grey30 lstyle 1 lwidth 0.3\n"
             "!end key";
        f.close();
    }

    {
        ofstream f("mutation_time_last_select.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Probability of mutation \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min -0.05 max 0.6\n";;
        for(int i = 1; i < nb+1; ++i){
        f << "    data mutation_time" << i << ".out  d" << i << "1=c1,c2\n"
             "    data mutation_time" << i << ".out  d" << i << "2=c1,c3\n"
             "    data mutation_time" << i << ".out  d" << i << "3=c1,c4\n"
             "    data mutation_time" << i << ".out  d" << i << "4=c1,c5\n"
             //"    d" << i << "1 err d" << i << "2 color " << col[i] << " errwidth 0.1\n"
             "!    d" << i << "3 err d" << i << "4 color lightgreen errwidth 0.1\n"
             "    let d" << i << "11 = d" << i << "1\n"
             "    let d" << i << "12 = d" << i << "3\n"
             "    d" << i << "11 lstyle 1 lwidth 0.06 color " << col[i] << "\n"
             "!    d" << i << "12 lstyle 1 lwidth 0.06 color green\n";
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tr\n"
             "      text \" last selected BCs \" lstyle 1 color black lwidth 0.07\n"
             "!      text \" all current CBs \" lstyle 1 color green lwidth 0.07\n"
             "end key";
        f.close();
    }

    {
        ofstream f("mutation_time.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Probability of mutation \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min -0.05 max 0.6\n";;
        for(int i = 1; i < nb+1; ++i){
        f << "    data mutation_time" << i << ".out  d" << i << "1=c1,c2\n"
             "    data mutation_time" << i << ".out  d" << i << "2=c1,c3\n"
             "    data mutation_time" << i << ".out  d" << i << "3=c1,c4\n"
             "    data mutation_time" << i << ".out  d" << i << "4=c1,c5\n"
             "!    d" << i << "1 err d" << i << "2 color lightblue errwidth 0.1\n"
             //"    d" << i << "3 err d" << i << "4 color " << col[i] << " errwidth 0.1\n"
             "    let d" << i << "11 = d" << i << "1\n"
             "    let d" << i << "12 = d" << i << "3\n"
             "!    d" << i << "11 lstyle 1 lwidth 0.06 color blue\n"
             "    d" << i << "12 lstyle 1 lwidth 0.06 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tr\n"
             "!      text \" last selected BCs \" lstyle 1 color blue lwidth 0.07\n"
             "      text \" all current CBs \" lstyle 1 color green lwidth 0.07\n"
             "end key";
        f.close();
    }
        /*
    {
        ofstream f("ncellzone.gle");
        f << "size 15 15
             amove 2.0 2.0
             set font texcmss
             begin graph
                 size 12 12
                 fullsize
                 xtitle " time [min] " hei .7 font texcmss
                 ytitle " fraction of tracked cells in zones " hei .7 font texcmss
                 xaxis hei .5 font texcmss
                 yaxis hei .5 font texcmss
                 xaxis min 0
                 yaxis min 0
                 data ncellzone.out d1=c1,c5
                 data ncellzone.out d2=c1,c6
                 data ./ncellzone-exp.exp d3=c1,c2
                 data ./ncellzone-exp.exp d4=c1,c4
                 data ./ncellzone-exp.exp d13=c1,c3
                 data ./ncellzone-exp.exp d14=c1,c5
                     d1 lstyle 1 lwidth 0.05 color red
                 d2 lstyle 1 lwidth 0.05 color blue
                 d3 marker ftriangle color red err d13 errwidth 0.1
                 d4 marker ftriangle color blue err d13 errwidth 0.1
             end graph
             begin key
                   hei .5
                   position tr
                   text " DZ to LZ exp " marker ftriangle color red
                   text " LZ to DZ exp " marker ftriangle color blue
                   text " in LZ sim " lstyle 1 lwidth 0.07 color red
                   text " in DZ sim " lstyle 1 lwidth 0.07 color blue
             end key
";
        f.close();
    }*/


    {
        ofstream f("ndivhisto.gle");
        f << "size 15 15\n"
             "amove 2.0 2.0\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" number of divisions attributed \" hei .7 font texcmss\n"
             "    ytitle \" selected BC count \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss\n"
             "    yaxis hei .5 font texcmss\n"
             "    xaxis min -1 max 8\n"
             "    yaxis min 0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "     data  ndivhisto" << i << ".out  d" << i << "1=c1,c2\n"
            "     d" << i << "1 lstyle 1 lwidth 0.05 color " << col[i] << "\n";
      }
       f << " end graph\n"
            " !set font texcmss\n"
            " !begin key\n"
            " !      hei .5\n"
            " !      position tr\n"
            " !      text \" all BCs \" color black lstyle 1 lwidth 0.3\n"
            " !      text \" selected BCs \" color grey30 lstyle 1 lwidth 0.3\n"
            " !end key\n";
        f.close();
    }

    /* todo later
    {
        ofstream f("ndivtime.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Number of divisions attributed to selected BC " hei .7 font texcmss
                 xaxis hei .5 font texcmss dticks 72
                 yaxis hei .5 font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min -1 max 8
             for(int i = 1; i < nb+1; ++i){
         f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                  "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                  "    d"<< i << " marker fcircle color " << col[i];
                  }

                 data ndivtime.out  d1=c1,c2
                 data ndivtime.out  d2=c1,c3
                 d1 err d2 color grey10 errwidth 0.1
                 let d11 = d1
                 d11 lstyle 1 lwidth 0.06 color black
             end graph
             !set font texcmss
             !begin key
             !      hei .5
             !      position tl
             !      text " until TC contact " lstyle 1 color black lwidth 0.07
             !      text " include selected " lstyle 1 color green lwidth 0.07
             !      text " include apoptotic " lstyle 1 color blue lwidth 0.07
             !end key
";
        f.close();
    }
    */

    /* later
    {
        ofstream f("nhaffin_c.gle");
        f << "size 15 15\n"
             "amove 2.5 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Number of high affinity cells \" hei .7 font texcmss\n"
             "        xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0\n"
             "    data  shaffin.out  d1=c1,c4\n"
             "    data  shaffin.out  d2=c1,c2\n"
             "    data  shaffin.out  d3=c1,c3\n"
             "    data  ssumout.out  d4=c1,c2\n"
             "    data  ssumcb.out  d5=c1,c2\n"
             "    data  ssumcc.out  d6=c1,c2\n"
             "!  	d1 lstyle 1 color black lwidth 0.05\n"
             "!  	d2 lstyle 2 color red lwidth 0.05\n"
             "!  	d3 lstyle 5 color green lwidth 0.05\n"
             "    let d11 = d1*d4\n"
             "    let d12 = d2*d5\n"
             "    let d13 = d3*d6";
                for(int i = 1; i < nb+1; ++i){
            f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                     "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                     "    d"<< i << " marker fcircle color " << col[i];
                     }

             "    d11 lstyle 1 color black lwidth 0.05\n"
             "    d12 lstyle 1 color red lwidth 0.05\n"
             "    d13 lstyle 1 color green lwidth 0.05\n"
             "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tl\n"
             "      text " accumulated output " lstyle 1 color black lwidth 0.06\n"
             "      text " CBs " lstyle 1 color red lwidth 0.06\n"
             "      text " CCs " lstyle 1 color green lwidth 0.06\n"
             "end key\n"
        f.close();
    }
    */

    /*
    {
        ofstream f("reach_dist.gle");
        f << "size 15 15
             amove 2.0 2.0
             set font TEXCMR
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Square root of time [sqrt(min)] " hei .7 font TEXCMR
                 x2title " Reached distance " hei .7 font TEXCMR
                 ytitle " distance from initial position in microns " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0
             !	yaxis min 0
                 data  reach_dist.out  d1=c2,c4
                 data  reach_dist.out  d2=c2,c5
                 let d3 = d1+d2
                     let d4 = d1-d2
                 data  reach_dist.out  d5=c2,c6
                 data  reach_dist.out  d6=c2,c7
                 let d7 = d5+d6
                 let d8 = d5-d6
                 let d10 = d1
                 let d11 = d5
             !        fill d4,d3 color grey10
             !	fill d7,d8 color grey10
                     d1 lstyle 1 lwidth 0.01 color red errdown d2
                 d5 lstyle 1 lwidth 0.01 color blue errup d6
                 d10 lstyle 1 lwidth 0.07 color black
                 d11 lstyle 1 lwidth 0.07 color black
             !        d2 lstyle 2 lwidth 0.05
             !        d3 lstyle 2 lwidth 0.05 color orange
             !        d8 lstyle 2 lwidth 0.05 color light_blue
             end graph
             begin key
                   hei .4
                   position tr
                   text " mean displacement " lstyle 1 lwidth 0.06 color red
                   text " sqrt(mean square displacement) " lstyle 1 lwidth 0.06 color blue
             end key
";
        f.close();
    }
    {
        ofstream f("reach_dist_x.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
               size 12 12
               fullsize
               xtitle " sqrt(time) [sqrt(min)]" hei .7 font TEXCMR
               x2title " Reached distances of individual cells " hei .7 font TEXCMR
               ytitle " Reached distance [microns]" hei .7 font TEXCMR
               xaxis font TEXCMR
               yaxis font TEXCMR
               xaxis min 0
               yaxis min 0
               data  reach_dist_x.out d1=c2,c3
               d1 lstyle 1 lwidth 0.05 color black
               data  reach_dist_x.out d2=c2,c4
               d2 lstyle 1 lwidth 0.05 color blue
               data  reach_dist_x.out d3=c2,c5
               d3 lstyle 1 lwidth 0.05 color red
               data  reach_dist_x.out d4=c2,c6
               d4 lstyle 1 lwidth 0.05 color green
               data  reach_dist_x.out d5=c2,c7
               d5 lstyle 1 lwidth 0.05 color yellow
               data  reach_dist_x.out d6=c2,c8
               d6 lstyle 1 lwidth 0.05 color cyan
               data  reach_dist_x.out d7=c2,c9
               d7 lstyle 1 lwidth 0.05 color magenta
               data  reach_dist_x.out d8=c2,c10
               d8 lstyle 1 lwidth 0.05 color orange
               data  reach_dist_x.out d9=c2,c11
               d9 lstyle 1 lwidth 0.05 color brown
               data  reach_dist_x.out d10=c2,c12
               d10 lstyle 1 lwidth 0.05 color grey
               data  reach_dist_x.out d11=c2,c13
               d11 lstyle 2 lwidth 0.05 color black
               data  reach_dist_x.out d12=c2,c14
               d12 lstyle 2 lwidth 0.05 color blue
               data  reach_dist_x.out d13=c2,c15
               d13 lstyle 2 lwidth 0.05 color red
               data  reach_dist_x.out d14=c2,c16
               d14 lstyle 2 lwidth 0.05 color green
               data  reach_dist_x.out d15=c2,c17
               d15 lstyle 2 lwidth 0.05 color yellow
               data  reach_dist_x.out d16=c2,c18
               d16 lstyle 2 lwidth 0.05 color cyan
               data  reach_dist_x.out d17=c2,c19
               d17 lstyle 2 lwidth 0.05 color magenta
               data  reach_dist_x.out d18=c2,c20
               d18 lstyle 2 lwidth 0.05 color orange
               data  reach_dist_x.out d19=c2,c21
               d19 lstyle 2 lwidth 0.05 color brown
               data  reach_dist_x.out d20=c2,c22
               d20 lstyle 2 lwidth 0.05 color grey
               data  reach_dist_x.out d22=c2,c24
               d22 lstyle 3 lwidth 0.05 color blue
               data  reach_dist_x.out d23=c2,c25
               d23 lstyle 3 lwidth 0.05 color red
               data  reach_dist_x.out d24=c2,c26
               d24 lstyle 3 lwidth 0.05 color green
               data  reach_dist_x.out d25=c2,c27
               d25 lstyle 3 lwidth 0.05 color yellow
               data  reach_dist_x.out d26=c2,c28
               d26 lstyle 3 lwidth 0.05 color cyan
               data  reach_dist_x.out d27=c2,c29
               d27 lstyle 3 lwidth 0.05 color magenta
               data  reach_dist_x.out d28=c2,c30
               d28 lstyle 3 lwidth 0.05 color orange
               data  reach_dist_x.out d30=c2,c32
               d30 lstyle 3 lwidth 0.05 color grey
               data  reach_dist_x.out d31=c2,c33
               d31 lstyle 4 lwidth 0.05 color black
               data  reach_dist_x.out d32=c2,c34
               d32 lstyle 4 lwidth 0.05 color blue
               data  reach_dist_x.out d33=c2,c35
               d33 lstyle 4 lwidth 0.05 color red
               data  reach_dist_x.out d34=c2,c36
               d34 lstyle 4 lwidth 0.05 color green
               data  reach_dist_x.out d35=c2,c37
               d35 lstyle 4 lwidth 0.05 color yellow
               data  reach_dist_x.out d36=c2,c38
               d36 lstyle 4 lwidth 0.05 color cyan
               data  reach_dist_x.out d37=c2,c39
               d37 lstyle 4 lwidth 0.05 color magenta
               data  reach_dist_x.out d38=c2,c40
               d38 lstyle 4 lwidth 0.05 color orange
               data  reach_dist_x.out d39=c2,c41
               d39 lstyle 4 lwidth 0.05 color brown
               data  reach_dist_x.out d40=c2,c42
               d40 lstyle 4 lwidth 0.05 color grey
               data  reach_dist_x.out d41=c2,c43
               d41 lstyle 5 lwidth 0.05 color black
               data  reach_dist_x.out d42=c2,c44
               d42 lstyle 5 lwidth 0.05 color blue
               data  reach_dist_x.out d43=c2,c45
               d43 lstyle 5 lwidth 0.05 color red
               data  reach_dist_x.out d44=c2,c46
               d44 lstyle 5 lwidth 0.05 color green
               data  reach_dist_x.out d45=c2,c47
               d45 lstyle 5 lwidth 0.05 color yellow
               data  reach_dist_x.out d46=c2,c48
               d46 lstyle 5 lwidth 0.05 color cyan
               data  reach_dist_x.out d47=c2,c49
               d47 lstyle 5 lwidth 0.05 color magenta
               data  reach_dist_x.out d48=c2,c50
               d48 lstyle 5 lwidth 0.05 color orange
               data  reach_dist_x.out d49=c2,c51
               d49 lstyle 5 lwidth 0.05 color brown
               data  reach_dist_x.out d50=c2,c52
               d50 lstyle 5 lwidth 0.05 color grey
               data  reach_dist_x.out d51=c2,c53
               d51 lstyle 6 lwidth 0.05 color black
               data  reach_dist_x.out d52=c2,c54
               d52 lstyle 6 lwidth 0.05 color blue
               data  reach_dist_x.out d53=c2,c55
               d53 lstyle 6 lwidth 0.05 color red
               data  reach_dist_x.out d54=c2,c56
               d54 lstyle 6 lwidth 0.05 color green
               data  reach_dist_x.out d55=c2,c57
               d55 lstyle 6 lwidth 0.05 color yellow
               data  reach_dist_x.out d56=c2,c58
               d56 lstyle 6 lwidth 0.05 color cyan
               data  reach_dist_x.out d57=c2,c59
               d57 lstyle 6 lwidth 0.05 color magenta
               data  reach_dist_x.out d58=c2,c60
               d58 lstyle 6 lwidth 0.05 color orange
               data  reach_dist_x.out d59=c2,c61
               d59 lstyle 6 lwidth 0.05 color brown
               data  reach_dist_x.out d60=c2,c62
               d60 lstyle 6 lwidth 0.05 color grey
               data  reach_dist_x.out d61=c2,c63
               d61 lstyle 7 lwidth 0.05 color black
               data  reach_dist_x.out d62=c2,c64
               d62 lstyle 7 lwidth 0.05 color blue
               data  reach_dist_x.out d63=c2,c65
               d63 lstyle 7 lwidth 0.05 color red
               data  reach_dist_x.out d64=c2,c66
               d64 lstyle 7 lwidth 0.05 color green
               data  reach_dist_x.out d65=c2,c67
               d65 lstyle 7 lwidth 0.05 color yellow
               data  reach_dist_x.out d66=c2,c68
               d66 lstyle 7 lwidth 0.05 color cyan
               data  reach_dist_x.out d67=c2,c69
               d67 lstyle 7 lwidth 0.05 color magenta
               data  reach_dist_x.out d68=c2,c70
               d68 lstyle 7 lwidth 0.05 color orange
               data  reach_dist_x.out d70=c2,c72
               d70 lstyle 7 lwidth 0.05 color grey
               data  reach_dist_x.out d71=c2,c73
               d71 lstyle 8 lwidth 0.05 color black
               data  reach_dist_x.out d72=c2,c74
               d72 lstyle 8 lwidth 0.05 color blue
               data  reach_dist_x.out d73=c2,c75
               d73 lstyle 8 lwidth 0.05 color red
               data  reach_dist_x.out d74=c2,c76
               d74 lstyle 8 lwidth 0.05 color green
               data  reach_dist_x.out d75=c2,c77
               d75 lstyle 8 lwidth 0.05 color yellow
               data  reach_dist_x.out d76=c2,c78
               d76 lstyle 8 lwidth 0.05 color cyan
               data  reach_dist_x.out d77=c2,c79
               d77 lstyle 8 lwidth 0.05 color magenta
               data  reach_dist_x.out d79=c2,c81
               d79 lstyle 8 lwidth 0.05 color brown
               data  reach_dist_x.out d80=c2,c82
               d80 lstyle 8 lwidth 0.05 color grey
               data  reach_dist_x.out d81=c2,c83
               d81 lstyle 9 lwidth 0.05 color black
               data  reach_dist_x.out d82=c2,c84
               d82 lstyle 9 lwidth 0.05 color blue
               data  reach_dist_x.out d84=c2,c86
               d84 lstyle 9 lwidth 0.05 color green
               data  reach_dist_x.out d85=c2,c87
               d85 lstyle 9 lwidth 0.05 color yellow
               data  reach_dist_x.out d86=c2,c88
               d86 lstyle 9 lwidth 0.05 color cyan
               data  reach_dist_x.out d87=c2,c89
               d87 lstyle 9 lwidth 0.05 color magenta
               data  reach_dist_x.out d88=c2,c90
               d88 lstyle 9 lwidth 0.05 color orange
               data  reach_dist_x.out d89=c2,c91
               d89 lstyle 9 lwidth 0.05 color brown
               data  reach_dist_x.out d90=c2,c92
               d90 lstyle 9 lwidth 0.05 color grey
               data  reach_dist_x.out d91=c2,c93
               d91 lstyle 10 lwidth 0.05 color black
               data  reach_dist_x.out d92=c2,c94
               d92 lstyle 10 lwidth 0.05 color blue
               data  reach_dist_x.out d93=c2,c95
               d93 lstyle 10 lwidth 0.05 color red
               data  reach_dist_x.out d94=c2,c96
               d94 lstyle 10 lwidth 0.05 color green
               data  reach_dist_x.out d95=c2,c97
               d95 lstyle 10 lwidth 0.05 color yellow
               data  reach_dist_x.out d96=c2,c98
               d96 lstyle 10 lwidth 0.05 color cyan
               data  reach_dist_x.out d97=c2,c99
               d97 lstyle 10 lwidth 0.05 color magenta
               data  reach_dist_x.out d98=c2,c100
               d98 lstyle 10 lwidth 0.05 color orange
             end graph
";
        f.close();
    }
    */

    {
        ofstream f("recycling_accu.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" recycling/(recycling+apoptosis+output) \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 max 1\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  xsumcb" << i << ".out  d" << i << "2=c1,c6\n"
             "    data  xsumcb" << i << ".out  d" << i << "1=c1,c7\n"
             "    d" << i << "2 lstyle 1 color " << col[i] << " lwidth 0.05\n"
             "!    d" << i << "1 lstyle 1 color red lwidth 0.05\n";
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tr\n"
             "      text \" accumulated \" lstyle 1 color blue lwidth 0.07\n"
             "!      text \" current \" lstyle 1 color red lwidth 0.07\n"
             "end key\n";
        f.close();
    }
        /*
    {
        ofstream f("s40aff.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " fraction of <40% affinity cells " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data  s048aff.out  d1=c1,c4
                 data  s048aff.out  d2=c1,c2
                 data  s048aff.out  d3=c1,c3
                 d1 lstyle 1 lwidth 0.04
                 d2 lstyle 2 lwidth 0.04
                 d3 lstyle 3 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " produced output " lstyle 1 lwidth 0.04
                   text " CBs " lstyle 2 lwidth 0.04
                   text " CCs " lstyle 3 lwidth 0.04
             end key
";
        f.close();
    }
    {
        ofstream f("s80aff.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " fraction of >=80% affinity cells " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data  s048aff.out  d1=c1,c10
                 data  s048aff.out  d2=c1,c8
                 data  s048aff.out  d3=c1,c9
                 d1 lstyle 1 lwidth 0.04
                 d2 lstyle 2 lwidth 0.04
                 d3 lstyle 3 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " produced output " lstyle 1 lwidth 0.04
                   text " CBs " lstyle 2 lwidth 0.04
                   text " CCs " lstyle 3 lwidth 0.04
             end key
";
        f.close();
    }
    {
        ofstream f("s4080aff.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " fraction of 40-80% affinity cells " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data  s048aff.out  d1=c1,c7
                 data  s048aff.out  d2=c1,c5
                 data  s048aff.out  d3=c1,c6
                 d1 lstyle 1 lwidth 0.04
                 d2 lstyle 2 lwidth 0.04
                 d3 lstyle 3 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " produced output " lstyle 1 lwidth 0.04
                   text " CBs " lstyle 2 lwidth 0.04
                   text " CCs " lstyle 3 lwidth 0.04
             end key
";
        f.close();
    }*/
    {
        ofstream f("saffin.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Mean affinity of BC \" hei .7 font texcmss\n"
             "        xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 max 1\n";
             for(int i = 1; i < nb+1; ++i){
         f << "    data  saffin"<< i << ".out  d"<< i << "1=c1,c2\n"
             "    data  saffin"<< i << ".out  d"<< i << "2=c1,c3\n"
             "    data  saffin"<< i << ".out  d"<< i << "3=c1,c4\n"
             "    data  saffin"<< i << ".out  d"<< i << "4=c1,c5\n"
             "    d"<< i << "1 lstyle 1 lwidth 0.05 color " << col[i] << "\n";
    }
    //         "    d"<< i << "2 lstyle 1 lwidth 0.05 color blue\n"
    //         "    d"<< i << "3 lstyle 1 lwidth 0.05 color green\n"
    //         "    d"<< i << "4 lstyle 1 lwidth 0.05 color grey30\n"
     f <<    "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .4\n"
             "      position br\n"
             "      text \" all GC-BC \" lstyle 1 lwidth 0.07 color black\n"
             "!      text \" div BC \" lstyle 1 lwidth 0.07 color blue\n"
             "!      text \" non-div BC \" lstyle 1 lwidth 0.07 color green\n"
             "!      text \" accumulated output \" lstyle 1 lwidth 0.07 color grey30\n"
             "end key\n";
        f.close();
    }


    {
        ofstream f("selected.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" % of selection events \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 max 1\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data xsumcb" << i << ".out d" << i << "1=c1,c5  ! # of recyclings in last hour\n"
             "    data ssumout" << i << ".out d" << i << "2=c1,c3 ! # of output in last hour\n"
             "    data ssumapo2" << i << ".out d" << i << "3=c1,c3 ! # of apoptosis events in last hour\n"
             "    let d" << i << "4 = d" << i << "1/(d" << i << "1+d" << i << "2+d" << i << "3)\n"
             "    let d" << i << "5 = (d" << i << "1+d" << i << "2)/(d" << i << "1+d" << i << "2+d" << i << "3)\n"
             "!    d" << i << "4 lstyle 1 color red lwidth 0.05\n"
             "    d" << i << "5 lstyle 1 color " << col[i] << " lwidth 0.05\n"
             "!	for check that this is the same as before in reycling.gle:\n"
             "! 	data  xsumcb" << i << ".out  d" << i << "11=c1,c7\n"
             "!  	d" << i << "11 lstyle 1 color black lwidth 0.05\n"
             "!\n";
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tr\n"
             "!      text \" % recycling by TFH \" lstyle 1 color red lwidth 0.07\n"
             "      text \" % positive selection by TFH \" lstyle 1 color blue lwidth 0.07\n"
             "end key\n";
        f.close();
    }

    {
        ofstream f("selected_recycle.gle");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" % of recycling events \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 max 1\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data xsumcb" << i << ".out d" << i << "1=c1,c5  ! # of recyclings in last hour\n"
             "    data ssumout" << i << ".out d" << i << "2=c1,c3 ! # of output in last hour\n"
             "    data ssumapo2" << i << ".out d" << i << "3=c1,c3 ! # of apoptosis events in last hour\n"
             "    let d" << i << "4 = d" << i << "1/(d" << i << "1+d" << i << "2+d" << i << "3)\n"
             "    let d" << i << "5 = (d" << i << "1+d" << i << "2)/(d" << i << "1+d" << i << "2+d" << i << "3)\n"
             "    d" << i << "4 lstyle 1 color " << col[i] << " lwidth 0.05\n"
             "!    d" << i << "5 lstyle 1 color blue lwidth 0.05\n"
             "!	for check that this is the same as before in reycling.gle:\n"
             "! 	data  xsumcb" << i << ".out  d" << i << "11=c1,c7\n"
             "!  	d" << i << "11 lstyle 1 color black lwidth 0.05\n"
             "!\n";
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tr\n"
             "      text \" % recycling by TFH \" lstyle 1 color red lwidth 0.07\n"
             "!      text \" % positive selection by TFH \" lstyle 1 color blue lwidth 0.07\n"
             "end key";
        f.close();
    }
        /*
    {
        ofstream f("sh40affin.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Fraction of more than 0.4 affinity output " hei .7 font texcmss
                     xaxis font texcmss dticks 72 hei .6
                 yaxis font texcmss hei .6
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data  s048aff.out  d1=c1,c7
                 data  s048aff.out  d2=c1,c10
                 let d3 = d1+d2
                 d3 lstyle 1 color black lwidth 0.05
             end graph
             set font texcmss
";
        f.close();
    }*/
        /* later
    {
        ofstream f("shaffin.gle");
        f << ""size 15 15
             amove 1.5 1.5
             begin graph
                 size 12 12
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " fraction of high affinity cells " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0 max 1
             for(int i = 1; i < nb+1; ++i){
         f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                  "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                  "    d"<< i << " marker fcircle color " << col[i];
                  }

                 data  shaffin.out  d1=c1,c4
                 data  shaffin.out  d2=c1,c2
                 data  shaffin.out  d3=c1,c3
                 d1 lstyle 1 lwidth 0.04
                 d2 lstyle 2 lwidth 0.04
                 d3 lstyle 3 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " produced output " lstyle 1 lwidth 0.04
                   text " CBs " lstyle 2 lwidth 0.04
                   text " CCs " lstyle 3 lwidth 0.04
             end key
;
        f.close();
    }
    {
        ofstream f("shaffin_c.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Fraction of high affinity cells " hei .7 font texcmss
                     xaxis hei .5 font texcmss dticks 72
                 yaxis hei .5 font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data  shaffin.out  d1=c1,c4
                 data  shaffin.out  d2=c1,c2
                 data  shaffin.out  d3=c1,c3
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color blue lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .4
                   position br
                   text " accumulated output " lstyle 1 color black lwidth 0.06
                   text " CBs " lstyle 1 color red lwidth 0.06
                   text " CCs " lstyle 1 color blue lwidth 0.06
             end key
";
        f.close();
    }*/
    {
        ofstream f("ssumapo1.gle");
        f << "size 15 15\n"
             "amove 2.5 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" t [h] \" hei .7 font TEXCMR\n"
             "    ytitle \" dead cells on the lattice \" hei .7 font TEXCMR\n"
             "    xaxis font TEXCMR\n"
             "    yaxis font TEXCMR\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 max 10\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  ssumapo1" << i << ".out  d" << i << "1=c1,c2""\n"
             "    d" << i << "1 lstyle 1 lwidth 0.04 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font TEXCMR";
        f.close();
    }
    /*
    {
        ofstream f("ssumapo2.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " Dead and macrophagocyted cells" hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0
             for(int i = 1; i < nb+1; ++i){
         f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                  "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                  "    d"<< i << " marker fcircle color " << col[i];
                  }

                 data  ssumapo2.out  d1=c1,c2
                 d1 lstyle 1 lwidth 0.04
             end graph
             set font TEXCMR
";
        f.close();
    }*/
    {
        ofstream f("ssumapo3.gle");
        f << "size 15 15\n"
             "amove 2.5 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" t [h] \" hei .7 font TEXCMR\n"
             "    ytitle \" dead cell frequency / GC volume\" hei .7 font TEXCMR\n"
             "    xaxis font TEXCMR\n"
             "    yaxis font TEXCMR\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 max 0.1\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  ssumapo2" << i << ".out  d" << i << "1=c1,c3\n"
             "     data  xvolume" << i << ".out d" << i << "2=c1,c4\n"
             "    let d" << i << "3 = d" << i << "1/d" << i << "2\n"
             "    d" << i << "3 lstyle 1 lwidth 0.04 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font TEXCMR";
        f.close();
    }
        /*
    {
        ofstream f("ssumapo3s.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " dead cell frequency / GC volume" hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0 max 0.1
                 data  ssumapo2s.out  d1=c1,c3
                 data  xvolumes.out d2=c1,c4
                 let d3 = d1/d2
                 d3 lstyle 1 lwidth 0.04
             end graph
             set font TEXCMR
";
        f.close();
    }
    */
    {
        ofstream f("ssumcb.gle");
        f << "size 15 15\n"
              "amove 2 2\n"
              "begin graph\n"
              "   size 12 12\n"
             "    fullsize\n"
             "    xtitle \" t [h] \" hei .7 font TEXCMR\n"
             "    ytitle \" total number of centroblasts \" hei .7 font TEXCMR\n"
             "    xaxis font TEXCMR\n"
             "    yaxis font TEXCMR\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0\n";
         for(int i = 1; i < nb+1; ++i){
         f << "    data  ssumcb"<< i << ".out  d"<< i << "1=c1,c2\n"
             "    d"<< i << "1 lstyle 1 lwidth 0.04 color " << col[i] << "\n";
         }
         f <<"end graph\n"
             "set font TEXCMR";
        f.close();
    }
    {
        ofstream f("ssumcc.gle");
        f << "size 15 15\n"
              "amove 2 2\n"
              "begin graph\n"
              "   size 12 12\n"
             "    fullsize\n"
             "    xtitle \" t [h] \" hei .7 font TEXCMR\n"
             "    ytitle \" total number of centrocytes \" hei .7 font TEXCMR\n"
             "    xaxis font TEXCMR\n"
             "    yaxis font TEXCMR\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  ssumcc" << i << ".out  d" << i << "1=c1,c2\n"
             "    d" << i << "1 lstyle 1 lwidth 0.04 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font TEXCMR";
        f.close();
    }
    {
        ofstream f("ssumccco.gle");
        f << "size 21 30\n"
             "amove 2.5 6\n"
             "begin graph\n"
             "    size 15 15\n"
             "    fullsize\n"
             "    xtitle \" t [h] \" hei .7 font TEXCMR\n"
             "    ytitle \" total number of successful CC-FDC contacts \" hei .7 font TEXCMR\n"
             "    xaxis font TEXCMR\n"
             "    yaxis font TEXCMR\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  ssumccco" << i << ".out  d" << i << "1=c1,c2\n"
             "    d" << i << "1 lstyle 1 lwidth 0.04 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font TEXCMR";
        f.close();
    }
    {
        ofstream f("ssumccse.gle");
        f << "size 21 30\n"
             "amove 2.5 6\n"
             "begin graph\n"
             "    size 15 15\n"
             "    fullsize\n"
             "    xtitle \" t [h] \" hei .7 font TEXCMR\n"
             "    ytitle \" total number of selected centrocytes \" hei .7 font TEXCMR\n"
             "    xaxis font TEXCMR\n"
             "    yaxis font TEXCMR\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  ssumccse"<< i << ".out  d"<< i << "1=c1,c2\n"
             "    d"<< i << "1 lstyle 1 lwidth 0.04 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font TEXCMR\n";
        f.close();
    }
    {
        ofstream f("ssumccun.gle");
        f << "size 21 30\n"
             "amove 2.5 6\n"
             "begin graph\n"
             "    size 15 15\n"
             "    fullsize\n"
             "    xtitle \" t [h] \" hei .7 font TEXCMR\n"
             "    ytitle \" total number of still unselected centrocytes \" hei .7 font TEXCMR\n"
             "    xaxis font TEXCMR\n"
             "    yaxis font TEXCMR\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  ssumccun"<< i << ".out  d"<< i << "1=c1,c2\n"
             "    d"<< i << "1 lstyle 1 lwidth 0.04 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font TEXCMR";
        f.close();
    }
    {
        ofstream f("ssumout.gle");
        f << "size 15 15\n"
             "amove 2.5 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Total number of produced output cells \" hei .7 font texcmss\n"
             "        xaxis font texcmss dticks 72\n"
             "    yaxis font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0\n";
             for(int i = 1; i < nb+1; ++i){
         f << "    data  ssumout"<< i << ".out  d"<< i << "1=c1,c2\n"
             "!    data  ssumout"<< i << ".out  d"<< i << "2=c1,c4\n"
             "    d"<< i << "1 lstyle 1 lwidth 0.05 color " << col[i] << "\n"
             "!    d"<< i << "2 lstyle 1 lwidth 0.05 color red\n";
         }
         f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tl\n"
             "      text \" all \" lstyle 1 color black lwidth 0.07\n"
             "!      text \" DEC205+ \" lstyle 1 color red lwidth 0.07\n"
             "end key";
        f.close();
    }
    {
        ofstream f("ssumoute.gle");
        f << "size 21 30\n"
             "amove 2.5 6\n"
             "begin graph\n"
             "    size 15 15\n"
             "    fullsize\n"
             "    xtitle \" t [h] \" hei .7 font TEXCMR\n"
             "    ytitle \" total number of emitted output-cells \" hei .7 font TEXCMR\n"
             "    xaxis font TEXCMR\n"
             "    yaxis font TEXCMR\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  ssumoute"<< i << ".out  d"<< i << "1=c1,c2\n"
             "    d"<< i << "1 lstyle 1 lwidth 0.04 color " << col[i] << "\n";
        }
        f << "end graph\n"
             "set font TEXCMR";
        f.close();
    }
             /* ???
    {
        ofstream f("ssumoutf.gle");
        f << "size 15 15\n"
             "amove 2.5 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" t [h] \" hei .7 font TEXCMR\n"
             "    ytitle \" frequency of produced output-cells \" hei .7 font TEXCMR\n"
             "    xaxis font TEXCMR\n"
             "    yaxis font TEXCMR\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 max 40\n"
             "!	yaxis min 0 max 5000";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  ssumout" << i << ".out  d" << i << "1=c1,c3\n"
             "    d" << i << "1 lstyle 1 lwidth 0.04 color " << col[i];
        }
        f <<"end graph\n"
             "set font TEXCMR";
        f.close();
    }*/

     for(int nc = 0; nc < 5; ++nc)
    {
        stringstream fname;
        fname << "tc_" << nc << "_interac.gle";
        ofstream f(fname.str().c_str());
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Number of TC \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "	  yaxis min 0 max 500\n";
        for(int i = 1; i < nb+1; ++i){
        f << "    data  tc"<< i << ".out d"<< i << "1=c1,c2 ! all\n"
             "    data  tc"<< i << ".out d"<< i << "2=c1,c3 ! 0 contacts\n"
             "    data  tc"<< i << ".out d"<< i << "3=c1,c4 ! 1 contact\n"
             "    data  tc"<< i << ".out d"<< i << "4=c1,c5 ! 2 contacts\n"
             "    data  tc"<< i << ".out d"<< i << "5=c1,c6 ! 3 contacts\n"
             "    data  tc"<< i << ".out d"<< i << "6=c1,c7 ! 4 contacts\n"
             "    data  tc"<< i << ".out d"<< i << "7=c1,c8 ! 5 contacts\n"
             "    data  tc"<< i << ".out d"<< i << "8=c1,c9 ! 6 contacts\n"
             "    d"<< i << nc+1 << " lstyle 1 color " << col[i] << " lwidth 0.05\n"
             "!    d"<< i << "2 lstyle 1 color blue lwidth 0.05\n"
             "!    d"<< i << "3 lstyle 1 color red lwidth 0.05\n"
             "!    d"<< i << "4 lstyle 1 color green lwidth 0.05\n"
             "!    d"<< i << "5 lstyle 1 color magenta lwidth 0.05\n"
             "!    d"<< i << "6 lstyle 1 color orange lwidth 0.05\n"
             "!    d"<< i << "7 lstyle 1 color cyan lwidth 0.05\n"
             "!    d"<< i << "8 lstyle 1 color grey30 lwidth 0.05\n";
        }
        f << "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .4\n"
             "      position tr\n";
        if(nc == 0) f << "      text \" all TC \" lstyle 1 color black lwidth 0.07\n";
        else f << "      text \"" << nc-1 << " BC in contact \" lstyle 1 color black lwidth 0.07\n";
        f << "!      text \" 1 BC in contact \" lstyle 1 color red lwidth 0.07\n"
             "!      text \" 2 BC in contact \" lstyle 1 color green lwidth 0.07\n"
             "!      text \" 3 BC in contact \" lstyle 1 color magenta lwidth 0.07\n"
             "!      text \" 4 BC in contact \" lstyle 1 color orange lwidth 0.07\n"
             "!      text \" 5 BC in contact \" lstyle 1 color cyan lwidth 0.07\n"
             "!      text \" 6 BC in contact \" lstyle 1 color grey30 lwidth 0.07\n"
             "end key";
        f.close();
    }
     /*
    {
        ofstream f("test-gauss-rand.gle");
        f << ""size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " x " hei .7 font TEXCMR
                 ytitle " N[x] " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
             !	xaxis min 0 max 504
             !	yaxis min 0 max 0.3
                 data  anabla.out  d1=c1,c2
                 data  anabla.out  d2=c1,c1
                 let d3 = 2565.7*exp(-1.0*d2*d2/(2.25))
                 d1 lstyle 1 lwidth 0.05 color black
                 d3 lstyle 3 lwidth 0.05 color red
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tr
             !      text " all CCs " lstyle 1 lwidth 0.06 color black
             !      text " unselected CCs " lstyle 2 lwidth 0.06 color black
             !      text " CCs in contact to FDCs " lstyle 3 lwidth 0.06 color blue
             !      text " selected CCs " lstyle 4 lwidth 0.06 color green
             !      text " apoptotic cells " lstyle 5 lwidth 0.06 color red
             !end key
;
        f.close();
    }
    {
        ofstream f("trace_a0001.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
               size 12 12
               fullsize
               xtitle " y [microns]" hei .7 font TEXCMR
               x2title " CB-tracking in GC " hei .7 font TEXCMR
               ytitle " z [microns]" hei .7 font TEXCMR
               xaxis font TEXCMR
               yaxis font TEXCMR
               xaxis min 0 max 325
               yaxis min 0 max 325
               data  trace_end0001.out d99=c2,c3
               d99 marker ftriangle msize 0.3 color black
               data  trace_dead0001.out d100=c2,c3
               d100 marker triangle msize 0.3 color red
               data  trace_begin0001.out d101=c2,c3
               d101 marker ftriangle msize 0.3 color green
               data  trace_a_a0001.out d1=c3,c4
               d1 lstyle 1 lwidth 0.06 color black
               data  trace_a_b0001.out d2=c3,c4
               d2 lstyle 1 lwidth 0.06 color blue
               data  trace_a_c0001.out d3=c3,c4
               d3 lstyle 1 lwidth 0.06 color cyan
               data  trace_a_d0001.out d4=c3,c4
               d4 lstyle 1 lwidth 0.06 color brown
               data  trace_a_f0001.out d5=c3,c4
               d5 lstyle 1 lwidth 0.06 color black
               data  trace_a_f0001.out d6=c6,c7
               d6 lstyle 1 lwidth 0.06 color magenta
               data  trace_a_g0001.out d7=c3,c4
               d7 lstyle 1 lwidth 0.06 color red
               data  trace_a_h0001.out d8=c3,c4
               d8 lstyle 1 lwidth 0.06 color magenta
             end graph
             begin key
               hei .4
               position tr
               text " CB-begin " marker ftriangle msize 0.5 color green
               text " CB-end " marker ftriangle msize 0.5 color black
               text " dead CB " marker triangle msize 0.5 color red
             end key
";
        f.close();
    }
    {
        ofstream f("trace_a0002.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
               size 12 12
               fullsize
               xtitle " y [microns]" hei .7 font TEXCMR
               x2title " CC-tracking in GC " hei .7 font TEXCMR
               ytitle " z [microns]" hei .7 font TEXCMR
               xaxis font TEXCMR
               yaxis font TEXCMR
               xaxis min 0 max 325
               yaxis min 0 max 325
               data  trace_end0002.out d99=c2,c3
               d99 marker fcircle msize 0.3 color black
               data  trace_dead0002.out d100=c2,c3
               d100 marker circle msize 0.3 color red
               data  trace_begin0002.out d101=c2,c3
               d101 marker fcircle msize 0.3 color green
               data  trace_a_a0002.out d1=c3,c4
               d1 lstyle 1 lwidth 0.06 color blue
               data  trace_a_a0002.out d2=c6,c7
               d2 lstyle 1 lwidth 0.06 color red
               data  trace_a_a0002.out d3=c9,c10
               d3 lstyle 1 lwidth 0.06 color green
               data  trace_a_a0002.out d4=c12,c13
               d4 lstyle 1 lwidth 0.06 color yellow
               data  trace_a_a0002.out d5=c15,c16
               d5 lstyle 1 lwidth 0.06 color cyan
               data  trace_a_a0002.out d6=c18,c19
               d6 lstyle 1 lwidth 0.06 color magenta
               data  trace_a_a0002.out d7=c21,c22
               d7 lstyle 1 lwidth 0.06 color orange
               data  trace_a_a0002.out d8=c24,c25
               d8 lstyle 1 lwidth 0.06 color brown
               data  trace_a_a0002.out d9=c27,c28
               d9 lstyle 1 lwidth 0.06 color grey
               data  trace_a_b0002.out d10=c3,c4
               d10 lstyle 1 lwidth 0.06 color black
               data  trace_a_b0002.out d11=c6,c7
               d11 lstyle 1 lwidth 0.06 color red
               data  trace_a_b0002.out d12=c9,c10
               d12 lstyle 1 lwidth 0.06 color green
               data  trace_a_b0002.out d13=c12,c13
               d13 lstyle 1 lwidth 0.06 color yellow
               data  trace_a_b0002.out d14=c15,c16
               d14 lstyle 1 lwidth 0.06 color cyan
               data  trace_a_b0002.out d15=c18,c19
               d15 lstyle 1 lwidth 0.06 color magenta
               data  trace_a_b0002.out d16=c21,c22
               d16 lstyle 1 lwidth 0.06 color orange
               data  trace_a_b0002.out d17=c24,c25
               d17 lstyle 1 lwidth 0.06 color brown
               data  trace_a_b0002.out d18=c27,c28
               d18 lstyle 1 lwidth 0.06 color grey
               data  trace_a_c0002.out d19=c3,c4
               d19 lstyle 1 lwidth 0.06 color blue
               data  trace_a_c0002.out d20=c6,c7
               d20 lstyle 1 lwidth 0.06 color red
               data  trace_a_c0002.out d21=c9,c10
               d21 lstyle 1 lwidth 0.06 color green
               data  trace_a_c0002.out d22=c12,c13
               d22 lstyle 1 lwidth 0.06 color yellow
               data  trace_a_c0002.out d23=c15,c16
               d23 lstyle 1 lwidth 0.06 color magenta
               data  trace_a_c0002.out d24=c18,c19
               d24 lstyle 1 lwidth 0.06 color orange
               data  trace_a_c0002.out d25=c21,c22
               d25 lstyle 1 lwidth 0.06 color grey
               data  trace_a_d0002.out d26=c3,c4
               d26 lstyle 1 lwidth 0.06 color black
               data  trace_a_d0002.out d27=c6,c7
               d27 lstyle 1 lwidth 0.06 color blue
               data  trace_a_d0002.out d28=c9,c10
               d28 lstyle 1 lwidth 0.06 color red
               data  trace_a_d0002.out d29=c12,c13
               d29 lstyle 1 lwidth 0.06 color green
               data  trace_a_d0002.out d30=c15,c16
               d30 lstyle 1 lwidth 0.06 color yellow
               data  trace_a_d0002.out d31=c18,c19
               d31 lstyle 1 lwidth 0.06 color cyan
               data  trace_a_d0002.out d32=c21,c22
               d32 lstyle 1 lwidth 0.06 color magenta
               data  trace_a_d0002.out d33=c24,c25
               d33 lstyle 1 lwidth 0.06 color orange
               data  trace_a_d0002.out d34=c27,c28
               d34 lstyle 1 lwidth 0.06 color grey
               data  trace_a_e0002.out d35=c3,c4
               d35 lstyle 1 lwidth 0.06 color black
               data  trace_a_e0002.out d36=c6,c7
               d36 lstyle 1 lwidth 0.06 color blue
               data  trace_a_e0002.out d37=c9,c10
               d37 lstyle 1 lwidth 0.06 color red
               data  trace_a_e0002.out d38=c12,c13
               d38 lstyle 1 lwidth 0.06 color green
               data  trace_a_e0002.out d39=c15,c16
               d39 lstyle 1 lwidth 0.06 color yellow
               data  trace_a_e0002.out d40=c18,c19
               d40 lstyle 1 lwidth 0.06 color cyan
               data  trace_a_e0002.out d41=c21,c22
               d41 lstyle 1 lwidth 0.06 color magenta
               data  trace_a_e0002.out d42=c24,c25
               d42 lstyle 1 lwidth 0.06 color orange
               data  trace_a_e0002.out d43=c27,c28
               d43 lstyle 1 lwidth 0.06 color brown
               data  trace_a_e0002.out d44=c30,c31
               d44 lstyle 1 lwidth 0.06 color grey
               data  trace_a_f0002.out d45=c3,c4
               d45 lstyle 1 lwidth 0.06 color blue
               data  trace_a_f0002.out d46=c6,c7
               d46 lstyle 1 lwidth 0.06 color red
               data  trace_a_f0002.out d47=c9,c10
               d47 lstyle 1 lwidth 0.06 color green
               data  trace_a_f0002.out d48=c12,c13
               d48 lstyle 1 lwidth 0.06 color yellow
               data  trace_a_f0002.out d49=c15,c16
               d49 lstyle 1 lwidth 0.06 color cyan
               data  trace_a_f0002.out d50=c18,c19
               d50 lstyle 1 lwidth 0.06 color orange
               data  trace_a_f0002.out d51=c21,c22
               d51 lstyle 1 lwidth 0.06 color brown
               data  trace_a_f0002.out d52=c24,c25
               d52 lstyle 1 lwidth 0.06 color grey
               data  trace_a_g0002.out d53=c3,c4
               d53 lstyle 1 lwidth 0.06 color black
               data  trace_a_g0002.out d54=c6,c7
               d54 lstyle 1 lwidth 0.06 color blue
               data  trace_a_g0002.out d55=c9,c10
               d55 lstyle 1 lwidth 0.06 color green
               data  trace_a_g0002.out d56=c12,c13
               d56 lstyle 1 lwidth 0.06 color yellow
               data  trace_a_g0002.out d57=c15,c16
               d57 lstyle 1 lwidth 0.06 color cyan
               data  trace_a_g0002.out d58=c18,c19
               d58 lstyle 1 lwidth 0.06 color magenta
               data  trace_a_g0002.out d59=c21,c22
               d59 lstyle 1 lwidth 0.06 color orange
               data  trace_a_g0002.out d60=c24,c25
               d60 lstyle 1 lwidth 0.06 color grey
               data  trace_a_h0002.out d61=c3,c4
               d61 lstyle 1 lwidth 0.06 color black
               data  trace_a_h0002.out d62=c6,c7
               d62 lstyle 1 lwidth 0.06 color blue
               data  trace_a_h0002.out d63=c9,c10
               d63 lstyle 1 lwidth 0.06 color red
               data  trace_a_h0002.out d64=c12,c13
               d64 lstyle 1 lwidth 0.06 color green
               data  trace_a_h0002.out d65=c15,c16
               d65 lstyle 1 lwidth 0.06 color yellow
               data  trace_a_h0002.out d66=c18,c19
               d66 lstyle 1 lwidth 0.06 color cyan
               data  trace_a_h0002.out d67=c21,c22
               d67 lstyle 1 lwidth 0.06 color brown
               data  trace_a_h0002.out d68=c24,c25
               d68 lstyle 1 lwidth 0.06 color grey
               data  trace_a_i0002.out d69=c3,c4
               d69 lstyle 1 lwidth 0.06 color black
               data  trace_a_i0002.out d70=c6,c7
               d70 lstyle 1 lwidth 0.06 color blue
               data  trace_a_i0002.out d71=c9,c10
               d71 lstyle 1 lwidth 0.06 color green
               data  trace_a_i0002.out d72=c12,c13
               d72 lstyle 1 lwidth 0.06 color yellow
               data  trace_a_i0002.out d73=c15,c16
               d73 lstyle 1 lwidth 0.06 color cyan
               data  trace_a_i0002.out d74=c18,c19
               d74 lstyle 1 lwidth 0.06 color magenta
               data  trace_a_i0002.out d75=c21,c22
               d75 lstyle 1 lwidth 0.06 color orange
               data  trace_a_i0002.out d76=c24,c25
               d76 lstyle 1 lwidth 0.06 color brown
             end graph
             begin key
               hei .4
               position tr
               text " CC-begin " marker fcircle msize 0.5 color green
               text " CC-end " marker fcircle msize 0.5 color black
               text " dead CC " marker circle msize 0.5 color red
             end key
";
        f.close();
    }
    {
        ofstream f("trace_a0004.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
               size 12 12
               fullsize
               xtitle " x [microns]" hei .7 font TEXCMR
               x2title " OUT-tracking in GC " hei .7 font TEXCMR
               ytitle " y [microns]" hei .7 font TEXCMR
               xaxis font TEXCMR
               yaxis font TEXCMR
               xaxis min 0 max 450
               yaxis min 0 max 450
               data  trace_end0004.out d9451=c1,c2
               d9451 marker fdiamond msize 0.3 color black
               data  trace_dead0004.out d9452=c1,c2
               d9452 marker diamond msize 0.3 color red
               data  trace_begin0004.out d9453=c1,c2
               d9453 marker fdiamond msize 0.3 color green
               data  trace_a_";
        f.close();
    }
    {
        ofstream f("trace_a0005.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
               size 12 12
               fullsize
               xtitle " y [microns]" hei .7 font TEXCMR
               x2title " TC-tracking in GC " hei .7 font TEXCMR
               ytitle " z [microns]" hei .7 font TEXCMR
               xaxis font TEXCMR
               yaxis font TEXCMR
               xaxis min 0 max 325
               yaxis min 0 max 325
               data  trace_end0005.out d99=c2,c3
               d99 marker fsquare msize 0.3 color black
               data  trace_dead0005.out d100=c2,c3
               d100 marker square msize 0.3 color red
               data  trace_begin0005.out d101=c2,c3
               d101 marker fsquare msize 0.3 color green
               data  trace_a_i0005.out d1=c3,c4
               d1 lstyle 1 lwidth 0.06 color grey
               data  trace_a_j0005.out d2=c3,c4
               d2 lstyle 1 lwidth 0.06 color black
               data  trace_a_j0005.out d3=c6,c7
               d3 lstyle 1 lwidth 0.06 color blue
               data  trace_a_j0005.out d4=c9,c10
               d4 lstyle 1 lwidth 0.06 color red
               data  trace_a_j0005.out d5=c12,c13
               d5 lstyle 1 lwidth 0.06 color green
               data  trace_a_j0005.out d6=c15,c16
               d6 lstyle 1 lwidth 0.06 color yellow
               data  trace_a_j0005.out d7=c18,c19
               d7 lstyle 1 lwidth 0.06 color cyan
               data  trace_a_j0005.out d8=c21,c22
               d8 lstyle 1 lwidth 0.06 color magenta
               data  trace_a_j0005.out d9=c24,c25
               d9 lstyle 1 lwidth 0.06 color orange
             end graph
             begin key
               hei .4
               position tr
               text " TC-begin " marker fsquare msize 0.5 color green
               text " TC-end " marker fsquare msize 0.5 color black
               text " dead TC " marker square msize 0.5 color red
             end key
";
        f.close();
    }
    {
        ofstream f("trace_a0008.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
               size 12 12
               fullsize
               xtitle " x [microns]" hei .7 font TEXCMR
               x2title " -tracking in GC " hei .7 font TEXCMR
               ytitle " y [microns]" hei .7 font TEXCMR
               xaxis font TEXCMR
               yaxis font TEXCMR
               xaxis min 0 max 450
               yaxis min 0 max 450
               data  trace_end0008.out d11=c1,c2
               d11 marker fstar msize 0.3 color black
               data  trace_dead0008.out d12=c1,c2
               d12 marker star msize 0.3 color red
               data  trace_begin0008.out d13=c1,c2
               d13 marker fstar msize 0.3 color green
               data  trace_a_a0008.out d1=c2,c3
               d1 lstyle 1 lwidth 0.06 color black
               data  trace_a_a0008.out d2=c5,c6
               d2 lstyle 1 lwidth 0.06 color blue
               data  trace_a_a0008.out d3=c8,c9
               d3 lstyle 1 lwidth 0.06 color red
               data  trace_a_a0008.out d4=c11,c12
               d4 lstyle 1 lwidth 0.06 color green
               data  trace_a_a0008.out d5=c14,c15
               d5 lstyle 1 lwidth 0.06 color yellow
               data  trace_a_a0008.out d6=c17,c18
               d6 lstyle 1 lwidth 0.06 color cyan
               data  trace_a_a0008.out d7=c20,c21
               d7 lstyle 1 lwidth 0.06 color magenta
               data  trace_a_a0008.out d8=c23,c24
               d8 lstyle 1 lwidth 0.06 color orange
               data  trace_a_a0008.out d9=c26,c27
               d9 lstyle 1 lwidth 0.06 color brown
               data  trace_a_a0008.out d10=c29,c30
               d10 lstyle 1 lwidth 0.06 color grey
             end graph
             begin key
               hei .4
               position tr
               text " -begin " marker fstar msize 0.5 color green
               text " -end " marker fstar msize 0.5 color black
               text " dead  " marker star msize 0.5 color red
             end key
";
        f.close();
    }
    {
        ofstream f("trace_a_cb.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
                 size 12 12
                 fullsize
                 xtitle " x [microns]" hei .7 font TEXCMR
                 x2title " CB-tracking in GC " hei .7 font TEXCMR
                 ytitle " y [microns]" hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 450
                 yaxis min 0 max 450
             !
                 data  trace_end0001.out d51=c1,c2 ! CB
                 data  trace_dead0001.out d55=c1,c2
                 data  trace_begin0001.out d59=c1,c2
                 d51 marker fsquare msize 0.3
                 d55 marker square msize 0.3
                 d59 marker ftriangle msize 0.3
             !
                 data  trace_a0001.out  d1=c2,c3
                 d1 lstyle 1 lwidth 0.06 color black
                 data  trace_a0001.out  d2=c5,c6
                 d2 lstyle 1 lwidth 0.06 color blue
                 data  trace_a0001.out  d3=c8,c9
                 d3 lstyle 1 lwidth 0.06 color red
                 data  trace_a0001.out  d4=c11,c12
                 d4 lstyle 1 lwidth 0.06 color green
                 data  trace_a0001.out  d5=c14,c15
                 d5 lstyle 1 lwidth 0.06 color yellow
             ! 	data  trace_a0001.out  d6=c17,c18
             !  	d6 lstyle 1 lwidth 0.06 color cyan
             ! 	data  trace_a0001.out  d7=c20,c21
             !  	d7 lstyle 1 lwidth 0.06 color magenta
             ! 	data  trace_a0001.out  d8=c23,c24
             !  	d8 lstyle 1 lwidth 0.06 color orange
             ! 	data  trace_a0001.out  d9=c26,c27
             !  	d9 lstyle 1 lwidth 0.06 color brown
             ! 	data  trace_a0001.out  d10=c29,c30
             !  	d10 lstyle 1 lwidth 0.06 color grey
             end graph
             begin key
                   hei .4
                   position tr
                   text " CB-start " marker ftriangle msize 0.5
                   text " CB-ends " marker fsquare msize 0.5
                   text " dead CB " marker square msize 0.5
             end key
";
        f.close();
    }
    {
        ofstream f("trace_a_cc.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
                 size 12 12
                 fullsize
                 xtitle " x [microns]" hei .7 font TEXCMR
                 x2title " CC-tracking in GC " hei .7 font TEXCMR
                 ytitle " y [microns]" hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 450
                 yaxis min 0 max 450
             !
                 data  trace_end0002.out d52=c1,c2 ! CC
                 data  trace_dead0002.out d56=c1,c2
                 data  trace_begin0002.out d60=c1,c2
                 d52 marker fcircle msize 0.3
                 d56 marker circle msize 0.3
                 d60 marker fdiamond msize 0.3
             !
                 data  trace_a0002.out  d1=c2,c3
                 d1 lstyle 1 lwidth 0.06 color black
                 data  trace_a0002.out  d2=c5,c6
                 d2 lstyle 1 lwidth 0.06 color blue
                 data  trace_a0002.out  d3=c8,c9
                 d3 lstyle 1 lwidth 0.06 color red
                 data  trace_a0002.out  d4=c11,c12
                 d4 lstyle 1 lwidth 0.06 color green
                 data  trace_a0002.out  d5=c14,c15
                 d5 lstyle 1 lwidth 0.06 color yellow
             ! 	data  trace_a0002.out  d6=c17,c18
             !  	d6 lstyle 1 lwidth 0.06 color cyan
             ! 	data  trace_a0002.out  d7=c20,c21
             !  	d7 lstyle 1 lwidth 0.06 color magenta
             ! 	data  trace_a0002.out  d8=c23,c24
             !  	d8 lstyle 1 lwidth 0.06 color orange
             ! 	data  trace_a0002.out  d9=c26,c27
             !  	d9 lstyle 1 lwidth 0.06 color brown
             ! 	data  trace_a0002.out  d10=c29,c30
             !  	d10 lstyle 1 lwidth 0.06 color grey
             end graph
             begin key
                   hei .4
                   position tr
                   text " CC-start " marker fdiamond msize 0.5
                   text " CC-ends " marker fcircle msize 0.5
                   text " dead CC " marker circle msize 0.5
             end key
";
        f.close();
    }
    {
        ofstream f("traces_a.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
               size 12 12
               fullsize
               xtitle " y [microns]" hei .7 font TEXCMR
               x2title " cell-tracking in the GC " hei .7 font TEXCMR
               ytitle " z [microns]" hei .7 font TEXCMR
               xaxis font TEXCMR
               yaxis font TEXCMR
               xaxis min 0 max 450
               yaxis min 0 max 450
               data  trace_end0001.out d99=c2,c3
               d99 marker ftriangle msize 0.3 color black
               data  trace_dead0001.out d107=c2,c3
               d107 marker triangle msize 0.3 color red
               data  trace_begin0001.out d115=c2,c3
               d115 marker ftriangle msize 0.3 color green
               data  trace_end0002.out d100=c2,c3
               d100 marker fcircle msize 0.3 color black
               data  trace_dead0002.out d108=c2,c3
               d108 marker circle msize 0.3 color red
               data  trace_begin0002.out d116=c2,c3
               d116 marker fcircle msize 0.3 color green
               data  trace_end0005.out d103=c2,c3
               d103 marker fsquare msize 0.3 color black
               data  trace_dead0005.out d111=c2,c3
               d111 marker square msize 0.3 color red
               data  trace_begin0005.out d119=c2,c3
               d119 marker fsquare msize 0.3 color green
               data  traces_a_a.out  d1=c3,c4
               d1 lstyle 1 lwidth 0.06 color black
               data  traces_a_a.out  d2=c6,c7
               d2 lstyle 1 lwidth 0.06 color blue
               data  traces_a_a.out  d3=c9,c10
               d3 lstyle 1 lwidth 0.06 color red
               data  traces_a_a.out  d4=c12,c13
               d4 lstyle 1 lwidth 0.06 color green
               data  traces_a_a.out  d5=c15,c16
               d5 lstyle 1 lwidth 0.06 color yellow
               data  traces_a_a.out  d6=c18,c19
               d6 lstyle 1 lwidth 0.06 color cyan
               data  traces_a_a.out  d7=c21,c22
               d7 lstyle 1 lwidth 0.06 color magenta
               data  traces_a_a.out  d8=c24,c25
               d8 lstyle 1 lwidth 0.06 color orange
               data  traces_a_a.out  d9=c27,c28
               d9 lstyle 1 lwidth 0.06 color brown
               data  traces_a_a.out  d10=c30,c31
               d10 lstyle 1 lwidth 0.06 color grey
               data  traces_a_b.out  d11=c3,c4
               d11 lstyle 1 lwidth 0.06 color black
               data  traces_a_b.out  d12=c6,c7
               d12 lstyle 1 lwidth 0.06 color blue
               data  traces_a_b.out  d13=c9,c10
               d13 lstyle 1 lwidth 0.06 color red
               data  traces_a_b.out  d14=c12,c13
               d14 lstyle 1 lwidth 0.06 color green
               data  traces_a_b.out  d15=c15,c16
               d15 lstyle 1 lwidth 0.06 color yellow
               data  traces_a_b.out  d16=c18,c19
               d16 lstyle 1 lwidth 0.06 color cyan
               data  traces_a_b.out  d17=c21,c22
               d17 lstyle 1 lwidth 0.06 color magenta
               data  traces_a_b.out  d18=c24,c25
               d18 lstyle 1 lwidth 0.06 color orange
               data  traces_a_b.out  d19=c27,c28
               d19 lstyle 1 lwidth 0.06 color brown
               data  traces_a_b.out  d20=c30,c31
               d20 lstyle 1 lwidth 0.06 color grey
               data  traces_a_c.out  d21=c3,c4
               d21 lstyle 1 lwidth 0.06 color black
               data  traces_a_c.out  d22=c6,c7
               d22 lstyle 1 lwidth 0.06 color blue
               data  traces_a_c.out  d23=c9,c10
               d23 lstyle 1 lwidth 0.06 color red
               data  traces_a_c.out  d24=c12,c13
               d24 lstyle 1 lwidth 0.06 color green
               data  traces_a_c.out  d25=c15,c16
               d25 lstyle 1 lwidth 0.06 color yellow
               data  traces_a_c.out  d26=c18,c19
               d26 lstyle 1 lwidth 0.06 color cyan
               data  traces_a_c.out  d27=c21,c22
               d27 lstyle 1 lwidth 0.06 color magenta
               data  traces_a_c.out  d28=c24,c25
               d28 lstyle 1 lwidth 0.06 color orange
               data  traces_a_c.out  d29=c27,c28
               d29 lstyle 1 lwidth 0.06 color brown
               data  traces_a_c.out  d30=c30,c31
               d30 lstyle 1 lwidth 0.06 color grey
               data  traces_a_d.out  d31=c3,c4
               d31 lstyle 1 lwidth 0.06 color black
               data  traces_a_d.out  d32=c6,c7
               d32 lstyle 1 lwidth 0.06 color blue
               data  traces_a_d.out  d33=c9,c10
               d33 lstyle 1 lwidth 0.06 color red
               data  traces_a_d.out  d34=c12,c13
               d34 lstyle 1 lwidth 0.06 color green
               data  traces_a_d.out  d35=c15,c16
               d35 lstyle 1 lwidth 0.06 color yellow
               data  traces_a_d.out  d36=c18,c19
               d36 lstyle 1 lwidth 0.06 color cyan
               data  traces_a_d.out  d37=c21,c22
               d37 lstyle 1 lwidth 0.06 color magenta
               data  traces_a_d.out  d38=c24,c25
               d38 lstyle 1 lwidth 0.06 color orange
               data  traces_a_d.out  d39=c27,c28
               d39 lstyle 1 lwidth 0.06 color brown
               data  traces_a_d.out  d40=c30,c31
               d40 lstyle 1 lwidth 0.06 color grey
               data  traces_a_e.out  d41=c3,c4
               d41 lstyle 1 lwidth 0.06 color black
               data  traces_a_e.out  d42=c6,c7
               d42 lstyle 1 lwidth 0.06 color blue
               data  traces_a_e.out  d43=c9,c10
               d43 lstyle 1 lwidth 0.06 color red
               data  traces_a_e.out  d44=c12,c13
               d44 lstyle 1 lwidth 0.06 color green
               data  traces_a_e.out  d45=c15,c16
               d45 lstyle 1 lwidth 0.06 color yellow
               data  traces_a_e.out  d46=c18,c19
               d46 lstyle 1 lwidth 0.06 color cyan
               data  traces_a_e.out  d47=c21,c22
               d47 lstyle 1 lwidth 0.06 color magenta
               data  traces_a_e.out  d48=c24,c25
               d48 lstyle 1 lwidth 0.06 color orange
               data  traces_a_e.out  d49=c27,c28
               d49 lstyle 1 lwidth 0.06 color brown
               data  traces_a_e.out  d50=c30,c31
               d50 lstyle 1 lwidth 0.06 color grey
               data  traces_a_f.out  d51=c3,c4
               d51 lstyle 1 lwidth 0.06 color black
               data  traces_a_f.out  d52=c6,c7
               d52 lstyle 1 lwidth 0.06 color blue
               data  traces_a_f.out  d53=c9,c10
               d53 lstyle 1 lwidth 0.06 color red
               data  traces_a_f.out  d54=c12,c13
               d54 lstyle 1 lwidth 0.06 color green
               data  traces_a_f.out  d55=c15,c16
               d55 lstyle 1 lwidth 0.06 color yellow
               data  traces_a_f.out  d56=c18,c19
               d56 lstyle 1 lwidth 0.06 color cyan
               data  traces_a_f.out  d57=c21,c22
               d57 lstyle 1 lwidth 0.06 color magenta
               data  traces_a_f.out  d58=c24,c25
               d58 lstyle 1 lwidth 0.06 color orange
               data  traces_a_f.out  d59=c27,c28
               d59 lstyle 1 lwidth 0.06 color brown
               data  traces_a_f.out  d60=c30,c31
               d60 lstyle 1 lwidth 0.06 color grey
               data  traces_a_g.out  d61=c3,c4
               d61 lstyle 1 lwidth 0.06 color black
               data  traces_a_g.out  d62=c6,c7
               d62 lstyle 1 lwidth 0.06 color blue
               data  traces_a_g.out  d63=c9,c10
               d63 lstyle 1 lwidth 0.06 color red
               data  traces_a_g.out  d64=c12,c13
               d64 lstyle 1 lwidth 0.06 color green
               data  traces_a_g.out  d65=c15,c16
               d65 lstyle 1 lwidth 0.06 color yellow
               data  traces_a_g.out  d66=c18,c19
               d66 lstyle 1 lwidth 0.06 color cyan
               data  traces_a_g.out  d67=c21,c22
               d67 lstyle 1 lwidth 0.06 color magenta
               data  traces_a_g.out  d68=c24,c25
               d68 lstyle 1 lwidth 0.06 color orange
               data  traces_a_g.out  d69=c27,c28
               d69 lstyle 1 lwidth 0.06 color brown
               data  traces_a_g.out  d70=c30,c31
               d70 lstyle 1 lwidth 0.06 color grey
               data  traces_a_h.out  d71=c3,c4
               d71 lstyle 1 lwidth 0.06 color black
               data  traces_a_h.out  d72=c6,c7
               d72 lstyle 1 lwidth 0.06 color blue
               data  traces_a_h.out  d73=c9,c10
               d73 lstyle 1 lwidth 0.06 color red
               data  traces_a_h.out  d74=c12,c13
               d74 lstyle 1 lwidth 0.06 color green
               data  traces_a_h.out  d75=c15,c16
               d75 lstyle 1 lwidth 0.06 color yellow
               data  traces_a_h.out  d76=c18,c19
               d76 lstyle 1 lwidth 0.06 color cyan
               data  traces_a_h.out  d77=c21,c22
               d77 lstyle 1 lwidth 0.06 color magenta
               data  traces_a_h.out  d78=c24,c25
               d78 lstyle 1 lwidth 0.06 color orange
               data  traces_a_h.out  d79=c27,c28
               d79 lstyle 1 lwidth 0.06 color brown
               data  traces_a_h.out  d80=c30,c31
               d80 lstyle 1 lwidth 0.06 color grey
               data  traces_a_i.out  d81=c3,c4
               d81 lstyle 1 lwidth 0.06 color black
               data  traces_a_i.out  d82=c6,c7
               d82 lstyle 1 lwidth 0.06 color blue
               data  traces_a_i.out  d83=c9,c10
               d83 lstyle 1 lwidth 0.06 color red
               data  traces_a_i.out  d84=c12,c13
               d84 lstyle 1 lwidth 0.06 color green
               data  traces_a_i.out  d85=c15,c16
               d85 lstyle 1 lwidth 0.06 color yellow
               data  traces_a_i.out  d86=c18,c19
               d86 lstyle 1 lwidth 0.06 color cyan
               data  traces_a_i.out  d87=c21,c22
               d87 lstyle 1 lwidth 0.06 color magenta
               data  traces_a_i.out  d88=c24,c25
               d88 lstyle 1 lwidth 0.06 color orange
               data  traces_a_i.out  d89=c27,c28
               d89 lstyle 1 lwidth 0.06 color brown
               data  traces_a_i.out  d90=c30,c31
               d90 lstyle 1 lwidth 0.06 color grey
               data  traces_a_j.out  d91=c3,c4
               d91 lstyle 1 lwidth 0.06 color black
               data  traces_a_j.out  d92=c6,c7
               d92 lstyle 1 lwidth 0.06 color blue
               data  traces_a_j.out  d93=c9,c10
               d93 lstyle 1 lwidth 0.06 color red
               data  traces_a_j.out  d94=c12,c13
               d94 lstyle 1 lwidth 0.06 color green
               data  traces_a_j.out  d95=c15,c16
               d95 lstyle 1 lwidth 0.06 color yellow
               data  traces_a_j.out  d96=c18,c19
               d96 lstyle 1 lwidth 0.06 color cyan
               data  traces_a_j.out  d97=c21,c22
               d97 lstyle 1 lwidth 0.06 color magenta
               data  traces_a_j.out  d98=c24,c25
               d98 lstyle 1 lwidth 0.06 color orange
             end graph
             begin key
               hei .4
               position tr
               text " CB-begin " marker ftriangle msize 0.5 color green
               text " CB-end " marker ftriangle msize 0.5 color black
               text " dead CB " marker triangle msize 0.5 color red
               text " CC-begin " marker fcircle msize 0.5 color green
               text " CC-end " marker fcircle msize 0.5 color black
               text " dead CC " marker circle msize 0.5 color red
               text " TC-begin " marker fsquare msize 0.5 color green
               text " TC-end " marker fsquare msize 0.5 color black
               text " dead TC " marker square msize 0.5 color red
             end key
";
        f.close();
    }
    {
        ofstream f("traces_r.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
               size 12 12
               fullsize
               xtitle " y [microns]" hei .7 font TEXCMR
               x2title " cell-traces starting at (0,0) " hei .7 font TEXCMR
               ytitle " z [microns]" hei .7 font TEXCMR
               xaxis font TEXCMR
               yaxis font TEXCMR
               xaxis min -150 max 150
               yaxis min -150 max 150
               data  trace_end0001.out d99=c5,c6
               d99 marker ftriangle msize 0.4 color black
               data  trace_dead0001.out d107=c5,c6
               d107 marker triangle msize 0.4 color red
               data  trace_end0002.out d100=c5,c6
               d100 marker fcircle msize 0.4 color black
               data  trace_dead0002.out d108=c5,c6
               d108 marker circle msize 0.4 color red
               data  trace_end0005.out d103=c5,c6
               d103 marker fsquare msize 0.4 color black
               data  trace_dead0005.out d111=c5,c6
               d111 marker square msize 0.4 color red
               data  traces_r_a.out  d1=c3,c4
               d1 lstyle 1 lwidth 0.06 color black
               data  traces_r_a.out  d2=c6,c7
               d2 lstyle 1 lwidth 0.06 color blue
               data  traces_r_a.out  d3=c9,c10
               d3 lstyle 1 lwidth 0.06 color red
               data  traces_r_a.out  d4=c12,c13
               d4 lstyle 1 lwidth 0.06 color green
               data  traces_r_a.out  d5=c15,c16
               d5 lstyle 1 lwidth 0.06 color yellow
               data  traces_r_a.out  d6=c18,c19
               d6 lstyle 1 lwidth 0.06 color cyan
               data  traces_r_a.out  d7=c21,c22
               d7 lstyle 1 lwidth 0.06 color magenta
               data  traces_r_a.out  d8=c24,c25
               d8 lstyle 1 lwidth 0.06 color orange
               data  traces_r_a.out  d9=c27,c28
               d9 lstyle 1 lwidth 0.06 color brown
               data  traces_r_a.out  d10=c30,c31
               d10 lstyle 1 lwidth 0.06 color grey
               data  traces_r_b.out  d11=c3,c4
               d11 lstyle 1 lwidth 0.06 color black
               data  traces_r_b.out  d12=c6,c7
               d12 lstyle 1 lwidth 0.06 color blue
               data  traces_r_b.out  d13=c9,c10
               d13 lstyle 1 lwidth 0.06 color red
               data  traces_r_b.out  d14=c12,c13
               d14 lstyle 1 lwidth 0.06 color green
               data  traces_r_b.out  d15=c15,c16
               d15 lstyle 1 lwidth 0.06 color yellow
               data  traces_r_b.out  d16=c18,c19
               d16 lstyle 1 lwidth 0.06 color cyan
               data  traces_r_b.out  d17=c21,c22
               d17 lstyle 1 lwidth 0.06 color magenta
               data  traces_r_b.out  d18=c24,c25
               d18 lstyle 1 lwidth 0.06 color orange
               data  traces_r_b.out  d19=c27,c28
               d19 lstyle 1 lwidth 0.06 color brown
               data  traces_r_b.out  d20=c30,c31
               d20 lstyle 1 lwidth 0.06 color grey
               data  traces_r_c.out  d21=c3,c4
               d21 lstyle 1 lwidth 0.06 color black
               data  traces_r_c.out  d22=c6,c7
               d22 lstyle 1 lwidth 0.06 color blue
               data  traces_r_c.out  d23=c9,c10
               d23 lstyle 1 lwidth 0.06 color red
               data  traces_r_c.out  d24=c12,c13
               d24 lstyle 1 lwidth 0.06 color green
               data  traces_r_c.out  d25=c15,c16
               d25 lstyle 1 lwidth 0.06 color yellow
               data  traces_r_c.out  d26=c18,c19
               d26 lstyle 1 lwidth 0.06 color cyan
               data  traces_r_c.out  d27=c21,c22
               d27 lstyle 1 lwidth 0.06 color magenta
               data  traces_r_c.out  d28=c24,c25
               d28 lstyle 1 lwidth 0.06 color orange
               data  traces_r_c.out  d29=c27,c28
               d29 lstyle 1 lwidth 0.06 color brown
               data  traces_r_c.out  d30=c30,c31
               d30 lstyle 1 lwidth 0.06 color grey
               data  traces_r_d.out  d31=c3,c4
               d31 lstyle 1 lwidth 0.06 color black
               data  traces_r_d.out  d32=c6,c7
               d32 lstyle 1 lwidth 0.06 color blue
               data  traces_r_d.out  d33=c9,c10
               d33 lstyle 1 lwidth 0.06 color red
               data  traces_r_d.out  d34=c12,c13
               d34 lstyle 1 lwidth 0.06 color green
               data  traces_r_d.out  d35=c15,c16
               d35 lstyle 1 lwidth 0.06 color yellow
               data  traces_r_d.out  d36=c18,c19
               d36 lstyle 1 lwidth 0.06 color cyan
               data  traces_r_d.out  d37=c21,c22
               d37 lstyle 1 lwidth 0.06 color magenta
               data  traces_r_d.out  d38=c24,c25
               d38 lstyle 1 lwidth 0.06 color orange
               data  traces_r_d.out  d39=c27,c28
               d39 lstyle 1 lwidth 0.06 color brown
               data  traces_r_d.out  d40=c30,c31
               d40 lstyle 1 lwidth 0.06 color grey
               data  traces_r_e.out  d41=c3,c4
               d41 lstyle 1 lwidth 0.06 color black
               data  traces_r_e.out  d42=c6,c7
               d42 lstyle 1 lwidth 0.06 color blue
               data  traces_r_e.out  d43=c9,c10
               d43 lstyle 1 lwidth 0.06 color red
               data  traces_r_e.out  d44=c12,c13
               d44 lstyle 1 lwidth 0.06 color green
               data  traces_r_e.out  d45=c15,c16
               d45 lstyle 1 lwidth 0.06 color yellow
               data  traces_r_e.out  d46=c18,c19
               d46 lstyle 1 lwidth 0.06 color cyan
               data  traces_r_e.out  d47=c21,c22
               d47 lstyle 1 lwidth 0.06 color magenta
               data  traces_r_e.out  d48=c24,c25
               d48 lstyle 1 lwidth 0.06 color orange
               data  traces_r_e.out  d49=c27,c28
               d49 lstyle 1 lwidth 0.06 color brown
               data  traces_r_e.out  d50=c30,c31
               d50 lstyle 1 lwidth 0.06 color grey
               data  traces_r_f.out  d51=c3,c4
               d51 lstyle 1 lwidth 0.06 color black
               data  traces_r_f.out  d52=c6,c7
               d52 lstyle 1 lwidth 0.06 color blue
               data  traces_r_f.out  d53=c9,c10
               d53 lstyle 1 lwidth 0.06 color red
               data  traces_r_f.out  d54=c12,c13
               d54 lstyle 1 lwidth 0.06 color green
               data  traces_r_f.out  d55=c15,c16
               d55 lstyle 1 lwidth 0.06 color yellow
               data  traces_r_f.out  d56=c18,c19
               d56 lstyle 1 lwidth 0.06 color cyan
               data  traces_r_f.out  d57=c21,c22
               d57 lstyle 1 lwidth 0.06 color magenta
               data  traces_r_f.out  d58=c24,c25
               d58 lstyle 1 lwidth 0.06 color orange
               data  traces_r_f.out  d59=c27,c28
               d59 lstyle 1 lwidth 0.06 color brown
               data  traces_r_f.out  d60=c30,c31
               d60 lstyle 1 lwidth 0.06 color grey
               data  traces_r_g.out  d61=c3,c4
               d61 lstyle 1 lwidth 0.06 color black
               data  traces_r_g.out  d62=c6,c7
               d62 lstyle 1 lwidth 0.06 color blue
               data  traces_r_g.out  d63=c9,c10
               d63 lstyle 1 lwidth 0.06 color red
               data  traces_r_g.out  d64=c12,c13
               d64 lstyle 1 lwidth 0.06 color green
               data  traces_r_g.out  d65=c15,c16
               d65 lstyle 1 lwidth 0.06 color yellow
               data  traces_r_g.out  d66=c18,c19
               d66 lstyle 1 lwidth 0.06 color cyan
               data  traces_r_g.out  d67=c21,c22
               d67 lstyle 1 lwidth 0.06 color magenta
               data  traces_r_g.out  d68=c24,c25
               d68 lstyle 1 lwidth 0.06 color orange
               data  traces_r_g.out  d69=c27,c28
               d69 lstyle 1 lwidth 0.06 color brown
               data  traces_r_g.out  d70=c30,c31
               d70 lstyle 1 lwidth 0.06 color grey
               data  traces_r_h.out  d71=c3,c4
               d71 lstyle 1 lwidth 0.06 color black
               data  traces_r_h.out  d72=c6,c7
               d72 lstyle 1 lwidth 0.06 color blue
               data  traces_r_h.out  d73=c9,c10
               d73 lstyle 1 lwidth 0.06 color red
               data  traces_r_h.out  d74=c12,c13
               d74 lstyle 1 lwidth 0.06 color green
               data  traces_r_h.out  d75=c15,c16
               d75 lstyle 1 lwidth 0.06 color yellow
               data  traces_r_h.out  d76=c18,c19
               d76 lstyle 1 lwidth 0.06 color cyan
               data  traces_r_h.out  d77=c21,c22
               d77 lstyle 1 lwidth 0.06 color magenta
               data  traces_r_h.out  d78=c24,c25
               d78 lstyle 1 lwidth 0.06 color orange
               data  traces_r_h.out  d79=c27,c28
               d79 lstyle 1 lwidth 0.06 color brown
               data  traces_r_h.out  d80=c30,c31
               d80 lstyle 1 lwidth 0.06 color grey
               data  traces_r_i.out  d81=c3,c4
               d81 lstyle 1 lwidth 0.06 color black
               data  traces_r_i.out  d82=c6,c7
               d82 lstyle 1 lwidth 0.06 color blue
               data  traces_r_i.out  d83=c9,c10
               d83 lstyle 1 lwidth 0.06 color red
               data  traces_r_i.out  d84=c12,c13
               d84 lstyle 1 lwidth 0.06 color green
               data  traces_r_i.out  d85=c15,c16
               d85 lstyle 1 lwidth 0.06 color yellow
               data  traces_r_i.out  d86=c18,c19
               d86 lstyle 1 lwidth 0.06 color cyan
               data  traces_r_i.out  d87=c21,c22
               d87 lstyle 1 lwidth 0.06 color magenta
               data  traces_r_i.out  d88=c24,c25
               d88 lstyle 1 lwidth 0.06 color orange
               data  traces_r_i.out  d89=c27,c28
               d89 lstyle 1 lwidth 0.06 color brown
               data  traces_r_i.out  d90=c30,c31
               d90 lstyle 1 lwidth 0.06 color grey
               data  traces_r_j.out  d91=c3,c4
               d91 lstyle 1 lwidth 0.06 color black
               data  traces_r_j.out  d92=c6,c7
               d92 lstyle 1 lwidth 0.06 color blue
               data  traces_r_j.out  d93=c9,c10
               d93 lstyle 1 lwidth 0.06 color red
               data  traces_r_j.out  d94=c12,c13
               d94 lstyle 1 lwidth 0.06 color green
               data  traces_r_j.out  d95=c15,c16
               d95 lstyle 1 lwidth 0.06 color yellow
               data  traces_r_j.out  d96=c18,c19
               d96 lstyle 1 lwidth 0.06 color cyan
               data  traces_r_j.out  d97=c21,c22
               d97 lstyle 1 lwidth 0.06 color magenta
               data  traces_r_j.out  d98=c24,c25
               d98 lstyle 1 lwidth 0.06 color orange
             end graph
             begin key
               hei .4
               position tr
               text " CB-end " marker ftriangle msize 0.5 color black
               text " dead CB " marker triangle msize 0.5 color red
               text " CC-end " marker fcircle msize 0.5 color black
               text " dead CC " marker circle msize 0.5 color red
               text " TC-end " marker fsquare msize 0.5 color black
               text " dead TC " marker square msize 0.5 color red
             end key
";
        f.close();
    }
    {
        ofstream f("transzone.gle");
        f << "size 15 15
             amove 2.0 2.0
             set font texcmss
             begin graph
                 size 12 12
                 fullsize
                 xtitle " position of zone boundary [microns] " hei .7 font texcmss
                 x2title " trans-migration " hei .7 font texcmss
                 ytitle " fraction of monitored cells " hei .7 font texcmss
                 xaxis hei .5 font texcmss
                 yaxis hei .5 font texcmss
                 xaxis min 0 max 320
             !	yaxis min -0.1 max 0.3
                 data  transzone.out  d1=c1,c5
                 data  transzone.out  d2=c1,c6
                 data  transzone.out  d3=c1,c7
                 let d4 = d1/d3
                 let d5 = d2/d3
                 let d6 = d4-d5
                 d4 marker fcircle msize 0.3 color red
                 d4 lstyle 1 lwidth 0.05 color red
                 d5 marker ftriangle msize 0.3 color blue
                 d5 lstyle 1 lwidth 0.05 color blue
                 d6 lstyle 1 lwidth 0.05 color black
                 data  transzone2.out  d11=c1,c5
                 data  transzone2.out  d12=c1,c6
                 data  transzone2.out  d13=c1,c7
                 let d14 = d11/d13
                 let d15 = d12/d13
                 let d16 = d14-d15
                 d14 marker circle msize 0.3 color red
                 d14 lstyle 2 lwidth 0.05 color red
                 d15 marker triangle msize 0.3 color blue
                 d15 lstyle 2 lwidth 0.05 color blue
             !	d16 lstyle 2 lwidth 0.05 color black
                 data  transzone4.out  d21=c1,c5
                 data  transzone4.out  d22=c1,c6
                 data  transzone4.out  d23=c1,c7
                 let d24 = d21/d23
                 let d25 = d22/d23
                 let d26 = d24-d25
                 d24 marker circle msize 0.3 color red
                 d24 lstyle 5 lwidth 0.04 color red
                 d25 marker triangle msize 0.3 color blue
                 d25 lstyle 5 lwidth 0.04 color blue
             !	d26 lstyle 2 lwidth 0.04 color black
             !        d1 errup d2 errwidth .2 lwidth .06 color red
             !        d3 errup d4 errwidth .2 lwidth .06 color blue
             !	fill d1,d5 color grey10
             !	fill d3,d6 color grey10
             end graph
             begin key
                   hei .4
                   position tr
                   text " up " lstyle 1 lwidth 0.06 color red
                   text " down " lstyle 1 lwidth 0.06 color blue
                   text " up+20micron " lstyle 2 lwidth 0.06 color red
                   text " up+40micron " lstyle 5 lwidth 0.06 color red
                   text " net up flux " lstyle 1 lwidth 0.06 color black
             end key
";
        f.close();
    }
    {
        ofstream f("turning_angle.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " turning angle [degree] " hei .7 font TEXCMR
                 x2title " turning angle distribution " hei .7 font TEXCMR
                 ytitle " percentage of occurrence " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0 max 190
                 yaxis min 0
                 data  turning_angle.out  d1=c1,c4
                 data  turning_mean.out d3=c1,c4
                 bar d1 width 8.0 fill grey40
             ! Reactivate this line to show error bars:
             !        d1 err d2 errwidth .2 lwidth .06
                 d3 marker star msize 0.7
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("v_t_i.gle");
        f << "size 15 8
             amove 2.0 1.5
             set font TEXCMR
             begin graph
                 size 12 6
                 fullsize
                 xtitle " t [min] " hei .7 font TEXCMR
                 x2title " interval-based speed " hei .7 font TEXCMR
                 ytitle " velocity [microns/min] " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0
                 yaxis min 0
             ! Insert any even y-column here:
                 data  v_t.out  d1=c1,c2
                 d1 marker fcircle
                 d1 lstyle 1 lwidth 0.04
             end graph
";
        f.close();
    }
    {
        ofstream f("v_t_p.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             set font TEXCMR
             begin graph
                 size 12 6
                 fullsize
                 xtitle " t [min] " hei .7 font TEXCMR
                 x2title " path-based speed " hei .7 font TEXCMR
                 ytitle " velocity [microns/min] " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0
                 yaxis min 0
             ! Insert any odd y-column here:
                 data  v_t.out  d1=c1,c3
                 d1 marker fcircle
                 d1 lstyle 1 lwidth 0.04
             end graph
";
        f.close();
    }
    {
        ofstream f("vdist.gle");
        f << "size 15 15
             amove 1 1
             begin graph
                 size 12 12
                 fullsize
                 xtitle " v [microns/min] " hei .7 font TEXCMR
                 ytitle " number of occurences " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0
                 yaxis min 0
                 data  vdist.out  d1=c2,c3
                 d1 lstyle 1 lwidth 0.05
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("vdistd.gle");
        f << "size 15 8
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " v [microns/min] " hei .7 font TEXCMR
                 ytitle " counts " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0 max 100
                 yaxis min 0
                 data  vdistd.out  d1=c2,c3
                 data  vmean.out d2=c3,c5
                 bar d1 width 1.8 fill grey40
                 d2 marker star msize 0.7
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("vdistp.gle");
        f << "size 15 15
             amove 1 1
             begin graph
                 size 12 12
                 fullsize
                 xtitle " v [microns/hr] " hei .7 font TEXCMR
                 ytitle " probability of occurences " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0
                 yaxis min 0
                 data  vdist.out  d1=c2,c4
                 d1 lstyle 1 lwidth 0.05
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("vhisto_i.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " v [microns/min] " hei .7 font TEXCMR
                 x2title " interval-based speed histogram " hei .7 font texcmss
                 ytitle " counts/tracked cell " hei .7 font texcmss
                 xaxis hei .5 font texcmss
                 yaxis hei .5 font texcmss
                 xaxis min -2 max 40
                 yaxis min 0
                 data  vhisto.out  d1=c1,c8
                 data  vhisto.out  d2=c1,c10
                 data  vmean_all.out d3=c1,c5
                 bar d1 width 1.0 fill grey40
                     d1 err d2 errwidth .15 lwidth .05
                 d3 marker star msize 0.7
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("vhisto_p.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " v [microns/min] " hei .7 font TEXCMR
                 x2title " path-based speed histogram " hei .7 font TEXCMR
                 ytitle " counts/tracked cells " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min -2 max 40
                 yaxis min 0
                 data  vhisto.out  d1=c1,c9
                 data  vhisto.out  d2=c1,c11
                 data  vmean_all.out d3=c3,c5
                 bar d1 width 0.9 fill grey40
             ! Reactivate this line to show error bars:
             !        d1 err d2 errwidth .2 lwidth .06
                 d3 marker star msize 0.7
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("vhisto_ppro.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " v [microns/min] " hei .7 font texcmss
                 x2title " path-based speed histogram " hei .7 font texcmss
                 ytitle " percentage " hei .7 font texcmss
                 xaxis hei .5 font texcmss
                 yaxis hei .5 font texcmss
                 xaxis min -2 max 25
                 yaxis min 0
                 data  vhisto.out  d1=c1,c15
                 data  vmean_all.out d3=c3,c5
                 bar d1 width 0.9 fill grey40
             ! Reactivate this line to show error bars:
             !        d1 err d2 errwidth .2 lwidth .06
                 d3 marker star msize 0.7
             end graph
             !set font texcmss
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("vhisto_psum.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             begin graph
                 size 12 6
                 fullsize
                 xtitle " v [microns/min] " hei .7 font TEXCMR
                 x2title " path-based speed histogram " hei .7 font TEXCMR
                 ytitle " counts " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min -2 max 40
                 yaxis min 0
                 data  vhisto.out  d1=c1,c3
             !	data  vhisto.out  d2=c1,c11
                 data  vmean_all.out d3=c3,c5
                 bar d1 width 1.8 fill grey40
             !        d1 err d2 errwidth .2 lwidth .06
                 d3 marker star msize 0.7
             end graph
             !set font TEXCMR
             !begin key
             !      hei .4
             !      position tl
             !      text " all cells " color black lstyle 1 lwidth 0.06
             !      text " cell1 " color red lstyle 1 lwidth 0.06
             !      text " cell2 " color cyan lstyle 1 lwidth 0.06
             !end key
";
        f.close();
    }
    {
        ofstream f("vmean_t.gle");
        f << "size 15 8.5
             amove 2.0 1.5
             set font TEXCMR
             begin graph
                 size 12 6
                 fullsize
                 xtitle " t [min] " hei .7 font TEXCMR
                 x2title " mean speed " hei .7 font TEXCMR
                 ytitle " speed [microns/min] " hei .7 font TEXCMR
                 xaxis hei .5 font TEXCMR
                 yaxis hei .5 font TEXCMR
                 xaxis min 0
                 yaxis min 0
                 data  vmean_t.out  d1=c1,c2
                 data  vmean_t.out  d2=c1,c3
                 data  vmean_t.out  d3=c1,c4
                 data  vmean_t.out  d4=c1,c5
                 let d5 = d1-d2
                 let d6 = d3+d4
                 d1 marker fcircle color red
                 d1 lstyle 1 lwidth 0.04 color red
                 d3 marker ftriangle color blue
                 d3 lstyle 1 lwidth 0.04 color blue
             !        d1 errup d2 errwidth .2 lwidth .06 color red
             !        d3 errup d4 errwidth .2 lwidth .06 color blue
                 fill d1,d5 color grey10
                 fill d3,d6 color grey10
             end graph
             begin key
                   hei .4
                   position tr
                   text " interval-based " marker fcircle color red
                   text " path-based " marker ftriangle color blue
             end key
";
        f.close();
    }
    {
        ofstream f("vol3d_data.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " t [hours] " hei .7 font TEXCMR
                 ytitle " total number of B-cells (GC-volume) " hei .7 font TEXCMR
                 xaxis font TEXCMR dticks 72
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0 max 16000
                 data liu91.exp  d8=c2,c3
                 let d9 = 12000*d8
                 data hol92.exp d10=c2,c3
                 let d11 = 10000*d10
             !	data  ssumcc.out d1=c1,c2
             !	data  ssumcb.out d2=c1,c2
                 data xvolume.out d4=c1,c4
             !	let d3 = d1+d2
                 d9 marker ftriangle
                 d11 marker fcircle
                 d4 lstyle 1 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " Liu et al. " marker ftriangle
                   text " Hollowood & Macartney " marker fcircle
                   text " in silico " lstyle 1 lwidth 0.04
             end key

";
        f.close();
    }
    {
        ofstream f("vol_data.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " t [hours] " hei .7 font TEXCMR
                 ytitle " total number of B-cells (GC-volume) " hei .7 font TEXCMR
                 xaxis font TEXCMR dticks 72
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0 max 2000
                 data liu91.exp  d8=c2,c3
                 let d9 = 1300*d8
                 data hol92.exp d10=c2,c3
                 let d11 = 1300*d10
             !	data  ssumcc.out d1=c1,c2
             !	data  ssumcb.out d2=c1,c2
                 data xvolume.out d4=c1,c4
             !	let d3 = d1+d2
                 d9 marker ftriangle
                 d11 marker fcircle
                 d4 lstyle 1 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " Liu et al. " marker ftriangle
                   text " Hollowood & Macartney " marker fcircle
                   text " in silico " lstyle 1 lwidth 0.04
             end key

";
        f.close();
    }
    {
        ofstream f("vol_korr.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " Volume of GC (CB+CC)" hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0
             ! 	data  xvolume.out  d1=c1,c2
             !	data  ssumout.out  d2=c1,c2
             !	data  ssumoute.out  d3=c1,c2
             !	data  ssumcc.out d5=c1,c2
             !	data  ssumcb.out d6=c1,c2
                 data  xvolume.out  d1=c1,c4
             !	let d4 = d1-d2+d3
             !	let d7 = d5+d6
                 d1 lstyle 1 lwidth 0.04
             !  	d4 lstyle 1 lwidth 0.04
             !	d7 lstyle 1 lwidth 0.04
             end graph
             set font TEXCMR
";
        f.close();
    }*/



     {
         ofstream f("vol_totalcb.gle");
         f << "size 15 15\n"
               "amove 2 2\n"
               "begin graph\n"
               "   size 12 12\n"
               "   fullsize\n"
               "   xtitle \" Time [days] \" hei .7 font texcmss\n"
               "   ytitle \" Number of B-cells \" hei .7 font texcmss\n"
               "   xaxis hei .5 font texcmss dticks 72\n"
               "   yaxis hei .5 font texcmss\n"
               "   xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
               "   xaxis min 0 max 504\n"
               "   yaxis min 0 ! max 10000\n";
         for(int i = 1; i < nb+1; ++i){
          f << "!   data liu91.exp  d" << i << "8=c2,c3\n"
               "!   let d" << i << "9 = 12000*d" << i << "8\n"
               "!   data hol92.exp d" << i << "10=c2,c3\n"
               "!   let d" << i << "11 = 10000*d" << i << "10\n"
               "!	data  ssumcc" << i << ".out d" << i << "1=c1,c2\n"
               "!	data  ssumcb" << i << ".out d" << i << "2=c1,c2\n"
               "    data xvolume" << i << ".out d" << i << "4=c1,c4\n"
               "    data dec205" << i << ".out d" << i << "5=c1,c2\n"
               "    data dec205" << i << ".out d" << i << "6=c1,c3\n"
               "!	let d" << i << "3 = d" << i << "1+d" << i << "2\n"
               "!	d" << i << "9 marker ftriangle\n"
               "!	d" << i << "11 marker fcircle\n"
               "    d" << i << "4 lstyle 1 color " << col[i] << " lwidth 0.05\n"
               "!    d" << i << "5 lstyle 1 color red lwidth 0.05\n"
               "!    d" << i << "6 lstyle 1 color blue lwidth 0.05\n";
          }
          f << "end graph\n"
               "set font texcmss\n"
               "begin key\n"
               "      hei .5\n"
               "      position tr\n"
               "!      text \" Liu et al. \" marker ftriangle\n"
               "!      text \" Hollowood & Macartney \" marker fcircle\n"
               "      text \" all BC \" lstyle 1 color black lwidth 0.07\n"
               "!      text \" dec205+/+ BC \" lstyle 1 color red lwidth 0.07\n"
               "!      text \" dec205-/- BC \" lstyle 1 color blue lwidth 0.07\n"
               "end key\n";
         f.close();
     }

     {
         ofstream f("vol_dec205.gle");
         f << "size 15 15\n"
               "amove 2 2\n"
               "begin graph\n"
               "   size 12 12\n"
               "   fullsize\n"
               "   xtitle \" Time [days] \" hei .7 font texcmss\n"
               "   ytitle \" Number of B-cells \" hei .7 font texcmss\n"
               "   xaxis hei .5 font texcmss dticks 72\n"
               "   yaxis hei .5 font texcmss\n"
               "   xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
               "   xaxis min 0 max 504\n"
               "   yaxis min 0 ! max 10000\n";
         for(int i = 1; i < nb+1; ++i){
         f <<  "!   data liu91.exp  d" << i << "8=c2,c3\n"
               "!   let d" << i << "9 = 12000*d" << i << "8\n"
               "!   data hol92.exp d" << i << "10=c2,c3\n"
               "!   let d" << i << "11 = 10000*d" << i << "10\n"
               "!	data  ssumcc" << i << ".out d" << i << "1=c1,c2\n"
               "!	data  ssumcb" << i << ".out d" << i << "2=c1,c2\n"
               "    data xvolume" << i << ".out d" << i << "4=c1,c4\n"
               "    data dec205" << i << ".out d" << i << "5=c1,c2\n"
               "    data dec205" << i << ".out d" << i << "6=c1,c3\n"
               "!	let d" << i << "3 = d" << i << "1+d" << i << "2\n"
               "!	d" << i << "9 marker ftriangle\n"
               "!	d" << i << "11 marker fcircle\n"
               "!    d" << i << "4 lstyle 1 color black lwidth 0.05\n"
               "    d" << i << "5 lstyle 1 color " << col[i] << " lwidth 0.05\n"
               "    d" << i << "6 lstyle 2 color " << col[i] << " lwidth 0.05\n";
          }
          f << "end graph\n"
               "set font texcmss\n"
               "begin key\n"
               "      hei .5\n"
               "      position tr\n"
               "!      text \" Liu et al. \" marker ftriangle\n"
               "!      text \" Hollowood & Macartney \" marker fcircle\n"
               "!      text \" all BC \" lstyle 1 color black lwidth 0.07\n"
               "      text \" dec205+/+ BC \" lstyle 1 color red lwidth 0.07\n"
               "      text \" dec205-/- BC \" lstyle 2 color blue lwidth 0.07\n"
               "end key\n";
         f.close();
     }


    {
        ofstream f("vol.glex");
        f << "size 15 15\n"
             "amove 2 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Number of B-cells \" hei .7 font texcmss\n"
             "    xaxis hei .5 font texcmss dticks 72\n"
             "    yaxis hei .5 font texcmss\n"
             "    xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0 ! max 10000\n"
             "    data liu91.exp  d8=c2,c3\n"
             "    let d9 = 12000*d8\n"
             "    data hol92.exp d10=c2,c3\n"
             "    let d11 = 10000*d10\n";
        for(int i = 1; i < nb+1; ++i){
        f << "!	data  ssumcc" << i << ".out d" << i << "1=c1,c2\n"
             "!	data  ssumcb" << i << ".out d" << i << "2=c1,c2\n"
             "    data xvolume" << i << ".out d" << i << "4=c1,c4\n"
             "  !  data dec205.out d5=c1,c2\n"
             "  !  data dec205.out d6=c1,c3\n"
             "!	let d3 = d1+d2\n"
             "!	d9 marker ftriangle\n"
             "!	d11 marker fcircle\n"
             "    d" << i << "4 lstyle 1 color " << col[i] << " lwidth 0.05\n";
        }
        f << "  !  d5 lstyle 1 color red lwidth 0.05\n"
             "  !  d6 lstyle 1 color blue lwidth 0.05\n"
             "end graph\n"
             "set font texcmss\n"
             "begin key\n"
             "      hei .5\n"
             "      position tr\n"
             "!      text \" Liu et al. \" marker ftriangle\n"
             "!      text \" Hollowood & Macartney \" marker fcircle\n"
             "      text \" all BC \" lstyle 1 color black lwidth 0.07\n"
             "!      text \" dec205+/+ BC \" lstyle 1 color red lwidth 0.07\n"
             "!      text \" dec205-/- BC \" lstyle 1 color blue lwidth 0.07\n"
             "end key";
        f.close();
    }

/*
    {
        ofstream f("xaff_ig.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Affinity of BC " hei .7 font texcmss
                 xaxis font texcmss dticks 72 hei .6
                     yaxis font texcmss hei .6
                     xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 1
             for(int i = 1; i < nb+1; ++i){
         f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                  "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                  "    d"<< i << " marker fcircle color " << col[i];
                  }

                 data  xaff_ig.out  d1=c1,c6
                 data  xaff_ig.out  d2=c1,c11
                 data  xaff_ig.out  d3=c1,c16
                 data  xaff_ig.out  d4=c1,c21
                 data  xaff_ig.out  d5=c1,c26
                 data  xaff_ig.out  d11=c1,c4
                 data  xaff_ig.out  d12=c1,c9
                 data  xaff_ig.out  d13=c1,c14
                 data  xaff_ig.out  d14=c1,c19
                 data  xaff_ig.out  d15=c1,c24
                 data  xaff_ig.out  d21=c1,c5
                 data  xaff_ig.out  d22=c1,c10
                 data  xaff_ig.out  d23=c1,c15
                 data  xaff_ig.out  d24=c1,c20
                 data  xaff_ig.out  d25=c1,c25
             !  	d1 lstyle 1 color black lwidth 0.05
             !	d2 lstyle 1 color red lwidth 0.05
             !	d3 lstyle 1 color green lwidth 0.05
             !	d4 lstyle 1 color blue lwidth 0.05
             !!	d5 lstyle 1 color magenta lwidth 0.05
             !  	d11 lstyle 2 color black lwidth 0.05
             !	d12 lstyle 2 color red lwidth 0.05
             !	d13 lstyle 2 color green lwidth 0.05
             !	d14 lstyle 2 color blue lwidth 0.05
             !!	d15 lstyle 2 color magenta lwidth 0.05
                 d21 lstyle 5 color black lwidth 0.05
                 d22 lstyle 5 color red lwidth 0.05
                 d23 lstyle 5 color green lwidth 0.05
                 d24 lstyle 5 color blue lwidth 0.05
             !	d25 lstyle 5 color magenta lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .5
                   position br
                   text " all BC " lstyle 1 color black lwidth 0.07
                   text " IgM-BC " lstyle 1 color red lwidth 0.07
                   text " IgG-BC " lstyle 1 color green lwidth 0.07
                   text " IgE-BC " lstyle 1 color blue lwidth 0.07
             !      text " IgA-BC " lstyle 1 color magenta lwidth 0.07
             end key
             begin key
                   hei .5
                   position tl
                   text " all BC " lstyle 1 color grey lwidth 0.07
                   text " OUT " lstyle 2 color grey lwidth 0.07
                   text " CB+CC " lstyle 5 color grey lwidth 0.07
             end key
";
        f.close();
    }*/

    {
        ofstream f("xapo_ig.gle");
        f << "size 15 15\n"
             "amove 2.5 2\n"
             "begin graph\n"
             "    size 12 12\n"
             "    fullsize\n"
             "    xtitle \" Time [days] \" hei .7 font texcmss\n"
             "    ytitle \" Frequency of apoptotic BC \" hei .7 font texcmss\n"
             "    xaxis font texcmss dticks 72 hei .6\n"
             "        yaxis font texcmss hei .6\n"
             "        xnames \"0\" \"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\"\n"
             "    xaxis min 0 max 504\n"
             "    yaxis min 0\n";
       for(int i = 1; i < nb+1; ++i){
       f << "     data  xapo_ig" << i << ".out  d" << i << "1=c1,c3\n"
            "     data  xapo_ig" << i << ".out  d" << i << "2=c1,c5\n"
            "     data  xapo_ig" << i << ".out  d" << i << "3=c1,c7\n"
            "     data  xapo_ig" << i << ".out  d" << i << "4=c1,c9\n"
            "     data  xapo_ig" << i << ".out  d" << i << "5=c1,c11\n"
            "     d" << i << "1 lstyle 1 color " << col[i] << " lwidth 0.05\n";
            /*"     d" << i << "2 lstyle 1 color red lwidth 0.05\n"
            "     d" << i << "3 lstyle 1 color green lwidth 0.05\n"
            "     d" << i << "4 lstyle 1 color blue lwidth 0.05\n"
            " !	  d" << i << "5 lstyle 1 color magenta lwidth 0.05\n";*/
       }
       f << " end graph\n"
            " set font texcmss\n"
            " begin key\n"
            "       hei .5\n"
            "       position tr\n"
            "       text \" all BC \" lstyle 1 color black lwidth 0.07\n"
            /*"       text " IgM-BC " lstyle 1 color red lwidth 0.07\n"
            "       text " IgG-BC " lstyle 1 color green lwidth 0.07\n"
            "       text " IgE-BC " lstyle 1 color blue lwidth 0.07\n"
            " !      text " IgA-BC " lstyle 1 color magenta lwidth 0.07\n"*/
            " end key";
        f.close();
    }



     /*
    {
        ofstream f("xapo_ig_smooth.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Frequency of apoptotic BC " hei .7 font texcmss
                 xaxis font texcmss dticks 72 hei .6
                     yaxis font texcmss hei .6
                     xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 ! max 0.3
                 data  xapo_ig_smooth.out  d1=c1,c3
                 data  xapo_ig_smooth.out  d2=c1,c5
                 data  xapo_ig_smooth.out  d3=c1,c7
                 data  xapo_ig_smooth.out  d4=c1,c9
                 data  xapo_ig_smooth.out  d5=c1,c11
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color green lwidth 0.05
                 d4 lstyle 1 color blue lwidth 0.05
             !	d5 lstyle 1 color magenta lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .5
                   position tr
                   text " all BC " lstyle 1 color black lwidth 0.07
                   text " IgM-BC " lstyle 1 color red lwidth 0.07
                   text " IgG-BC " lstyle 1 color green lwidth 0.07
                   text " IgE-BC " lstyle 1 color blue lwidth 0.07
             !      text " IgA-BC " lstyle 1 color magenta lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("xapo_ig_zones_smooth.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Frequency of apoptotic BC " hei .7 font texcmss
                 xaxis font texcmss dticks 72 hei .6
                     yaxis font texcmss hei .6
                     xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0 max 0.3
                 data  xapo_ig_dz_smooth.out  d1=c1,c3
                 data  xapo_ig_dz_smooth.out  d2=c1,c5
                 data  xapo_ig_dz_smooth.out  d3=c1,c7
                 data  xapo_ig_dz_smooth.out  d4=c1,c9
                 data  xapo_ig_dz_smooth.out  d5=c1,c11
                 data  xapo_ig_lz_smooth.out  d11=c1,c3
                 data  xapo_ig_lz_smooth.out  d12=c1,c5
                 data  xapo_ig_lz_smooth.out  d13=c1,c7
                 data  xapo_ig_lz_smooth.out  d14=c1,c9
                 data  xapo_ig_lz_smooth.out  d15=c1,c11
                 d1 lstyle 2 color black lwidth 0.05
                 d2 lstyle 2 color red lwidth 0.05
                 d3 lstyle 2 color green lwidth 0.05
                 d4 lstyle 2 color blue lwidth 0.05
             !	d5 lstyle 2 color magenta lwidth 0.05
                 d11 lstyle 5 color black lwidth 0.05
                 d12 lstyle 5 color red lwidth 0.05
                 d13 lstyle 5 color green lwidth 0.05
                 d14 lstyle 5 color blue lwidth 0.05
             !	d15 lstyle 5 color magenta lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .5
                   position tr
                   text " DZ " lstyle 2 color grey lwidth 0.07
                   text " LZ " lstyle 5 color grey lwidth 0.07
                   text " all BC " lstyle 1 color black lwidth 0.07
                   text " IgM-BC " lstyle 1 color red lwidth 0.07
                   text " IgG-BC " lstyle 1 color green lwidth 0.07
                   text " IgE-BC " lstyle 1 color blue lwidth 0.07
             !      text " IgA-BC " lstyle 1 color magenta lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("xlogouts.gle");
        f << "size 15 15
             amove 1.5 1.5
             begin graph
                 size 12 12
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " afinity of output cells " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0
                 data  xlogouts.out  d1=c1,c2
                 d1 marker triangle
             end graph
             set font TEXCMR
";
        f.close();
    }
    {
        ofstream f("xsumb1b2.gle");
        f << "size 17 17
             amove 1.5 1
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " total number blasts 1 and 2 " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0
                 yaxis min 0
                 data  xsumb1.out  d1=c1,c2
                 data  xsumb2.out  d2=c1,c2
                 let d3 = d1+d2
                 d1 lstyle 1 color red lwidth 0.06
                 d2 lstyle 1 color cyan lwidth 0.06
                 d3 lstyle 1 color black lwidth 0.06
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tl
                   text " all blastss " lstyle 1 color black lwidth 0.07
                   text " blast 1 " lstyle 1 color red lwidth 0.07
                   text " blast 2 " lstyle 1 color cyan lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("xsumb1b2l.gle");
        f << "size 17 17
             amove 1.5 1
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " total number blasts 1 and 2 " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 yaxis log
                 xaxis min 0
                 data  xsumb1.out  d1=c1,c2
                 data  xsumb2.out  d2=c1,c2
                 let d3 = d1+d2
                 d1 lstyle 1 color red lwidth 0.06
                 d2 lstyle 1 color cyan lwidth 0.06
                 d3 lstyle 1 color black lwidth 0.06
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tl
                   text " all blastss " lstyle 1 color black lwidth 0.07
                   text " blast 1 " lstyle 1 color red lwidth 0.07
                   text " blast 2 " lstyle 1 color cyan lwidth 0.07
             end key
";
        f.close();
    }
    */
     /* log scale
    {
        ofstream f("xsumbc_ig.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " GC-BC population [cells] " hei .7 font texcmss
                 xaxis font texcmss dticks 72 hei .6
                     yaxis font texcmss hei .6
                     xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis log
                 yaxis min 0.1 max 100000
             for(int i = 1; i < nb+1; ++i){
         f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                  "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                  "    d"<< i << " marker fcircle color " << col[i];
                  }

                 data  xsumbc_ig.out  d1=c1,c2
                 data  xsumbc_ig.out  d2=c1,c6
                 data  xsumbc_ig.out  d3=c1,c11
                 data  xsumbc_ig.out  d4=c1,c16
                 data  xsumbc_ig.out  d5=c1,c21
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color green lwidth 0.05
                 d4 lstyle 1 color blue lwidth 0.05
             !	d5 lstyle 1 color magenta lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .5
                   position tr
                   text " all BC " lstyle 1 color black lwidth 0.07
                   text " IgM-BC " lstyle 1 color red lwidth 0.07
                   text " IgG-BC " lstyle 1 color green lwidth 0.07
                   text " IgE-BC " lstyle 1 color blue lwidth 0.07
             !      text " IgA-BC " lstyle 1 color magenta lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("xsumbc_igfrac.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " % of GC-BC population [cells] " hei .7 font texcmss
                 xaxis font texcmss hei .6 dticks 72
                     xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
             ! the following is for a plot with days post immunisation:
             !	xtitle " days p.i. " hei .7 font texcmss
             !	xaxis ftick 60 dticks 72
             !        xnames "10" "13" "16" "19" "22" "25" "28\n"
                 xaxis min 0 max 504
                     yaxis font texcmss hei .6
             !	yaxis log
             !	yaxis min 0.1 max 100000
                 yaxis min 0 max 100
                 data  ./xsumbc_ig.out  d1=c1,c2  ! all
                 data  ./xsumbc_ig.out  d2=c1,c6  ! IgM
                 data  ./xsumbc_ig.out  d3=c1,c11 ! IgG
                 data  ./xsumbc_ig.out  d4=c1,c16 ! IgE
                 data  ./xsumbc_ig.out  d5=c1,c21 ! IgA
             !	data  expdata_jinshu/ig-fraction.out d22=c1,c2
             !	data  expdata_jinshu/ig-fraction.out d23=c1,c3
             !	data  expdata_jinshu/ig-fraction.out d24=c1,c4
             !	data  expdata_jinshu/ig-fraction.out d25=c1,c5
             !	data  expdata_jinshu/ig-fraction.out d26=c1,c6
             !	data  expdata_jinshu/ig-fraction.out d27=c1,c7
                 let d11 = d1  ! all BC
                 let d12 = 100*d2/d11  ! IgM
                 let d13 = 100*d3/d11  ! IgG
                 let d14 = 100*d4/d11  ! IgE
             !	let d15 = 100*d5/d11
             !        d11 marker ftriangle msize 0.05 color black
                     d12 marker ftriangle msize 0.05 color red
             !        d5 lstyle 1 lwidth 0.05 err d6 color green
                     d13 marker ftriangle msize 0.05 color green
             !        d7 lstyle 1 lwidth 0.05 err d8 color blue
                     d14 marker ftriangle msize 0.05 color blue
             !        d22 marker square msize 0.3 err d23 lwidth 0.06 color red
             !        d24 marker square msize 0.3 err d25 lwidth 0.06 color green
             !        d26 marker square msize 0.3 err d27 lwidth 0.06 color blue
             end graph
             set font texcmss
             begin key
                   hei .5
                   position tr
             !      text " all BC " lstyle 1 color black lwidth 0.07
                   text " %IgM+ BC " lstyle 1 color red lwidth 0.07
                   text " %IgG+ BC " lstyle 1 color green lwidth 0.07
                   text " %IgE+ BC " lstyle 1 color blue lwidth 0.07
             !      text " %IgA+ BC " lstyle 1 color magenta lwidth 0.07
             end key
";
        f.close();
    }
    {
        ofstream f("xsumbco_ig.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " GC-BC and GC-OUT population [cells] " hei .7 font texcmss
                 xaxis font texcmss dticks 72 hei .6
                     yaxis font texcmss hei .6
                     xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis log
                 yaxis min 0.1 max 100000
                 data  xsumbco_ig.out  d1=c1,c2
                 data  xsumbco_ig.out  d2=c1,c6
                 data  xsumbco_ig.out  d3=c1,c11
                 data  xsumbco_ig.out  d4=c1,c16
                 data  xsumbco_ig.out  d5=c1,c21
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color green lwidth 0.05
                 d4 lstyle 1 color blue lwidth 0.05
             !	d5 lstyle 1 color magenta lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .5
                   position tr
                   text " all BC " lstyle 1 color black lwidth 0.07
                   text " IgM-BC " lstyle 1 color red lwidth 0.07
                   text " IgG-BC " lstyle 1 color green lwidth 0.07
                   text " IgE-BC " lstyle 1 color blue lwidth 0.07
             !      text " IgA-BC " lstyle 1 color magenta lwidth 0.07
             end key
";
        f.close();
    }*/



        /* TOOODOOOO
    {

        ofstream f("xsumcb.gle");
         f << "size 15 15\n"
               "amove 2 2\n"
               "begin graph\n"
               "   size 12 12\n"
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " total number of centroblasts " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0
             for(int i = 1; i < nb+1; ++i){
         f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                  "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                  "    d"<< i << " marker fcircle color " << col[i];
                  }

                 data  xsumcb.out  d1=c1,c2
                 data  xsumcb.out  d2=c1,c3
                 let d3 = d1-d2
                 d1 lstyle 1 lwidth 0.04
                 d2 lstyle 2 lwidth 0.04
                 d3 lstyle 3 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " all CBs " lstyle 1 lwidth 0.04
                   text " not recycled CBs " lstyle 2 lwidth 0.04
                   text " recycled CBs " lstyle 3 lwidth 0.04
             end key
";
        f.close();
    }
    {
        ofstream f("xsumcb_c.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " time [days] " hei .7 font TEXCMR
                 ytitle " number of centroblasts " hei .7 font TEXCMR
                 xaxis font TEXCMR dticks 72
                 yaxis font TEXCMR
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0
             for(int i = 1; i < nb+1; ++i){
         f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                  "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                  "    d"<< i << " marker fcircle color " << col[i];
                  }

                 data  xsumcb.out  d1=c1,c2
                 data  xsumcb.out  d2=c1,c3
                 let d3 = d1-d2
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color blue lwidth 0.05
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " all CBs " lstyle 1 color black lwidth 0.06
                   text " not recycled CBs " lstyle 1 color red lwidth 0.06
                   text " recycled CBs " lstyle 1 color blue lwidth 0.06
             end key
";
        f.close();
    }*/

/*
    {
        ofstream f("xsumcbcc.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " t [h] " hei .7 font texcmss
                 ytitle " ratio of CC to CB" hei .7 font texcmss
                 xaxis font texcmss
                 yaxis font texcmss
                 xaxis min 0 max 504
                 yaxis min 0 max 2
                 data  xsumcc.out  d1=c1,c2
                 data  xsumcb.out  d2=c1,c2
                 let d3=d1/d2
                 d3 lstyle 1 lwidth 0.05 color black
             end graph
";
        f.close();
    }*/
     /*
    {
        ofstream f("xsumcc.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " total number of centrocytes " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0
                 data  xsumcc.out  d1=c1,c2
                 data  xsumcc.out  d2=c1,c3
                 data  xsumcc.out  d3=c1,c4
                 data  xsumcc.out  d4=c1,c5
                 data  xsumcc.out  d5=c1,c6
                 data  xsumcc.out  d5=c1,c7
                 data  xsumcc.out  d5=c1,c8
                for(int i = 1; i < nb+1; ++i){
            f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                     "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                     "    d"<< i << " marker fcircle color " << col[i];
                     }

                 d1 lstyle 1 lwidth 0.04
                 d2 lstyle 2 lwidth 0.04
                 d3 lstyle 3 lwidth 0.04
                 d4 lstyle 4 lwidth 0.04
                 d5 lstyle 5 lwidth 0.04
                 d6 lstyle 6 lwidth 0.04
                 d7 lstyle 7 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " all CCs " lstyle 1 lwidth 0.04
                   text " unselected CCs " lstyle 2 lwidth 0.04
                   text " CCs in contact to FDCs " lstyle 3 lwidth 0.04
                   text " FDC-selected CCs " lstyle 4 lwidth 0.04
                   text " CCs in contact to TFHs " lstyle 5 lwidth 0.04
                   text " selected CCs " lstyle 6 lwidth 0.04
                   text " apoptotic cells " lstyle 7 lwidth 0.04
             end key
";
        f.close();
    }*/


        /*  TODOOOO
    {
        ofstream f("xsumcc_c.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Number of centrocytes " hei .7 font texcmss
                 xaxis font texcmss dticks 72
                 yaxis font texcmss
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0
             for(int i = 1; i < nb+1; ++i){
         f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                  "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                  "    d"<< i << " marker fcircle color " << col[i];
                  }

                 data  xsumcc.out  d1=c1,c2
                 data  xsumcc.out  d2=c1,c3
                 data  xsumcc.out  d3=c1,c4
                 data  xsumcc.out  d4=c1,c5
                 data  xsumcc.out  d5=c1,c6
                 data  xsumcc.out  d6=c1,c7
                 data  xsumcc.out  d7=c1,c8
                 d1 lstyle 1 lwidth 0.05 color black
                 d2 lstyle 2 lwidth 0.05 color black
                 d3 lstyle 3 lwidth 0.05 color blue
                 d4 lstyle 4 lwidth 0.05 color green
                 d5 lstyle 5 lwidth 0.05 color red
                 d6 lstyle 6 lwidth 0.05 color magenta
                 d7 lstyle 7 lwidth 0.05 color cyan
             end graph
             set font texcmss
             begin key
                   hei .4
                   position tr
                   text " all CCs " lstyle 1 lwidth 0.06 color black
                   text " unselected CCs " lstyle 2 lwidth 0.06 color black
                   text " CCs in contact to FDCs " lstyle 3 lwidth 0.06 color blue
                   text " FDC-selected CCs " lstyle 4 lwidth 0.06 color green
                   text " CCs in contact to TFHs " lstyle 5 lwidth 0.06 color red
                   text " selected CCs " lstyle 6 lwidth 0.06 color magenta
                   text " apoptotic cells " lstyle 7 lwidth 0.06 color cyan
             end key
";
        f.close();
    }
    {
        ofstream f("xsumout_ig.gle");
        f << "size 15 15
             amove 2.5 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " Time [days] " hei .7 font texcmss
                 ytitle " Integrated output [cells] " hei .7 font texcmss
                 xaxis font texcmss dticks 72 hei .6
                     yaxis font texcmss hei .6
                     xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis log
                 yaxis min 0.1 max 100000
             !	yaxis min 0 max 30000
             for(int i = 1; i < nb+1; ++i){
         f << "    data  axis" << i << ".out  d" << i << "=c1,c4\n"
                  "    d"<< i << " lstyle 1 color " << col[i] << " lwidth 0.04";
                  "    d"<< i << " marker fcircle color " << col[i];
                  }

                 data  xsumout_ig.out  d1=c1,c3
                 data  xsumout_ig.out  d2=c1,c5
                 data  xsumout_ig.out  d3=c1,c7
                 data  xsumout_ig.out  d4=c1,c9
                 data  xsumout_ig.out  d5=c1,c11
                 d1 lstyle 1 color black lwidth 0.05
                 d2 lstyle 1 color red lwidth 0.05
                 d3 lstyle 1 color green lwidth 0.05
                 d4 lstyle 1 color blue lwidth 0.05
             !	d5 lstyle 1 color magenta lwidth 0.05
             end graph
             set font texcmss
             begin key
                   hei .5
                   position tl
                   text " all BC " lstyle 1 color black lwidth 0.07
                   text " IgM-BC " lstyle 1 color red lwidth 0.07
                   text " IgG-BC " lstyle 1 color green lwidth 0.07
                   text " IgE-BC " lstyle 1 color blue lwidth 0.07
             !      text " IgA-BC " lstyle 1 color magenta lwidth 0.07
             end key
";
        f.close();
    }
    */
     /*
    {
        ofstream f("xtraces_a.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
               size 12 12
               fullsize
               xtitle " x [microns]" hei .7 font TEXCMR
               x2title " cell-tracking in the GC " hei .7 font TEXCMR
               ytitle " y [microns]" hei .7 font TEXCMR
               xaxis font TEXCMR
               yaxis font TEXCMR
               xaxis min 0 max 450
               yaxis min 0 max 450
               data  trace_end0001.out d51=c1,c2
               d51 marker ftriangle msize 0.3 color black
               data  trace_dead0001.out d58=c1,c2
               d58 marker triangle msize 0.3 color red
               data  trace_begin0001.out d65=c1,c2
               d65 marker ftriangle msize 0.3 color green
               data  trace_end0002.out d52=c1,c2
               d52 marker fcircle msize 0.3 color black
               data  trace_dead0002.out d59=c1,c2
               d59 marker circle msize 0.3 color red
               data  trace_begin0002.out d66=c1,c2
               d66 marker fcircle msize 0.3 color green
               data  traces_a.out  d1=c2,c3
               d1 lstyle 1 lwidth 0.06 color black
               data  traces_a.out  d2=c5,c6
               d2 lstyle 1 lwidth 0.06 color blue
               data  traces_a.out  d3=c8,c9
               d3 lstyle 1 lwidth 0.06 color red
               data  traces_a.out  d4=c11,c12
               d4 lstyle 1 lwidth 0.06 color green
               data  traces_a.out  d5=c14,c15
               d5 lstyle 1 lwidth 0.06 color yellow
               data  traces_a.out  d6=c17,c18
               d6 lstyle 1 lwidth 0.06 color cyan
               data  traces_a.out  d7=c20,c21
               d7 lstyle 1 lwidth 0.06 color magenta
               data  traces_a.out  d8=c23,c24
               d8 lstyle 1 lwidth 0.06 color orange
               data  traces_a.out  d9=c26,c27
               d9 lstyle 1 lwidth 0.06 color brown
               data  traces_a.out  d10=c29,c30
               d10 lstyle 1 lwidth 0.06 color grey
               data  traces_a.out  d11=c32,c33
               d11 lstyle 1 lwidth 0.06 color black
               data  traces_a.out  d12=c35,c36
               d12 lstyle 1 lwidth 0.06 color blue
               data  traces_a.out  d13=c38,c39
               d13 lstyle 1 lwidth 0.06 color red
               data  traces_a.out  d14=c41,c42
               d14 lstyle 1 lwidth 0.06 color green
               data  traces_a.out  d15=c44,c45
               d15 lstyle 1 lwidth 0.06 color yellow
               data  traces_a.out  d16=c47,c48
               d16 lstyle 1 lwidth 0.06 color cyan
               data  traces_a.out  d17=c50,c51
               d17 lstyle 1 lwidth 0.06 color magenta
               data  traces_a.out  d18=c53,c54
               d18 lstyle 1 lwidth 0.06 color orange
               data  traces_a.out  d19=c56,c57
               d19 lstyle 1 lwidth 0.06 color brown
               data  traces_a.out  d20=c59,c60
               d20 lstyle 1 lwidth 0.06 color grey
             end graph
             begin key
               hei .4
               position tr
               text " CB-begin " marker ftriangle msize 0.5 color green
               text " CB-end " marker ftriangle msize 0.5 color black
               text " dead CB " marker triangle msize 0.5 color red
               text " CC-begin " marker fcircle msize 0.5 color green
               text " CC-end " marker fcircle msize 0.5 color black
               text " dead CC " marker circle msize 0.5 color red
             end key
";
        f.close();
    }
    {
        ofstream f("xtraces_r.gle");
        f << "size 15 15
             amove 2 2
             set font TEXCMR
             begin graph
               size 12 12
               fullsize
               xtitle " x [microns]" hei .7 font TEXCMR
               x2title " cell-traces starting at (0,0) " hei .7 font TEXCMR
               ytitle " y [microns]" hei .7 font TEXCMR
               xaxis font TEXCMR
               yaxis font TEXCMR
               xaxis min -100 max 100
               yaxis min -100 max 100
               data  trace_end0001.out d51=c4,c5
               d51 marker ftriangle msize 0.4 color black
               data  trace_dead0001.out d58=c4,c5
               d58 marker triangle msize 0.4 color red
               data  trace_end0002.out d52=c4,c5
               d52 marker fcircle msize 0.4 color black
               data  trace_dead0002.out d59=c4,c5
               d59 marker circle msize 0.4 color red
               data  traces_r.out  d1=c2,c3
               d1 lstyle 1 lwidth 0.06 color black
               data  traces_r.out  d2=c5,c6
               d2 lstyle 1 lwidth 0.06 color blue
               data  traces_r.out  d3=c8,c9
               d3 lstyle 1 lwidth 0.06 color red
               data  traces_r.out  d4=c11,c12
               d4 lstyle 1 lwidth 0.06 color green
               data  traces_r.out  d5=c14,c15
               d5 lstyle 1 lwidth 0.06 color yellow
               data  traces_r.out  d6=c17,c18
               d6 lstyle 1 lwidth 0.06 color cyan
               data  traces_r.out  d7=c20,c21
               d7 lstyle 1 lwidth 0.06 color magenta
               data  traces_r.out  d8=c23,c24
               d8 lstyle 1 lwidth 0.06 color orange
               data  traces_r.out  d9=c26,c27
               d9 lstyle 1 lwidth 0.06 color brown
               data  traces_r.out  d10=c29,c30
               d10 lstyle 1 lwidth 0.06 color grey
               data  traces_r.out  d11=c32,c33
               d11 lstyle 1 lwidth 0.06 color black
               data  traces_r.out  d12=c35,c36
               d12 lstyle 1 lwidth 0.06 color blue
               data  traces_r.out  d13=c38,c39
               d13 lstyle 1 lwidth 0.06 color red
               data  traces_r.out  d14=c41,c42
               d14 lstyle 1 lwidth 0.06 color green
               data  traces_r.out  d15=c44,c45
               d15 lstyle 1 lwidth 0.06 color yellow
               data  traces_r.out  d16=c47,c48
               d16 lstyle 1 lwidth 0.06 color cyan
               data  traces_r.out  d17=c50,c51
               d17 lstyle 1 lwidth 0.06 color magenta
               data  traces_r.out  d18=c53,c54
               d18 lstyle 1 lwidth 0.06 color orange
               data  traces_r.out  d19=c56,c57
               d19 lstyle 1 lwidth 0.06 color brown
               data  traces_r.out  d20=c59,c60
               d20 lstyle 1 lwidth 0.06 color grey
             end graph
             begin key
               hei .4
               position tr
               text " CB-end " marker ftriangle msize 0.5 color black
               text " dead CB " marker triangle msize 0.5 color red
               text " CC-end " marker fcircle msize 0.5 color black
               text " dead CC " marker circle msize 0.5 color red
             end key
";
        f.close();
    }
    {
        ofstream f("xvolume.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " Volume of GC (fraction of occupied lattice points)" hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0 max 1
                 data  xvolume.out  d1=c1,c4
                 data  xvolume.out  d2=c1,c5
                 data  xvolume.out  d3=c1,c6
                 d1 lstyle 3 lwidth 0.04
                 d2 lstyle 2 lwidth 0.04
                 d3 lstyle 1 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " all cells " lstyle 3 lwidth 0.04
                   text " without FDCs " lstyle 2 lwidth 0.04
                   text " without FDCs and OUTs " lstyle 1 lwidth 0.04
             end key
";
        f.close();
    }
    {
        ofstream f("zone_da.gle");
        f << "size 15 15
             amove 1.5 1.5
             begin graph
                 size 12 12
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " relative part of CB,nrCB,CC in dark zone " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0
                 data  zone_da.out  d1=c1,c2
                 data  zone_da.out  d2=c1,c3
                 data  zone_da.out  d3=c1,c4
                 d1 lstyle 1 lwidth 0.04
                 d2 lstyle 2 lwidth 0.04
                 d3 lstyle 3 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " % CB " lstyle 1 lwidth 0.04
                   text " % not recycled CB " lstyle 2 lwidth 0.04
                   text " % CC " lstyle 3 lwidth 0.04
             end key
";
        f.close();
    }*/

        /*
    {
        ofstream f("zone_da_c.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " time [days] " hei .7 font TEXCMR
                 ytitle " fraction of CB and CC in dark zone " hei .7 font TEXCMR
                 xaxis font TEXCMR dticks 72
                 yaxis font TEXCMR
                 xnames "0" "3" "6" "9" "12" "15" "18" "21\n"
                 xaxis min 0 max 504
                 yaxis min 0
                 data  zone_da.out  d1=c1,c2
                 data  zone_da.out  d2=c1,c3
                 data  zone_da.out  d3=c1,c4
                 d1 lstyle 1 color red lwidth 0.05
                 d2 lstyle 1 color black lwidth 0.05
                 d3 lstyle 1 color blue lwidth 0.05
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " % CB " lstyle 1 color red lwidth 0.06
                   text " % not recycled CB " lstyle 1 color black lwidth 0.06
                   text " % CC " lstyle 1 color blue lwidth 0.06
             end key
";
        f.close();
    }
    */
     /*
    {
        ofstream f("zone_li.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " t [h] " hei .7 font TEXCMR
                 ytitle " relative part of CB,nrCB,CC in light zone " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 504
                 yaxis min 0
                 data  zone_li.out  d1=c1,c2
                 data  zone_li.out  d2=c1,c3
                 data  zone_li.out  d3=c1,c4
                 d1 lstyle 1 lwidth 0.04
                 d2 lstyle 2 lwidth 0.04
                 d3 lstyle 3 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " % CB " lstyle 1 lwidth 0.04
                   text " % not recycled CB " lstyle 2 lwidth 0.04
                   text " % CC " lstyle 3 lwidth 0.04
             end key
";
        f.close();
    }
    {
        ofstream f("zonecb.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " z [10\mu m]" hei .7 font TEXCMR
                 ytitle " relative number of centroblasts " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 1 max 31
                 yaxis min 0
             ! 	data zone_ini.out  d1=c1,c2
                 data zone0003.out  d2=c1,c2
                 data zone0006.out  d3=c1,c2
                 data zone0009.out  d4=c1,c2
                 data zone0012.out  d5=c1,c2
                 data zone0015.out  d6=c1,c2
                 data zone0018.out  d7=c1,c2
                 data zone0021.out  d8=c1,c2
             !  	d1 lstyle 8 lwidth 0.04
                 d2 lstyle 7 lwidth 0.04
                 d3 lstyle 6 lwidth 0.04
                 d4 lstyle 5 lwidth 0.04
                 d5 lstyle 4 lwidth 0.04
                 d6 lstyle 3 lwidth 0.04
                 d7 lstyle 2 lwidth 0.04
                 d8 lstyle 1 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
             !      text " day 0 " lstyle 8 lwidth 0.04
                   text " day 3 " lstyle 7 lwidth 0.04
                   text " day 6 " lstyle 6 lwidth 0.04
                   text " day 9 " lstyle 5 lwidth 0.04
                   text " day 12 " lstyle 4 lwidth 0.04
                   text " day 15 " lstyle 3 lwidth 0.04
                   text " day 18 " lstyle 2 lwidth 0.04
                   text " day 21 " lstyle 1 lwidth 0.04
             end key
";
        f.close();
    }
    {
        ofstream f("zonecbcc.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " z [10\mu m]" hei .7 font TEXCMR
                 ytitle " centroblasts / centrocytes " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 1 max 31
                 yaxis min 0 max 20
                 data zone0006.out  d3=c1,c5
                 data zone0009.out  d4=c1,c5
                 data zone0012.out  d5=c1,c5
                 data zone0015.out  d6=c1,c5
                 data zone0018.out  d7=c1,c5
                 data zone0021.out  d8=c1,c5
                 d3 lstyle 6 lwidth 0.04
                 d4 lstyle 5 lwidth 0.04
                 d5 lstyle 4 lwidth 0.04
                 d6 lstyle 3 lwidth 0.04
                 d7 lstyle 2 lwidth 0.04
                 d8 lstyle 1 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
                   text " day 6 " lstyle 6 lwidth 0.04
                   text " day 9 " lstyle 5 lwidth 0.04
                   text " day 12 " lstyle 4 lwidth 0.04
                   text " day 15 " lstyle 3 lwidth 0.04
                   text " day 18 " lstyle 2 lwidth 0.04
                   text " day 21 " lstyle 1 lwidth 0.04
             end key
";
        f.close();
    }
    {
        ofstream f("zonecc.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " z [10\mu m]" hei .7 font TEXCMR
                 ytitle " relative number of centrocytes " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 1 max 31
                 yaxis min 0
             ! 	data zone_ini.out  d1=c1,c3
                 data zone0003.out  d2=c1,c4
                 data zone0006.out  d3=c1,c4
                 data zone0009.out  d4=c1,c4
                 data zone0012.out  d5=c1,c4
                 data zone0015.out  d6=c1,c4
                 data zone0018.out  d7=c1,c4
                 data zone0021.out  d8=c1,c4
             !  	d1 lstyle 8 lwidth 0.04
                 d2 lstyle 7 lwidth 0.04
                 d3 lstyle 6 lwidth 0.04
                 d4 lstyle 5 lwidth 0.04
                 d5 lstyle 4 lwidth 0.04
                 d6 lstyle 3 lwidth 0.04
                 d7 lstyle 2 lwidth 0.04
                 d8 lstyle 1 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
             !      text " day 0 " lstyle 8 lwidth 0.04
                   text " day 3 " lstyle 7 lwidth 0.04
                   text " day 6 " lstyle 6 lwidth 0.04
                   text " day 9 " lstyle 5 lwidth 0.04
                   text " day 12 " lstyle 4 lwidth 0.04
                   text " day 15 " lstyle 3 lwidth 0.04
                   text " day 18 " lstyle 2 lwidth 0.04
                   text " day 21 " lstyle 1 lwidth 0.04
             end key
";
        f.close();
    }
    {
        ofstream f("zonenbcc.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " z [10\mu m]" hei .7 font TEXCMR
                 ytitle " not recycled centroblasts / centrocytes " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 1 max 31
                 yaxis min 0 max 20
             ! 	data zone_ini.out  d1=c1,c6
                 data zone0003.out  d2=c1,c6
                 data zone0006.out  d3=c1,c6
                 data zone0009.out  d4=c1,c6
                 data zone0012.out  d5=c1,c6
                 data zone0015.out  d6=c1,c6
                 data zone0018.out  d7=c1,c6
                 data zone0021.out  d8=c1,c6
             !  	d1 lstyle 8 lwidth 0.04
                 d2 lstyle 7 lwidth 0.04
                 d3 lstyle 6 lwidth 0.04
                 d4 lstyle 5 lwidth 0.04
                 d5 lstyle 4 lwidth 0.04
                 d6 lstyle 3 lwidth 0.04
                 d7 lstyle 2 lwidth 0.04
                 d8 lstyle 1 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
             !      text " day 0 " lstyle 8 lwidth 0.04
                   text " day 3 " lstyle 7 lwidth 0.04
                   text " day 6 " lstyle 6 lwidth 0.04
                   text " day 9 " lstyle 5 lwidth 0.04
                   text " day 12 " lstyle 4 lwidth 0.04
                   text " day 15 " lstyle 3 lwidth 0.04
                   text " day 18 " lstyle 2 lwidth 0.04
                   text " day 21 " lstyle 1 lwidth 0.04
             end key
";
        f.close();
    }
    {
        ofstream f("zonenrcb.gle");
        f << "size 21 30
             amove 2.5 6
             begin graph
                 size 15 15
                 fullsize
                 xtitle " z [10\mu m]" hei .7 font TEXCMR
                 ytitle " relative number of not recycled centroblasts " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 1 max 31
                 yaxis min 0
             ! 	data zone_ini.out  d1=c1,c3
                 data zone0003.out  d2=c1,c3
                 data zone0006.out  d3=c1,c3
                 data zone0009.out  d4=c1,c3
                 data zone0012.out  d5=c1,c3
                 data zone0015.out  d6=c1,c3
                 data zone0018.out  d7=c1,c3
                 data zone0021.out  d8=c1,c3
             !  	d1 lstyle 8 lwidth 0.04
                 d2 lstyle 7 lwidth 0.04
                 d3 lstyle 6 lwidth 0.04
                 d4 lstyle 5 lwidth 0.04
                 d5 lstyle 4 lwidth 0.04
                 d6 lstyle 3 lwidth 0.04
                 d7 lstyle 2 lwidth 0.04
                 d8 lstyle 1 lwidth 0.04
             end graph
             set font TEXCMR
             begin key
                   hei .4
                   position tr
             !      text " day 0 " lstyle 8 lwidth 0.04
                   text " day 3 " lstyle 7 lwidth 0.04
                   text " day 6 " lstyle 6 lwidth 0.04
                   text " day 9 " lstyle 5 lwidth 0.04
                   text " day 12 " lstyle 4 lwidth 0.04
                   text " day 15 " lstyle 3 lwidth 0.04
                   text " day 18 " lstyle 2 lwidth 0.04
                   text " day 21 " lstyle 1 lwidth 0.04
             end key
";
        f.close();
    }
    {
        ofstream f("zonercb.gle");
        f << "size 15 15
             amove 2 2
             begin graph
                 size 12 12
                 fullsize
                 xtitle " z [\mu m]" hei .7 font TEXCMR
                 ytitle " fraction of recycled CB " hei .7 font TEXCMR
                 xaxis font TEXCMR
                 yaxis font TEXCMR
                 xaxis min 0 max 450
                 yaxis min 0 max 0.7
                 data zone_ini.out  d1=c1,c2
                 data zone0006.out  d2=c1,c2
                 data zone0009.out  d3=c1,c2
                 data zone0012.out  d4=c1,c2
                 data zone0021.out  d5=c1,c2
                 data zone_ini.out  d6=c1,c3
                 data zone0006.out  d7=c1,c3
                 data zone0009.out  d8=c1,c3
                 data zone0012.out  d9=c1,c3
                 data zone0021.out  d10=c1,c3
                 let d11 = d1-d6
                 let d12 = d2-d7
                 let d13 = d3-d8
                 let d14 = d4-d9
                 let d15 = d5-d10
                 d11 lstyle 1 marker fcircle lwidth 0.05 msize 0.3
                 d12 lstyle 1 marker fdiamond lwidth 0.05 msize 0.3
                 d13 lstyle 1 marker star lwidth 0.05 msize 0.6
                 d14 lstyle 2 marker triangle lwidth 0.05 msize 0.3
                 d15 lstyle 2 marker square lwidth 0.05 msize 0.3
             end graph
             set font TEXCMR
             begin key
                 hei .4
                 position tr
                 text " day 3 " marker fcircle
                 text " day 6 " marker fdiamond
                 text " day 9 " marker star msize 0.5
                     text " day 12 " marker triangle
                     text " day 21 " marker square
                     text " dark zone " lstyle 1 lwidth 0.05
                 text " no dark zone " lstyle 2 lwidth 0.05
             end key
";
        f.close();
    }
    {
        ofstream f(".gle");
        f << "";
        f.close();
    }
    {
        ofstream f(".gle");
        f << "";
        f.close();
    }
    */
}
