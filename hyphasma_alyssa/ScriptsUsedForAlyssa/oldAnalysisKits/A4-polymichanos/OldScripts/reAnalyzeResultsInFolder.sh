function reanalyze2 {
	cp ../hyphasma/tools/mkfigureSave ../hyphasma/tools/mkfigure 
	#cd $1
	#LIST=find -maxdepth 1 -type d
	#echo list
	#for f in $(find -maxdepth 1 -type d);
	#for f in $(ls -d */);
	for f in */;
	do
		echo "${f%/}"
		cd ../hyphasma/tools/
		./analysis ../../scripts/$f &> trash.txt
		cd ../../scripts/
		cp $f/resultpage.pdf ${f%/}.pdf
	done
}

reanalyze2 

