SETLOCAL

:reanalyze2 {
	copy ..\hyphasma\tools\mkfigureSave ..\hyphasma\tools\mkfigure 
	for /d %%f in (*) do (
		echo %%f
		cd ..\hyphasma\tools\
		analysis.bat ..\..\scripts\%%f 
		cd ..\..\scripts\
		copy %%f\resultpage.pdf %%f.pdf
	)
	EXIT \B 0
}

call :reanalyze2 

