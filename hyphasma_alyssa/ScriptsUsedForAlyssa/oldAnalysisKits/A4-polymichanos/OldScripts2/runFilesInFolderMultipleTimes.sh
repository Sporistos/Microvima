FILES="
tcmult013modSelecblock-1RESCUE-t=0fac=1
tcmult013modSelecblock-1RESCUE-t=0fac=1.15
tcmult013modSelecblock-1RESCUE-t=0fac=1.2
tcmult013modSelecblock-1RESCUE-t=0fac=1.25
tcmult013modSelecblock-1RESCUE-t=0fac=1.3
";

cd ../hyphasma/
qmake src/hyphasma.pro
make
cd ..
cd scripts/

function process {
	echo "Processing $1"
	DATE=$(date +%Y-%m-%d_%H_%M_%S)
	NEWFOLDER="$1-Done$DATE"
	mkdir $NEWFOLDER
	echo "Creating folder $NEWFOLDER"
	cp cxcl12_3d_5micron.sig $NEWFOLDER
	cp cxcl13_3d_5micron.sig $NEWFOLDER
	
	cd $NEWFOLDER
	cp ../../hyphasma/parameter_files/$1.par 0-usedParameterSet.par
	cp ../../hyphasma/parameter_files/$1.par $1.par
	echo "Launching hyphasma $1"
	./../../hyphasma/hyphasma $1 #&> $NEWFOLDER/outputBash.txt
	cd ..
	cd ../hyphasma/tools/
	./analysis ../../scripts/$NEWFOLDER/ &> trash.txt
	cd ../../scripts
	#if [ ! -f "$NEWFOLDER/resultpage.pdf" ] ; then
	#  rm -rf $NEWFOLDER
	#fi
	cp $NEWFOLDER/resultpage.pdf $NEWFOLDER.pdf
	rm $NEWFOLDER/cxcl12_3d_5micron.sig
	rm $NEWFOLDER/cxcl13_3d_5micron.sig
}
	
for f in $FILES
do
	echo " ... next ..."; 
	process $f &
done
