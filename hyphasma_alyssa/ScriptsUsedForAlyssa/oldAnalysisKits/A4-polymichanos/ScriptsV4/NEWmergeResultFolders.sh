

function merge {
	rm allOutputs/*
	mkdir allOutputs
		
	#cd $1
	#LIST=find -maxdepth 1 -type d
	#echo list
	#for f in $(find -maxdepth 1 -type d);
	#for f in $(ls -d */);
	
	#echo "\\begin{verbatim}" >> allOutputs/index.tex  
	var=0
	for f in */;
	do
	# note : leave spaces inside the []  https://stackoverflow.com/questions/4277665/how-do-i-compare-two-string-variables-in-an-if-statement-in-bash
		if [ "$f" == "allOutputs/" ]
		then
			echo "Discarding $f"
		else {
			if [ "$f" == "mergedOutputs/" ]
			then
				echo "Discarding $f"
			else {
				var=$((var+1))
				echo "($var) Copying files from $f"
				echo "$var = ${f%Done*}" >> allOutputs/index.tex  
				cd $f
				cp ../../../hyphasma/tools/average .
				./average ssumout.out 1 5 5
				mv averaged.out ssumout_smooth.out
				#rm average
			
				for g in *.out;
				do
				
					cp "$g" "../allOutputs/${g%.*}$var.out"
				done
				cd ..
			
				#echo "${f%/}"
				#cd ../hyphasma/tools/
				#./analysis ../../scripts/$f #&> trash.txt
				#cd ../../scripts/
				#cp $f/resultpage.pdf ${f%/}.pdf
			}
			fi
		}
		fi
	
	done	
	
}

merged output should not exist, so it is not taken as a simulation
rm mergedOutputs/*
rm -rf mergedOutputs

merge

Rscript mergeFolders.R
Rscript mergeDeath.R
Rscript mergeGTI.R
Rscript mergeVol.R


cd ../../hyphasma/tools/
./analysis ../../scripts/mergedOutputs/ #&> trash.txt
cd ../../scripts
#if [ ! -f "$NEWFOLDER/resultpage.pdf" ] ; then
#  rm -rf $NEWFOLDER
#fi
#cp mergedOutputs/resultpage.pdf mergedOutputs.pdf