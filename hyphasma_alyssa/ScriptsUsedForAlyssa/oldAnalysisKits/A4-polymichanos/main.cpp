#include "../hyphasma/src/setparam.h"
#include <QString>
#include <fstream>
int matrixIndex(Ig_classes i,Ig_classes j ){
    return nIg_classes * i + j;
}

/// Note: There is a copy constructor missing in DynArray
int main(void){
    ofstream f("../polymichanos/listFiles.txt", ios::out);
/*
    // 1 - Autotest
    if(true){
        Parameter par;
        par.wahl("../polymichanos/bcinflow09", false, false);// the false is very important here !!!
        cout << "... Auto-check: rewrites the default file, reads it, and checks that no parameter was changed by input/output." << endl;
        par.write("bcinflow09recon.par");

        Parameter par1;
        par1.wahl("../polymichanos/bcinflow09", false, false);
        Parameter par2;
        par2.wahl("bcinflow09recon", false, false); // do not transform to rate, will change the output file
        cout << "\nChecking that the generated file is correct..." << endl;
        Werte::compareParameterSets(par1.Value, par2.Value);
        cout << "... end of comparison" << endl;
    }*/





    if(true){
        string fFile;
  for(int fn = 0; fn < 6; ++fn){
      if(fn == 0) fFile = string("../hyphasma/parameter_files/OUT");
      if(fn == 1) fFile = string("../hyphasma/parameter_files/OUT-NoDynMut");
      if(fn == 2) fFile = string("../hyphasma/parameter_files/bcintime15InTime");
      if(fn == 3) fFile = string("../hyphasma/parameter_files/bcintime14CTRL");
      if(fn == 4) fFile = string("../hyphasma/parameter_files/bcintime13MultpMHCDNDNoDynMut");
      if(fn == 5) fFile = string("../hyphasma/parameter_files/bcintime13MultpMHCDND");

    for(int pmodif = 0; pmodif < 10; ++pmodif){

        Parameter par;
        par.wahl(fFile.c_str(), false, false);// the false is very important here !!!
        //par.Value.XX = XX;

           /*    time_tc_selection_block = 24;       // (hours) -> no blocking    382
           factor_tc_selection_block = 1.0;    //  -> no blocking           383
           time_DND_block = 24;                //                           384
           factor_DND_block = 1.0;             //  -> no blocking           385
           mode_tc_selection_block   (0=no block, 1=tc_rescue_time, 2=tc_search_duration, 3=tc_interaction_time)
           */
        QString basename = QString(fFile.c_str());
        cout << "\n\n\n\nCASE "<< pmodif << endl;
        switch(pmodif){


        case 0:{
            vector<double> vals = {3,4,5,6,7,8, 9};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.CB_fixed_times_of_divisions_in_expansion = vals[i];
                string created = (basename + QString("modInitDivTimes") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }
        case 1:{
            vector<double> vals = {1.0, 1.5, 1.8, 2.0, 2.2, 2.5, 3, 4, 6};
            for(int i = 0; i < vals.size(); ++i){ // default is 2.0
                par.Value.newBCinflux_rate = vals[i];
                string created = (basename + QString("modInflowRate") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            } // Might need to change relative
            break;
        }
        case 2:{
            Parameter par;
            par.wahl("../FileManip/bcinflow09", true, true);
            vector<double> vals = {1,3 , 5, 7, 9, 11}; // basal is 9
            for(int i = 0; i < vals.size(); ++i){
                par.Value.pMHC_dependent_K = vals[i];
                string created = (basename + QString("modDNDK") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }
        case 3:{
            Parameter par;
            par.wahl("../FileManip/bcinflow09", true, true);
            vector<double> vals = {100, 150, 200, 250, 300, 350, 400, 450, 500};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.totalTC = vals[i];
                string created = (basename + QString("modTotalTC") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }
        case 5:{
            vector<double> vals = {0.2, 0.35, 0.5, 0.7, 1.0, 2.0};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.mutation = vals[i];
                string created = (basename + QString("modMutation") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                //cout << "COMPARE\n";
                //compare(par1.Value, par.Value);
                f << created << endl;
            }
            break;
        }
        case 6:{
            vector<double> vals = {12, 24, 36, 48, 60, 72};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.Start_Differentiation = vals[i];
                string created = (basename + QString("modStartOut") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                //cout << "COMPARE\n";
                //compare(par1.Value, par.Value);
                f << created << endl;
            }
            break;
        }

        case 7:{
            vector<double> vals = {5,     6,   6.5,    7,    7.5,    8,     8.5,    9,  9.5,    10};
            par.Value.TCell = 150;
            par.Value.pMHC_dependent_K = 7;
            for(int i = 0; i < vals.size(); ++i){
                par.Value.CB_fixed_times_of_divisions_in_expansion = vals[i];
                string created = (basename + QString("K7-T150-modInitDiv") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                //cout << "COMPARE\n";
                //compare(par1.Value, par.Value);
                f << created << endl;
            }
            break;
        }

        case 8:{
            vector<double> vals = {0.4, 0.5, 0.7, 0.9};
            par.Value.CB_fixed_times_of_divisions_in_expansion = 9;
            //par.Value.pMHC_dependent_K = 7;
            for(int i = 0; i < vals.size(); ++i){
                par.Value.newBCinflux_rate= vals[i];
                string created = (basename + QString("iniDiv9-ModInflo") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                //cout << "COMPARE\n";
                //compare(par1.Value, par.Value);
                f << created << endl;
            }
            break;
        }

        default:{}


        }
    }
  }
    }

    //2 - Analysis of parameters modifying the dynamics of the GC
    if(false)
    for(int pmodif = 0; pmodif < 10; ++pmodif){
        Parameter par;
         par.wahl("../polymichanos/bcinflow09", false, false);// the false is very important here !!!

        // ????
        par.Value.min_seeder_dist = 5;
        par.Value.max_seeder_dist = 5;

        // Activates Antibody feedback
        par.Value.use_ab_dynamics = 0;  // put off first
        // ok : mk_ab is already on  // 47 : Antibody production rate in Mol per hour and output cell";
        // ok : antibody_resolution = 0: no bins, and antibody production is not used

        // note : divisions mode :
        // fixed_time_of_divisions_mode = 3,
        //  meaning that p_founder_divplus = CB_fixed_times_of_divisions_in_expansion
        // = 6 by default
        par.Value.Start_Differentiation = 72; //???


        QString basename = QString("../hyphasma/parameter_files/bcinflow09-Dist5-");
        cout << "\n\n\n\nCASE "<< pmodif << endl;
        switch(pmodif){
        case 0:{
            vector<double> vals = {3,4,5,6,7,8, 9};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.CB_fixed_times_of_divisions_in_expansion = vals[i];
                string created = (basename + QString("modInitDivTimes") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }
        case 1:{
            vector<double> vals = {1.0, 1.5, 1.8, 2.0, 2.2, 2.5, 3, 4, 6};
            for(int i = 0; i < vals.size(); ++i){ // default is 2.0
                par.Value.newBCinflux_rate = vals[i];
                string created = (basename + QString("modInflowRate") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            } // Might need to change relative
            break;
        }
        case 2:{
            Parameter par;
            par.wahl("../FileManip/bcinflow09", true, true);
            vector<double> vals = {1,3 , 5, 7, 9, 11}; // basal is 9
            for(int i = 0; i < vals.size(); ++i){
                par.Value.pMHC_dependent_K = vals[i];
                string created = (basename + QString("modDNDK") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }
        case 3:{
            Parameter par;
            par.wahl("../FileManip/bcinflow09", true, true);
            vector<double> vals = {100, 150, 200, 250, 300, 350, 400, 450, 500};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.totalTC = vals[i];
                string created = (basename + QString("modTotalTC") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }
        case 5:{
            vector<double> vals = {0.2, 0.35, 0.5, 0.7, 1.0, 2.0};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.mutation = vals[i];
                string created = (basename + QString("modMutation") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                //cout << "COMPARE\n";
                //compare(par1.Value, par.Value);
                f << created << endl;
            }
            break;
        }
        case 6:{
            vector<double> vals = {12, 24, 36, 48, 60, 72};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.Start_Differentiation = vals[i];
                string created = (basename + QString("modStartOut") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                //cout << "COMPARE\n";
                //compare(par1.Value, par.Value);
                f << created << endl;
            }
            break;
        }

        case 7:{
            vector<double> vals = {5,     6,   6.5,    7,    7.5,    8,     8.5,    9,  9.5,    10};
            par.Value.TCell = 150;
            par.Value.pMHC_dependent_K = 7;
            for(int i = 0; i < vals.size(); ++i){
                par.Value.CB_fixed_times_of_divisions_in_expansion = vals[i];
                string created = (basename + QString("K7-T150-modInitDiv") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                //cout << "COMPARE\n";
                //compare(par1.Value, par.Value);
                f << created << endl;
            }
            break;
        }

        case 8:{
            vector<double> vals = {0.4, 0.5, 0.7, 0.9};
            par.Value.CB_fixed_times_of_divisions_in_expansion = 9;
            //par.Value.pMHC_dependent_K = 7;
            for(int i = 0; i < vals.size(); ++i){
                par.Value.newBCinflux_rate= vals[i];
                string created = (basename + QString("iniDiv9-ModInflo") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                //cout << "COMPARE\n";
                //compare(par1.Value, par.Value);
                f << created << endl;
            }
            break;
        }

        default:{}
        } // end switch
    } // end for
/*

    // 3 - Analysis of Switching probabilities, for Carla
    if(true)
    for(int pmodif = 0; pmodif < 5; ++pmodif){
        Parameter par;
        par.wahl("../polymichanos/bcinflow09", false, false);// the false is very important here !!!
        par.Value.do_switch_classes = 2;
        QString basename = QString("../hyphasma/parameter_files/bcinflow09-");

        // Vary switching matrix, F = 0 in the beginning
        switch(pmodif){
        case 0:{
            vector<double> vals = {0.002, 0.005,0.0065,0.008,0.009,0.011,0.0185, 0.03};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.Founder_IgX[IgM] = 1.0;
                par.Value.Founder_IgX[IgG] = 0.0;
                par.Value.switch_matrix[matrixIndex(IgM,IgG)] = vals[i];
                par.Value.switch_matrix[matrixIndex(IgM,IgM)] = 1.0 - vals[i];

                string created = (basename + QString("F=0modSwitchRate") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }
        // Vary switching matrix, F = 0 in the beginning, cross values of p=Switch proba and g=Decay rate
        case 1:{
            vector<double> valsF = {0.005,0.0185, 0.03, 0.06};
            vector<double> valsG = {0.001, 0.002,0.005,0.01,0.02};
            for(int i = 0; i < valsF.size(); ++i){
                for(int j = 0; j < valsG.size(); ++j){
                    par.Value.Founder_IgX[IgM] = 1.0;
                    par.Value.Founder_IgX[IgG] = 0.0;
                    par.Value.switch_matrix[matrixIndex(IgM,IgG)] = valsF[i];
                    par.Value.switch_matrix[matrixIndex(IgM,IgM)] = 1.0 - valsF[i];

                    par.Value.decay_proba_switch = valsG[j];

                    string created = (basename + QString("F=0modRate=") + QString::number(valsF[i]) + QString("-G=") + QString::number(valsG[j]) + QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }
        // Vary switching matrix, F = 95 in the beginning, p=0.0185 and g=Modif decay rate
        case 2:{
            vector<double> vals = {0.002, 0.005,0.0065,0.008,0.009,0.011,0.0185, 0.03};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.Founder_IgX[IgM] = 0.05;
                par.Value.Founder_IgX[IgG] = 0.95;
                par.Value.decay_proba_switch = 0.0185;
                par.Value.switch_matrix[matrixIndex(IgM,IgG)] = vals[i];
                par.Value.switch_matrix[matrixIndex(IgM,IgM)] = 1.0 - vals[i];

                string created = (basename + QString("F=95p=0.0185modSwitchRate") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }
        // No Switching, F = changing
        case 3:{
            vector<double> vals = {0.05, 0.1,0.2,0.3,0.4,0.45,0.5, 0.55, 0.6, 0.7, 0.8, 0.9, 0.95};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.Founder_IgX[IgM] = 1.0 - vals[i];
                par.Value.Founder_IgX[IgG] = vals[i];
                par.Value.decay_proba_switch = 0.0;
                par.Value.switch_matrix[matrixIndex(IgM,IgG)] = 0.0;
                par.Value.switch_matrix[matrixIndex(IgM,IgM)] = 1.0;
                string created = (basename + QString("NoSwitchmodInitFreq") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }
        // No Switching, F = changing
        case 4:{
            vector<double> vals = {0.05, 0.1,0.2,0.3,0.4,0.45,0.5, 0.55, 0.6, 0.7, 0.8, 0.9, 0.95};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.Founder_IgX[IgM] = 1.0 - vals[i];
                par.Value.Founder_IgX[IgG] = vals[i];
                par.Value.decay_proba_switch = 0.0;
                par.Value.switch_matrix[matrixIndex(IgM,IgG)] = 0.0;
                par.Value.switch_matrix[matrixIndex(IgM,IgM)] = 1.0;
                par.Value.max_hamming_antigens = 5;
                par.Value.min_hamming_antigens = 5;
                string created = (basename + QString("NoSwitchDist5modInitFreq") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }

        case 5:{
            vector<double> vals = {0.05, 0.1,0.2,0.3,0.4,0.45,0.5, 0.55, 0.6, 0.7, 0.8, 0.9, 0.95};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.Founder_IgX[IgM] = 1.0 - vals[i];
                par.Value.Founder_IgX[IgG] = vals[i];
                par.Value.decay_proba_switch = 0.0;
                par.Value.switch_matrix[matrixIndex(IgM,IgG)] = 0.0;
                par.Value.switch_matrix[matrixIndex(IgM,IgM)] = 1.0;
                par.Value.max_hamming_antigens = 5;
                par.Value.min_hamming_antigens = 5;
                string created = (basename + QString(" ") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }

        } // end switch
    } // end if
    */

    // 4 - Blocking CD40L,
    if(false){
        string fFile;
  for(int fn = 0; fn < 2; ++fn){
      if(fn == 0) fFile = string("../hyphasma/parameter_files/OUT");
      //if(fn == 0) fFile = string("../hyphasma/parameter_files/bcinflow09");
      if(fn == 1) fFile = string("../hyphasma/parameter_files/tcmult013");
      if(fn == 2) fFile = string("../hyphasma/parameter_files/tcmult016");

    for(int pmodif = 0; pmodif < 10; ++pmodif){

        Parameter par;
        par.wahl(fFile.c_str(), false, false);// the false is very important here !!!
        //par.Value.XX = XX;

           /*    time_tc_selection_block = 24;       // (hours) -> no blocking    382
           factor_tc_selection_block = 1.0;    //  -> no blocking           383
           time_DND_block = 24;                //                           384
           factor_DND_block = 1.0;             //  -> no blocking           385
           mode_tc_selection_block   (0=no block, 1=tc_rescue_time, 2=tc_search_duration, 3=tc_interaction_time)
           */
        QString basename = QString(fFile.c_str());
        cout << "\n\n\n\nCASE "<< pmodif << endl;
        switch(pmodif){
        /*case 0:{
            par.Value.time_DND_block = 24;
            vector<double> vals = {0.0, 0.05, 0.1, 0.2, 0.35, 0.5, 0.65, 0.8, 0.85, 0.9, 0.95, 1.0};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.factor_DND_block = vals[i];
                string created = (basename + QString("StopDND-24h-Coeff") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }
        case 1:{
            vector<double> valsT = {0, 6, 12, 18, 24, 36, 48, 60, 72, 96, 120, 150};
            vector<double> valsG = {0.0, 0.05, 0.1, 0.2, 0.35, 0.5, 0.65, 0.8, 0.85, 0.9, 0.95, 1.0};
            for(int i = 0; i < valsT.size(); ++i){
                for(int j = 0; j < valsG.size(); ++j){
                    par.Value.time_DND_block = valsT[i];
                    par.Value.factor_DND_block = valsG[j];
                    string created = (basename + QString("modDNDblock-t=") + QString::number(valsT[i]) + QString("fac=") + QString::number(valsG[j]) +QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }*/
        case 2:{
            vector<double> valsT = {0, 24, 36, 48, 60, 72, 96, 120, 150, 200, 250};
            vector<double> valsG = {0.4, 0.8, 1.0, 2, 3, 4, 6, 8};
            for(int i = 0; i < valsT.size(); ++i){
                for(int j = 0; j < valsG.size(); ++j){
                    par.Value.mode_tc_selection_block = 1;
                    par.Value.time_tc_selection_block = valsT[i];
                    par.Value.factor_tc_selection_block = valsG[j];
                    string created = (basename + QString("blockRESCUE-t=") + QString::number(valsT[i]) + QString("fac=") + QString::number(valsG[j]) +QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }
        case 3:{
            vector<double> valsT = {0, 24, 36, 48, 60, 72, 96, 120, 150, 200, 250};
            vector<double> valsG = {0.0, 0.1, 0.2, 0.4, 0.6, 0.8, 1.0, 2, 5};
            for(int i = 0; i < valsT.size(); ++i){
                for(int j = 0; j < valsG.size(); ++j){
                    par.Value.mode_tc_selection_block = 2;
                    par.Value.time_tc_selection_block = valsT[i];
                    par.Value.factor_tc_selection_block = valsG[j];
                    string created = (basename + QString("blockSEARCHDUR-t=") + QString::number(valsT[i]) + QString("fac=") + QString::number(valsG[j]) +QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }
        case 4:{
            vector<double> valsA0 = {0, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
            vector<double> valsnHill = {1.0, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.25, 2.5, 4};
            for(int i = 0; i < valsA0.size(); ++i){
                for(int j = 0; j < valsnHill.size(); ++j){
                    par.Value.TFHsignal_of_P0divisions = valsA0[i];
                    par.Value.pMHC_dependent_nHill= valsnHill[j];
                    string created = (basename + QString("modA0-") + QString::number(valsA0[i]) + QString("-n") + QString::number(valsnHill[j]) +QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }
        case 5:{
            vector<double> valsT = {0, 24, 36, 48, 72, 96, 120, 150, 200};
            vector<double> valsG = {0.2, 0.4, 0.5, 0.6, 0.8};
            for(int i = 0; i < valsT.size(); ++i){
                for(int j = 0; j < valsG.size(); ++j){
                    par.Value.mode_tc_selection_block = 4;
                    par.Value.time_tc_selection_block = valsT[i];
                    par.Value.factor_tc_selection_block = valsG[j];
                    string created = (basename + QString("KaiblockSSIGNAL-t=") + QString::number(valsT[i]) + QString("fac=") + QString::number(valsG[j]) +QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }/*
        case 5:{
            vector<double> valsT = {0, 24, 36, 48, 60, 72, 96, 120, 150, 200, 250};
            vector<double> valsG = {0, 0.1, 0.2, 0.4, 0.6, 0.8, 0.9, 1.0, 1.1, 1.25, 1.5, 2, 5};
            for(int i = 0; i < valsT.size(); ++i){
                for(int j = 0; j < valsG.size(); ++j){
                    par.Value.mode_tc_selection_block = 4;
                    par.Value.time_tc_selection_block = valsT[i];
                    par.Value.factor_tc_selection_block = valsG[j];
                    string created = (basename + QString("blockSSIGNAL-t=") + QString::number(valsT[i]) + QString("fac=") + QString::number(valsG[j]) +QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }
        /*case 5:{
           vector<double> vals = {0.0, 0.1, 0.2, 0.35, 0.5, 0.75, 0.9};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.time_DND_block = 24;
                par.Value.factor_founder_div_block = vals[i];
                par.Value.factor_DND_block = 0.5;
                string created = (basename + QString("DNDblock24h-0.5DND-Found") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }/*
        case 4:{
            vector<double> vals = {1,2};
            for(int i = 0; i < vals.size(); ++i){
                //par.Value.XXX = vals[i];
                string created = (basename + QString("modXXX") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }*/
        } // end switch
    } // end for
  }
  }

    if(false){
        string fFile;
//  for(int fn = 0; fn < 3; ++fn){
      int fn = 0;
//      if(fn == 0) fFile = string("../hyphasma/parameter_files/bcinflow09");
        if(fn == 0) fFile = string("../hyphasma/parameter_files/OUT");

//      if(fn == 0) fFile = string("../hyphasma/parameter_files/bcintime13Tfh");
      if(fn == 1) fFile = string("../hyphasma/parameter_files/tcmult013");
      if(fn == 2) fFile = string("../hyphasma/parameter_files/tcmult016");

    for(int pmodif = 0; pmodif < 10; ++pmodif){

        Parameter par;
        par.wahl(fFile.c_str(), false, false);// the false is very important here !!!
        //par.Value.XX = XX;

           /*    time_tc_selection_block = 24;       // (hours) -> no blocking    382
           factor_tc_selection_block = 1.0;    //  -> no blocking           383
           time_DND_block = 24;                //                           384
           factor_DND_block = 1.0;             //  -> no blocking           385
           mode_tc_selection_block   (0=no block, 1=tc_rescue_time, 2=tc_search_duration, 3=tc_interaction_time)
           */
        QString basename = QString(fFile.c_str());
        cout << "\n\n\n\nCASE "<< pmodif << endl;
        switch(pmodif){
/*        case 0:{
            par.Value.tc_search_duration_mode = 1;
            vector<double> vals = {0.1, 0.12, 0.13, 0.14, 0.15, 0.175, 0.2, 0.3, 0.4, 0.5, 0.7, 0.85, 1.0, 2.0};
            for(int i = 0; i < vals.size(); ++i){
                par.Value.TC_rescue_time = vals[i];
                string created = (basename + QString("modTimeOut013-") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }*/
        /*case 0:{
            vector<double> valsRes = {0.1, 0.12, 0.13, 0.14, 0.15, 0.175, 0.2, 0.3, 0.4, 0.5, 0.7, 0.85, 1.0, 2.0};
            vector<double> valsDur = {0.2, 0.4, 0.6, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0, 1.1, 1.2, 1.5, 2.};
            for(int i = 0; i < valsRes.size(); ++i){
                for(int j = 0; j < valsDur.size(); ++j){
                    par.Value.TC_rescue_time = valsRes[i];
                    par.Value.tc_search_duration_per_FDCcontact= valsDur[j];
                    string created = (basename + QString("mod-Resc=") + QString::number(valsRes[i]) + QString("add=") + QString::number(valsDur[j]) +QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }
        case 1:{
            par.Value.tc_search_duration_mode = 1; // fixed searching time
            vector<double> valsRes = {0.1, 0.12, 0.13, 0.14, 0.15, 0.175, 0.2, 0.3, 0.4, 0.5, 0.7, 0.85, 1.0, 2.0};
            vector<double> valsTotal = {0.2, 0.4, 0.6, 0.8, 1.0, 1.25, 1.5, 1.75, 2.0, 2.5, 3, 3.5, 5.0, 7.5, 10};
            for(int i = 0; i < valsRes.size(); ++i){
                for(int j = 0; j < valsTotal.size(); ++j){
                    par.Value.TC_rescue_time = valsRes[i];
                    par.Value.tc_search_duration_fixed= valsTotal[j];
                    string created = (basename + QString("TOUT-Resc=") + QString::number(valsRes[i]) + QString("Tot=") + QString::number(valsTotal[j]) +QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }*/

/*        case 2:{
            vector<double> valsT = {0, 6, 12, 18, 24, 36, 48, 60, 72, 96, 120, 150};
            vector<double> valsG = {0.0, 0.2, 0.4, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2, 3, 4, 5, 8};
            for(int i = 0; i < valsT.size(); ++i){
                for(int j = 0; j < valsG.size(); ++j){
                    par.Value.mode_tc_selection_block = 1;
                    par.Value.time_tc_selection_block = valsT[i];
                    par.Value.factor_tc_selection_block = valsG[j];
                    string created = (basename + QString("modSelecblock-1RESCUE-t=") + QString::number(valsT[i]) + QString("fac=") + QString::number(valsG[j]) +QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }
        case 3:{
            vector<double> valsT = {0, 6, 12, 18, 24, 36, 48, 60, 72, 96, 120, 150};
            vector<double> valsG = {0.0, 0.1, 0.2, 0.4, 0.6, 0.7, 0.8, 0.9, 0.95, 1.0, 1.05, 1.1, 1.2, 1.25, 1.3, 1.4, 1.5};
            for(int i = 0; i < valsT.size(); ++i){
                for(int j = 0; j < valsG.size(); ++j){
                    par.Value.mode_tc_selection_block = 2;
                    par.Value.time_tc_selection_block = valsT[i];
                    par.Value.factor_tc_selection_block = valsG[j];
                    string created = (basename + QString("modSelecblock-2SEARCHDUR-t=") + QString::number(valsT[i]) + QString("fac=") + QString::number(valsG[j]) +QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }
        case 4:{
            vector<double> valsT = {0, 6, 12, 18, 24, 36, 48, 60, 72, 96, 120, 150};
            vector<double> valsG = {0.0, 0.1, 0.2, 0.4, 0.6, 0.7, 0.8, 0.9, 0.95, 1.0, 1.05, 1.1, 1.2, 1.25, 1.3, 1.4, 1.5};
            for(int i = 0; i < valsT.size(); ++i){
                for(int j = 0; j < valsG.size(); ++j){
                    par.Value.mode_tc_selection_block = 3;
                    par.Value.time_tc_selection_block = valsT[i];
                    par.Value.factor_tc_selection_block = valsG[j];
                    string created = (basename + QString("modSelecblock-3INTERTIME-t=") + QString::number(valsT[i]) + QString("fac=") + QString::number(valsG[j]) +QString(".par")).toStdString();
                    par.write(created);
                    f << created << endl;
                }
            }
            break;
        }*/
        } // end switch
    } // end for
  }


    // 10 - E:mpty Analysis
    if(false)
    for(int pmodif = 0; pmodif < 10; ++pmodif){
        Parameter par;
        par.wahl("../polymichanos/bcinflow09", false, false);// the false is very important here !!!
        //par.Value.XX = XX;

        QString basename = QString("../hyphasma/parameter_files/bcinflow09-");
        cout << "\n\n\n\nCASE "<< pmodif << endl;
        switch(pmodif){
        case 0:{
            vector<double> vals = {1,2};
            for(int i = 0; i < vals.size(); ++i){
                //par.Value.XXX = vals[i];
                string created = (basename + QString("modXXX") + QString::number(vals[i]) + QString(".par")).toStdString();
                par.write(created);
                f << created << endl;
            }
            break;
        }
        } // end switch
    } // end for

    f.close();
    return 0;
}
/*int bla(){
    par.Value.pMHC_dependent_P_max;


    OK par.Value.totalTC;
    vector<double> vals = {};

    par.Value.Start_Differentiation; //??
    par.Value.totalB;
    par.Value.totalBss; //????
    par.Value.newBCinflux_stop;




    par.Value.TC_rescue_time;



    par.Value.GC_radius;
    par.Value.Start_Mutation;
    par.Value.StartOutput;
    par.Value.proliferate; // Not used ?
    par.Value.mutation;
    par.Value.CB_fixed_times_of_divisions;
    par.Value.collectFDCperiod;
    par.Value.TCell; //???


    // DND:

            // what are blasts ???

 // Antigens: APeakNumber
}
*/
