#include "spline.h"
#include "tableCourse.h"

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
using namespace std;

double maxVec(vector<double> v){
    return *max_element(v.begin(),v.end());
}

vector<double> smoothen(vector<double> input, int width){
    vector<double> res;
    res.resize(input.size(), 0);
    for(unsigned int i = 0; i < input.size(); ++i){
        double sum = 0;
        double cpt = 0;
        for(int j = max(0,(int) i-width / 2); j <= min((int) input.size()-1, (int)  i+ (width / 2)); ++j){
            cpt++;
            sum += input[j];
        }
        if(cpt > 1e-9) res[i] = sum / cpt;
    }
    return res;
}

double sq(double x){return x*x;}

#define out of5
int main(void){

    ofstream of5;
    of5.open("GTI-detailsfit.txt");

    TableCourse TGTI = TableCourse(string("GCdynKaiTable.txt"));
    //cout << TGTI.print() << endl;

    //TableCourse TXvol = TableCourse(string("xvolume.out"), 9, 504, false);
    //cout << TXvol.print() << endl;

    TableCourse TSumOut = TableCourse(string("ssumout.out"), 5, 504, false);
    //cout << TSumOut.print() << endl;

    TableCourse TSum = TableCourse(string("ssum.out"), 2, 504, false);
    //cout << TSumOut.print() << endl;

    TableCourse TSApo = TableCourse(string("ssumapo1.out"), 2, 504, false);
    //cout << TSApo.print() << endl;

    // Want to do :

    // 1 - take splines for GC volume, GTI and GTI / Vol
   // tk::spline sVol = tk::spline();
   // sVol.set_points(TXvol.getTimePoints(), TXvol.getTimeCourse(2), false);

    // now from ssum
    tk::spline sVol = tk::spline();
    sVol.set_points(TSum.getTimePoints(), TSum.getTimeCourse(0), false);

    //for(int i = 1; i < 50; i = i + 1){
    //    cout << i << "\t" << TXvol(2,i-1) << "\t" <<  sVol(i) << endl;
    //}



    vector<double> smoothenedGTIvol = smoothen(TSumOut.getTimeCourse(1), 10);

    tk::spline sGTIold = tk::spline();
    sGTIold.set_points(TSumOut.getTimePoints(), TSumOut.getTimeCourse(1), true);


    for(double i = 1; i < 500; i = i + 1){
    //    cout << i << "\t" << TSumOut(1,i-1) << "\t" << smoothenedGTIvol[i] << "\t" <<  sGTIold(i) << endl;
    }

    tk::spline sGTI = tk::spline();
    sGTI.set_points(TSumOut.getTimePoints(), smoothenedGTIvol, true);


    // 1- plot time courses:
    double MaxVolData = maxVec(TGTI.getTimeCourse(0));
//    double MaxVolSim = maxVec(TXvol.getTimeCourse(2));
    double MaxVolSim = maxVec(TSum.getTimeCourse(0));

    double MaxGTIperVolData = maxVec(TGTI.getTimeCourse(2));
    double MaxGTIperVolsim = 0;
    for(unsigned int i = 0; i < min(TSumOut.attribut.size(), TSum.attribut.size()); ++i){
        MaxGTIperVolsim = max(MaxGTIperVolsim, TSumOut(1,i) / (max(0.0001, TSum(0,i)))); // TXvol(2,i)
    }
    out << "MaxGTI/Vol in sim: " << MaxGTIperVolsim << endl;
    out << "MaxGTI/Vol data  : " << MaxGTIperVolData << endl;
    double coeffMultGTIsim = MaxGTIperVolData / MaxGTIperVolsim;
    out << "Suggested coefficient, +/- some percents:" << coeffMultGTIsim << endl;
    out << "Max Vol in sim   : " << MaxVolSim << endl;
    out << "Max Vol in data  : " << MaxVolData << endl;
    double coeffDivVolsim = MaxVolSim / MaxVolData;
    out << "Suggested coefficient, +/- some percents:" << coeffDivVolsim << endl;


    ofstream of1;
    of1.open("GTIfitting.txt");


    out << "TimeStart\tRSS(Vol)\tCoeffV\tRSStot\tCoeffV\tCoeffG" << endl;
    of1 << "TimeStart\tRSS(Vol)\tCoeffV\tRSStot\tCoeffV\tCoeffG" << endl;

    // fitting for only Vol
    double bestRSSGlob = 1e10;
    double bestCfGTIGlob = NAN;
    double bestCfVolGlob = NAN;
    double bestDelay = NAN;
    double bestRSSGlob2 = 1e10;
    double bestCfGTIGlob2 = NAN;
    double bestCfVolGlob2 = NAN;
    double bestDelay2 = NAN;


    int cpt = 0;
    for(double timeStartGC = 10; timeStartGC <= 80; timeStartGC = timeStartGC + 0.25){
    //double timeStartGC = 22;
        cpt++;
        if(!cpt) out << "Comparison data - sim when making simulations start at " << timeStartGC << "hours" << endl;
        if(!cpt)out << "time\tDataGTI/Vol\tDataVol\tTimeInSim\tGTIsim\tVolSim\tVolsimMult\tGTIpVolsim\tGTIpVolMult\tAddedRSS" << endl;

        // fitting for only Vol
        double bestRSS = 1e10;
        double bestCfGTI = NAN;
        double bestCfVol = NAN;

        // fitting for only Vol
        double bestRSS2 = 1e10;
        double bestCfGTI2 = NAN;
        double bestCfVol2 = NAN;

        for(double coeffVol = 0.75; coeffVol <= 1.5; coeffVol += 0.005){ // loop in loop not needed, do better next time
        //double coeffVol = 1.0; {
            for(double coeffGTI = 0.75; coeffGTI <= 2.0; coeffGTI += 0.005){
            //double coeffGTI = 1.0; {
                double RSScoeff = 0;
                double RSScoeff2 = 0;
                //duouble logRSScoeff = 0;
                for(int i = 0; i < TGTI.nbLignes; ++i){
                    double thetime = TGTI.attribut[i];
                    double timeinsim = thetime - timeStartGC;
                    double GTIsim = sGTI(timeinsim);
                    double VolSim = sVol(timeinsim);
                    if(timeinsim > 0){
                        double GTIperVol = GTIsim / (max(0.0001, VolSim));
                        double addRSS = 0;
                        if(TGTI(1,i) > 1e-6) addRSS += sq((TGTI(0,i) - VolSim / (coeffDivVolsim * coeffVol)) / (TGTI(1,i)));
                        double addRSS2 = addRSS;
                        if(TGTI(3,i) > 1e-6) addRSS2 += sq((TGTI(2,i) - GTIperVol * (coeffMultGTIsim * coeffGTI)) / (TGTI(3,i)));
                        //cout << time << "\t" << TGTI(2,i) << "\t" << TGTI(0,i) << "\t" << timeinsim << "\t" << GTIsim << "\t" << VolSim << "\t" << GTIperVol << "\t" << addRSS << endl;
                        if(!cpt)out << thetime << "\t" << TGTI(2,i) << "\t" << TGTI(0,i) << "\t" << timeinsim << "\t" << GTIsim << "\t" << VolSim << "\t" << VolSim / (coeffDivVolsim * coeffVol) << "\t" << GTIperVol << "\t" << GTIperVol * (coeffMultGTIsim * coeffGTI) << "\t" << addRSS << endl;

                        RSScoeff += addRSS;
                        RSScoeff2 += addRSS2;
                        //logRSScoeff +=
                    }
                }
                //cout << "RSS for coeff " << RSScoeff << endl;
                if(RSScoeff < bestRSS){
                    bestRSS = RSScoeff;
                    bestCfGTI = coeffMultGTIsim * coeffGTI;
                    bestCfVol = coeffDivVolsim * coeffVol;
                }

                if(RSScoeff2 < bestRSS2){
                    bestRSS2 = RSScoeff2;
                    bestCfGTI2 = coeffMultGTIsim * coeffGTI;
                    bestCfVol2 = coeffDivVolsim * coeffVol;
                }
            }
        }

        //cout << "RSS for coeff " << RSScoeff << endl;
        if(bestRSS < bestRSSGlob){
            bestRSSGlob = bestRSS;
            bestCfGTIGlob = bestCfGTI;
            bestCfVolGlob = bestCfVol;
            bestDelay = timeStartGC;
        }

        if(bestRSS2 < bestRSSGlob2){
            bestRSSGlob2 = bestRSS2;
            bestCfGTIGlob2 = bestCfGTI2;
            bestCfVolGlob2 = bestCfVol2;
            bestDelay2 = timeStartGC;
        }
        out << timeStartGC << "\t" << bestRSS << "\t" << bestCfVol << "\t" << bestRSS2 << "\t" << bestCfVol2 << "\t" << bestCfGTI2 << endl;
        of1 << timeStartGC << "\t" << bestRSS << "\t" << bestCfVol << "\t" << bestRSS2 << "\t" << bestCfVol2 << "\t" << bestCfGTI2 << endl;
    }
    of1.close();

    ofstream of2;
    of2.open("GTI-BestDataShiftedDelay1.txt");
    of2 << "time\ttimeinsim\tGCvolsim\tGTIpervolsim\n";
    for(int i = (int) bestDelay + 1; i < 504; ++i){
        double timeinsim = i - bestDelay;
        double GTIsim = sGTI(timeinsim);
        double VolSim = sVol(timeinsim);
        double GTIperVol = GTIsim / (max(0.0001, VolSim));
        of2 << i << "\t" << timeinsim << "\t" << VolSim / (bestCfVolGlob) << "\t" << GTIperVol * (coeffMultGTIsim /* default */) << endl;
    }
    of2.close();

    ofstream of3;
    of3.open("GTI-BestDataShiftedDelay2.txt");
    of3 << "time\ttimeinsim\tGCvolsim\tGTIpervolsim\n";
    for(int i = (int) bestDelay2 + 1; i < 504; ++i){
        double timeinsim = i - bestDelay2;
        double GTIsim = sGTI(timeinsim);
        double VolSim = sVol(timeinsim);
        double GTIperVol = GTIsim / (max(0.0001, VolSim));
        of3 << i << "\t" << timeinsim << "\t" << VolSim / (bestCfVolGlob2) << "\t" << GTIperVol * (bestCfGTIGlob2) << endl;
    }
    of3.close();

    out << "End of fitting: best values" << endl;
    out << "Fitting, only GC volume " << endl;
    out << "Best delay\t: " << bestDelay << endl;
    out << "bestRSSGlob\t" << bestRSSGlob << endl;
    out << "bestCfVolGlob\t" << bestCfVolGlob << endl;
    out << endl;
    out << "Fitting GC volume and GTI " << endl;
    out << "Best delay\t : " << bestDelay2 << endl;
    out << "bestRSSGlob\t" << bestRSSGlob2 << endl;
    out << "bestCfVolGlob\t" << bestCfVolGlob2 << endl;
    out << "bestCfGTIGlob2\t" << bestCfGTIGlob2 << endl;

    of5.close();


    // GTI / (CB + CC + apo)

    //

    //vector<double> getTimeCourse(int var);
    //vector<double> getTimePoints();



    return -1;

}
