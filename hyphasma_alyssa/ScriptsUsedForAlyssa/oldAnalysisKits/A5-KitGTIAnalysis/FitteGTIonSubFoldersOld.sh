cd ../5-KitGTIAnalysis/
make
cd ../scripts/

#this function is to make it parallel
function gtiAndCopyUp {
	cd $1
	./analyzeGTI_GC
	gle *.gle
	latex -interaction=nonstopmode resultGTI.tex
	latex -interaction=nonstopmode resultGTI.tex
	dvipdf resultpage.dvi
	cp resultGTI.pdf ../fitteGTI$1.pdf
	cd ..
}

function gtiSubFolders {
	for f in */;
	do
		echo "${f%/}"
		cp 5-KitGTIAnalysis/*.* $f/. -f
		# old way (non parallel)
		#cd $f
		#./mergeResultFolders.sh
		#cd ..
		#cp -r $f/mergedOutputs merged$f
		trap 'kill %1' SIGINT	#to stopp all subroutines when killing the original one
		gtiAndCopyUp $f &
	done
}


gtiSubFolders

