function reanalyze2 {
	g++ glesSD.cpp -o glesSD.out -std=c++11
	#rm allOutputs/*
	if [ ! -d allOutputs ]; then
		mkdir allOutputs
	fi
	rm index.tex
		
	#cd $1
	#LIST=find -maxdepth 1 -type d
	#echo list
	#for f in $(find -maxdepth 1 -type d);
	#for f in $(ls -d */);
	
	#echo "\\begin{verbatim}" >> allOutputs/index.tex  
	var=1
	for f in */;
	do
	# note : leave spaces inside the []  https://stackoverflow.com/questions/4277665/how-do-i-compare-two-string-variables-in-an-if-statement-in-bash
		if [ "$f" == "allOutputs/" ]
		then
			echo "Discarding $f"
		else {
			if [ "$f" == "mergedOutputs/" ]
			then
				echo "Discarding $f"
			else {
				echo "($var) Copying files from $f"
				echo "$var = ${f%Done*}" >> index.tex  
				cd $f
				for g in *.out;
				do
					cp "$g" "../allOutputs/${g%.*}$var.out"
				done
				cd ..
				
			#echo "${f%/}"
			#cd ../hyphasma/tools/
			#./analysis ../../scripts/$f #&> trash.txt
			#cd ../../scripts/
			#cp $f/resultpage.pdf ${f%/}.pdf
			var=$((var+1))
			}
			fi
		}
		fi
	
	done	
	cd allOutputs
	rm *.gle
	rm *.eps  #.eps are a problem for the shiftFiles.R looking for *.out
	# creates all gle files
	#R --vanilla < ../shiftFiles.R 
	./../glesSD.out $((var-1)) ../index.tex
	
	for i in *.gle; do gle -cairo $i; done
	#echo "\\end{verbatim}" >> index.tex  
	
	
	latex -interaction=nonstopmode -shell-escape ../resultCompare.tex > trashtex.txt
	#latex -interaction=nonstopmode -shell-escape ../resultCompare.tex
	dvipdf resultCompare.dvi
	dvipdfm resultCompare.dvi
	cp resultCompare.pdf ../resultCompare.pdf
	
}

reanalyze2 

