Find the data for the run gc3djem08 in hyphasma10.02.1

Find the data for the following runs relevant 
for the JEM-paper in v8.04.2(tmp):

gc3d024a:		no chemotaxis
gc3d024b:		weak chemotaxis for 6 hours
gc3d024c:		moderate chemotaxis for 6 hours
gc3d024c_restore:	same as gc3d024c with restored population kinetics
gc3d024d:		moderate chemotaxis for 6 hours with desensitisation

gc3d024:		gc3d024b with 90 tracked cells only for traces-figures

gc3d024b_nolimit:	same as gc3d024b with unlimited CXCL13 sensitivity
gc3d024c_nolimit:	same as gc3d024c with unlimited CXCL13 sensitivity
gc3d024c_nolimitrestore:same as gc3d024c_nolimit with restored population kinetics
gc3d024d_nolimit:       same as gc3d024d with unlimited CXCL13 sensitivity

In each of these (for gc3d024 just 90 cells):
	track05:	tracking 6000 cells for 30 minutes
	track1:		tracking 6000 cells for 1 hour
	track3:		tracking 6000 cells for 3 hours
	track10:	tracking 6000 cells for 10 hours


In Thilo's gle-files the corresponding directories have to be adopted
(sequence a,b,c,d is paper1,2(x),3,4 in the previous data).
The volume and affinity maturation curves have to be collected from
xvolume.out-files in the respective directories.

Note that cell clumping is reduced because the cell in average
differentiate at a site which is farer away from the clump
(compared to signal-induced differentiation). This implies that
the motility coefficient is not markably changed. This is not
an effect of the changed population kinetics. If the kinetics
are restored, almost the same picture emerges 
(see gc3d024c_restore). However, it is changed (thus clumping
is more pronounced) if CXCL13 is not limited to 6 hours
(see gc3d024b,c,d_nolimit and gc3d024c_nolimitrestore).