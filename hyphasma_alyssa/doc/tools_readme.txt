=========================================
Running simulations:
=========================================

The simulation is started with
>> run <parname>
where <parname> is the parameter file withou suffix
which should be saved in the subdirectory <parameter_files>.

<run> also calls <save2dir>
which moves all result-files into a subdirectory of <results>
with a name identical to <parname>.
Note that a previous run with the same name would be overwritten!
So don't run the same parameter file twice.

<run> also calls <clear>
which removes all unnecessary files from the root directory.

Remark:
<run> simplifies the run - save - clear procedure.
These batches may also be called one by one:
>> hyphasma ./parameter_files/<parname>
>> save2dir <parname>
>> clear
where <parname> is the parameter file name as above.
This allows to save simulations based on the same parameter file  
with different names in the subdirectory <results>.


=========================================
Basic data analysis
=========================================
A saved simulation can be analysed by calling 
>> analysis <simname>
in the subdirectory <tools>,
where <simname> is the simulation-directory-basename in <results>. 
<analysis> requires <average>, <mkfigure>, and <resultpage.tex>
in the <tools> directory.
gle-files in <gle> and experimental data in <data> may be needed.
The analysis is saved in <resultpage.pdf> in <simname> and displayed.

Remark on displayed analysis files:
The set of data displayed can be changed by adapting <resultpage.tex>
in the directory <tools> (don't change in <simname> as it is overwritten
when <analysis> is called in <tools>).
One may also rename <resultpage.tex> locally in <simname>, modify
and compile the new tex-file for particular analyses (then the eps-files
need to be kept -- see below).

eps-files are deleted by default to save space. 
This might be changed by out-commenting this line in <analysis>.

<resultpage.pdf> is displayed by default. 
This might be changed by out-commenting this line in <analysis>.



=========================================
brainbow analysis
=========================================
Clonal events are collected in brainbow.out in the results directories.
Depending on the settings of the simulation, an inital brainbow
staining and analysis has been performed and saved in output files
clon*.out.

A new or first brainbow staining can be induced by calling <brainbowmk>
and is subsequently analysed for clonal and color dominance.
This is called by:
>> brainbowmk <simname>
where <simname> is the name of the directory in the directory <results>
that contains brainbow.out.

The settings for the analysis are taken from <tools/brainbow_file.cpp>.
If these settings are to be changed, this file has to be edited.
It is then used for compiling before the analysis.

Note that each time <brainbowmk> is called, the previous analysis
is stored in a subdirectory of <simname> called <clonality_previous>.
This allows to keep the original analysis. However, it is replaced at
each call of <brainbowmk>, thus, only the last two analyses are kept.

Visualisation of the files clon*.out generated in this way are performed
with R. Corresponding routines are found in <tools/Rfiles/>.




=========================================
Cell tracking analysis
=========================================
When cell tracking is active according to the setting in the parameter file,
a file <trackraw.out> is generated and saved in the result directory.

This file can be analysed by evoking:
>> celltracking <simname>
in the <tools> directory, where <simname> is the name of the directory
in the results directory.

This generates a number of new out files plus generates figures based
on gle-files, which are shown on the screen.

TO DO:
A better version for the future would be to use a tex-file as for the
standard analysis.


=========================================
=========================================




=========================================
=========================================
