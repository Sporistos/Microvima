#ifdef TROUBLE


enum states {
  // state of knots, attribution to cell types
  nocell,
  CB,
  CC,
  FDC,
  TC,
  out,
  blast1,
  blast2,
  BETA,
  N_cells
};

struct miniSpacePoint {
    miniSpacePoint();
    states cell;
    long listi;
    long FDClisti; // network of FDCs (then, the index of the FDC)
};

enum signal_molecule {
  sig_differ2CC,     // hypothetic signal inducing CB differentiation to CC
  sig_proliferate,   // hypothetic signal inducing mitosis
  CXCL12,            // Ligand for CXCR4 expressed on B cells
  CXCL13,            // Ligand for CXCR5 expressed on B cells
  antibody,          // soluble antibody specific for antigen
  antigen,           // soluble antigen
  SEMA4D,            // soluble SEMA4D semaphorin
  NB_signals            // number of signals
};

struct miniSignalPoint {
    miniSignalPoint();
    double signal[NB_signals];
    double signal_temp[NB_signals];
};

struct mini3DGrid {
    mini3DGrid();
    int prodimvec[3];
    long Index(const int &i, const int &j, const int &k){
        return i + j * prodimvec[0] + k * prodimvec[0] * prodimvec[1];
    }
    void get_koord(long int wo, long * k){
        long int reduce,p;
        int dim = 3;
        for (int n = dim - 1; n >= 0; n--) {
          p = 1;
          for (short i = 0; i < n; i++) {
            p *= prodimvec[i];
          }
          reduce = wo / p;
          k[n] = reduce;
          wo -= reduce * p;
        }
    }
};



struct miniCell {
    // added for miniGC
    states type_of_cell;

    // fields for all cells
    bool responsive2signal[NB_signals]; // to  say if follow gradients of

    // CC fields :
    enum centrocytes {
      unselected,contact,FDCselected,TCcontact,selected,apoptosis
    };
    centrocytes CCstate;
    // unselected fields
    bool selectable;// = 0;
    bool mobile; // = 0;
    double clock; // = 0;
    double selected_clock; //  time since became an unselected CentroCyte, continue increasing in contact mode
    double  bound_ag_index;
    double  nFDCcontacts;

    // FDC specific fields :
    int volume;
    vector<long> fragments; // [f] position of each fragment
    vector< vector<double> > antigen_amount; // [f][0]
    vector< vector< vector<double> > >ic_amount; //[f][0][0]
};




struct miniGC {


    // cellman fields
    double time;

    vector<miniCell> OUT_list;
    vector<miniCell> FDC_list;
    vector<miniCell> CB_list;
    vector<miniCell> CC_list;
    vector<miniCell> TC_list;
    vector<long> STROMA_list;

    vector<miniSignalPoint> sigsknot;   // use : sigsknot[index]->signal[CXCL12]
    vector<miniSpacePoint> cellknot;    // use : cellknot[index]->cell or listi








double p_dif(double time){


}

double p_dif2out(double time){


}

double p_dif2out_DEC(double time){

}

void move(miniCell* C){
    if (OUT_list[li].contact_inhibited) {
      // reset contact_inhibited in order to ensure that it remains false in general
      OUT_list[li].contact_inhibited = false;
      // this also implies volume==1 and moved==0.
      // save this cell for later processing
      redo.add(OUT_list[li].index);
    }
}


void signal_production(miniCell* C){

}

void PM_differentiate(double dt){

}

// cellFDC::vesicle = par.Value.FDCvesicle;

void simulate(){
    long int nmax = long ((p.Value.tmax - p.Value.tmin) / p.Value.deltat + 0.5);
    for (n = 1; n <= nmax; n++) {
        time += p.Value.deltat;
        //function c.time_step()
        {
            // Manipulations : injecting antibodies or OVA

            // list of impossible moves to retry
            dynarray<long> redo(100, 1, 0);


            // Update the OUT cells
            //calc_OUT(l, s, shape, redo);
            {
                vector<double> m;
                random2_sequence(m, OUT_list.size(););
                for (n = 0; n < m.size(); n++) {
                    miniCell* C = &(OUT_list[m[n]]);
                    if(!(move(C, time, redo))){     // If moving was blocked,
                        redo.push_back(C->index);
                    }
                    //signal_production(C);
                    //p_mk_ab = par.Value.mk_ab * par.Value.deltat / par.Value.ag_threshold;
                    signal_secretion(C,antibody,vesicle /* yes/no */ ,p_mk_ab);

                }
            }

            // Update the OUT EXT cells to become producers
            // shape.PM_differentiate(soutext, soutextproduce, dt);
            {

            }

            // Makes the producer cells produce antibodies
            //if (use_antibody_bins) {
            //  Ab.produce_antibodies_outside(shape);
            {
                for (int n = 0; n < n_producers; n++) {
                  // get the Index of Ab in AffinitySpace associated with Ab-producing cell type <n>
                  nASindex = AS.get_AbProducingIndex(n);
                  // calculate the change of Ab-amount for this Ab (production and degradation)
                  d_ab = AS.get_AbProducingCells(n) * antibody_production_factor
                         - antibody_degradation * AS.get_AbAmount(nASindex);
                  // add this amount on AffinitySpace
                  AS.put_Ab(nASindex,d_ab);
                  // add this amount to the respective Ab-bins for each antigen
                  for (unsigned int a = 0; a < ab_bins.size(); a++) {
                    ab_bins[a].production(nASindex,d_ab,AS);
                  }
                }
            }

            // Update the FDC cells
            // calc_FDC(l, s, Ab, shape); // cellman
            {
                vector<double> m;
                random2_sequence(m, FDC_list.size(););
                for (n = 0; n < m.size(); n++) {
                    miniCell* C = &(FDC_list[int (m[n])]);
                    // cellFDC::signal_production(l.knot[m[n]].guest_grid_pointer, s);
                    {
                        //p_mksignal = par.Value.mksignal * par.Value.deltat;
                        if(p_mksignal > 0.) signal_secretion(C, sig_differ2CC, vesicle /* yes/no */, p_mksignal); // here, decides to secrete via vesicles, so integer ???
                        // p_mkCXCL13 = par.Value.mkCXCL13 (mol/(cell l hr)) * par.Value.deltat (hr) * par.Value.dx * par.Value.dx * par.Value.dx * 1.e-15 (l) * par.N_A;
                        if(p_mkCXCL13 > 0.) signal_secretion(C, CXCL13, vesicle /* yes/no */,p_mkCXCL13);
                        // p_mkSEMA4D = par.Value.mk_SEMA4D * par.Value.deltat * par.Value.dx * par.Value.dx * par.Value.dx * 1.e-15  * par.N_A;
                        if(p_mkSEMA4D > 0.) signal_secretion(C, SEMA4D,  esicle /* yes/no */,p_mkSEMA4D);
                    }

                    // two ways of managing antibodies feedback:
                    if(mk_ab > 0.){
                        // C->mk_immune_complex(dt, s);
                        {
                            // for each fragment of the FDC,
                            for (int f = 0; f < C->volume; f++){
                                double d_ic = d_t * (ic_k_on * antigen_amount[f][0] * ab - ic_k_off * ic_amount[f][0][0]);
                                /*l.*/sigsknot[C->fragments[f]].signal[antibody] -= d_ic;
                                C->antigen_amount[f][0] -= d_ic;
                                C->ic_amount[f][0][0] += d_ic;
                            }
                        }
                    } else { // means use_antibody_bins == 1. NOTE : in this case, the antibody is not consumed
                        // C->mk_immune_complex(dt, Ab, AS);
                        {
                            for (int f = 0; f < C->volume; f++) {
                                for (int a = 0; a < n_Antigen; a++) {
                                    for (int i = 0; i <= ABS.antibodies_resolution; i++) {
                                        double ab = 1.e+15 * ABS.ab_bins[a].antibodies[i];
                                        double dic = d_t * (ABS.ab_bins[a].k_on[i] * antigen_amount[f][a] * ab
                                                  - ABS.ab_bins[a].k_off[i] * ic_amount[f][i][a]);
                                        C->antigen_amount[f][a] -= dic / cellFDC::ag_threshold;         // in units of ag_threshold
                                        C->ic_amount[f][i][a] += dic;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Update the Stromal cells
            // calc_Stroma(l, s, Ab, shape);
            {
                if((par.Value.mkCXCL12 > 0.) || (par.Value.bound_CXCL12 > 0.)){
                    for (int n = 0; n < STROMA_list.benutzt(); n++) {
                        s.signal_put(STROMA_list[n], CXCL12, p_mkCXCL12);
                    }
                }
            }

            // Update the CC cells
            // calc_CC(l, s, Ab, shape);
            {
                vector<double> m;
                random2_sequence(m, CC_list.size(););
                for (n = 0; n < m.size(); n++) {
                    miniCell* C = &(CC_list[int (m[n])]);
                    switch (C->CCstate) {


                    /// UNSELECTED state : looking for antigens on FDCs
                    ///  selected_clock //  time since became an unselected CentroCyte
                    ///  bool selectable = 0;
                    ///  bool mobile = 0;
                    ///  clock = 0;     //  time since last interaction
                    ///                 //  becomes mobile when clock > ICAM_delay
                    ///                 //  becomes selectable when clock > test_delay
                    ///  nFDCcontacts   // number of antigens captured
                    case unselected: {
                      // Option : if selection does not need FDC, proceed directly to next step
                      if ((cellCC::CC_FDC_selection == 0) /*|| C->DEC205_ova */) {
                          //CC_list[li].go2TCselection(shape);
                          C->CCstate = FDCselected;
                          break;
                      } //else {// case that selection of CC has to go through FDC contact:

                      //
                      if (not (cellCC::collectFDCsignals)) {
                          //err = CC_list[li].apoptose(shape);
                          //CellCC::p_apo = par.Value.apoptosis * par.Value.deltat;
                          if(rand() < p_apo){ // DEATH
                            //C->make_apoptosis()
                            C->CCstate = apoptosis;
                            C->responsive2signal[CXCL12] = false;
                            C->responsive2signal[CXCL13] = false;
                            if (apoptotic_motility_mode == 1) C->responsive2signal[CXCL13] = true; // The walking dead
                            if (apoptotic_motility_mode == 3) C->responsive2signal[CXCL12] = true;
                            break;
                          }
                      }

                      //When created / leaving an interaction, there is a delay before moving again or being selected again
                      //C->set_selectable();
                      {
                          //C->set_clock();
                          // ICAM_delay = long (par.Value.CC_ICAM_delay / par.Value.deltat + 0.5);      // Duration of motility suppression for each affinity test (-1=none, hr)  -1
                          // test_delay = long (par.Value.CC_test_delay / par.Value.deltat + 0.5); }    // Time gap between affinity tests 0.02
                          ++clock;
                          if ((mobile == 0) && (ICAM_delay > 0) && (clock > ICAM_delay)) { mobile = 1; }
                          if ((selectable == 0) && (test_delay > -1) && (clock > test_delay)) { selectable = 1; }
                      }

                      // ?????
                      if (CC_list[li].CXCR5failure == 0) {
                        CC_list[li].set_CXCR5expression();        // time desensitisation
                        CC_list[li].resensitise4CXCL13(s);        // undercritical CXCL13 resensitisation
                      } else if (CC_list[li].CXCR5failure == 2) {
                        CC_list[li].set_CXCR4expression();        // time desensitisation
                        CC_list[li].resensitise4CXCL12(s);        // undercritical CXCL12 resensitisation
                      } // else if CC_list[li].CXCR5failure==1 do random walk

                      // ask whether serial collection of FDC signals shall be stopped:
                      // err = CC_list[li].stop_collecting_FDCsignals(shape, dt);
                      //{
                      selected_clock += dt;
                      if ((collectFDCsignals) && (selected_clock > collectFDCperiod)) {
                          if (nFDCcontacts == 0) {
                              //make_apoptosis(shape);
                              C->CCstate = apoptosis;
                              C->responsive2signal[CXCL12] = false;
                              C->responsive2signal[CXCL13] = false;
                              if (apoptotic_motility_mode == 1) C->responsive2signal[CXCL13] = true; // The walking dead
                              if (apoptotic_motility_mode == 3) C->responsive2signal[CXCL12] = true;
                          } else {
                              //progress_selection_state(shape);
                              {
                                  if (TC_CC_selection == 0){
                                      state = selected;     // that's it
                                      if (immunoglobulin_class::do_switch_classes == 1) { IgX.class_switch(); }
                                      selected_clock = 0.;
                                      individual_dif_delay = set_selected_CC_delay();
                                  } else {
                                      state = FDCselected;
                                  }
                              }

                              if (reset_antigen_after_collection > 0) {
                                  double factor = double (reset_antigen_after_collection) / double (nFDCcontacts);
                                  nFDCcontacts = reset_antigen_after_collection;
                                  for (unsigned int f = 0; f < collected_ag_portions.size(); f++) {
                                     collected_ag_portions[f] = int(factor * collected_ag_portions[f] + 0.5); /// Philippe : Missing a double here
                                  }
                              }
                          }
                          break; // no movement if processes to next state
                      } //} end of function stop collecting
                      else { // if continue collecting
                          long fdc_pos = C->contact2FDC(l); // returns the position of neighboring FDC (ie position of the fragment)
                          if ((C->selectable == 1) && (fdc_pos != -1)) {
                          miniCell* F = FDC_list[/*l.*/cellknot[fdc_pos].FDClisti];
                          int FDC_frag_index = F->get_fragment_index(fdc_pos);

                          // Which antigen do you see ?
                          int FDC_ag_index; // = F->local_interaction_with_ag(FDC_frag_index, C->pos_ss, shape); // FDC_ag_index == -1 if no antigen was taken
                          {
                              // uses the information   antigen_amount[frag_index][antigen index] from each FDC
                              if (ag_distribution_mode == 1 /* only one AG per fragment */ ) FDC_ag_index = get_highest_amount_ag(frag_index,shape);
                              if ((ag_distribution_mode == 1) && (ag_detection_mode == 0)) get_highest_affinity_ag(frag_index,BCRpos_ss,shape);
                              if ((ag_distribution_mode == 1) && (ag_detection_mode == 1)) get_highest_amount_ag(frag_index,shape);
                          }

                          // determines a threshold for binding
                          double threshold = 0.; // basal case
                          if (cellOUT::use_threshold == 1) threshold = cellOUT::average_affinity; //par.Value.use_ab_dynamics "Ag presenting Ab adapt to PC-produced Ab-quality (1=yes, 0=no)";
                          if (cellOUT::use_threshold == 2) threshold = cellOUT::max_affinity;
                          if (cellOUT::use_threshold == 3) threshold = Ab.average_ab_affinity(FDC_ag_index);  // returns threshold=1 (=no binding in bind_antigen(...)) if FDC_ag_index==-1

                          // try to bind_antigen with these parameters
                          //err = CC_list[li].bind_antigen(FDC_list[fdc_li],FDC_frag_index,FDC_ag_index,shape, threshold);
                          {
                              if (FDC_ag_index > 0) {
                                  double bindprobability = shape.ag_binding_probability(pos_ss,shape.get_Antigen(ag_index),threshold) * BCRexpression;
                                  if (ignore_affinity >= 0) bindprobability = ignore_affinity;
                                  if (drandom() < bindprobability) {
                                      //bool success = false; // = fdc.consume_ag(frag_index,ag_index);
                                      if ((antigen_amount[frag_index][ag_index] >= 1.) && (drandom() < antigen_amount[frag_index][ag_index] / antigen_saturation)) {
                                          --antigen_amount[frag_index][ag_index];
                                          bound_ag_index = ag_index; // save the type of Ag that was bound
                                          state = contact;
                                          n_fdc_encounters = 0;
                                          break;    // Will not move
                                      }
                                  }
                              }
                              clock = 0;        // Interaction failed -> has to look for a new interaction
                              selectable = 0;
                              if (ICAM_delay > 0) { mobile = 0; }
                              ++n_fdc_encounters;
                          }
                          // if didn't break, will move
                          if (C->mobile == 1) {
                              if(!(CC_list[li].move(li, l, s, trackdata, time))){
                                  redo.push_back(C->index);
                              }
                          }
                      break; /// Philippe : you don't enter the next case, right ?
                    }


                  ///  contact state : currintly binding an AG on a FDCs
                  ///  selected_clock //  time since became an unselected CentroCyte, continue increasing in contact mode
                  ///  bound_ag_index;
                  ///  nFDCcontacts   // number of antigens captured
                    case contact: {
                      CC_list[li].selected_clock += dt;
                      //CC_list[li].select(shape);
                      {
                      if (drandom() < p_sel) {  //p_sel = par.Value.selection * par.Value.deltat; "Rate of positive selection at FDCs"
                          if (drandom() < p_FDCsignalling) {  // par.Value.FDCsignalling
                              if (collectFDCsignals) {
                                  //add_collected_ag();
                                  {
                                      ++collected_ag_portions[bound_ag_index];
                                      bound_ag_index = -1;
                                  }
                                  // Returns as Winner
                                  ++nFDCcontacts;
                                  //return2unselected(shape);
                                  {
                                      state = unselected;
                                      clock = 0;
                                      selectable = 0;
                                  }
                              } else {
                                  //progress_selection_state(shape);
                                  {
                                      if (TC_CC_selection == 0) {
                                        state = selected;     // that's it
                                        if (immunoglobulin_class::do_switch_classes == 1) { IgX.class_switch(); }
                                        selected_clock = 0.;
                                        individual_dif_delay = set_selected_CC_delay();
                                      } else {
                                        // if TC help is needed go to state FDCselected first
                                        state = FDCselected;
                                      }
                                  }
                              }
                          } else {
                              // Returns as Looser
                              //return2unselected(shape);
                              {
                                  state = unselected;
                                  clock = 0;
                                  selectable = 0;
                              }
                          }
                      }
                      }
                      CC_list[li].responsive2signal[CXCL13] = false;        // If not already done downregulate CXCR5 expression here at the latest:
                      break;
                    }

                    case FDCselected: {
                        //if (CC_list[li].apoptose(shape) != 0) {
                        {
                            //CellCC::p_apo = par.Value.apoptosis * par.Value.deltat;
                            if(rand() < p_apo){ // DEATH
                                //C->make_apoptosis()
                                C->CCstate = apoptosis;
                                C->responsive2signal[CXCL12] = false;
                                C->responsive2signal[CXCL13] = false;
                                if (apoptotic_motility_mode == 1) C->responsive2signal[CXCL13] = true; // The walking dead
                                if (apoptotic_motility_mode == 3) C->responsive2signal[CXCL12] = true;
                                break;
                            }
                        }

                        // Try to find a TC:
                        long tc_i = CC_list[li].get_contact(TC, l); // returns the index of a random T cell in the nearest neighbors (the index of the T cell)

                        if (CC_list[li].CXCR5failure == 0) {
                          CC_list[li].set_CXCR5expression();             // time desensitisation
                          CC_list[li].resensitise4CXCL13(s);             // undercritical CXCL13 resensitisation
                        } else if (CC_list[li].CXCR5failure == 2) {
                          CC_list[li].set_CXCR4expression();             // time desensitisation
                          CC_list[li].resensitise4CXCL12(s);             // undercritical CXCL12 resensitisation
                        }                               // else if CC_list[li].CXCR5failure==1 do random walk

                        if (tc_i != -1) { // if there is a T cell around
                            //C->bind_TC(TC_list[l.cellknot[tc_i].listi], l, shape);
                            {
                                state = TCcontact;
                                tc_clock = 0.;
                                tc_signal_duration = 0.;
                                tc_index = tcell.index;
                                // Different scenario for T cell help :
                                // 1 - the T cell see the total amount of antigens
                                if ((collectFDCsignals)&& (present_specific_ag2TC == 0)) tcell.make_tc_cc_link(index,nFDCcontacts,DEC205_ova);
                                // 2 - the T cell sees the antigen with the maximum amount
                                if ((collectFDCsignals)&& (present_specific_ag2TC == 1)) tcell.make_tc_cc_link(index,get_max_collected_ag(false),DEC205_ova);
                                // 3 - the T cell sees according to its TCR
                                // not implemented yet if ((collectFDCsignals)&& (present_specific_ag2TC == 2))
                                //In the three cases, same function called :
                                {
                                    // updates the T CELL PART
                                    state = TC_CCcontact;
                                    CC_nn[n_CC_nn] = index;
                                    if(DEC205) CC_affinity[n_CC_nn] = 1.0e+10;
                                    else CC_affinity[n_CC_nn] = double (nFDCcontacts);  //(or double(get_max_collected_ag(false)))
                                    ++n_CC_nn;
                                    changed_nn = 1;
                                }

                                // 4 - Don't care the amount of AG, gives the BCR and the antigen
                                if ((! collectFDCsignals)&& (present_specific_ag2TC == 0)) tcell.make_tc_cc_link(index,pos_ss,get_max_collected_ag(true),shape,DEC205_ova);
                                // 5 - Don't care the amount of AG, looks at the TCR and the antigen
                                if ((! collectFDCsignals)&& (present_specific_ag2TC == 1)) tcell.make_tc_cc_link(index,pos_ss,-1,shape,DEC205_ova);
                                {
                                    // updates the T CELL PART
                                    state = TC_CCcontact;
                                    CC_nn[n_CC_nn] = index;
                                    if(DEC205) CC_affinity[n_CC_nn] = 1.0e+10;
                                    else {
                                        if (ag_index == -1) {
                                          CC_affinity[n_CC_nn] = shape.affinity(CCpos, pos_ss); // TCR to BCR
                                        } else {
                                          CC_affinity[n_CC_nn] = shape.affinity(CCpos, shape.get_Antigen(ag_index)); // TCR to AG
                                        }
                                    }
                                    ++n_CC_nn;
                                    changed_nn = 1;
                                }


                            }
                        } else { // else, move away
                            if(! C->move(li, l, s, trackdata, time)){
                                redo.push_back(C->index);
                            }
                        }
                      }
                      // cout<<" FDCselected: nomove="<<nomove;
                      break;
                    }

                      /// tc_clock += dt;
                      /// tc_index
                    case TCcontact: {
                      CC_list[li].responsive2signal[CXCL13] = false;
                      //CC_list[li].got_tc_signals(dt, TC_list[l.cellknot[CC_list[li].tc_index].listi], l,shape);
                      {
                          tc_clock += dt;

                          // gets signal if the T cell is pointing to me
                          long ix[l.dim];
                          short howmany = l.get_nn_directed2(tcell.polarity,tc_index,ix);
                          if ((howmany == 1) && (ix[0] == index)) {
                            tc_signal_duration += dt;
                          }

                          // decides if the signal is enough, or die
                          if (not (DEC205_ova) || (tc_clock > tc_dec205ova_binding_time)) {
                            //short gotit = get_tc_selected(shape);
                             if (tc_signal_duration >= tc_rescue_time) {
                                 cummulative_ag_collection_selected[min(nFDCcontacts,  max_n_of_ag_portions)]++;
                                 state = selected;
                                 if (immunoglobulin_class::do_switch_classes == 1) { IgX.class_switch(); }
                                 selected_clock = 0.;
                                 individual_dif_delay = set_selected_CC_delay();
                                 if (pMHC_dependent_division) {
                                   pMHC_dependent_number_of_divisions = get_pMHC_dependent_division();
                                 }
                             //} return 1 in this case
                                 // continue : gotit = 1
                                 if(TCselect_prob < 1) {
                                       if (drandom() > TCselect_prob) {
                                           make_apoptosis(shape);
                                       }
                                 }
                                 tcell.liberateCC(index); // in all cases
                                 // updates CC_nn and CC_affinity to be the only current interacting cells
                                 // if (n_CC_nn == 0) { state = TCnormal; }

                             }
                             else { // gotit = 0
                                 if(negativeTCselection){
                                     //if(get_tc_apoptosis(shape)){
                                     if (tc_clock >= tc_time) {
                                         make_apoptosis(shape);
                                         tcell.liberateCC(index); // only if dies
                                         // updates CC_nn and CC_affinity to be the only current interacting cells
                                         // if (n_CC_nn == 0) { state = TCnormal; }
                                     }
                                 }
                                 // Not successfull and not dead, stays as TC contact,
                             }
                          }
                      }
                      break;
                    }

                    case selected: {
                      CC_list[li].responsive2signal[CXCL13] = false;


                      if (CC_list[li].selected4output) {
                        if (CC_list[li].final_differentiation()) {
                          CC_differ2out(m[n], li, l, shape);
                          CCexists = false;
                        } else {
                          // nomove=
                          CC_list[li].move(li, l, s, trackdata, time);
                          //redo.push_back(C->index);
                        }
                      } else {
                        // if not yet differentiation type selected
                        err = CC_list[li].dif2OUTorCB(dt);
                        if (err == 0) {
                          // nomove=
                          CC_list[li].move(li, l, s, trackdata, time);
                        } else {
                          if (err == 2) {
                            // case differentiate to CB
                            CC_differ2CB(m[n], li, l, shape);
                            CCexists = false;
                          } else {
                            if (err == 1) {
                              // case differentiate to output without further delay
                              CC_differ2out(m[n], li, l, shape);
                              CCexists = false;
                            }
                            // if (err==3) do nothing now if differentiate to output but with rate
                            // p_final_differentiation
                          }
                        }
                      }
                      // cout<<" selected: err="<<err<<", nomove="<<nomove;
                      break;
                    }

                    case apoptosis: {
                      // Probability to leave the system through macrophage transport
                      // cout<<" apoptosis: ";
                      if (CC_list[li].macrophagocyte(shape) == 1) {
                        // (switch to cell=empty).
                        if (CC_list[li].trackit == true) {
                          double tmpi[l.dim];
                          l.get_koord(CC_list[li].index, tmpi);
                          trackdata.Stop_movement(CC_list[li].trackno,  nocell,
                                                  time,
                                                  tmpi,
                                                  CC_list[li].polarity);
                        }
                        if (track_mutations) {
                          // write the deletion event to the brainbow class
                          // parameters: (double time, bool founder, bool birth, long mother_index, long
                          // ss_position)
                          trackmutations.write(time,
                                               false,
                                               false,
                                               CC_list[li].brainbow_index,
                                               CC_list[li].pos_ss);
                        }
                        // cout<<" deleted CC! contact_inhibited="<<CC_list[li].contact_inhibited<<"\n";
                        del_CC(m[n], li, l);
                        CCexists = false;
                      } else {
                        // if not yet eaten ...
                        // CC determined for apoptosis may continue to move
                        bool keep_motility = CC_list[li].set_apoptotic_motility(s);
                        // nomove=
                        if (keep_motility) {
                          CC_list[li].move(li, l, s, trackdata, time);
                          //redo.push_back(C->index);
                        }
                      }
                      break;
                    }
                  }     // end switch
                }
            }
        }

            enum centroblasts {
              cb_normal,cb_differentiate,
              cb_G1,cb_G0,cb_S,cb_G2,cb_M,         // keep these together!
              cb_divide,cb_stop_dividing,
              cb_statenumber
            };

        // Update the CB cells
            /// Three options :
            /// - CB differentiate in response to FDC-contact (mk_signal<-0.5)..
            ///   par.Value.mksignal < -0.5) --> signal_use[sig_differ2CC] = -1             // Rate of diff2CC signal production = 0
            /// - CB rate differentiation independent of FDC.
            ///   par.Value.mksignal = 0 --> signal_use[sig_differ2CC] == 0)
            /// - CB differentiate in response to FDC-derived signal.
            ///   par.Value.mksignal > 0 --> signal_use[sig_differ2CC] = 1;
            ///
            /// Three options for receptor_use : receptor_use = par.Value.CBreceptor_use;       //CB use receptors for signal-induced differentiation
            ///  CBreceptor use = 0 // basal option
            ///
            ///
            ///  CBreceptor use = 1
            ///         // receptor_activation is fraction, and receptor_dissociation=K, receptor_binding unimportant
            ///      receptor_activation = par.Value.CBreceptor_activation;
            ///      receptor_binding = par.Value.CBreceptor_binding;
            ///      receptor_dissociation = par.Value.CBreceptor_dissociation;
            ///      receptor_activation *= par.Value.CBreceptor_total;
            ///      receptors = par.Value.CBreceptor_total;
            ///
            ///  CBreceptor use = 2
            ///         // receptor_binding=k_+*deltat, receptor_dissociation=k_-*deltat, receptor_activation is total number
            ///      receptor_binding *= par.Value.deltat; // deltat should be small enough that this is < 1
            ///      receptor_dissociation *= par.Value.deltat;
            ///
        // calc_CB(mCB, mCBlang, ss_save, l, s, shape, redo);
        {
            vector<double> m;
            random2_sequence(m, CB_list.size(););
            for (n = 0; n < m.size(); n++) {
                miniCell* C = &(CB_list[int (m[n])]);
                //void frag_cell::set_clock() {
                {
                    ++clock;
                    for (int a = 0; a < volume; a++) {
                      ++t_immobile[a];
                    }
                }
                //CB_list[li].get_new_state(m[n], dt, l, s);
                {
                    /// Two ways of making proliferation : either by signal or by deciding the number of divisions
                    /// 1st way : cb_normal
                    //switch (C->CBstate) {
                    if(state = cb_normal){
                        if (par.Value.mksignal == -1) {
                            if (contact(FDC,l) == 0) { state = cb_differentiate; break;}
                        }
                        if (par.Value.mksignal == 0) {
                            state = cb_differentiate; break;
                        }
                        if(par.Value.mksignal > 0){
                            switch (receptor_use){
                                case 0: {
                                if(s.sigsknot[i].signal[sig_differ2CC] >= 1.0){
                                    state = cb_differentiate;
                                    --s.sigsknot[i].signal[sig_differ2CC];
                                    --s.sigsknot[i].signal_new[sig_differ2CC];
                                    ++s.signal_used[sig_differ2CC];
                                    break;
                                    }
                                }

                                case 1: {
                                //receptor_ligand = fragcell::bind_ss_receptor_ligand(sig_differ2CC,receptor_dissociation,receptors,receptor_ligand,l,s);
                                {
                                    old = receptor_ligand; // previous value
                                    // computes the average of the signal in the fragments (non-borders)
                                    double n = 0.0;
                                    double average = 0.0;
                                    for (int f = 0; f < volume; f++) {
                                      if (l.object_border(fragments[f]) == 1) {
                                        ++n;
                                        average += s.sigsknot[fragments[f]].signal[sig];
                                      }
                                    }
                                    average /= n;
                                    // decides how much of the signal is eaten
                                    double newvalue; // = get_ss_receptor_ligand(K, r0, average);
                                    {
                                        return (receptors * average) / (receptor_dissociation + average);
                                    }

                                    // now removes the eaten ligand
                                    for (int f = 0; f < volume; f++) {
                                        if (l.object_border(fragments[f]) == 1) {
                                            s.sigsknot[fragments[f]].signal_new[sig] -= ((newvalue - old) / n);
                                        }
                                    }

                                    receptor_ligand = newvalue;

                                    if (receptor_ligand > receptor_activation) {
                                        state = cb_differentiate;
                                        receptor_ligand = 0.0;
                                    }
                                    break;
                                }
                                }

                                case 2: {
                                    //receptor_ligand += bind_receptor_ligand(sig_differ2CC, (kplus=)receptor_binding, (kminus)receptor_dissociation, (ro) receptors, (rold) receptor_ligand,l,s);
                                    {
                                        double n = 0.0; // number of non-border objects
                                        double average = 0.0;
                                        for (int f = 0; f < volume; f++) {
                                          if (l.object_border(fragments[f]) == 1) {
                                            ++n;
                                            average += s.sigsknot[fragments[f]].signal[sig];
                                          }
                                        }
                                        average /= n;

                                        double delta_binding; // = get_receptor_ligand(kplus, kminus, average, r0, rold);
                                        {
                                            return receptor_binding * average * (receptors - receptor_ligand) - receptor_dissociation * receptor_ligand;
                                        }
                                        s.signal_used[sig] += delta_binding;
                                        double rest = delta_binding;

                                        // While there is a rest to take from neighbors eats signal,
                                        while (rest > 1.0e-08 || rest < -1.0e-08) {
                                          average = rest / n;
                                          for (int f = 0; f < volume; f++) {
                                            if (l.object_border(fragments[f]) == 1) {
                                                // if there is enough signal to eat it there
                                                if (s.sigsknot[fragments[f]].signal_new[sig] >= average) {
                                                  s.sigsknot[fragments[f]].signal_new[sig] -= average;
                                                  rest -= average;
                                                } else { // eats the maximum and next time, eat from less neighbors
                                                  rest -= s.sigsknot[fragments[f]].signal_new[sig];
                                                  s.sigsknot[fragments[f]].signal_new[sig] = 0.0;
                                                  --n;
                                                }
                                            }
                                          }
                                        }
                                        // return
                                        receptor_ligand = delta_binding;
                                    }

                                    if (receptor_ligand > receptor_activation) {
                                        state = cb_differentiate;
                                        receptor_ligand = 0.0;         // ligand is really used here!
                                    }
                                    break;
                                }
                            }
                        }
                    } // end of case for cb__normal

                    /// Fields required for cell divisions :
                    /// cycle_state_time;
                    /// state
                    /// Duration of CB cell cycle phase G1 (hours):
                    /// 2.0
                    /// Duration of CB cell cycle phase S (hours):
                    /// 1.0
                    /// Duration of CB cell cycle phase G2 (hours):
                    /// 2.5
                    /// Duration of CB cell cycle phase M (hours):
                    /// 0.5
                    /// Duration of CB cell cycle phase G0 (hours):
                    /// 0.0
                    /// Width of Gaussian variation of phases (fraction of average duration):
                    /// 1.0
                    /// Number of required cell cycles before differentiation (cell cycle times) (h):
                    /// 2.0
                    else if (par.Value.CB_fixed_times_of_divisions > 0){  //(fixed_number_of_divisions()) Number of required cell cycles before differentiation (cell cycle times) (h) -> default 2.0
                        cycle_state_time += dt;
                        if (cycle_state_time >= time_of_cycle_state_switch) {
                            if (state < cb_divide) {
                                //state = progress_cycle_phase();
                                {
                                    if (state == cb_G1) { return cb_S; } else if (state == cb_S) {
                                      return cb_G2;
                                    } else if (state == cb_G2) {
                                      return cb_M;
                                    } else if (state == cb_M) {
                                      return cb_divide;
                                    } else if (state == cb_G0) {
                                      return cb_S;
                                    }
                                    return cb_G0;
                                }
                                if (state != cb_divide) {time_of_cycle_state_switch = set_cycle_state_duration(state);}
                                cycle_state_time = 0.;
                            }
                        }
                    }

                }   // end function get new state

                if ((s.signal_use[glucose] == 1) && (s.signal_use[oxygen] == 1)) {
                  CB_list[li].use_nutrient(s, l.knot[CB_list[li].index].guest_grid_pointer);
                }
                if (CB_list[li].status == necrotic) {
                  macrophagocyte_CB(m[n], li, l, shape);
                  break;
                }

                CB_list[li].set_CXCR4expression();
                CB_list[li].resensitise4CXCL12(s);

                /// Fields for division :
                /// Maximal distance for CB proliferation (microm):
                /// 0.000000e+00
                /// Fraction of volume up to which proliferation is possible (1=no restriction):
                /// 1.000000e+00
                /// Proliferation rate (h):
                /// 9.0

                // does the division, depending on the two ways :
                if (par.Value.CB_fixed_times_of_divisions > 0){ // based on division times
                    if(state == cb_divide){
                        //errp = cellman::proliferate_CB(m[n], li, l, shape);
                        {
                            // long pp[4] to get the different errors :
                            long position_to_prolif = C->ask_mitosis(pp, l);
                            {
                                // proliferation probability :
                                p_now = p_pro; // p_pro = par.Value.proliferate * par.Value.deltat;     // Proliferation rate 9.0 hrs
                                // or if (IgX.Ig_class == IgE) { p_now /= IgE_factor_cellcycle; }
                                if ((volume == 1) && (target_volume == 1)) {
                                    //return find_mitosis_place(p_now,(forceit) state == cb_divide,(dx_max) max_pro_distance,pp,l);
                                    {
                                        if ((state == cb_divide) || (drandom() < p_now)) {
                                            // wants to divide
                                            l.nn = l.nn_permuts.get_rand_set(); // randomize neighbors
                                            short int n = 0;
                                            err = 1; // means not found
                                            while (n < l.dim2 && err == 1) {
                                                  j = l.knot[index].near_n[int (l.nn[n])];
                                                  if ((j != -1) && (l.cellknot[j].cell == nocell)) {
                                                        err = 0;
                                                  }
                                                  ++n;
                                            }
                                            /// Phi a return is missing here
                                        }
                                    }
                                } else { // case of more fragment object
                                    if (volume > 0.9 * target_volume && ((state == cb_divide) || (drandom() < p_now))) {
                                      return 0;} // prolif
                                    return 1; /// what ???
                                }
                            }

                            // following of proliferate_CB
                            if ((cellCB::target_volume == 1) && (j >= 0)) {
                                cellCB newCB = CB_list[li]; // copy everything --> we are lost
                                //newCB.make_CB_new();
                                {
                                    state = cb_normal;
                                    if (fixed_number_of_divisions()) {
                                      state = cb_G1;
                                      time_of_cycle_state_switch = set_cycle_state_duration(state);
                                    }
                                    cycle_state_time = 0.;
                                }
                                //CB_list[li].set_remaining_divisions();
                                //newCB.set_remaining_divisions();
                                {
                                    if (fixed_number_of_divisions()) {
                                        n_divisions2do--;
                                        if (n_divisions2do <= 0) {
                                            state = cb_stop_dividing;
                                        }
                                    }
                                }
                                //newli = put_CB(j, newCB, l, shape);
                                {
                                    newCB.get_radius(l.dim);
                                    long li = CB_list.add(newCB);
                                    CB_list[li].ini(i, li, time, l, shape);
                                    {
                                        index = i;
                                        born_index = i;
                                        born_time = t;
                                        volume = 1;
                                        fragments[0] = i;
                                        l.get_koord(i,barycenter);
                                        for (short a = 0; a < l.dim; a++) {
                                          last_position[a] = barycenter[a];
                                        }
                                        l.set_knot(i,CB,li);
                                        shape.add_cell(sCB,pos_ss);
                                        shape.add_cell(total,pos_ss);
                                        p_mutation = p_mut;
                                        l.get_random_direction(polarity);
                                        get_radius(l.dim);
                                    }
                                    return li;
                                }
                            } else if (j == 0) { /// means in the fragment version
                                cellCB newCB = CB_list[li];
                                //newCB.make_CB_new();
                                {
                                    state = cb_normal;
                                    if (fixed_number_of_divisions()) {
                                      state = cb_G1;
                                      time_of_cycle_state_switch = set_cycle_state_duration(state);
                                    }
                                    cycle_state_time = 0.;
                                }
                                CB_list[li].set_remaining_divisions();
                                newCB.set_remaining_divisions();
                                {
                                    if (fixed_number_of_divisions()) {
                                        n_divisions2do--;
                                        if (n_divisions2do <= 0) {
                                            state = cb_stop_dividing;
                                        }
                                    }
                                }
                                newli = CB_list.add(newCB);
                                //err = CB_list[li].mitosis(i, li, newli, CB_list[newli], l);
                                {
                                    //frag_cell::do_mitosis(CB,i,li,newli,newCell,l);
                                    //very complicated function ...
                                }
                                if (err == 0) {
                                   // Actualize shape space statistics
                                   shape.add_cell(sCB, CB_list[newli].pos_ss);
                                   shape.add_cell(total, CB_list[newli].pos_ss);
                                   // check_connection of the result
                                   if (checkit == 2) {
                                     CB_list[li].check_connection(CB, li, l);
                                     CB_list[newli].check_connection(CB, newli, l);
                                   }
                                   // end of preliminary check
                                   ++pp[0];
                                   if (outputfiles == 0) {
                                     prolog << time << "  " << pp[0] << "\n";
                                   }
                                 } else {
                                   exit(1);
                                 }
                            }

                            // if division,
                            if (err == 0) {
                                bool asymmetric_division = false;
                                CB_list[newli].p_mutation = CB_list[li].p_mutation;
                                if ((retain_ag) && (drandom() < divide_ag_asymmetric)){
                                    asymmetric_division = true;
                                }


                                // switche
                                if (immunoglobulin_class::do_switch_classes == 2) {
                                  CB_list[li].IgX.class_switch();
                                  CB_list[newli].IgX.class_switch();
                                }

                                // mutation
                                if (time >= mutation_start_time) {
                                    if (not (asymmetric_division)
                                        || not (retain_ag && (CB_list[li].retained_ag > 0.)     /// Keep loaded antigen after BC selection for recycling
                                                && cellCB::ag_loaded_CB_stop_mutation)) {  // this is true by default

                                        CB_list[li].mutate(shape);
                                        CB_list[newli].mutate(shape);

                                    }


                                }

                                if (retain_ag) {
                                    if (asymmetric_division) {
                                        CB_list[newli].iamhighag = false;
                                        double all_ag = CB_list[li].retained_ag;
                                        double pitmp = cellCB::asymmetric_polarity_index;
                                        // +++++++++++ OPTION +++++++++++++++++++++++++++++++++++++++++++++
                                        // vary the PI value around its mean
                                        double shift = 0.;
                                        double gaussf;
                                        double width = 0.;
                                        // Define the width of a smooth distribution around the PI set in the parameter file:
                                        if (pitmp < 1. - 4.0 * cellCB::smooth_PI) {
                                          width = cellCB::smooth_PI * pitmp;
                                        } else {
                                          width = cellCB::smooth_PI * ((1.0 - pitmp)) * pitmp;            // linear switch from
                                                                                                          // smooth_PI to 0%
                                        }
                                        // ... this generates a fairly symmetric distribution around the set PI value (pitmp).
                                        // However, it becomes asymmetric when smooth_PI is too large compared to pitmp.
                                        // Note that the asymmetric distribution is generated because
                                        // large values are cut off below in the while loop.
                                        double tmp = 3.0;
                                        while (tmp<0. || tmp> 1.) {
                                          gaussf = drandom();
                                          if ((gaussf == 1.) || (gaussf == 0.)) {
                                            shift = 0;
                                          } else {
                                            shift = width * log((1. - gaussf) / gaussf);
                                          }
                                          tmp = (pitmp + shift);
                                          // Note that with cellCB::smooth_PI=0., everything still works and results in tmp=pitmp
                                          // here.
                                          // cout<<shift<<", "<<tmp<<"; ";
                                        }
                                    //### Can't we just use the sampling function I defined somewhere ? (Michael)
                                        // cout<<"\n";
                                        pitmp = tmp;
                                        // +++++++ end OPTION +++++++++++++++++++++++++++++++++++++++++++++
                                    // pitmp contains the fraction of ag which is kept in CB_list[li]
                                        CB_list[li].retained_ag = pitmp * all_ag;
                                    CB_list[li].adapt_specific_ag(pitmp);
                                        CB_list[newli].retained_ag = all_ag - CB_list[li].retained_ag;
                                    CB_list[newli].adapt_specific_ag(1-pitmp);

                                        // cout<<"all="<<all_ag<<" cell1="<<CB_list[li].retained_ag<<"
                                        // cell2="<<CB_list[newli].retained_ag<<"\n";

                                    } else { // divide symmetric
                                        double all_ag = CB_list[li].retained_ag;
                                        CB_list[newli].retained_ag = all_ag / 2.;
                                    CB_list[newli].adapt_specific_ag(0.5);
                                        CB_list[li].retained_ag = all_ag - CB_list[newli].retained_ag;
                                    CB_list[li].adapt_specific_ag(0.5);
                                        CB_list[newli].iamhighag = false;
                                        CB_list[li].iamhighag = false;

                                    }

                                }
                            } // end if division

                        } // end proliferate _CB


                    } else if (state == cb_stop_dividing){
                        // C->ask_differentiate();
                        {
                            if (((state == cb_differentiate) || (state == cb_stop_dividing))
                                && (double (volume) / double (target_volume) <= limit_volume)
                                && (drandom() < p_dif)) {// && volume>0.9*target_volume)
                                    err = 0;}
                            if (err == 0) {
                              if (ag_loaded_CB_diff2output && (retained_ag > 0.) && iamhighag) { diff2output = true; }  // default = true
                            }
                            return err;
                        }
                        // if differentiates
                        if(err){
                            if (CB_list[li].diff2output) {
                              CB_differ2OUT(m[n], li, l, shape);
                            } else {
                              differ2CC_CB(m[n], li, l, shape);
                            }
                        }
                        /// Philippe ; the minimum bumber of cycles to differentiate is not incorporated ??? Looks like only for DEC205
                    }
                } else { // based on signal
                    errd = 1;
                    errp = 1;
                    chose = irandom(2);
                    if (chose == 0) {
                      errd = CB_list[li].ask_differentiate();            // errd==0 if, yes, differentiate!
                      if (errd == 0) {
                        if (CB_list[li].diff2output) {
                          CB_differ2OUT(m[n], li, l, shape);
                        } else {
                          differ2CC_CB(m[n], li, l, shape);
                        }
                      }
                    }
                    if (chose == 1) {
                      errp = proliferate_CB(m[n], li, l, shape);
                    }
                    // Differentiate wurde probiert. Falls misserfolg, try proliferate
                    if ((chose == 0) && (errd != 0)) {
                      errp = proliferate_CB(m[n], li, l, shape);
                    }
                    // Proliferate wurde probiert.   Falls misserfolg, try differentiate
                    if ((chose == 1) && (errp != 0)) {
                      errd = CB_list[li].ask_differentiate();
                      if (errd == 0) {
                        if (CB_list[li].diff2output) {
                          CB_differ2OUT(m[n], li, l, shape);
                        } else {
                          differ2CC_CB(m[n], li, l, shape);
                        }
                      }
                    }


                }


                // MOVE
                if ((errd != 0) && (CB_list[li].state != cb_M)) {
                  // call movement:
                  double moved = CB_list[li].move(li, l, s, trackdata, time);

                  // Treat the case of initiated move but suppression by lack of space:
                  if (CB_list[li].contact_inhibited) {
                    // this also implies volume==1 and moved==0.
                    // reset contact_inhibited in order to ensure that it remains false in general
                    CB_list[li].contact_inhibited = false;
                    // save this cell for later processing
                    redo.add(CB_list[li].index);
                    // moved=try2exchange_cells(m[n],li,CB,CB_list[li].polarity,l);
                    // cout<<"  back in calc_CB if contact_inhibited ...\n";
                  }

                }

                if ((errd != 0) && (CB_list[li].state != cb_G0) && (CB_list[li].state != cb_M)
                    && (CB_list[li].state != cb_S)
                    && (CB_list[li].state != cb_stop_dividing)) {
                  // call cell growth:
                  CB_list[li].grow(li, l);
                }

            } // end for each cell

            //BCinflux(l, shape, s);
            {
                if (p_BCinflux > 0) {
                    double p_in = p_BCinflux;
                    if (do_smooth_stopBCinflux) {
                      p_in = p_BCinflux / (1.0 + exp((time - t_stopBCinflux) / smooth_stopBCinflux));
                    } else if (time > t_stopBCinflux) {
                      p_in = 0;
                    }

                    if (drandom() < p_in) {
                        cellCB newCB;
                        long k[l.dim];
                        while (not (foundaplace)) {
                          for (int i = 0; i < l.dim; i++) {
                            k[i] = irandom(l.prodimvec[i]);
                          }
                          getpos = l.Index(k);
                          // As the <states> value <nocell> is only possible for <grid_states> unequal <external>,
                          // it is sufficient to check <states>==<nocell> (even though the random values above
                          // might generate points outside the reaction volume):
                          if ((getpos != -1) && (l.cellknot[getpos].cell == nocell)) {
                            foundaplace = true;
                          }
                          // ### note that this loop will not end when the grid is full!
                          // ### put this in the grid class and return an error message when the grid is full
                          // ### and use a different strategy when most grid points are occupied.
                        }
                        newCB.pos_ss = shape.get_Seeder();
                        newCB.make_CB_new();
                        {

                        }

                        if (cellCB::fixed_number_of_divisions()) {
                          // set the number of divisions
                          newCB.n_divisions2do = get_founder_times_of_division();
                        }
                        if (s.signal_use[CXCL12]) {
                          newCB.responsive2signal[CXCL12] = true;
                        }
                        long newindex = put_CB(getpos, newCB, l, shape);
                        cellCB::average_seeder_affinity
                          = (n_founder * cellCB::average_seeder_affinity
                             + shape.best_affinity_norm(newCB.pos_ss)) / (n_founder + 1);
                        if ((cellOUT::use_threshold > 0) && (cellOUT::initial_ab_affinity < 0)) {
                          // These values of cellOUT::average_ and max_affinity are used to define thresholds
                          // of binding antigen (in cellman::calc_CC()).
                          // If cellOUT::initial_ab_affinity>=0 this value is used as initial threshold
                          // and was set in cellOUT::set_statics() and should not be changed here.
                          // The new seeder cell has to be included to define the new threshold,
                          // but only before start of output production:
                          if (n_outs == 0) {
                            cellOUT::average_affinity = cellCB::average_seeder_affinity;
                            double aff = shape.best_affinity_norm(newCB.pos_ss);
                            if (aff > cellOUT::max_affinity) {
                              cellOUT::max_affinity = aff;
                            }
                          }
                        }

                        ++n_founder;
                        CB_list[newindex].preload_with_ag();
                        long ag_pos = shape.get_Antigen(shape.get_nearest_Antigen(CB_list[newindex].pos_ss));


                    }
                }
            } // end BC influx
        } // end Calc_ CB


        // Calc_TC calc_TC(mTC, mTClang, l, s, shape, redo);
        {
            vector<double> m;
            random2_sequence(m, TC_list.size(););
            for (n = 0; n < m.size(); n++) {
                miniCell* C = &(TC_list[int (m[n])]);
                switch (C->TCstate) {
                case TCnormal: {
                    TC_list[li].move(li, l, s, trackdata, time);
                    if (cellTC::do_division) {
                      // check whether a division-related action is necessary
                      TC_list[li].ask_enter_cell_cycle();            // resets state and times if answer is yes
                    }
                    break;
                }
                case TC_CCcontact:{
                    TC_list[li].set_polarity(l);
                    break;
                }
                case TCdivide:{
                    TC_list[li].move(li, l, s, trackdata, time); // no redo ??
                    if (TC_list[li].contact_inhibited) {
                      // reset contact_inhibited in order to ensure that it remains false in general
                      TC_list[li].contact_inhibited = false;
                      // this also implies volume==1 and moved==0.
                      // save this cell for later processing
                      redo.add(TC_list[li].index);
                      // short moved=try2exchange_cells(m[n],li,TC,TC_list[li].polarity,l);
                      // if (moved==1) nomove=0;
                    }


                    bool divide_now = TC_list[li].progress_cell_cycle(dt);
                    if (divide_now) {
                      // in ask_mitosis, division is forced by the state==TCdivide (<cellTC::proliferation> is
                      // ignored)
                      long j = TC_list[li].ask_mitosis(pp, l);
                      if (j >= 0) {
                        divide_TC(m[n], li, j, l, shape);
                      }
                    }
                    break;
                }

                }
            }
        }


        if (allow_exchange) {
          // Now process the contact inhibited movements as saved in redo[]:
          // Note that no further random sequence is needed as the cell-positions where already
          // written in a random order into redo[].
          // cerr<<"in allow_exchange ...\n";
          while (redo.benutzt() > 0) {
            // process the first cell
            long partner = retry_movement(redo, l);
            // cerr<<"partner="<<partner<<"\n";
            // partner contains -1 if no movement was performed
            // or the grid-index of the partner if the cells were exchanged.
            // Delete the processed cell and eventually its partner from the list
            redo.erase_jump(0);
            if (partner > -1) {
              long where_partner = redo.find(partner);
              if (where_partner >= 0) {
                redo.erase_jump(where_partner);
              }
              // Note that processing the partner anyway is not an option, because before the
              // position in redo[] would have to be adopted to the novel position.
            }
          }
          // cerr<<"After redo!\n";
        }

        s.signal_diffuse(l);
    }


}
}



};
#endif
