#include "trackfate.h"
#include <math.h>

long fateTRACK::abs_cell_index = 0;
long fateTRACK::abs_track_index = 0;
string fateTRACK::filename = "trackfate.out";
int fateTRACK::TRACKDIM = 2000;

fateTRACK::fateTRACK() {
}
fateTRACK::~fateTRACK() {
}
bool fateTRACK::mk_fate_track_file() {
  // Check whether the file to save those objects exists:
  ofstream f(filename.c_str());
  bool fileexists = f.good();
  // if not, open it for further append of data later on
  if (fileexists == false) {
    f.open(filename.c_str());
    f.close();
  }
  return not(fileexists); // true when the file was newly generated, false when it existed before
}
void fateTRACK::mk_new_fate_track() {
  // Initialise a fate_list for a single cell object
  if (fate_list.size() > 0) { 
    //cerr << "Cleared fate_list with index " << trackindex << " in mk_new_fate_track()\n";
    fate_list.clear(); 
  }
  fate_list.reserve(TRACKDIM); 
  // this does not allocate memory if the vector has sufficient capacity
  trackindex = abs_track_index;
  //cerr << "Made " << trackindex << " mk_new_fate_track()\n";
  ++abs_track_index;

}
void fateTRACK::show_track() {
  for (unsigned int i = 0; i < fate_list.size(); i++) {
    cout << abs_cell_index << "  "
	 << trackindex << "  "
	 << fate_list[i].cellstate << "  "
	 << fate_list[i].pos_ss << "  "
	 << fate_list[i].affinity << "  "
	 << fate_list[i].t << "  "
	 << fate_list[i].nFDCcontacts << "  "
	 << fate_list[i].pMHC << "  "
	 << fate_list[i].FDCselected_clock << "  "
	 << fate_list[i].tc_search_period << "  "
	 << fate_list[i].FoxO << "  "
	 << fate_list[i].mTORC1 << "  "
	 << fate_list[i].ICOSL << "  "
	 << fate_list[i].nTCcontacts << "  "
	 << fate_list[i].TFHsignal << "  "
	 << fate_list[i].DND << "\n";
  }
}
void fateTRACK::write_fate(short c, int pos_ss, double affinity,
			   double t, int nFDCcontacts, double pMHC, double FDCclock, 
			   double tc_search_period, 
			   double foxo, double mtor, 
			   double ICOSL, int nTCcontacts, double TFHsignal, double DND) {
  /* Adds an entry to the vector with cell fate development */
  fatetrack_data tmp;
  tmp.cellstate = c;
  tmp.pos_ss = pos_ss;
  tmp.affinity = affinity;
  tmp.t = t;
  tmp.nFDCcontacts = nFDCcontacts;
  tmp.pMHC = pMHC;
  tmp.FDCselected_clock = FDCclock;
  tmp.tc_search_period = tc_search_period;
  tmp.FoxO = foxo;
  tmp.mTORC1 = mtor;
  tmp.ICOSL = ICOSL;
  tmp.nTCcontacts = nTCcontacts;
  tmp.TFHsignal = TFHsignal;
  tmp.DND = DND;
  fate_list.push_back(tmp);
}
void fateTRACK::write2file() {

    // Philippe 2018-01-08 This file is way too big, gor 1.2 GB for one simulation => comment it all.
  /* Writes a finished fate track to the file <filename> */
  /*ofstream f;
  f.open(filename.c_str(), ofstream::app);
  for (unsigned long i=0; i < fate_list.size(); i++) {
    f << abs_cell_index << "  "
      //      << trackindex << "  "
      << fate_list[i].cellstate << "  "
      << fate_list[i].pos_ss << "  "
      << fate_list[i].affinity << "  "
      << fate_list[i].t << "  "
      << fate_list[i].nFDCcontacts << "  "
      << fate_list[i].pMHC << "  "
      << fate_list[i].FDCselected_clock << "  "
      << fate_list[i].tc_search_period << "  "
      << fate_list[i].FoxO << "  "
      << fate_list[i].mTORC1 << "  "
      << fate_list[i].ICOSL << "  "
      << fate_list[i].nTCcontacts << "  "
      << fate_list[i].TFHsignal << "  "
      << fate_list[i].DND << "\n";
  }
  f.close();*/
  ++abs_cell_index;
  fate_list.clear();
}

