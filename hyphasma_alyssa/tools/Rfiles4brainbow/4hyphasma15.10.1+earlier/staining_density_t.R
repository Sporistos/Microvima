##################################################################################################################
# additional analysis done only for a subset
#library(ggplot2)
##################################################################################################################
# General settings used in all graphs
# the number of species and the time frame should correspond to the values in the parameter file:
Nruns=50
# set the x range
tmax=12
# set the y range
ymin=0
ymax=100
# for more than one species, these can be shown as different colors in each plot:
colors<-c("red","green","blue","magenta","cyan")
# plotdata should take values of enum experiments { } as defined in difevolve.h:
plotdata=-1
# remove the message that some data were not plotted
options(warn=-1)
##################################################################################################################
# brainbow48c2n.eps
file="brainbow48c2n.eps"
file
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  plot_cols <- rainbow(12)
  xsim <- 100*sim[,3]/sim[,7]
  ysim <- 100*sim[,4]
  if (count==1) {
     # set the margins and set the line width
     par(mai=c(1,1,0.5,0.5), col="black") 
     # generate the graph and plot the first curve
     plot(xsim, ysim, col=plot_cols[sim[,2]/24], pch=14+sim[,2]/24, type="p", cex.axis=1.2, 
          cex.lab=1.5, cex=1.2, xlab="% stained ", ylab="% of colour dominant", 
          xlim=c(ymin,ymax), ylim=c(ymin,ymax))
     title(main = "Colour dominance versus stained cell fraction")
  } else {
     par(new=TRUE, col="black")
     plot(xsim,ysim, col=plot_cols[sim[,2]/24], pch=14+sim[,2]/24, type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(ymin,ymax), ylim=c(ymin,ymax))
  }
  par(new=TRUE, col="black")
  plot(xsim, ysim, col="grey", type="l", cex=1.0, 
       xlab="", ylab="", axes=FALSE, xlim=c(ymin,ymax), ylim=c(ymin,ymax))
}
dev.off()
##################################################################################################################
