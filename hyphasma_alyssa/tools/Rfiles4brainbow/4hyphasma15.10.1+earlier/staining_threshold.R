##################################################################################################################
# additional analysis done only for a subset
#library(ggplot2)
##################################################################################################################
# General settings used in all graphs
# the number of species and the time frame should correspond to the values in the parameter file:
Nruns=50
# set the x range
tmax=12
# set the y range
ymin=0
ymax=100
# for more than one species, these can be shown as different colors in each plot:
colors<-c("red","green","blue","magenta","cyan")
# plotdata should take values of enum experiments { } as defined in difevolve.h:
plotdata=-1
# remove the message that some data were not plotted
options(warn=-1)
##################################################################################################################
min_list = seq(0.0,0.5,by=0.05)
for (i in 1:length(min_list)) {
  min_stained = min_list[i]
  print(paste("Run with min_stained=",min_stained,"..."))
  # brainbow48_minXX_real.eps
  file=paste("brainbow48_min",100*min_stained,"_real.eps", sep="")
  print(file)
  # set the output file name
  cairo_ps(width=15, height=8, file)
  # go through all Nruns
  dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
  count=0
  for (i in dir_list) { 
    count=count+1
    # read next data set
    ifile<-paste(i,"/clonality.out", sep="")
    sim<- read.table(ifile, header=FALSE, skip=1)
    if (count==1) {
       # set the margins and set the line width
       par(mai=c(1,1,0.5,0.5), col="black") 
       # generate the graph and plot the first curve
       plot(sim[,2]/24-0.4, 100*sim[,4], col=(sim[,5]+10), pch=14+sim[,5], type="p", cex.axis=1.2, 
                   cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="% of the dominant", 
                   xlim=c(0,tmax), ylim=c(ymin,ymax))
       title(main = paste("Tamoxifen at day 2 after GC onset staining 40% of the cells with bias, stain threshold ",min_stained))
    } else {
       # prevent opening of a new graph and set the color
       par(new=TRUE, col="black")
       #par(new=TRUE)
       # plot without remaking axes
       plot(sim[,2]/24-0.4,100*sim[,4], col=sim[,5]+10, pch=14+sim[,5], type="p", cex=1.2, 
            xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
    }
    par(new=TRUE, col="black")
    plot(sim[,2]/24+0.4,100*sim[,8], col=sim[,9]+10, pch=0+sim[,9], type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(ymin,ymax))
    if (min_stained>0) {
      segments(sim[,2]/24-0.4, 100*sim[,4]*round(((sim[,3]/sim[,7])-min_stained)+0.5), 
               sim[,2]/24+0.4, 100*sim[,8]*round(((sim[,3]/sim[,7])-min_stained)+0.5), lwd=0.5)
    } else {
      segments(sim[,2]/24-0.4, 100*sim[,4], sim[,2]/24+0.4, 100*sim[,8], lwd=0.5)
    }
  }
  dev.off()
}
##################################################################################################################
