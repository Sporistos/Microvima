##################################################################################################################
# plots color dominance versus lineage dominance for a particular day after tamoxifen, 
# lineage dominance is color coded
##################################################################################################################
#library(ggplot2)
# set the x range
tmax=12
# set the y range
ymin=0
ymax=100
# remove the message that some data were not plotted
options(warn=-1)
# set the day for evaluation:
day=11
# color2lineage_dayXX.eps
file=paste("color2lineage_day",day,".eps",sep="")
file
# set the output file name
cairo_ps(width=15, height=8, file)
# go through all Nruns
dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
count=0
for (i in dir_list) { 
  count=count+1
  # read next data set
  ifile<-paste(i,"/clonality_lineage.out", sep="")
  sim<- read.table(ifile, header=FALSE, skip=1)
  plot_cols <- rainbow(10)
  xsim <- 100*sim[day+1,8]
  ysim <- 100*sim[day+1,4]
  if (count==1) {
     # set the margins and set the line width
     par(mai=c(1,1,0.5,0.5), col="black") 
     plot(xsim, ysim, col=plot_cols[xsim/10], pch=14+sim[,2]/24, type="p", cex.axis=1.2, 
          cex.lab=1.5, cex=1.2, xlab="% of lineage dominant", ylab="% of colour dominant", 
          xlim=c(ymin,ymax), ylim=c(ymin,ymax))
     title(main = "Colour versus lineage dominance")
  } else {
     par(new=TRUE, col=FALSE)
     plot(xsim, ysim, col=plot_cols[xsim/10], pch=14+sim[,2]/24, type="p", cex=1.2, 
          xlab="", ylab="", axes=FALSE, xlim=c(ymin,ymax), ylim=c(ymin,ymax))
  }
}
dev.off()
##################################################################################################################
