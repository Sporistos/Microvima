################################################################################################
# histogram of the fraction of colors for all used brainbow colors
################################################################################################
#library(ggplot2)
# set the y range
ymin=0
ymax=100
use_threshold <- 0.0
# remove the message that some data were not plotted
options(warn=-1)
################################################################################################
# set the number of thresholds to be used
time_list = seq(0,19,by=1)
for (i in 1:length(time_list)) {
  time_this = time_list[i]
  print(paste("Staining histogram for time=",time_this,"..."))
  file=paste("color_dominance_histo_threshold",use_threshold,"_day",time_this,".eps",sep="")
  file
  # set the output file name
  cairo_ps(width=15, height=8, file)
  totalfile<-paste("result_table_day",time_this,".txt", sep="")
  alldata<- read.table(totalfile, header=FALSE, skip=1)
#  col_clone <- cbind(alldata[,4],alldata[,6])
#  print(col_clone[,2])
  par(mai=c(1,1,0.5,0.5)) 
  hist(alldata[,4], breaks=40, col="red", xlim=c(0,1), ylim=c(0,ymax),
       cex.lab=1.5, cex.axis=1.5,
       main=paste("color (red) versus clonal (black shading lines) dominance, day ",time_this,", staining threshold ",use_threshold,sep=""),
       xlab="color or clonal dominance", cex=1.5)
  par(new=TRUE)
  hist(alldata[,6], breaks=40, col="black", density=20, xlim=c(0,1), ylim=c(0,ymax),
       main="", xlab="", ylab="", axes=FALSE)
  dev.off()
}
################################################################################################
