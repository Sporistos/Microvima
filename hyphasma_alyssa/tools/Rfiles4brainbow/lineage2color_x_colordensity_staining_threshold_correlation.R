##################################################################################################################
# correlation and p-value for clonal versus color dominance times color density for different staining thresholds
##################################################################################################################
#library(ggplot2)
# set the x range
tmax=12
# set the y range
ymin=0
ymax=100
# remove the message that some data were not plotted
options(warn=-1)
##################################################################
## @brief Extract data for clonal and color dominance from file
##        for a single GC given notation in a single file
## @param gcpath string path to data files for germinal center
## @return data.frame Time, ColorDominance and ClonalDominance
##                    for each GC
get_corr_data <- function(gcpath) {
  cdf <- read.table(paste(gcpath,"/clonality_lineage.out", sep=""), header=FALSE, skip=1)
  cdf <- cdf[,c(2,4,8,3,7)]
  names(cdf) <- c("Time","ColorDominance","ClonalDominance","StainedCells","TotalCells")
  cdf
}
##################################################################
get_stain_level_data <- function(gcpath) {
  sdf <- read.table(paste(gcpath,"/clonality_lineage.out", sep=""), header=FALSE, skip=1)
  sdf <- sdf[,c(2,3,7)]
  names(sdf) <- c("Time","StainedCells","TotalCells")
  sdf
}
##################################################################
## @brief Read data for all 50 GCs in one in silico experiment
## @param folder string folder with one subfolder for each GC
## @return data.frame Time, ColorDominance and ClonalDominance
##                    for each GC
read_data <- function(folder) {
  count=0
  GCPaths <- list.dirs(path = folder, full.names = TRUE, recursive = FALSE)
  for (ifolder in GCPaths) {
     count=count+1
     if (count==1) { 
	AllDataSets <- get_corr_data(ifolder)
     } else {
	corr_data <- get_corr_data(ifolder)
	AllDataSets <- rbind(AllDataSets,corr_data)
     }
  }
  AllDataSets
}
##################################################################
file=paste("lineage2color_x_colordensity_staining_threshold_correlation.eps", sep="")
print(file)
# set the output file name
cairo_ps(width=15, height=8, file)
filep=paste("lineage2color_x_colordensity_staining_threshold_corr_pvalue.eps", sep="")
print(filep)
cairo_ps(width=15, height=8, filep)
dev.list()
dev.set(2)
#
AllDataSets <- read_data(".")
#print(AllDataSets)
times <- unique(AllDataSets$Time)
tmax <- times[length(times)]/24
print(tmax)
#print(times)
min_list = seq(0.0,0.5,by=0.05)
#linecol = c("black","magenta","blue","cyan","green","orange","red")
linecol = rainbow(length(min_list))
count=0
corrdata <- c(0)
for (i in 1:length(min_list)) {
  count=count+1
  corrdata <- NULL
  min_stained = min_list[i]
  print(paste("Run with min_stained=",min_stained,"..."))
  for (t in times) {
     #print(paste("Time is ",t," hours:",sep=""))
     dataoft <- AllDataSets[AllDataSets$Time==t,]
     #print(dataoft)
     if (min_stained>0) {
	datatmp <- dataoft[dataoft$StainedCells>min_stained*dataoft$TotalCells,]
	dataoft <- datatmp
     }	
     #print(paste(length(dataoft[,1])," elements found",sep=""))
     #print(dataoft)
     if (length(dataoft[,1])>3) {
#       print(dataoft$ColorDominance)
#       print(dataoft$StainedCells/dataoft$TotalCells)	
       dataproduct <- dataoft$ColorDominance*dataoft$StainedCells/dataoft$TotalCells
#       print(dataproduct)	
       correlation <- cor.test(dataproduct,dataoft$ClonalDominance,method="spearman")
       corrdata <- rbind(corrdata,c(t,correlation$estimate,correlation$p.value))
     }
  }
  #print(corrdata)
  dev.set(2)
  if (count==1) {
     # set the margins and set the line width
     par(mai=c(1,1,0.5,0.5), col="black", pch=0) 
     # generate the graph and plot the first curve
     plot(corrdata[,1]/24, corrdata[,2], type="l", cex.axis=1.2, lty=1, lwd=2, col=linecol[count],
                   cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="Spearman correlation coefficient", 
                   xlim=c(0,tmax), ylim=c(-1,1))
       title(main = paste("Spearman correlation coefficient of lineage dominance and colour dominance times density with different staining thresholds"))
  } else {
       # prevent opening of a new graph and set the color
       par(new=TRUE, col="black", pch=0)
       # plot without remaking axes
       plot(corrdata[,1]/24,corrdata[,2], type="l", cex=1.2, lty=1, lwd=2, col=linecol[count],
            xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(-1,1))
  }
  dev.set(3)
  if (count==1) {
     # set the margins and set the line width
     par(mai=c(1,1,0.5,0.5), col="black", pch=0) 
     # generate the graph and plot the first curve
     plot(corrdata[,1]/24, corrdata[,3], log="y", type="l", cex.axis=1.2, lty=1, lwd=2, col=linecol[count],
                   cex.lab=1.5, cex=1.2, xlab="Days post tamoxifen", ylab="p-value", 
                   xlim=c(0,tmax), ylim=c(1.0e-16,1))
       title(main = paste("Spearman correlation coefficient p-value of lineage dominance and colour dominance times density with different staining thresholds"))
  } else {
       # prevent opening of a new graph and set the color
       par(new=TRUE, col="black", pch=0)
       # plot without remaking axes
       plot(corrdata[,1]/24,corrdata[,3], log="y", type="l", cex=1.2, lty=1, lwd=2, col=linecol[count],
            xlab="", ylab="", axes=FALSE, xlim=c(0,tmax), ylim=c(1.0e-16,1))
  }
}
dev.set(2)
legend(tmax-2.5,0, min_list, lty=1, lwd=2, col=linecol, title="staining threshold")
dev.set(3)
legend(tmax-2.5,1, min_list, lty=1, lwd=2, col=linecol, title="staining threshold")
dev.off()
dev.off()
##################################################################################################################

