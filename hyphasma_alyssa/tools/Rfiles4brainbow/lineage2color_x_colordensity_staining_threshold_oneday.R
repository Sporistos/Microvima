##################################################################################################################
# dotplot for lineage dominance versus color dominance times color density for different staining thresholds
##################################################################################################################
#library(ggplot2)
# set the x range
tmax=12
# set the y range
ymin=0
ymax=100
# remove the message that some data were not plotted
options(warn=-1)
##################################################################################################################
# set the number of thresholds to be used
use_threshold <- 0.4
time_list = seq(3,19,by=4)
for (i in 1:length(time_list)) {
  time_this = time_list[i]
  print(paste("Dotplot for time=",time_this,"..."))
  file=paste("lineage2color_x_colordensity_staining_threshold_day",time_this,".eps",sep="")
  file
  # set the output file name
  cairo_ps(width=15, height=8, file)
  # go through all Nruns
  dir_list <- list.dirs(path = ".", full.names = TRUE, recursive = FALSE)
  count=0
  times <- c(0)
  for (idir in dir_list) { 
    count=count+1
    # read next data set
    ifile<-paste(idir,"/clonality_lineage.out", sep="")
    simall<- read.table(ifile, header=FALSE, skip=1)
    # Reduce the data to those with a minimum staining level of <time_this>
    sim <- simall[simall[,2]/24==time_this,]
    # staining level is calculated as number of stained cells divided by total number of cells
    stainsim=0
    if (sim[,7]>0) stainsim <- sim[,3]/sim[,7]
    plotcolor <- "grey"
    if (stainsim>=use_threshold) plotcolor <- "green"
    # fraction of color dominant times stained fraction
    xsim <- sim[,4]*stainsim 
    # fraction of dominant lineage
    ysim <- 100*sim[,8]
    if (count==1) {
       # set the margins and set the line width
       par(mai=c(1,1,0.5,0.5), col="black") 
       # generate the graph and plot the first curve
       plot(xsim, ysim, col=plotcolor, pch=7, type="p", cex.axis=1.2, 
            cex.lab=1.5, cex=1.2,  xlab="% of color dominant times % stained", ylab="% of lineage dominant",
            xlim=c(0,1), ylim=c(ymin,ymax))
       title(main = paste("Lineage dominance versus color dominance times density at day ",time_this,", grey symbols = below staining threshold=",use_threshold,sep=""))
    } else {
       # prevent opening of a new graph and set the color
       par(new=TRUE, col=FALSE)
       # plot without remaking axes
       plot(xsim, ysim, col=plotcolor, pch=7, type="p", cex=1.2, 
            xlab="", ylab="", axes=FALSE, xlim=c(0,1), ylim=c(ymin,ymax))
    }
  }
#  legend(0.88,100, times, pch=times, lwd=2, col=plot_cols, title="time post tamoxifen [d]")
  dev.off()
}
##################################################################################################################
