##################################################################################################################
# additional analysis done only for a subset
#library(ggplot2)
##################################################################################################################
# General settings used in all graphs
# set the y range
ymin=0
ymax=100
# for more than one species, these can be shown as different colors in each plot:
colors<-c("red","green","blue","magenta","cyan")
# plotdata should take values of enum experiments { } as defined in difevolve.h:
plotdata=-1
# remove the message that some data were not plotted
#options(warn=-1)
##################################################################################################################
# lineage_pie_dayX.eps
##################### find out the length of clone_fractions[X,] (number of lines) and put it in the for-loop
# go through all runs
runthis="036"
  # set the output file name
  ifile<-paste("./",runthis,"/clone_lineage_fractions.out", sep="")
  # read next data set
  clone_fractions<- read.table(ifile, header=FALSE, skip=1)
  bla <- dim(clone_fractions[,])
  Ntimes <- bla[1]
  for (j in 1:Ntimes) {
      time<-clone_fractions[j,1]/24
      cellnum<-clone_fractions[j,2]
      founder<-clone_fractions[j,3]
      file=paste("./",runthis,"/lineage_pie_day",time,".eps", sep="")
      print(file)
      cairo_ps(width=3, height=3, file)
      #print(clone_fractions[1,])
      #print(length(clone_fractions[1,]))
      x <- clone_fractions[j,4:(founder+3)]
      remove <- c(0)
      x <- x[! x %in% remove]
      #print(x)
      #print(length(x))
      clonenum<-length(x)
      if (clonenum>0) {
        # set the margins and set the line width
        par(mai=c(0.2,0.2,0.2,0.2), col="black") 
        # generate the graph and plot the first curve
        #pie(t(x), labels=x, col=rainbow(clonenum), radius=1.0, clockwise=FALSE)
        pie(t(x), labels="", col=rainbow(clonenum), radius=1.0, clockwise=FALSE)
        title(main = paste("day ",time,", ",clonenum," lineages, ",cellnum," cells",sep=""))
      }
      dev.off()
      #print("done.")
  }
##################################################################################################################
# lineage_pie_subset_dayX.eps
# go through all runs
  # set the output file name
  ifile<-paste("./",runthis,"/clone_lineage_fractions_rand.out", sep="")
  # read next data set
  clone_fractions<- read.table(ifile, header=FALSE, skip=1)
  bla <- dim(clone_fractions[,])
  Ntimes <- bla[1]
  for (j in 1:Ntimes) {
      time<-clone_fractions[j,1]/24
      cellnum<-clone_fractions[j,2]
      founder<-clone_fractions[j,3]
      file=paste("./",runthis,"/lineage_pie_subset_day",time,".eps", sep="")
      print(file)
      cairo_ps(width=3, height=3, file)
      #print(clone_fractions[1,])
      #print(length(clone_fractions[1,]))
      x <- clone_fractions[j,4:(founder+3)]
      remove <- c(0)
      x <- x[! x %in% remove]
      #print(x)
      #print(length(x))
      clonenum<-length(x)
      if (clonenum>0) {
        # set the margins and set the line width
        par(mai=c(0.2,0.2,0.2,0.2), col="black") 
        # generate the graph and plot the first curve
        #pie(t(x), labels=x, col=rainbow(clonenum), radius=1.0, clockwise=FALSE)
        pie(t(x), labels="", col=rainbow(clonenum), radius=1.0, clockwise=FALSE)
        title(main = paste("day ",time,", ",clonenum," lineages, ",cellnum," cells",sep=""))
      }
      dev.off()
  }
##################################################################################################################

