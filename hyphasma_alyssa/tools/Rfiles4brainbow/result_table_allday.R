##################################################################################################################
# correlation and p-value for clonal versus color dominance times color density for different staining thresholds
##################################################################################################################
#library(ggplot2)
library(Hmisc)
# set the x range
tmax=12
# set the y range
ymin=0
ymax=100
# remove the message that some data were not plotted
options(warn=1)
##################################################################
## @brief Extract data for clonal and color dominance from file
##        for a single GC given notation in a single file
## @param gcpath string path to data files for germinal center
## @return data.frame Time, ColorDominance and ClonalDominance
##                    for each GC
get_corr_data <- function(gcpath) {
  cdf <- read.table(paste("./",gcpath,"/clonality_lineage.out", sep=""), header=FALSE, skip=1)
  clonedf <- read.table(paste("./",gcpath,"/clonality.out", sep=""), header=FALSE, skip=1)
  coldf <- read.table(paste("./",gcpath,"/clone_colour_fractions.out", sep=""), header=FALSE, skip=1)
  ncolors <- length(coldf)
  coldf <- coldf[,c(seq(4,ncolors))]
  cdf <- cbind(as.numeric(gcpath),cdf[,c(2,4,8)],clonedf[,c(8)],cdf[,c(3,7)])
#  cdf <- cbind(as.numeric(gcpath),cdf[,c(2,4,8,3,7)])
  cdf <- cbind(cdf,coldf)
  names(cdf) <- c("Index","Time","ColorDominance","LineageDominance","ClonalDominance","StainedCells","TotalCells","Color1","Color2")
  #if (gcpath=="./187") { print(cdf) }
  cdf
}
##################################################################
get_stain_level_data <- function(gcpath) {
  sdf <- read.table(paste(gcpath,"/clonality_lineage.out", sep=""), header=FALSE, skip=1)
  sdf <- sdf[,c(2,3,7)]
  names(sdf) <- c("Time","StainedCells","TotalCells")
  sdf
}
##################################################################
## @brief Read data for all 50 GCs in one in silico experiment
## @param folder string folder with one subfolder for each GC
## @return data.frame Time, ColorDominance and ClonalDominance
##                    for each GC
read_data <- function(folder) {
  count=0
  GCPaths <- list.dirs(path = folder, full.names = FALSE, recursive = FALSE)
  for (ifolder in GCPaths) {
     count=count+1
     if (count==1) { 
	AllDataSets <- get_corr_data(ifolder)
     } else {
	corr_data <- get_corr_data(ifolder)
	AllDataSets <- rbind(AllDataSets,corr_data)
     }
  }
  AllDataSets
}
##################################################################
#
AllDataSets <- read_data(".")
time_list = seq(0,19,by=1)
for (i in 1:length(time_list)) {
  time_this=time_list[i]
  print(paste("Generate result table for day ",time_this," after tamoxifen (stain_time)", sep=""))
  dataoft <- AllDataSets[AllDataSets$Time/24==time_this,]
  #print(dataoft)
  dataoft.df <- as.data.frame(dataoft)
  write.table(dataoft.df, paste("./result_table_day",time_this,".txt", sep=""))
}
##################################################################################################################

