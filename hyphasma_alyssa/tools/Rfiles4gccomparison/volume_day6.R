require(dplyr)
require(ggplot2)
require(reshape2)

read_replicate <- function(i, basepath="../../results/v16.07.15", contpath="ige008x-all", treatpath="ige009x-all", filename="xvolume.out") {
  read_xsum_out <- function(path) {
    read.table(file.path(path, filename), skip=1) %>% transmute(time=V1, Volume=V4)
  }
  control <- file.path(basepath, contpath, sprintf("%03i", i)) %>% read_xsum_out %>% filter(time==144)
  treatment <- file.path(basepath, treatpath, sprintf("%03i", i)) %>% read_xsum_out %>% filter(time==144)
  data.frame(VolumeRatio=treatment$Volume/control$Volume)
}

colMeansSd <- function(dfh) {
 means <- dfh %>% apply(2, mean) %>% t %>% as.data.frame %>% melt(value.name="Mean")
 sd <- dfh %>% apply(2, sd) %>% t %>% as.data.frame %>% melt(value.name="SD") %>% select(SD)
 cbind(means,sd)
}

summarizedData <- lapply(0:99, read_replicate) %>% rbind_all
plotdata <- colMeansSd(summarizedData)
write.table(plotdata, "volume_day6.dat")
plt <- ggplot(plotdata, aes(x="Number of GC B cells at day 6", y=Mean)) + geom_bar(stat="identity", color="black", fill="green") + 
  geom_errorbar(aes(x="Number of GC B cells at day 6",ymin=Mean-SD,ymax=Mean+SD),width=.3,size=1) + xlab("") + ylab("Mutant/Control")
ggsave("volume_day6.eps")
