/* vol_stat.C, 23.12.2004.
   Code zum einlesen von GC-Kinetik-Daten aus strukturell identischen files 
   in nummerierten Unterverzeichnissen.
   Die files werden im Quellcode angegeben,
   der Code compiliert,
   die Ausfuehrung des Codes liest die Daten ein,
   berechnet Mittelwerte und Standardabweichung,
   und schreibt die Daten in das gewuenschte file raus.
*/

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <math.h>

const short maxcols=5;

const int fn=30; // ### insert number of files to read
const int valn=504; // ### insert number of values to read in each file
const short xcol=1; // ### insert the column for the x-value
const short xcol2=1; // ### insert the column for the x-value
const short xcol3=1; // ### insert the column for the x-value
//const short ycol[]; // ### insert the column for the y-value
const short y0col=2; // ### insert the column for the first y-value
const short y0col2=4; // ### insert the column for the first y-value
const short y0col3=3; // ### insert the column for the first y-value
const short y1col=3;
const short y2col=4;
const short y3col=5;
const short y4col=6;
const short y1col2=3;
const short y2col2=4;
const short y3col2=5;
const short y4col2=6;
const short y1col3=3;
const short y2col3=4;
const short y3col3=5;
const short y4col3=6;
const short nycols=1; // ### insert number of y-values to read
const int ignore=2; // ### insert number of lines to ignore at file start
const int ignore2=1; // ### insert number of lines to ignore at file start
const int ignore3=1; // ### insert number of lines to ignore at file start
const char outputfilename[30]="fracdivide_stat.out"; // ### insert the output filename

double f(double a, double b, double c) {
  if (b+c==0) return 0.0;
  else return a/(b+c);
}

const char* getname(int& i) { // ### insert filenames here
  switch(i) {
  case  0: return "./01/xsumcb.out";
  case  1: return "./02/xsumcb.out";
  case  2: return "./03/xsumcb.out";
  case  3: return "./04/xsumcb.out";
  case  4: return "./05/xsumcb.out";
  case  5: return "./06/xsumcb.out";
  case  6: return "./07/xsumcb.out";
  case  7: return "./08/xsumcb.out";
  case  8: return "./09/xsumcb.out";
  case  9: return "./10/xsumcb.out";
  case 10: return "./11/xsumcb.out";
  case 11: return "./12/xsumcb.out";
  case 12: return "./13/xsumcb.out";
  case 13: return "./14/xsumcb.out";
  case 14: return "./15/xsumcb.out";
  case 15: return "./16/xsumcb.out";
  case 16: return "./17/xsumcb.out";
  case 17: return "./18/xsumcb.out";
  case 18: return "./19/xsumcb.out";
  case 19: return "./20/xsumcb.out";
  case 20: return "./21/xsumcb.out";
  case 21: return "./22/xsumcb.out";
  case 22: return "./23/xsumcb.out";
  case 23: return "./24/xsumcb.out";
  case 24: return "./25/xsumcb.out";
  case 25: return "./26/xsumcb.out";
  case 26: return "./27/xsumcb.out";
  case 27: return "./28/xsumcb.out";
  case 28: return "./29/xsumcb.out";
  case 29: return "./30/xsumcb.out";
  case 30: return "./31/xsumcb.out";
  case 31: return "./32/xsumcb.out";
  case 32: return "./33/xsumcb.out";
  case 33: return "./34/xsumcb.out";
  case 34: return "./35/xsumcb.out";
  case 35: return "./36/xsumcb.out";
  case 36: return "./37/xsumcb.out";
  case 37: return "./38/xsumcb.out";
  case 38: return "./39/xsumcb.out";
  case 39: return "./40/xsumcb.out";
  case 40: return "./41/xsumcb.out";
  case 41: return "./42/xsumcb.out";
  case 42: return "./43/xsumcb.out";
  case 43: return "./44/xsumcb.out";
  case 44: return "./45/xsumcb.out";
  case 45: return "./46/xsumcb.out";
  case 46: return "./47/xsumcb.out";
  case 47: return "./48/xsumcb.out";
  case 48: return "./49/xsumcb.out";
  case 49: return "./0/xsumcb.out";
  } // end of switch
  return "";
}

const char* getname2(int& i) { // ### insert filenames here
  switch(i) {
  case  0: return "./01/xvolume.out";
  case  1: return "./02/xvolume.out";
  case  2: return "./03/xvolume.out";
  case  3: return "./04/xvolume.out";
  case  4: return "./05/xvolume.out";
  case  5: return "./06/xvolume.out";
  case  6: return "./07/xvolume.out";
  case  7: return "./08/xvolume.out";
  case  8: return "./09/xvolume.out";
  case  9: return "./10/xvolume.out";
  case 10: return "./11/xvolume.out";
  case 11: return "./12/xvolume.out";
  case 12: return "./13/xvolume.out";
  case 13: return "./14/xvolume.out";
  case 14: return "./15/xvolume.out";
  case 15: return "./16/xvolume.out";
  case 16: return "./17/xvolume.out";
  case 17: return "./18/xvolume.out";
  case 18: return "./19/xvolume.out";
  case 19: return "./20/xvolume.out";
  case 20: return "./21/xvolume.out";
  case 21: return "./22/xvolume.out";
  case 22: return "./23/xvolume.out";
  case 23: return "./24/xvolume.out";
  case 24: return "./25/xvolume.out";
  case 25: return "./26/xvolume.out";
  case 26: return "./27/xvolume.out";
  case 27: return "./28/xvolume.out";
  case 28: return "./29/xvolume.out";
  case 29: return "./30/xvolume.out";
  case 30: return "./31/xvolume.out";
  case 31: return "./32/xvolume.out";
  case 32: return "./33/xvolume.out";
  case 33: return "./34/xvolume.out";
  case 34: return "./35/xvolume.out";
  case 35: return "./36/xvolume.out";
  case 36: return "./37/xvolume.out";
  case 37: return "./38/xvolume.out";
  case 38: return "./39/xvolume.out";
  case 39: return "./40/xvolume.out";
  case 40: return "./41/xvolume.out";
  case 41: return "./42/xvolume.out";
  case 42: return "./43/xvolume.out";
  case 43: return "./44/xvolume.out";
  case 44: return "./45/xvolume.out";
  case 45: return "./46/xvolume.out";
  case 46: return "./47/xvolume.out";
  case 47: return "./48/xvolume.out";
  case 48: return "./49/xvolume.out";
  case 49: return "./0/xvolume.out";
  } // end of switch
  return "";
}

const char* getname3(int& i) { // ### insert filenames here
  switch(i) {
  case  0: return "./01/ssumout.out";
  case  1: return "./02/ssumout.out";
  case  2: return "./03/ssumout.out";
  case  3: return "./04/ssumout.out";
  case  4: return "./05/ssumout.out";
  case  5: return "./06/ssumout.out";
  case  6: return "./07/ssumout.out";
  case  7: return "./08/ssumout.out";
  case  8: return "./09/ssumout.out";
  case  9: return "./10/ssumout.out";
  case 10: return "./11/ssumout.out";
  case 11: return "./12/ssumout.out";
  case 12: return "./13/ssumout.out";
  case 13: return "./14/ssumout.out";
  case 14: return "./15/ssumout.out";
  case 15: return "./16/ssumout.out";
  case 16: return "./17/ssumout.out";
  case 17: return "./18/ssumout.out";
  case 18: return "./19/ssumout.out";
  case 19: return "./20/ssumout.out";
  case 20: return "./21/ssumout.out";
  case 21: return "./22/ssumout.out";
  case 22: return "./23/ssumout.out";
  case 23: return "./24/ssumout.out";
  case 24: return "./25/ssumout.out";
  case 25: return "./26/ssumout.out";
  case 26: return "./27/ssumout.out";
  case 27: return "./28/ssumout.out";
  case 28: return "./29/ssumout.out";
  case 29: return "./30/ssumout.out";
  case 30: return "./31/ssumout.out";
  case 31: return "./32/ssumout.out";
  case 32: return "./33/ssumout.out";
  case 33: return "./34/ssumout.out";
  case 34: return "./35/ssumout.out";
  case 35: return "./36/ssumout.out";
  case 36: return "./37/ssumout.out";
  case 37: return "./38/ssumout.out";
  case 38: return "./39/ssumout.out";
  case 39: return "./40/ssumout.out";
  case 40: return "./41/ssumout.out";
  case 41: return "./42/ssumout.out";
  case 42: return "./43/ssumout.out";
  case 43: return "./44/ssumout.out";
  case 44: return "./45/ssumout.out";
  case 45: return "./46/ssumout.out";
  case 46: return "./47/ssumout.out";
  case 47: return "./48/ssumout.out";
  case 48: return "./49/ssumout.out";
  case 49: return "./0/ssumout.out";
  } // end of switch
  return "";
}

std::ifstream in[fn];
std::ifstream in2[fn];
std::ifstream in3[fn];

void nextline(std::ifstream& s) {
  char d='a';
  int i=0;
  while (int(d)!=10 && i<200) {
    s.get(d); 
    i++;
  }
}

void openall() {
  const char* filename;
  for (int i=0; i<fn; i++) {
    filename=getname(i);
    in[i].open(filename);
    for (int j=0; j<ignore; j++) nextline(in[i]);
    std::cout<<"open file "<<filename<<".\n";
    filename=getname2(i);
    in2[i].open(filename);
    for (int j=0; j<ignore2; j++) nextline(in2[i]);
    std::cout<<"open file "<<filename<<".\n";
    filename=getname3(i);
    in3[i].open(filename);
    for (int j=0; j<ignore3; j++) nextline(in3[i]);
    std::cout<<"open file "<<filename<<".\n";
  }
}

void closeall() {
  for (int i=0; i<fn; i++) { in[i].close(); in2[i].close(); in3[i].close(); }
}

int main()
{
  int i,n;
  short cols,thiscol,abcol,abcol2,abcol3;
  // initialize x-value
  double x,x2,x2tmp,x2ttmp,tmp,ttmp;
  // initialize y-values
  double y[maxcols];
  double sigma[maxcols];
  double val[maxcols][fn];
  short ycol[maxcols];
  ycol[0]=y0col;
  ycol[1]=y1col;
  ycol[2]=y2col;
  ycol[3]=y3col;
  ycol[4]=y4col;
  short ycol2[maxcols];
  ycol2[0]=y0col2;
  ycol2[1]=y1col2;
  ycol2[2]=y2col2;
  ycol2[3]=y3col2;
  ycol2[4]=y4col2;
  short ycol3[maxcols];
  ycol3[0]=y0col3;
  ycol3[1]=y1col3;
  ycol3[2]=y2col3;
  ycol3[3]=y3col3;
  ycol3[4]=y4col3;
  if (fn<=1) { 
    std::cout<<"Es sind mehr als "<<fn<<" Dateien gefordert!\n";
    exit(1);
  }

  // open the necessary files
  openall();
  std::ofstream result(outputfilename);
  std::cout<<"Write in file "<<outputfilename<<" ... ";

  // read values from all files
  for (i=0; i<valn; i++) {
    for (cols=0; cols<maxcols; cols++) {
      y[cols]=0.0;
      sigma[cols]=0.0;
      for (n=0; n<fn; n++) val[cols][n]=0.0;
    }
    x=0.0; x2=0.0; x2tmp=0.0; x2ttmp=0.0;
    for (n=0; n<fn; n++) {
      for (cols=1; cols<xcol; cols++) in[n]>>tmp;
      for (cols=1; cols<xcol2; cols++) in2[n]>>tmp;
      for (cols=1; cols<xcol3; cols++) in3[n]>>tmp;
      in[n]>>x2; 
      in2[n]>>x2tmp;
      in3[n]>>x2ttmp;
      if (n==0) { x=x2; }
      else if (x!=x2) {
	std::cout<<"Error: Inconsistent x-values in file 0 and "<<"n"<<":"
		 <<"x="<<x<<" x2="<<x2
		 <<"; at value number "<<i<<"\n";
	exit(1);
      } else if (x2!=x2tmp) {
	std::cout<<"Error: Inconsistent x-values in both files.\n"
		 <<"x2="<<x2<<" x2tmp="<<x2tmp
		 <<"; at value number "<<i<<"\n";
	exit(1);
      } else if (x2tmp!=x2ttmp) {
	std::cout<<"Error: Inconsistent x-values in both files.\n"
		 <<"x2tmp="<<x2tmp<<" x2ttmp="<<x2ttmp
		 <<"; at value number "<<i<<"\n";
	exit(1);
      }
      abcol=xcol; abcol2=xcol2; abcol3=xcol3;
      for (thiscol=0; thiscol<nycols; thiscol++) {
	for (cols=abcol+1; cols<ycol[thiscol]; cols++) in[n]>>tmp;
	for (cols=abcol2+1; cols<ycol2[thiscol]; cols++) in2[n]>>tmp;
	for (cols=abcol3+1; cols<ycol3[thiscol]; cols++) in3[n]>>tmp;
	if (in[n].eof()==1 || in2[n].eof()==1 || in3[n].eof()==1)
  	  std::cout<<"Unexpected end of file in file n="<<n<<"!!!\n";
	else { in[n]>>val[thiscol][n]; in2[n]>>tmp; in3[n]>>ttmp; }
	// now calculate the function
	val[thiscol][n]=f(val[thiscol][n],tmp,ttmp);
	// continues as for one file and value only
	y[thiscol]+=val[thiscol][n];
	//std::cout<<thiscol<<" "<<y[thiscol]<<";  ";
	abcol=ycol[thiscol];
	abcol2=ycol2[thiscol];
	abcol3=ycol3[thiscol];
      }
      nextline(in[n]);
      nextline(in2[n]);
      nextline(in3[n]);
    }
    // schreibe x Wert in das neue file
    result<<x<<"   ";
    // jetzt ist der x-Wert in x und die y-Werte aus allen files in val[][] drin
    for (cols=0; cols<nycols; cols++) {
      // berechne Mittelwerte
      y[cols]/=fn;
      //std::cout<<"cols="<<cols<<" y="<<y[cols]<<"\n";
      // berechne Standardabweichung
      for (n=0; n<fn; n++) sigma[cols]+=pow(val[cols][n]-y[cols],2.);
      sigma[cols]/=double(fn-1);
      sigma[cols]=sqrt(sigma[cols]);
      // schreibe y Wert in das neue file
      result<<y[cols]<<"   "<<sigma[cols]<<"   ";
    }
    // Zeilenwechsel im neuen file
    result<<"\n";
  }
  
  // close all files
  std::cout<<"done.\n";
  result.close();
  closeall();
  return 1;
}


