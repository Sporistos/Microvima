# if new files are added corresponding changes have to be made in the rscript ExtractStat.R

files=(saffin_ags xvolume ssumout saffin_out_ags)

#nfiles=${#files[@]}

#echo $nfiles
#timepoints=(72 168 312)

rm -r results


# go through each directory in the current folder
for f in */ ; do

     #echo "$f"
     
    folder=$(basename $f)
    
    echo $folder

     cd $f 
          
     i=0
     
     cp ../ExtractStat.R .
     
     cp ../StatPlot.R
     
     for fsub in */ ; do
     
        # copy the files from each subdirectory

        i=$(( $i + 1 ))
        
        echo $i
        
        for index in ${!files[@]}; do
        
        name="${files[index]}_$i.out"
        
        echo $name
     
        cp $fsub/"${files[index]}.out" $name # rename as well 
        
        done
             
     done
     
     
     Rscript ExtractStat.R $folder ${files[*]}
     
     cd ../
     
done

# collect the results together 

mkdir results

for f in */ ; do

    if [[ $(basename $f) != "results" ]]
    then
        echo $f
        cp $f/final* results/.
    fi
    
     
done
 
for f in */ ; do

    if [[ $(basename $f) != "results" ]]
    then
        cd $f
        rm final* *.out *.R
        cd ..
    fi
    
     
done

#rm *.out *.R
