# Use:  ./runFilesHere.sh N_repeats N_cores
# 	This scrit takes all the .par files in this folder, and simulates them N_repeats time, with different seeds.
# 	It runs in parallel 


if [ -z "$1" ]  # if an argument is not given
  then
    echo "ERR, first argument should be seed"
  else
    currentSeed="$1"
fi

if [ -z "$2" ]  # if an argument is not given
  then
    NbCores=4
  else
    NbCores="$2"
fi

if [ ! -f  cxcl12_3d_5micron.sig ]; then
    cp ../hyphasma/cxcl12_3d_5micron.sig cxcl12_3d_5micron.sig
fi

if [ ! -f  cxcl13_3d_5micron.sig ]; then
    cp ../hyphasma/cxcl13_3d_5micron.sig cxcl13_3d_5micron.sig
fi

echo "Running files with $NbRepeats repeat(s) on $NbCores parallel simulations max "


# Recompiles the code of hyphasma, in case it has been changed
cd ../hyphasma/
qmake src/hyphasma.pro
make
cd ..
cd scripts/

# for each parameter file,
function process {
	echo "Processing $1 $2"
	DATE=$(date +%Y-%m-%d_%H_%M_%S)
	NEWFOLDER="$1-S$2Done$DATE"
	mkdir $NEWFOLDER
	echo "Creating folder $NEWFOLDER"
	cp cxcl12_3d_5micron.sig $NEWFOLDER
	cp cxcl13_3d_5micron.sig $NEWFOLDER
	
	cd $NEWFOLDER
	cp ../$1.par 0-usedParameterSet.par
	cp ../$1.par $1.par
	echo "Launching hyphasma $1"
	./../../hyphasma/hyphasma $1 -s $2 #> $NEWFOLDER/outputBash.txt
	cd ..
	cd ../hyphasma/tools/
	#./analysis ../../scripts/$NEWFOLDER/ &> trash.txt
	cd ../../scripts
	#if [ ! -f "$NEWFOLDER/resultpage.pdf" ] ; then
	#  rm -rf $NEWFOLDER
	#fi
	cp $NEWFOLDER/resultpage.pdf $NEWFOLDER.pdf
	rm $NEWFOLDER/cxcl12_3d_5micron.sig
	rm $NEWFOLDER/cxcl13_3d_5micron.sig
}

var=0
RANDOM='date +%s';
for nS in $(seq 1 $NbRepeats); do
	#currentSeed="$RANDOM"
	for i in *.par; do  
		echo "Processing ${i%.*} with seed $currentSeed ($var)"
		var=$((var+1))
		#process ${i%.*} $currentSeed &
		if [ $var == $NbCores ]; then 
			echo "Waiting "
			process ${i%.*} $currentSeed 
			var=0
		else 
			trap 'kill %1' SIGINT
			process ${i%.*} $currentSeed &
		fi
		
	done
done
trap - SIGINT
echo "FINISHED"




			#for job in 'jobs -p'; do
			#	echo "Waiting for job $job"
			#	wait $job || let "FAIL+=1"
			#done
			#wait 
