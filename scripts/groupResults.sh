for i in *.par; do  
	echo "Grouping ${i%.*} files"
	group=${i%.*}
	if [ ! -d "$group" ]; then
		mkdir $group
	fi
	for j in $group-S*; do
		if [ -d $j ]; then
			echo "Moving $j folder"
			mv $j $group/
		fi
	done
done

#for j in `find . -type d -name 'bla'`; do