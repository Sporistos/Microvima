function singleAnalysis {
	cd ../hyphasma/tools/
	./analysis ../../scripts/$1  &> trash.txt
	cd ../../scripts/
	cp $1/resultpage.pdf ${1%/}.pdf
	cp $1/resultpage.ps ${1%/}.ps
}

function reanalyze2 {
	cp ../hyphasma/tools/mkfigureSave ../hyphasma/tools/mkfigure 
	#cd $1
	#LIST=find -maxdepth 1 -type d
	#echo list
	#for f in $(find -maxdepth 1 -type d);
	#for f in $(ls -d */);
	for f in */;
	do
		echo "${f%/}"
		trap 'kill %1' SIGINT
		singleAnalysis $f &
	done
}

reanalyze2 

FAIL=0
trap - SIGINT
for job in `jobs -p`
do
  #echo $job
    wait $job || let "FAIL+=1"
done
echo -e "\e[39m" #default color

if [ "$FAIL" == "0" ];
then
echo " ================================== Reanalysis FINISHED =================================="
else
echo " ====================== Renalysis failed - ($FAIL) folders failed ========================"
fi
