set listFiles=bcinflow09 bcinflow09-Dist5-F

HYPHASMA="..\hyphasma"

cd ..\hyphasma\
REM qmake src/hyphasma.pro -spec win32-g++
REM mingw32-make
cd ..

cd scripts\
for %%f in (%listFiles%) do (
	echo " ... next ..."; 
	call :process %%f
)



EXIT \B 0


:process 
	echo "Processing %1"
		
	set YYYY=%DATE:~6,4%
	set MM=%DATE:~0,2%
	set DD=%DATE:~3,2%

	set HH=%TIME: =0%
	set HH=%HH:~0,2%
	set MI=%TIME:~3,2%
	set SS=%TIME:~6,2%
	set FF=%TIME:~9,2%

	set codeTime=%DD%-%MM%-%YYYY%_at%HH%-%MI%-%SS%
	REM	DATE=$(date +%Y-%m-%d_%H_%M_%S)
	set NEWFOLDER="%1-Done%codeTime%"
	mkdir %NEWFOLDER%
	echo "Creating folder %NEWFOLDER%"
	copy cxcl12_3d_5micron.sig %NEWFOLDER%
	copy cxcl13_3d_5micron.sig %NEWFOLDER%
		pause 

	cd %NEWFOLDER%
	copy ..\..\hyphasma\parameter_files\%1.par 0-usedParameterSet.par
	copy ..\..\hyphasma\parameter_files\%1.par %1.par
	echo "Launching hyphasma %1"
    ..\..\hyphasma\release\hyphasma %1
	cd ..
	cd ..\hyphasma\tools\
	analysis.bat ..\..\scripts\%NEWFOLDER%\ 
	cd ..\..\scripts
	REM #if [ ! -f "$NEWFOLDER/resultpage.pdf" ] ; then
	REM #  rm -rf $NEWFOLDER
	REM #fi
	copy %NEWFOLDER%\resultpage.pdf %NEWFOLDER%.pdf
	del %NEWFOLDER%\cxcl12_3d_5micron.sig
	del %NEWFOLDER%\cxcl13_3d_5micron.sig
		pause 

	EXIT \B 0
