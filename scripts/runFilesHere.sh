# Use:  ./runFilesHere.sh N_repeats N_cores
# 	This scrit takes all the .par files in this folder, and simulates them N_repeats time, with different seeds.
# 	It runs in parallel 


if [ -z "$1" ]  # if an argument is not given
  then
    NbRepeats=1
  else
    NbRepeats="$1"
fi

if [ -z "$2" ]  # if an argument is not given
  then
    NbCores=4
  else
    NbCores="$2"
fi

if [ ! -f  cxcl12_3d_5micron.sig ]; then
    cp ../hyphasma/cxcl12_3d_5micron.sig cxcl12_3d_5micron.sig
fi

if [ ! -f  cxcl13_3d_5micron.sig ]; then
    cp ../hyphasma/cxcl13_3d_5micron.sig cxcl13_3d_5micron.sig
fi

echo "--------------------------------------------------------------------------------------------------------------------" 
echo "Running .par files in the folder, with $NbRepeats repeat(s) on $NbCores parallel simulations max at the same time"


# Recompiles the code of hyphasma, in case it has been changed

echo -e "\e[44m" #blue background
echo ""
echo "   -> Re-Compiles hyphasma ..."
cd ../hyphasma/
if [ -f hyphasma ]; then rm hyphasma  
fi

if [[ "$OSTYPE" == "linux-gnu" ]]; then
	qmake src/hyphasma.pro
	make
	if [ ! -f  Zapotec ]; then
		echo -e "\e[31m" #red
		echo "--------------------------------------------------------------------------------------------------------------------" 
		echo "Error: Hyphasma could not be compiled/found... Sorry ! Maybe try to compile it with QtCreator by opening src/hyphasma.pro"
		echo -e "\e[39m" #default
		exit 1;
	fi
fi
if [[ "$OSTYPE" == "msys" ]]; then
	qmake src/hyphasma.pro -spec win32-g++
	mingw32-make
	if [ ! -f  release/hyphasma.exe ]; then
		echo -e "\e[31m" #red
		echo "--------------------------------------------------------------------------------------------------------------------" 
		echo "Error: Hyphasma could not be compiled/found... Sorry ! Maybe try to compile it with QtCreator by opening src/hyphasma.pro"
		echo -e "\e[39m" #default
		exit 1;
	fi
	cp release/hyphasma.exe hyphasma
fi
echo -e "\e[40m" #black background
echo "   -> Compiling successful !"
echo ""
cd ..
cd scripts/


# for each parameter file,
function process {
	echo -e "\e[39m" #default
	echo ""
	echo "Simulating $1.par with seed $2"
	DATE=$(date +%Y-%m-%d_%H_%M_%S)
	NEWFOLDER="$1-S$2Done$DATE"
	mkdir $NEWFOLDER
	echo "Creating folder $NEWFOLDER"
	cp *.sig $NEWFOLDER
	cp *.sig $NEWFOLDER
	
	cd $NEWFOLDER
	cp ../$1.par 0-usedParameterSet.par
	cp ../$1.par $1.par
	echo "   -> Calling ./Zapotec $1 -s $2"
	echo ""
		
	echo -e "\e[32m" #Green
	./../../hyphasma/Zapotec $1 -s $2 #> $NEWFOLDER/outputBash.txt
	cd ..
	cd ../hyphasma/tools/
	#./analysis ../../scripts/$NEWFOLDER/ &> trash.txt
	cd ../../scripts
	#if [ ! -f "$NEWFOLDER/resultpage.pdf" ] ; then
	#  rm -rf $NEWFOLDER
	#fi
	#cp $NEWFOLDER/resultpage.pdf $NEWFOLDER.pdf
	rm $NEWFOLDER/*.sig
	rm $NEWFOLDER/*.sig
}

var=0
set RANDOM='date +%s';
nFiles=0
FAIL=0

#echo "   -> Starting random seed used : $RANDOM"
for nS in $(seq 1 $NbRepeats); do
	currentSeed="$RANDOM"
	for i in *.par; do  
		if [ -f  $i ]; then  #only if it is a file, not a folder
			#echo "Processing ${i%.*} with seed $currentSeed ($var)"
			nFiles=$((nFiles+1))
			var=$((var+1))
			#process ${i%.*} $currentSeed &
			sleep 2 #time for a simulation to show the startint messages
			if [ $var == $NbCores ]; then 
				echo -e "\e[31m" #red
				echo ""
				echo " ... Waiting for simulations to finish before next wave ..."
				echo ""
				echo -e "\e[39m" #default
				process ${i%.*} $currentSeed 
				var=0
			else 
				trap 'kill %1' SIGINT
				process ${i%.*} $currentSeed &
			fi
		fi
		
	done
done
trap - SIGINT

for job in `jobs -p`
do
  #echo $job
    wait $job || let "FAIL+=1"
done

echo -e "\e[39m" #default color
if [ "$FAIL" == "0" ];
then
echo " ================================== Run Scripts Here FINISHED ($nFiles Files processed) =================================="
else
echo " =========================== Run Scripts Here, There was a problem, ($FAIL) simulations failed ==========================="
fi




			#for job in 'jobs -p'; do
			#	echo "Waiting for job $job"
			#	wait $job || let "FAIL+=1"
			#done
			#wait 
