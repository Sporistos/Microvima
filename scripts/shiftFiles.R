files <- list.files( pattern="*.out", full.names=T, recursive=FALSE)

lapply(files, function(x) {
    print(cat("Reading file", x));
    a <- read.table(x, header=FALSE);
    a$V1 = a$V1 + 31;
    write.table(a, x, quote=F, row.names=F, col.names=F);
})

#a <- read.table("xvolume1.out", header=FALSE);
#a$V1 = a$V1 + 33;
#write.table(a, "xvolume1.out", quote=F, row.names=F, col.names=F);
